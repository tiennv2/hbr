<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;

class Box extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    
	    $result = '';
	    require 'Box/'. $layout .'.phtml';
	    
	    return $result;
	}
}