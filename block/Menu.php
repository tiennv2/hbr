<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;

class Menu extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $ssSystem  = new Container('system');
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    
	    $cache = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'Menu_' . str_replace('.', '_', $options['code']) .'_'. $ssSystem->language;
	    $result = $cache->getItem($cache_key);
	    
	    if (empty($result)) {
	        $table     = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\MenuTable');
    	    $arrMenu   = $table->listItem($options, array('task' => 'list-by-code'));
    	    
    	    $xhtml     = '';
    	    require 'Menu/'. $layout .'.phtml';
    	    
    	    if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
    	        $cache->setItem($cache_key, $result);
    	    }
	    }
	    
	    return $result;
	}
}