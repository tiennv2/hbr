<?php
namespace Block;
use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;

class PostItemBox extends AbstractHelper {
	
	public function __invoke($params = null, $options = null, $layout = 'default'){
	    $ssSystem  = new Container('system');
	    $view      = $this->getView();
	    $settings  = $view->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
	    
	    $cache     = $view->getHelperPluginManager()->getServiceLocator()->get('cache');
	    $cache_key = 'PostItemBox_' . $options['box'] .'_'. $options['type'] .'_'. $ssSystem->language;
	    $result    = $cache->getItem($cache_key);
	    
	    if (empty($result)) {
	        $result    = '';
	        $table     = $view->getHelperPluginManager()->getServiceLocator()->get('Post\Model\PostItemTable');
	        $items     = $table->listItem($options, array('task' => 'list-item-box'));
	        require 'PostItemBox/'. $options['type'] .'_'. $layout .'.phtml';
	        
	        if($options['cache'] == true && $settings['General.System.Cache']['value'] == 'true') {
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    return $result;
	}
}