<?php
return array(
    'module_layouts' => array(
        'Admin' 	=> 'layout/backend',
        'Post' 	    => 'layout/frontend',
        'User' 	    => 'layout/frontend',
    )
);
?>