<?php
// Đường dẫn đến thư mục chứa thư mục hiện thời
chdir(dirname(__FILE__));

define('APP_KEY', 'x2017');

// Định nghĩa môi trường thực thi của ứng dụng
define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));	// development - production

// Định nghĩa đường dẫn đến thư mục ứng dụng
define('PATH_APPLICATION', realpath(dirname(__FILE__)));
define('PATH_LIBRARY', PATH_APPLICATION . '/library');
define('PATH_MODULE', PATH_APPLICATION . '/module');
define('PATH_VENDOR', PATH_APPLICATION . '/vendor');
define('PATH_PUBLIC', PATH_APPLICATION . '/public');
define('PATH_BLOCK', PATH_APPLICATION . '/block');
define('PATH_CAPTCHA', PATH_PUBLIC . '/captcha');
define('PATH_FILES', PATH_PUBLIC . '/files');
define('PATH_SCRIPTS', PATH_PUBLIC . '/scripts');
define('PATH_TEMPLATE', PATH_PUBLIC . '/template');

// Định nghĩa đường dẫn url
define('URL_APPLICATION', '');
define('URL_PUBLIC', URL_APPLICATION . '/public');
define('URL_FILES', URL_PUBLIC . '/files');
define('URL_SCRIPTS', URL_PUBLIC . '/scripts');
define('URL_TEMPLATE', URL_PUBLIC . '/template');
define('URL_HBR', URL_HBR . 'https://hbr.edu.vn');

// HTMLPurifier
define('HTMLPURIFIER_PREFIX', PATH_VENDOR);

define('LANGUAGE', 'vi - Tiếng Việt;en - Tiếng Anh');
define('DOMAIN', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]");
define('DOMAIN_ERP', 'https://langmaster.vn/erp');
define('DOMAIN_ERP_ROOT', 'https://langmaster.vn');

// Table name
define('TABLE_PREFIX', 'x_');
define('TABLE_MENU',                TABLE_PREFIX . 'menu');
define('TABLE_SETTING',             TABLE_PREFIX . 'setting');
define('TABLE_DYNAMIC',             TABLE_PREFIX . 'dynamic');
define('TABLE_DOCUMENT',            TABLE_PREFIX . 'document');
define('TABLE_USER',                TABLE_PREFIX . 'user');
define('TABLE_USER_GROUP',          TABLE_PREFIX . 'user_group');
define('TABLE_USER_PERMISSION',     TABLE_PREFIX . 'user_permission');
define('TABLE_POST_CATEGORY',       TABLE_PREFIX . 'post_category');
define('TABLE_POST_ITEM',           TABLE_PREFIX . 'post_item');
define('TABLE_PAGE',           		TABLE_PREFIX . 'page');
define('TABLE_FORM',                TABLE_PREFIX . 'form');
define('TABLE_FORM_DATA',           TABLE_PREFIX . 'form_data');
define('TABLE_PRODUCT_CART',        TABLE_PREFIX . 'product_cart');
define('TABLE_COURSE_CATEGORY',     TABLE_PREFIX . 'course_category');
define('TABLE_COURSE_GROUP',        TABLE_PREFIX . 'course_group');
define('TABLE_COURSE_ITEM',         TABLE_PREFIX . 'course_item');
define('TABLE_COURSE_DETAIL',       TABLE_PREFIX . 'course_detail');
define('TABLE_DOCUMENT_CATEGORY',   TABLE_PREFIX . 'document_category');
define('TABLE_DOCUMENT_ITEM',       TABLE_PREFIX . 'document_item');
define('TABLE_CODE',       			TABLE_PREFIX . 'code');
define('TABLE_CODE_REPORT',       	TABLE_PREFIX . 'code_report');
define('TABLE_CODE_SALE',       	TABLE_PREFIX . 'code_sale');

// Table CRM
define('TABLE_CRM_DOCUMENT',        TABLE_PREFIX . 'document');
define('TABLE_CRM_CONTACT',        	TABLE_PREFIX . 'contact');
define('TABLE_CRM_CONTACT_COURSE',  TABLE_PREFIX . 'contact_course');
define('TABLE_CRM_CONTRACT',        TABLE_PREFIX . 'contract');
define('TABLE_CRM_LOCATION_CITY',   TABLE_PREFIX . 'location_city');
define('TABLE_CRM_REPORT',   		TABLE_PREFIX . 'report');
define('TABLE_CRM_CAMPAIGN',   		TABLE_PREFIX . 'campaign');
define('TABLE_CRM_CAMPAIGN_DATA',   TABLE_PREFIX . 'campaign_data');
define('TABLE_CRM_FORM',   			TABLE_PREFIX . 'form');
define('TABLE_CRM_FORM_DATA',		TABLE_PREFIX . 'form_data');
define('TABLE_CRM_PAYMENT',		    TABLE_PREFIX . 'payment');
define('TABLE_CRM_COURSE',		    TABLE_PREFIX . 'course');

define('DOMAIN', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]");