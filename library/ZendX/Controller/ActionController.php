<?php

namespace ZendX\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

class ActionController extends AbstractActionController {
    protected $_urlController;
    protected $_viewModel;
    protected $_params;
    protected $_table;
    protected $_form;
    protected $_language;
    protected $_options = array(
        'tableName', 'formName'
    );
    protected $_paginator = array(
        'itemCountPerPage'	=> 20,
        'pageRange'			=> 5,
        'options'           => array(10, 20, 50, 100, 200, 500, 1000)
    );
    
    public function setPluginManager(PluginManager $plugins) {
        $this->getEventManager()->attach(MvcEvent::EVENT_DISPATCH, array($this, 'onInit'), 100);
        $this->getEventManager()->attach(MvcEvent::EVENT_DISPATCH, array($this, 'onDispath'));
        parent::setPluginManager($plugins);
    }
    
    public function onInit(MvcEvent $e) {
        // Thiết lập ngôn ngữ
        $ssSystem = new Container('system');
        $ssSystem->language = $ssSystem->language ? $ssSystem->language : 'vi';
        
        // Lấy thông tin setting
        $language = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'Language'), array('task' => 'cache-by-code'));
        if(!empty($language['Language']['params'])) {
            $this->_language = unserialize($language['Language']['params']);
            $this->_language['language_active'] = $ssSystem->language;
        }
        
        // Lấy thông tin config
        $config = $this->getServiceLocator()->get('config');
        
        // Get Module - Controller - Action
        $routeMatch = $e->getRouteMatch();
        $controllerArray = explode('\\', $routeMatch->getParam('controller'));
        
        // Truyền một phần tử ra ngoài view
        $this->_params['module']        = strtolower(preg_replace('/\B([A-Z])/', '-$1', $controllerArray[0]));
        $this->_params['controller']    = strtolower(preg_replace('/\B([A-Z])/', '-$1', $controllerArray[2]));
        $this->_params['action']        = $routeMatch->getParam('action');
        $this->_params['moduleRoute']   = $config['router']['routes']['routeAdmin']['options']['route'];
        
        // Lấy thông tin route
        $route = $this->params()->fromRoute();
        $route['routeName'] = $routeMatch->getMatchedRouteName();
        $this->_params['route'] = $route;
        
        // Thiết lập link controller
        $this->_urlController = $this->_params['module'] . '/' . $this->_params['controller'];
        
        // Thiết lập layout cho Controller
        $this->layout($config['module_layouts'][$controllerArray[0]]);
        
        // Thiết lập các tham số của template
        $template = explode('/',  $config['module_layouts'][$controllerArray[0]]);
        $this->_params['template'] = array(
            'pathTheme'             => PATH_TEMPLATE . '/'. $template[1],
            'pathThemeTemplate'     => PATH_TEMPLATE . '/'. $template[1] .'/template',
            'pathImg'               => PATH_TEMPLATE .'/'. $template[1] .'/img',
            'pathCss' 	            => PATH_TEMPLATE .'/'. $template[1] .'/css',
            'pathJs' 	            => PATH_TEMPLATE .'/'. $template[1] .'/js',
            'pathPlugin'	        => PATH_TEMPLATE .'/'. $template[1] .'/plugins',
            'pathHtml'	            => PATH_TEMPLATE .'/'. $template[1] .'/html',
        	'urlTheme'              => URL_TEMPLATE .'/'. $template[1],
        	'urlThemeTemplate'      => URL_TEMPLATE .'/'. $template[1] .'/template',
        	'urlImg'                => URL_TEMPLATE .'/'. $template[1] .'/img',
        	'urlCss' 	            => URL_TEMPLATE .'/'. $template[1] .'/css',
        	'urlJs' 	            => URL_TEMPLATE .'/'. $template[1] .'/js',
        	'urlPlugin'	            => URL_TEMPLATE .'/'. $template[1] .'/plugins',
        	'urlHtml'	            => URL_TEMPLATE .'/'. $template[1] .'/html',
        );
        
        // Kiểm tra quyền đăng nhập
        if($this->_params['module'] == 'admin') {
            if($this->_params['action'] != 'login' && $this->_params['action'] != 'logout' && $this->_params['controller'] != 'notice' && $this->_params['controller'] != 'api') {
                $loggedStatus = $this->identity() ? true : false;
                
                if($loggedStatus == false) {
                    return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'user', 'action' => 'login'));
                } else {
                    $userInfo   = new \ZendX\System\UserInfo();
                    $permission = $userInfo->getPermission();
                    
                    if(empty($permission['privileges'])) {
                        return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
                    } else {
                        if($permission['privileges'] != 'full') {
                            $aclObj = new \ZendX\System\Acl($permission['role'], $permission['privileges']);
                            
                            if($aclObj->isAllowed($this->_params) == false) {
                                $urlNoAccess = $this->url()->fromRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
                                $response = $this->getResponse();
                                $response->getHeaders()->addHeaderLine('Location', $urlNoAccess);
                                $response->setStatusCode(302);
                                $response->sendHeaders();
                            
                                $this->getEvent()->stopPropagation();
                                return $response;
                            }
                        }
                    }
                }
            }
        }
        
        // Gọi đến function chạy đầu tiên
        $this->init();
    }
    
    public function onDispath(MvcEvent $e) {
        // Truyền tất cả params ra ngoài layout
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->arrParams 	= $this->_params;
        $viewModel->viewModel 	= $this->_viewModel;
        $viewModel->language	= $this->_language;
    }
    
    public function init() {
    }
    
    public function getTable() {
        if(empty($this->_table)) {
            $this->_table = $this->getServiceLocator()->get($this->_options['tableName']);
        }
    
        return $this->_table;
    }
    
    public function getForm() {
        if(empty($this->_form)) {
            $this->_form = $this->getServiceLocator()->get('FormElementManager')->get($this->_options['formName']);
        }
    
        return $this->_form;
    }
    
    public function setLayout($layout) {
        $this->_params['layout'] = $layout;
    }
    
    public function goRoute($actionInfo = null) {
        $actionInfo['controller'] = !empty($actionInfo['controller']) ? $actionInfo['controller'] : $this->_params['controller'];
        $actionInfo['action'] = !empty($actionInfo['action']) ? $actionInfo['action'] : 'index';
        $actionInfo['route'] = !empty($actionInfo['route']) ? $actionInfo['route'] : 'routeAdmin/default';
        
        $paramRoute = array('controller' => $actionInfo['controller'], 'action' => $actionInfo['action']);
        if(!empty($actionInfo['id'])) {
            $paramRoute['id'] = $actionInfo['id'];
        }
        return $this->redirect()->toRoute($actionInfo['route'], $paramRoute);
    }
    
    public function goUrl($url = null) {
        return $this->redirect()->toUrl($url);
    }
    
    public function getInfoSystem() {
        $manager        = $this->getServiceLocator()->get('ModuleManager');
        $modules        = $manager->getLoadedModules();
        $loadedModules          = array_keys($modules);
        $skipActionsList        = array('notFoundAction', 'getMethodFromAction');
        $skipControllersList    = array('notice', 'nested', 'api', 'database');
        
        $arrAction = array();
        
        $wordFirst = new \ZendX\Functions\WordFirst();
        foreach ($loadedModules as $loadedModule) {
        	if($loadedModule == 'Admin') {
	            $moduleClass = '\\' .$loadedModule . '\Module';
	            $moduleObject = new $moduleClass;
	            $config = $moduleObject->getConfig();
	        
	            $controllers = $config['controllers']['invokables'];
	            foreach ($controllers as $key => $moduleClass) {
	                $tmpArray = get_class_methods($moduleClass);
	                $controllerActions = array();
	                foreach ($tmpArray as $action) {
	                    if (substr($action, strlen($action)-6) === 'Action' && !in_array($action, $skipActionsList)) {
	                        $controllerActions[] = substr($wordFirst->lowerFirstUpper($action), 0, -7);
	                    }
	                }
	        
	                $module     = $wordFirst->lowerFirstUpper($loadedModule);
	                $controller = explode('\\Controller\\', $moduleClass);
	                $controller = $wordFirst->lowerFirstUpper(substr($controller[1], 0, -10));
	                $action     = $controllerActions;
	        
	                if (!in_array($controller, $skipControllersList)) {
	                    $arrAction[$module][$controller] = $action;
	                }
	            }
        	}
        }
        
        return $arrAction;
    }
    
    public function statusAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params, array('task' => 'change-status'));
        } else {
            $this->goRoute();
        }
    
        return $this->response;
    }
    
    public function deleteAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid'])) {
                $result = $this->getTable()->deleteItem($this->_params, array('task' => 'delete-item'));
                $message = 'Xóa '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        $this->goRoute();
    }
    
    public function orderingAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid']) && !empty($this->_params['data']['ordering'])) {
                $result = $this->getTable()->changeOrdering($this->_params, array('task' => 'change-ordering'));
                $message = 'Sắp xếp '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        $this->goRoute();
    }
}