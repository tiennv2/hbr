<?php
namespace ZendX\Functions;

class Number {
    
    public function __construct() {
        
    }

    public function formatToData($dataSource){
        $result     = null;
        $result     = preg_replace('/\D/', '', $dataSource);
		return $result;
	}
	
	public function formatToPhone($dataSource){
	    $result = preg_replace('/\D/', '', $dataSource);
	    return $result;
	}

}