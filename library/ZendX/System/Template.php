<?php
namespace ZendX\System;

class Template {
    
	public function __construct(){
		
	}
	
	public function getLayout(){
	    $dir    = PATH_TEMPLATE . '/frontend/layout';
        $files  = opendir($dir);
        
        $layout = array('default' => 'Mặc định');
        while (false !== ($filename = readdir($files))) {
            if(strpos($filename, '.phtml') == true && $filename != 'default.phtml' && $filename != 'home.phtml') {
                $filename = str_replace('.phtml', '', $filename);
                $layout[$filename] = $filename;
            }
        }
        
        return $layout;
	}
	
	public function getTemplate(){
	    $dir        = PATH_TEMPLATE . '/frontend/template';
        $templates  = scandir($dir, 1);
        
        $optionTemplate = array();
        foreach ($templates AS $template) {
            if(substr($template, 0, 1) != '.') {
                $files  = opendir($dir . '/' . $template);
                
                $tmp = explode('-', $template);
                if($tmp[0] != 'form') {
                	$optionTemplate[$template . '/index'] = $template;
                }
            }
        }
        
        return $optionTemplate;
	}
	
	public function getForm(){
	    $dir        = PATH_TEMPLATE . '/frontend/template';
        $templates  = scandir($dir, 1);
        
        $optionTemplate = array();
        foreach ($templates AS $template) {
            if(substr($template, 0, 1) != '.') {
                $files  = opendir($dir . '/' . $template);
                
                $tmp = explode('-', $template);
                if($tmp[0] == 'form') {
                	$optionTemplate[$template . '/index'] = $template;
                }
            }
        }
        
        return $optionTemplate;
	}
}