<?php
namespace ZendX\System;

use Zend\Session\Container;

class UserInfo {
	
	public function __construct(){
		$ssInfo	= new Container(APP_KEY . '_user');
	}
	
	public function storeInfo($data){
		$ssInfo	= new Container(APP_KEY . '_user');
		//$ssInfo->setExpirationSeconds(7200);
		$ssInfo->user		= $data['user'];
		$ssInfo->group		= $data['group'];
		$ssInfo->permission	= $data['permission'];
	}
	
	public function storeUserInfo($dataUser){
	    $ssInfo		  = new Container(APP_KEY . '_user');
	    $ssInfo->user = $dataUser;
	}
	
	public function destroyInfo(){
		$ssInfo	= new Container(APP_KEY . '_user');
		$ssInfo->getManager()->getStorage()->clear(APP_KEY . '_user');
	}
	
	public function getUserInfo($element = null){
		$ssInfo		= new Container(APP_KEY . '_user');
		$userInfo	= $ssInfo->user;
		
		$result	= ($element == null) ? $userInfo : $userInfo->$element;
		return $result;
	}
	
	public function setUserInfo($key, $value){
		$ssInfo		= new Container(APP_KEY . '_user');
		$ssInfo->user[$key] = $value;
	}
	
	public function getGroupInfo($element = null){
		$ssInfo		= new Container(APP_KEY . '_user');
		$groupInfo	= $ssInfo->group;
	
		$result	= ($element == null) ? $groupInfo : $groupInfo->$element;
		return $result;
	}

	public function getPermission($element = null){
		$ssInfo			= new Container(APP_KEY . '_user');
		$permissionInfo	= $ssInfo->permission;
	
		$result	= ($element == null) ? $permissionInfo : $permissionInfo[$element];
		return $result;
	}
}