<!DOCTYPE html><html>
<head>
    <title>HBR Business School - Trường doanh nhân HBR</title><meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="✍ Đăng ký kênh: http://bit.ly/DangKyKenhHBR ------------------ Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào" />
	<meta name="image" content="https://img.youtube.com/vi/4a2qhJ9D7Xw/maxresdefault.jpg" />
	<meta property="og:image" content="https://img.youtube.com/vi/4a2qhJ9D7Xw/maxresdefault.jpg" />
	<meta property="og:image:type" content="image/png" />
	<meta property="og:image:width" content="720" />
	<meta property="og:image:height" content="379" />
	<meta property="og:title" content="Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào?" />
	<meta property="og:description" content="✍ Đăng ký kênh: http://bit.ly/DangKyKenhHBR ------------------ Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào" />
	<meta name="twitter:image:src" content="https://img.youtube.com/vi/4a2qhJ9D7Xw/maxresdefault.jpg" />
	<meta name="author" content="https://www.facebook.com/youtube/" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="www.youtube.com" />
	<meta name="twitter:title" content="Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào?" />
	<meta name="twitter:description" content="✍ Đăng ký kênh: http://bit.ly/DangKyKenhHBR ------------------ Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào" />
	<meta name="twitter:domain" content="www.youtube.com" />
	<meta itemprop="name" content="Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào?" />
	<meta itemprop="image" content="https://img.youtube.com/vi/4a2qhJ9D7Xw/maxresdefault.jpg" />
	<meta itemprop="description" content="✍ Đăng ký kênh: http://bit.ly/DangKyKenhHBR ------------------ Quản trị nhân sự hiện đại - Unilever tổ chức đào tạo đội ngũ nhân sự thành công thế nào" />
</head>
<body class="body">
<?php
// PHP permanent URL redirection
header("Location: https://www.youtube.com/watch?v=4a2qhJ9D7Xw&t=187s", true, 301);
exit();
?>
</body>
</html>