<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\Json\Json;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class ApiController extends ActionController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function selectAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
        
        $items = $table->listItem($this->_params, null);
        
        $results = array();
        
        if($items->count() > 0) {
            $data_id = $this->_params['data']['data-id'];
            $data_text = explode(',', $this->_params['data']['data-text']);
            
            $results[] = array('id' => '', 'text' => ' - Chọn - ');
            foreach ($items AS $item) {
                $text = '';
                for ($i = 0; $i < count($data_text) ; $i++) {
                    if($i == 0) {
                        $text .= $item[$data_text[$i]];
                    } else {
                        if(!empty($item[$data_text[$i]])) {
                            $text .= ' - ' . $item[$data_text[$i]];
                        }
                    }
                }
                $results[] = array(
                    'id' => $item[$data_id],
                    'text' => $text,
                );
            }
        }
        
        if(!empty($this->_params['route']['id'])) {
            $results = $results[1];
        }
        
        if(!empty($this->_params['data']['term'])) {
            $termResults = array();
            foreach ($results AS $result) {
                $pos = strpos(strtolower($result['text']), strtolower($this->_params['data']['term']));
                if($pos !== false) {
                    $termResults[] = $result;
                }
            }
            echo Json::encode($termResults);
        } else {
            echo Json::encode($results);
        }
        
        return $this->response;
    }
    
    public function dataAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
    
        $items          = $table->listItem($this->_params, null);
    
        if(!empty($this->_params['route']['id'])) {
            $results = $items->current();
        } else {
            foreach ($items AS $item) {
                $results[$item['id']] = $item;
            }
        }
    
        echo Json::encode($results);
    
        return $this->response;
    }
    
    public function countAction() {
        $adapter        = $this->getServiceLocator()->get('dbConfig');
        $tableGateway   = new TableGateway($this->_params['data']['data-table'], $adapter, null);
        $table          = new \Admin\Model\ApiTable($tableGateway);
    
        $results        = $table->countItem($this->_params, null);
    
        echo Json::encode($results);
    
        return $this->response;
    }
    
    public function languageAction() {
        $ssSystem = new Container('system');
        if($this->_params['route']['code'] != $ssSystem->language) {
            $ssSystem->language = $this->_params['route']['code'];
        }
        
        if(!$this->getRequest()->isXmlHttpRequest()) {
            $this->redirect()->toRoute('routeHome');
        }
        
        return $this->response;
    }

    public function sitemapAction() {
        $filename = PATH_APPLICATION . '/sitemap.xml';
        $arrValue = array();
    
        // Đường dẫn trang chủ
        $arrValue[] = array(
            'loc' => $this->url()->fromRoute('routeHome', array(), array('force_canonical' => true)),
            'lastmod' => '',
            'changefreq' => '',
            'priority' => '',
        );
         
        // Đường dẫn page
        $categories = $this->getServiceLocator()->get('Admin\Model\PageTable')->listItem(null, array('task' => 'list-all'));
        foreach ($categories AS $key => $val) {
            $arrValue[] = array(
                'loc' => $this->url()->fromRoute('routePostCategory', array('alias' => $val['alias']), array('force_canonical' => true)),
                'lastmod' => '',
                'changefreq' => '',
                'priority' => '',
            );
        }

        // Đường dẫn Danh mục
        $categories = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'cache'));
        foreach ($categories AS $key => $val) {
            $arrValue[] = array(
                'loc' => $this->url()->fromRoute('routePostCategory', array('alias' => $val['alias']), array('force_canonical' => true)),
                'lastmod' => '',
                'changefreq' => '',
                'priority' => '',
            );
        }
         
        // Đường dẫn bài viết
        $items = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->listItem(null, array('task' => 'list-all'));
        foreach ($items AS $key => $val) {
            $arrValue[] = array(
                'loc' => $this->url()->fromRoute('routePostItem', array('category_alias' => $val['category_alias'], 'alias' => $val['alias']), array('force_canonical' => true)),
                'lastmod' => '',
                'changefreq' => '',
                'priority' => '',
            );
        }
    
    
        $doc = new \DOMDocument();
    
        //Tạo
        $doc->version 	= '1.0';
        $doc->encoding 	= 'UTF-8';
    
        $note_goc 	= 	$doc->createElement( 'urlset' );
        $doc		->	appendChild($note_goc);
        $note_goc	->	appendChild($doc->createAttribute("xmlns"))
                    ->	appendChild($doc->createTextNode("http://www.sitemaps.org/schemas/sitemap/0.9"));
    
        foreach ( $arrValue AS $key => $val ) {
            $note_url = $doc->createElement( 'url' );
            $note_goc->appendChild($note_url);
             
            $loc      	= $doc->createElement( 'loc', $val['loc'] );
            //$lastmod   	= $doc->createElement( 'lastmod', $val['lastmod'] );
             
            $note_url->appendChild($loc);
            //$note_url->appendChild($lastmod);
        }
    
        $doc->formatOutput = true;
    
        $doc->saveXML();
        $doc->save($filename);
    
        $this->_viewModel['caption'] = 'Tạo sitemap';
        return new ViewModel($this->_viewModel);
    }

    public function listPageAction() {

        $this->_viewModel['items'] = $this->getServiceLocator()->get('Admin\Model\PageTable')->listItem($this->_params['data'], array('task' => 'list-page'));
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}
