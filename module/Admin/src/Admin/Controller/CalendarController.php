<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;
use ZendX\System\UserInfo;

class CalendarController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\FormDataTable';
        $this->_options['formName'] = 'formAdminFormData';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']                  = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'date';
        $this->_params['ssFilter']['order']                     = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_type']               = $ssFilter->filter_type;
        $this->_params['ssFilter']['filter_keyword']            = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_date_begin']         = $ssFilter->filter_date_begin;
        $this->_params['ssFilter']['filter_date_end']           = $ssFilter->filter_date_end;
        $this->_params['ssFilter']['filter_user_action']        = $ssFilter->filter_user_action;
        
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']   = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber']  = $this->params()->fromRoute('page', 1);
        $this->_params['paginator']             = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        $ssFilter	= new Container(__CLASS__);
        $data = $this->_params['data'];
    
        if($this->getRequest()->isPost()) {
    
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->order_by                 = $data['order_by'];
            $ssFilter->order                    = $data['order'];
            $ssFilter->filter_type              = $data['filter_type'];
            $ssFilter->filter_date_begin        = $data['filter_date_begin'];
            $ssFilter->filter_date_end          = $data['filter_date_end'];
            $ssFilter->filter_user_action       = $data['filter_user_action'];
            $ssFilter->filter_user              = $data['filter_user'];
        }
        
        $this->goRoute();
    }
    
    public function indexAction() {
        $userInfo = $this->_userInfo->getUserInfo();
        $user_id  = $userInfo['id'];
        $ssFilter = new Container(__CLASS__);
        $ssFilter->currentPageNumber = $this->_paginator['currentPageNumber'];
        $myForm	= new \Admin\Form\Search\FormData($this->getServiceLocator(), $this->_params['ssFilter']);
        $myForm->setData($this->_params['ssFilter']);
        
        $date           = date('d-m-Y');
        $user_action    = 'success';
        $user_action_1  = 'follow';
        $call           = 1;
        $items              = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->listItem(array('data' => array('created' => $date, 'history_return' => $date, 'user_action' => $user_action, 'user_id' => $user_id), 'ssFilter' => $this->_params['ssFilter']), array('task' => 'list-all'));
        $list_contact       = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->listItem(array('data' => array('history_return' => $date, 'user_action' => $user_action_1)), array('task' => 'list-contact'));
        $item_call          = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->listItem(array('data' => array('created' => $date, 'call_status' => $call)), array('task' => 'list-call'));
        $item_call_out      = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->listItem(array('data' => array('history_return' => $date, 'call_status' => $call)), array('task' => 'list-call-out'));

        $this->_viewModel['myForm']	                = $myForm;
        $this->_viewModel['items']                  = $items;
        $this->_viewModel['contact']                = $list_contact;
        $this->_viewModel['item_call']              = $item_call;
        $this->_viewModel['item_call_out']          = $item_call_out;
        $this->_viewModel['count']                  = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->countItem(array('data' => array('created' => $date, 'history_return' => $date, 'user_action' => $user_action, 'user_id' => $user_id), 'ssFilter' => $this->_params['ssFilter']), array('task' => 'list-all'));
        $this->_viewModel['user']                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['product']                = $this->getServiceLocator()->get('Admin\Model\FormTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['sale_contact_type']      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contact-type')), array('task' => 'cache-alias'));
        $this->_viewModel['history_status_id']      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'history-status')), array('task' => 'cache'));
        $this->_viewModel['level']                  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'contact-level')), array('task' => 'cache-alias'));
        $this->_viewModel['sex']                    = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $this->_viewModel['userInfo']               = $this->_userInfo->getUserInfo();
        $this->_viewModel['caption']                = 'Lịch làm việc';
        return new ViewModel($this->_viewModel);
    }
}
















