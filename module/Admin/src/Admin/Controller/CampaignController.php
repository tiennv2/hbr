<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class CampaignController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\CampaignTable';
        $this->_options['formName'] = 'formAdminCampaign';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['filter_keyword']                = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_date_begin']             = $ssFilter->filter_date_begin;
        $this->_params['ssFilter']['filter_date_end']               = $ssFilter->filter_date_end;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']   = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber']  = $this->params()->fromRoute('page', 1);
        $this->_params['paginator']             = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        $ssFilter	= new Container(__CLASS__);
        $data = $this->_params['data'];
    
        if($this->getRequest()->isPost()) {
    
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->filter_keyword           = $data['filter_keyword'];
            $ssFilter->filter_date_begin        = $data['filter_date_begin'];
            $ssFilter->filter_date_end          = $data['filter_date_end'];
        }
        
        $this->goRoute();
    }
    
    public function indexAction() {
        $ssFilter = new Container(__CLASS__);
        $ssFilter->currentPageNumber = $this->_paginator['currentPageNumber'];
        
        $myForm	= new \Admin\Form\Search\Campaign($this->getServiceLocator(), $this->_params['ssFilter']);
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));

        $this->_viewModel['myForm']	                = $myForm;
        $this->_viewModel['items']                  = $items;
        $this->_viewModel['count']                  = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['userInfo']               = $this->_userInfo->getUserInfo();
        $this->_viewModel['caption']                = 'Link checking - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function addAction(){
        $myForm = $this->getForm();
    
        if ($this->getRequest()->isPost()) {
            $myForm->setInputFilter(new \Admin\Filter\Campaign(array('id' => $this->_params['data']['id'], 'data' => $this->_params['data'], 'route' => $this->_params['route'])));
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if ($myForm->isValid()) {
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
    
                $this->getServiceLocator()->get('Admin\Model\CampaignTable')->saveItem($this->_params, array('task' => 'add-item'));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được thêm thành công');
                if ($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'add'));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['caption']    = 'Campaign - Thêm mới';
        return new ViewModel($this->_viewModel);
    }
    
    public function editAction(){
        $ssFilter = new Container(__CLASS__);
        $myForm = new \Admin\Form\Checking\Edit($this->getServiceLocator());
        
        if(!empty($this->_params['data']['id'])) {
            $campaign = $this->getServiceLocator()->get('Admin\Model\CampaignTable')->getItem(array('id' => $this->_params['data']['id']));
            $myForm->setData($campaign);
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        
        if ($this->getRequest()->isPost()) {
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setData($this->_params['data']);
                if ($myForm->isValid()) {
                    $this->_params['item'] = $campaign;
                    $this->getServiceLocator()->get('Admin\Model\CampaignTable')->saveItem($this->_params, array('task' => 'edit-item'));
        
                    $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['caption']    = 'Campaign - Sửa';
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function exportAction() {
        $dateFormat             = new \ZendX\Functions\Date();
    
        $items                  = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false));
        
        $user                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $sale_group             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $sale_branch            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $sale_contact_type      = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contact-type')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $sale_history_action    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-action')), array('task' => 'cache'));
        $sale_history_result    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-result')), array('task' => 'cache'));
        $sale_source_group      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-group')), array('task' => 'cache'));
        $sale_source_access     = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-access')), array('task' => 'cache'));
        $location_city          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
        $location_district      = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
        
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'date', 'title' => 'Ngày', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'date', 'title' => 'Tháng', 'type' => 'date', 'format' => 'm'),
            array('field' => 'phone', 'title' => 'Điện thoại', 'type' => 'phone'),
            array('field' => 'name', 'title' => 'Họ tên'),
            array('field' => 'email', 'title' => 'Email'),
            array('field' => 'birthday', 'title' => 'Ngày sinh', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'birthday_year', 'title' => 'Năm sinh'),
            array('field' => 'location_city_id', 'title' => 'Tỉnh thành', 'type' => 'data_source', 'data_source' => $location_city),
            array('field' => 'location_district_id', 'title' => 'Quận/huyện', 'type' => 'data_source', 'data_source' => $location_district),
            array('field' => 'address', 'title' => 'Địa chỉ', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'facebook', 'title' => 'Facebook', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'type', 'title' => 'Phân loại', 'type' => 'data_source', 'data_source' => $sale_contact_type),
            array('field' => 'user_id', 'title' => 'Người quản lý', 'type' => 'data_source', 'data_source' => $user, 'data_source_field' => 'name'),
            array('field' => 'sale_group_id', 'title' => 'Đội nhóm', 'type' => 'data_source', 'data_source' => $sale_group),
            array('field' => 'sale_branch_id', 'title' => 'Cơ sở', 'type' => 'data_source', 'data_source' => $sale_branch),
            array('field' => 'source_group_id', 'title' => 'Nguồn khách hàng', 'type' => 'data_source', 'data_source' => $sale_source_group),
            array('field' => 'history_created', 'title' => 'Ngày chăm sóc cuối', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'history_created_by', 'title' => 'Người chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $user, 'data_source_field' => 'name'),
            array('field' => 'history_action_id', 'title' => 'Hành động chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $sale_history_action, 'data_source_field' => 'name'),
            array('field' => 'history_result_id', 'title' => 'Kết quả chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $sale_history_result, 'data_source_field' => 'name'),
            array('field' => 'history_content', 'title' => 'Nội dung chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'history_return', 'title' => 'Ngày hẹn chăm sóc lại', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'store', 'title' => 'Kho'),
            array('field' => 'contract_total', 'title' => 'Hợp đồng'),
        );
    
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Set document properties
        $objPHPExcel->getProperties()->setCreator($this->_userInfo->getUserInfo('name'))
                                     ->setLastModifiedBy($this->_userInfo->getUserInfo('username'))
                                     ->setTitle("Export");
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $dateFormat->formatToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        $value = $data['data_source'][$item[$data['field']]][$field];
                        break;
                    case 'data_serialize':
                        $data_serialize = $item[$data['data_serialize_field']] ? unserialize($item[$data['data_serialize_field']]) : array();
                        $value = $data_serialize[$data['field']];
                        
                        if(!empty($data['data_source'])) {
                            $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                            $value = $data['data_source'][$data_serialize[$data['field']]][$field];
                        }
                        break;
                    default:
                        $value = $item[$data['field']];
                }
    
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
            $startRow++;
        }
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }

}
