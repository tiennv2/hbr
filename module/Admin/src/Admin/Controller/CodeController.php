<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class CodeController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\CodeTable';
        $this->_options['formName'] = 'formAdminCode';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']              = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']                 = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']         = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_send']           = $ssFilter->filter_send;
        $this->_params['ssFilter']['filter_created']        = $ssFilter->filter_created;
        $this->_params['ssFilter']['filter_course_id']      = $ssFilter->filter_course_id;
        $this->_params['ssFilter']['filter_document_id']    = $ssFilter->filter_document_id;
        $this->_params['ssFilter']['filter_keyword']        = $ssFilter->filter_keyword;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
    
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
    
            $ssFilter->filter_created       = $data['filter_created'];
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_send          = $data['filter_send'];
            $ssFilter->filter_course_id     = $data['filter_course_id'];
            $ssFilter->filter_document_id   = $data['filter_document_id'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\Code($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['document']   = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "code-type" ), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache'));
        $this->_viewModel['course']     = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']    = 'Mã kích hoạt';
        return new ViewModel($this->_viewModel);
    }
    
    public function addAction() {
        $myForm			= $this->getForm();
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
    
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'add-item'));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute();
                } else if($controlAction == 'save') {
                    $this->goRoute();
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = 'Mã kích hoạt - Thêm mới';
        return new ViewModel($this->_viewModel);
    }
    
    public function editAction() {
        $myForm			= $this->getForm();
        
        $task = 'add-item';
        $caption = 'Mã kích hoạt - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Code(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Mã kích hoạt - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute();
                } else if($controlAction == 'save') {
                    $this->goRoute();
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function statusAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            //$this->getTable()->changeStatus($this->_params, array('task' => 'change-status'));
        } else {
            $this->goRoute();
        }
    
        return $this->response;
    }
    
    public function onOffAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params, array('task' => 'update-onoff'));
        } else {
            $this->goRoute();
        }
    
        return $this->response;
    }
    
    public function printAction() {
        $viewModel = new ViewModel($this->_viewModel);
        
        if($this->getRequest()->isPost()){
            $this->_params['data'] = $this->getRequest()->getFiles()->toArray();
            
            if(!empty($this->_params['data']['file_import']['tmp_name'])){
                $upload 		= new \ZendX\File\Upload();
                $file_import	= $upload->uploadFile('file_import', PATH_FILES . '/import/', array('task' => 'rename', 'file_name' => 'import'));
                $viewModel->setVariable('file_import', $file_import);
                $viewModel->setVariable('import', true);
                 
                require_once PATH_VENDOR . '/Excel/PHPExcel/IOFactory.php';
                $objPHPExcel = \PHPExcel_IOFactory::load(PATH_FILES . '/import/import.xlsx');
                 
                $sheetData = $objPHPExcel->getActiveSheet(1)->toArray(null, true, true, true);
                $viewModel->setVariable('sheetData', $sheetData);
            }
        }
        
        $viewModel->setTerminal(true);
        
        return $viewModel;
    }
    
    public function exportAction() {
        $date       = new \ZendX\Functions\Date();
    
        $status     = array(0 => array('name' => 'Chưa sử dụng'), 1 => array('name' => 'Đã sử dụng'));
        $send       = array(0 => array('name' => 'Chưa gửi'), 1 => array('name' => 'Đã gửi'));
        $document   = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "code-type" ), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache'));
        $course     = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
        $items      = $this->getTable()->listItem($this->_params, array('task' => 'list-export'));
    
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'code', 'title' => 'Mã kích hoạt'),
            array('field' => 'empty', 'title' => 'Mã học viên'),
            array('field' => 'course_id', 'title' => 'Khóa học', 'type' => 'data_source', 'data_source' => $course),
            array('field' => 'created', 'title' => 'Ngày tạo', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'status', 'title' => 'Trạng thái', 'type' => 'data_source', 'data_source' => $status),
            array('field' => 'document_id', 'title' => 'Loại', 'type' => 'data_source', 'data_source' => $document),
            array('field' => 'send', 'title' => 'Trạng thái', 'type' => 'data_source', 'data_source' => $send),
        );
    
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Set document properties
        $objPHPExcel->getProperties()->setTitle("Export");
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
    
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $date->formatToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        $value = $data['data_source'][$item[$data['field']]][$field];
                        break;
                    default:
                        $value = $item[$data['field']];
                }
    
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
            $startRow++;
        }
    
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Code.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
    
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }
}
