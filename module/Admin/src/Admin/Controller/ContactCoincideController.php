<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class ContactCoincideController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ContactCoincideTable';
        $this->_options['formName'] = 'formAdminContactCoincide';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']                      = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'date';
        $this->_params['ssFilter']['order']                         = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_type']                   = $ssFilter->filter_type;
        $this->_params['ssFilter']['filter_keyword']                = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_date_begin']             = $ssFilter->filter_date_begin;
        $this->_params['ssFilter']['filter_date_end']               = $ssFilter->filter_date_end;
        $this->_params['ssFilter']['filter_date_type']              = $ssFilter->filter_date_type;
        $this->_params['ssFilter']['filter_sale_branch']            = $ssFilter->filter_sale_branch ? $ssFilter->filter_sale_branch : $this->_userInfo->getUserInfo('sale_branch_id');
        $this->_params['ssFilter']['filter_marketing_team'] 	    = $ssFilter->filter_marketing_team;
        $this->_params['ssFilter']['filter_history_result']         = $ssFilter->filter_history_result;
        $this->_params['ssFilter']['filter_contact_type']           = $ssFilter->filter_contact_type;
        $this->_params['ssFilter']['filter_product_interest']       = $ssFilter->filter_product_interest;
        $this->_params['ssFilter']['filter_location_city']          = $ssFilter->filter_location_city;
        $this->_params['ssFilter']['filter_location_district']      = $ssFilter->filter_location_district;
        $this->_params['ssFilter']['filter_history_status']         = $ssFilter->filter_history_status;
        $this->_params['ssFilter']['filter_contact_source']         = $ssFilter->filter_contact_source;
        $this->_params['ssFilter']['filter_marketing_channel']      = $ssFilter->filter_marketing_channel;
        $this->_params['ssFilter']['filter_user']                   = $ssFilter->filter_user;
        
        $sale_group_ids = $this->_userInfo->getUserInfo('sale_group_ids') ? explode(',', $this->_userInfo->getUserInfo('sale_group_ids')) : array();
        if(!empty($ssFilter->filter_sale_group)) {
            $this->_params['ssFilter']['filter_sale_group'] = $ssFilter->filter_sale_group;
        } else {
            if(!empty($this->_userInfo->getUserInfo('sale_group_id') && count($sale_group_ids) <= 1)) {
                $this->_params['ssFilter']['filter_sale_group'] = $this->_userInfo->getUserInfo('sale_group_id');
            }
        }
        
        if(!empty($ssFilter->filter_user)) {
            $this->_params['ssFilter']['filter_user'] = $ssFilter->filter_user;
        } else {
            if(!empty($this->_userInfo->getUserInfo('sale_group_id')) && empty($this->_userInfo->getUserInfo('sale_group_ids'))) {
                $this->_params['ssFilter']['filter_user'] = $this->_userInfo->getUserInfo('id');
            }
        }
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']   = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber']  = $this->params()->fromRoute('page', 1);
        $this->_params['paginator']             = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        $ssFilter	= new Container(__CLASS__);
        $data = $this->_params['data'];
    
        if($this->getRequest()->isPost()) {
    
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->order_by                 = $data['order_by'];
            $ssFilter->order                    = $data['order'];
            $ssFilter->filter_type              = $data['filter_type'];
            $ssFilter->filter_keyword           = $data['filter_keyword'];
            $ssFilter->filter_date_begin        = $data['filter_date_begin'];
            $ssFilter->filter_date_end          = $data['filter_date_end'];
            $ssFilter->filter_date_type         = $data['filter_date_type'];
            $ssFilter->filter_marketing_team    = $data['filter_marketing_team'];
            $ssFilter->filter_history_result 	= $data['filter_history_result'];
            $ssFilter->filter_contact_type      = $data['filter_contact_type'];
            $ssFilter->filter_product_interest  = $data['filter_product_interest'];
            $ssFilter->filter_location_city     = $data['filter_location_city'];
            $ssFilter->filter_location_district = $data['filter_location_district'];
            $ssFilter->filter_history_status    = $data['filter_history_status'];
            $ssFilter->filter_contact_source    = $data['filter_contact_source'];
            $ssFilter->filter_marketing_channel = $data['filter_marketing_channel'];
            $ssFilter->filter_user              = $data['filter_user'];
            
            if(!empty($data['filter_sale_group'])) {
                if($ssFilter->filter_sale_group != $data['filter_sale_group']) {
                    $ssFilter->filter_user = null;
                    $ssFilter->filter_sale_group = $data['filter_sale_group'];
                }
            } else {
                $ssFilter->filter_user = null;
                $ssFilter->filter_sale_group = $data['filter_sale_group'];
            }
            
            if(!empty($data['filter_sale_branch'])) {
                if($ssFilter->filter_sale_branch != $data['filter_sale_branch']) {
                    $ssFilter->filter_sale_group = null;
                    $ssFilter->filter_user = null;
                    $ssFilter->filter_sale_branch = $data['filter_sale_branch'];
                }
            } else {
                $ssFilter->filter_sale_group = null;
                $ssFilter->filter_user = null;
                $ssFilter->filter_sale_branch = $data['filter_sale_branch'];
            }
        }
        
        if($ssFilter['filter_date_type'] == 'created') {
            if(empty($ssFilter->filter_date_begin)) {
                $ssFilter->filter_date_begin = date('01/m/Y');
                $ssFilter->filter_date_end = date('t/m/Y');
            }
        }
        
        if($this->_params['route']['type'] == 'history-return') {
            $ssFilter->filter_date_begin        = date('d/m/Y');
            $ssFilter->filter_date_end          = date('d/m/Y');
            $ssFilter->filter_date_type         = 'history_return';
            $ssFilter->filter_history_status    = '';
        }
    
        if($this->_params['route']['type'] == 'history-status') {
            $ssFilter->filter_date_begin        = '';
            $ssFilter->filter_date_end          = '';
            $ssFilter->filter_date_type         = '';
            $ssFilter->filter_history_status    = 'no';
        }
        
        $this->goRoute();
    }
    
    public function indexAction() {
        $ssFilter = new Container(__CLASS__);
        $ssFilter->currentPageNumber = $this->_paginator['currentPageNumber'];
        
        $myForm	= new \Admin\Form\Search\ContactCoincide($this->getServiceLocator(), $this->_params['ssFilter']);
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));

        $this->_viewModel['myForm']	                = $myForm;
        $this->_viewModel['items']                  = $items;
        $this->_viewModel['count']                  = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['userInfo']               = $this->_userInfo->getUserInfo();
        $this->_viewModel['caption']                = 'Danh sách trùng - Danh sách';
        return new ViewModel($this->_viewModel);
    }

    public function viewAction() {
        if(!empty($this->_params['data']['id'])) {
            $item                   = $this->getTable()->getItem($this->_params['data'], null);
            $options                = !empty($item['options']) ? unserialize($item['options']) : array();
            $product_id['id']       = $options['product_id'];
            $sex                    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache-alias'));
            $city                   = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
            $district               = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
            $product_interest       = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem($product_id);
            $level                  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'contact-level')), array('task' => 'cache-alias'));
            $marketing_channel      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'marketing-channel')), array('task' => 'cache'));
            $marketing_team         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'marketing-team')), array('task' => 'cache'));
            $this->_viewModel['caption']                = 'Thông tin liên hệ';
            $this->_viewModel['item']                   = $item;
            $this->_viewModel['sex']                    = $sex;
            $this->_viewModel['city']                   = $city;
            $this->_viewModel['district']               = $district;
            $this->_viewModel['product_interest']       = $product_interest;
            $this->_viewModel['level']                  = $level;
            $this->_viewModel['marketing_channel']      = $marketing_channel;
            $this->_viewModel['marketing_team']         = $marketing_team;
        }else{
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        $this->_viewModel['settings']   = $this->_settings;
        $this->_viewModel['caption']    = 'Thông tin chi tiết về khách hàng';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function editAction(){
        $myForm = new \Admin\Form\ContactCoincide\Edit($this->getServiceLocator());
    
        if(!empty($this->_params['data']['id'])) {
            $contact_id = $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contact_id['contact_id']));
            $contact['company_problem'] = explode(',', $contact['company_problem']);
            $myForm->setData($contact);
    
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contact\Edit($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['item'] = $contact;
                    $this->_params['settings'] = $this->_settings;
    
                    // Cập nhập contact
                    $this->_params['data']['id'] = $contact['id'];
                    $this->_params['data']['item'] = $contact;
                    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    // Update lịch sử chăm sóc cho khách hàng
                    $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'add-history'));
                    $this->_params['data']['contact_id'] = $contact;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\HistoryTable')->saveItem($this->_params, array('task' => 'contact-add-history'));
    
                    $this->flashMessenger()->addMessage('Sửa thông tin khách hàng thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contact']    = $contact;
        $this->_viewModel['settings']   = $this->_settings;
        $this->_viewModel['userInfo']   = $this->_userInfo->getUserInfo();
        $this->_viewModel['caption']    = 'Cập nhập data';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function historyAddAction() {
        $myForm = new \Admin\Form\Contact\History($this->getServiceLocator());
    
        if(!empty($this->_params['data']['id'])) {
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contact\History($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item']      = $contact;
                    $this->_params['settings']  = $this->_settings;
    
                    // Thêm lịch sử chăm sóc
                    $result = $this->getServiceLocator()->get('Admin\Model\HistoryTable')->saveItem($this->_params, array('task' => 'add-item'));
                    
                    // Cập nhật lịch sử chăm sóc cuối cho liên hệ
                    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    $this->flashMessenger()->addMessage('Thêm lịch sử thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $contact_options = !empty($contact['options']) ? unserialize($contact['options']) : array();
                $contact = array_merge($contact, $contact_options);
                
                unset($contact['history_action_id']);
                unset($contact['history_result_id']);
                unset($contact['history_content']);
                unset($contact['history_return']);
                $myForm->setData($contact);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contact']    = $contact;
        $this->_viewModel['settings']   = $this->_settings;
        $this->_viewModel['userInfo']   = $this->_userInfo->getUserInfo();
        $this->_viewModel['caption']    = 'Thêm lịch sử chăm sóc';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function historyListAction() {
        $items = $this->getServiceLocator()->get('Admin\Model\HistoryTable')->listItem(array('data' => array('contact_coincide_id' => $this->_params['data']['id'])), array('task' => 'list-ajax'));
        $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_params['data']['id']));
    
        $this->_viewModel['items']    	            = $items;
        $this->_viewModel['user']                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['sale_group']             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $this->_viewModel['sale_branch']            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $this->_viewModel['sale_history_action']    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-action')), array('task' => 'cache'));
        $this->_viewModel['sale_history_result']    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-result')), array('task' => 'cache'));
        $this->_viewModel['product_interest']       = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'product-interest')), array('task' => 'cache'));
        $this->_viewModel['caption']                = 'Danh sách lịch sử chăm sóc của khách hàng: '. $contact['name'] .' - '. $contact['phone'];
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function changeUserAction(){
        $myForm     = new \Admin\Form\Contact\ChangeUser($this->getServiceLocator(), $this->_userInfo->getUserInfo());
        $caption    = 'Liên hệ - Chuyển quyền quản lý'; 
        
        if($this->getRequest()->isPost()){
            if(!empty($this->_params['data']['contact_ids'])) {
                $contact_ids = $this->_params['data']['contact_ids'];
                $myForm->setInputFilter(new \Admin\Filter\Contact\ChangeUser(array('data' => $this->_params['data'])));
                $myForm->setData($this->_params['data']);
        
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['user'] = $this->getServiceLocator()->get('Admin\Model\UserTable')->getItem(array('id' => $this->_params['data']['user_id']));
        
                    $result = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'change-user'));
        
                    $this->flashMessenger()->addMessage('Chuyển quyền quản lý '. $result .' liên hệ thành công');
                    $this->goRoute();
                }
            } else {
                $contact_ids = @implode(',', $this->_params['data']['cid']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        $this->_viewModel['contact_ids']    = $contact_ids;
        $this->_viewModel['myForm']	        = $myForm;
        $this->_viewModel['caption']        = $caption;
        return new ViewModel($this->_viewModel);
    }

    public function noAcceptAction(){
        $myForm = new \Admin\Form\ContactCoincide\NoAccept($this->getServiceLocator());
    
        if(!empty($this->_params['data']['id'])) {
            $contact_coincide = $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }

        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\ContactCoincide\NoAccept($this->_params));
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    $this->_params['item'] = $contact_coincide;
    
                    // Xóa đi
                    $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->deleteItem($this->_params, null);

                    $this->flashMessenger()->addMessage('Sửa thông tin khách hàng thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']             = $myForm;
        $this->_viewModel['contact_coincide']   = $contact_coincide;
        $this->_viewModel['caption']            = 'Xác nhận không chuyển quản lý';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }

    public function acceptAction(){
        $myForm = new \Admin\Form\ContactCoincide\Accept($this->getServiceLocator());
    
        if(!empty($this->_params['data']['id'])) {
            $contact_coincide = $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }

        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\ContactCoincide\Accept($this->_params));
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    $this->_params['item'] = $contact_coincide;
                    //Chuyển quyền quản lý
                    $this->_params['id'] = $this->_params['item']['contact_id'];
                    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem($this->_params, null);
                    $this->_params['contact'] = $contact;
                    $this->getServiceLocator()->get('Admin\Model\ContactTable')->changeUser($this->_params, null);

                    //Xóa tất cả các contact trùng trong bảng trùng
                    $this->_params['item']['contact_id'] = $this->_params['id'];
                    $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->deleteItem($this->_params, 'delete-items');
                    $this->flashMessenger()->addMessage('Chuyển quyền quản lý thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']             = $myForm;
        $this->_viewModel['contact_coincide']   = $contact_coincide;
        $this->_viewModel['caption']            = 'Xác nhận chuyển quản lý';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }

    public function deleteAction()
    {
        $items = $this->_params['data'];
        $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->deleteItem($items['cid'], 'delete-coincide');
        return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'contact-coincide', 'action' => 'index'));
    }

    public function changeContactCoincideAction()
    {
        $items = $this->_params['data'];
        $this->getServiceLocator()->get('Admin\Model\ContactCoincideTable')->changeContactCoincide($items['cid']);
        return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'contact-coincide', 'action' => 'index'));
    }
    
    public function importAction() {
        $myForm = new \Admin\Form\Contact\Import($this->getServiceLocator(), $this->_params);
        $myForm->setInputFilter(new \Admin\Filter\Contact\Import($this->_params));
    
        $this->_viewModel['caption']    = 'Import liên hệ';
        $this->_viewModel['myForm']		= $myForm;
        $viewModel = new ViewModel($this->_viewModel);
    
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()){
                // Check liên hệ
                $item = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('phone' => $this->_params['data']['phone']), array('task' => 'by-phone'));
                if(!empty($item)) {
                    echo 'Tồn tại';
                } else {
                    $user = $this->getServiceLocator()->get('Admin\Model\UserTable')->getItem(array('id' => $this->_params['data']['user_id']), null);
                    $this->_params['data']['sale_branch_id'] = $user['sale_branch_id'];
                    $this->_params['data']['sale_group_id'] = $user['sale_group_id'];
                    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'import-insert'));
                    echo 'Hoàn thành';
                }
                
                return $this->response;
            }
        } else {
            if($this->getRequest()->isPost()){
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    if(!empty($this->_params['data']['file_import']['tmp_name'])){
                        $upload 		= new \ZendX\File\Upload();
                        $file_import	= $upload->uploadFile('file_import', PATH_FILES . '/import/', array());
                    }
                    $viewModel->setVariable('file_import', $file_import);
                    $viewModel->setVariable('import', true);
                     
                    require_once PATH_VENDOR . '/Excel/PHPExcel/IOFactory.php';
                    $objPHPExcel = \PHPExcel_IOFactory::load(PATH_FILES . '/import/'. $file_import);
                     
                    $sheetData = $objPHPExcel->getActiveSheet(1)->toArray(null, true, true, true);
                    $viewModel->setVariable('sheetData', $sheetData);
                }
            }
        }
    
        return $viewModel;
    }
    
    public function exportAction() {
        $dateFormat             = new \ZendX\Functions\Date();
    
        $items                  = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false));
        
        $user                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $sale_group             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $sale_branch            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $sale_contact_type      = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contact-type')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $sale_history_action    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-action')), array('task' => 'cache'));
        $sale_history_result    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-result')), array('task' => 'cache'));
        $sale_source_group      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-group')), array('task' => 'cache'));
        $sale_source_access     = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-access')), array('task' => 'cache'));
        $location_city          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
        $location_district      = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
        
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'date', 'title' => 'Ngày', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'date', 'title' => 'Tháng', 'type' => 'date', 'format' => 'm'),
            array('field' => 'phone', 'title' => 'Điện thoại', 'type' => 'phone'),
            array('field' => 'name', 'title' => 'Họ tên'),
            array('field' => 'email', 'title' => 'Email'),
            array('field' => 'birthday', 'title' => 'Ngày sinh', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'birthday_year', 'title' => 'Năm sinh'),
            array('field' => 'location_city_id', 'title' => 'Tỉnh thành', 'type' => 'data_source', 'data_source' => $location_city),
            array('field' => 'location_district_id', 'title' => 'Quận/huyện', 'type' => 'data_source', 'data_source' => $location_district),
            array('field' => 'address', 'title' => 'Địa chỉ', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'facebook', 'title' => 'Facebook', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'type', 'title' => 'Phân loại', 'type' => 'data_source', 'data_source' => $sale_contact_type),
            array('field' => 'user_id', 'title' => 'Người quản lý', 'type' => 'data_source', 'data_source' => $user, 'data_source_field' => 'name'),
            array('field' => 'sale_group_id', 'title' => 'Đội nhóm', 'type' => 'data_source', 'data_source' => $sale_group),
            array('field' => 'sale_branch_id', 'title' => 'Cơ sở', 'type' => 'data_source', 'data_source' => $sale_branch),
            array('field' => 'source_group_id', 'title' => 'Nguồn khách hàng', 'type' => 'data_source', 'data_source' => $sale_source_group),
            array('field' => 'history_created', 'title' => 'Ngày chăm sóc cuối', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'history_created_by', 'title' => 'Người chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $user, 'data_source_field' => 'name'),
            array('field' => 'history_action_id', 'title' => 'Hành động chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $sale_history_action, 'data_source_field' => 'name'),
            array('field' => 'history_result_id', 'title' => 'Kết quả chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options', 'data_source' => $sale_history_result, 'data_source_field' => 'name'),
            array('field' => 'history_content', 'title' => 'Nội dung chăm sóc', 'type' => 'data_serialize', 'data_serialize_field' => 'options'),
            array('field' => 'history_return', 'title' => 'Ngày hẹn chăm sóc lại', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'store', 'title' => 'Kho'),
            array('field' => 'contract_total', 'title' => 'Hợp đồng'),
        );
    
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Set document properties
        $objPHPExcel->getProperties()->setCreator($this->_userInfo->getUserInfo('name'))
                                     ->setLastModifiedBy($this->_userInfo->getUserInfo('username'))
                                     ->setTitle("Export");
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $dateFormat->formatToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        $value = $data['data_source'][$item[$data['field']]][$field];
                        break;
                    case 'data_serialize':
                        $data_serialize = $item[$data['data_serialize_field']] ? unserialize($item[$data['data_serialize_field']]) : array();
                        $value = $data_serialize[$data['field']];
                        
                        if(!empty($data['data_source'])) {
                            $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                            $value = $data['data_source'][$data_serialize[$data['field']]][$field];
                        }
                        break;
                    default:
                        $value = $item[$data['field']];
                }
    
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
            $startRow++;
        }
    
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }

}
