<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class ContractController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ContractTable';
        $this->_options['formName'] = 'formAdminContract';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']              = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'date';
        $this->_params['ssFilter']['order']                 = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword']        = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_date_begin']     = $ssFilter->filter_date_begin;
        $this->_params['ssFilter']['filter_date_end']       = $ssFilter->filter_date_end;
        $this->_params['ssFilter']['filter_date_type']      = $ssFilter->filter_date_type;
        $this->_params['ssFilter']['filter_sale_group']     = $ssFilter->filter_sale_group;
        $this->_params['ssFilter']['filter_user']           = $ssFilter->filter_user;
        $this->_params['ssFilter']['filter_product'] 	    = $ssFilter->filter_product;
        $this->_params['ssFilter']['filter_debt']           = $ssFilter->filter_debt;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    // Tìm kiếm
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->order_by                 = $data['order_by'];
            $ssFilter->order                    = $data['order'];
            $ssFilter->filter_keyword           = $data['filter_keyword'];
            $ssFilter->filter_date_begin        = $data['filter_date_begin'];
            $ssFilter->filter_date_end          = $data['filter_date_end'];
            $ssFilter->filter_date_type         = $data['filter_date_type'];
            $ssFilter->filter_product 	        = $data['filter_product'];
            $ssFilter->filter_debt 	            = $data['filter_debt'];
            $ssFilter->filter_user              = $data['filter_user'];
            
            if(!empty($data['filter_sale_group'])) {
                if($ssFilter->filter_sale_group != $data['filter_sale_group']) {
                    $ssFilter->filter_user = null;
                    $ssFilter->filter_sale_group = $data['filter_sale_group'];
                }
            } else {
                $ssFilter->filter_user = null;
                $ssFilter->filter_sale_group = $data['filter_sale_group'];
            }
            
            if($ssFilter['filter_date_type'] == 'date_debt') {
                if(empty($ssFilter->filter_date_begin)) {
                    $ssFilter->filter_date_begin = date('01/m/Y');
                    $ssFilter->filter_date_end = date('t/m/Y');
                }
            }
        }
    
        $this->goRoute();
    }
    
    // Danh sách
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\Contract($this->getServiceLocator(), $this->_params['ssFilter']);
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        if( $this->_params['ssFilter']['filter_date_type'] == 'date_debt') {
            $count = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false))->count();
        } else {
            $count = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));;
        }

        $user = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));

        $this->_viewModel['myForm']	                = $myForm;
        $this->_viewModel['items']                  = $items;
        $this->_viewModel['count']                  = $count;
        $this->_viewModel['user']                   = $user;
        $this->_viewModel['payment']                = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'payment')), array('task' => 'cache'));
        $this->_viewModel['vat']                    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'vat')), array('task' => 'cache'));
        $this->_viewModel['sale_group']             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $this->_viewModel['sale_branch']            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $this->_viewModel['product']                = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(array('where' => array('status' => 1)), array('task' => 'cache'));

        $this->_viewModel['caption']                = 'Đơn hàng - Danh sách';
        
        return new ViewModel($this->_viewModel);
    }
    
    // Thêm mới hợp đồng
    public function addAction() {
        $myForm = $this->getForm();
        
        if($this->getRequest()->isPost()){
            $myForm->setInputFilter(new \Admin\Filter\Contract(array('data' => $this->_params['data'], 'route' => $this->_params['route'])));
            
            $myForm->setData($this->_params['data']);
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['item'] = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem($this->_params['data'], array('task' => 'by-phone'));
                
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'add-item'));
                $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem(array('id' => $result), array('task' => 'update-code'));
                
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');

                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'add'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'view', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['products']   = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']    = 'Đơn hàng - Thêm mới';
        return new ViewModel($this->_viewModel);
    }
    
    // Xem chi tiết hợp đồng
    public function viewAction() {
        if(!empty($this->params('id'))) {
            $item = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->params('id')));
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['item']                       = $item;
        $this->_viewModel['contact']                    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
        $this->_viewModel['user']                       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['company_department']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'company-department')), array('task' => 'cache'));
        $this->_viewModel['location_district']          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
        $this->_viewModel['product_interest']           = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'product-interest')), array('task' => 'cache'));
        $this->_viewModel['sex']                        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache-alias'));
        $this->_viewModel['payment']                    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'payment')), array('task' => 'cache'));
        $this->_viewModel['product']                    = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['vat']                        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'vat')), array('task' => 'cache'));
        $this->_viewModel['caption']                    = 'Xem chi tiết đơn hàng';
        return new ViewModel($this->_viewModel);
    }
    
    // Xem nhanh hợp đồng
    public function quickViewAction() {
        if(!empty($this->_params['data']['id'])) {
            $item = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['item']                       = $item;
        $this->_viewModel['contact']                    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
        $this->_viewModel['user']                       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['sale_contact_type']          = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contact-type')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $this->_viewModel['product_interest']           = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'product-interest')), array('task' => 'cache'));
        $this->_viewModel['vat']                        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'vat')), array('task' => 'cache'));
        $this->_viewModel['product']                    = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['sex']                        = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $this->_viewModel['caption']                    = 'Xem chi tiết đơn hàng';
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        
        return $viewModel;
    }
    
    // Sửa hợp đồng
    public function editAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $myForm = new \Admin\Form\Contract\Edit($this->getServiceLocator(), $this->_params);
        if(!empty($this->_params['data']['id'])) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
            $myForm->setData($contract);
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                if($myForm->isValid()){
                    //$this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    
                    //$this->_params['item'] = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem($this->_params['data'], array('task' => 'by-phone'));
                    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'edit-item'));
                    $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                    echo 'success';
                    return $this->response;
                }
            }
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['products']   = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']    = 'Sửa đơn hàng';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Sửa sản phẩm
    public function editProductAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $numberFormat = new \ZendX\Functions\Number();
        $myForm = new \Admin\Form\Contract\EditProduct($this->getServiceLocator(), $this->_params);
        
        if(!empty($this->_params['data']['id'])) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\EditProduct($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item'] = $contract;
                    $this->_params['contact'] = $contact;
    
                    // Tính lại giá tiền khi thay đổi sản phẩm
                    $price = $numberFormat->formatToNumber($this->_params['data']['price']);
                    $price_promotion = 0;
                    $price_promotion_percent = $contract['price_promotion_percent'];
                    $price_promotion_price = $contract['price_promotion_price'];
                    $price_paid = $contract['price_paid'];
                    $price_accrued = $contract['price_accrued'];
                    
                    if(!empty($contract['price_promotion_percent'])) {
                        $price_promotion = $contract['price_promotion_percent'] / 100 * $price;
                    }
                    if(!empty($contract['price_promotion_price'])) {
                        $price_promotion = $price_promotion + $contract['price_promotion_price'];
                    }
                    
                    $price_total = $price - $price_promotion;
                    $price_owed = $price_total - $price_paid + $price_accrued;
                    
                    $this->_params['data']['price'] = $price;
                    $this->_params['data']['price_promotion'] = $price_promotion;
                    $this->_params['data']['price_promotion_percent'] = $price_promotion_percent;
                    $this->_params['data']['price_promotion_price'] = $price_promotion_price;
                    $this->_params['data']['price_total'] = $price_total;
                    $this->_params['data']['price_owed'] = $price_owed;
                    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    $this->flashMessenger()->addMessage('Cập nhật dữ liệu thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['caption']    = 'Sửa sản phẩm';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Sửa ưu đãi
    public function editPromotionAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $numberFormat = new \ZendX\Functions\Number();
        $myForm = new \Admin\Form\Contract\EditPromotion($this->getServiceLocator(), $this->_params);
    
        if(!empty($this->_params['data']['id'])) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contract_options = !empty($contract['options']) ? unserialize($contract['options']) : array();
            $contract = array_merge($contract, $contract_options);
            $contract['date'] = $dateFormat->formatToView($contract['date']);
    
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
    
            $myForm->setData($contract);
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\EditPromotion($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item'] = $contract;
                    $this->_params['contact'] = $contact;
    
                    // Tính lại giá tiền khi thay đổi sản phẩm
                    $price = $numberFormat->formatToNumber($this->_params['data']['price']);
                    $price_promotion = 0;
                    $price_promotion_percent = $numberFormat->formatToNumber($this->_params['data']['price_promotion_percent']);
                    $price_promotion_price = $numberFormat->formatToNumber($this->_params['data']['price_promotion_price']);
                    $price_paid = $contract['price_paid'];
                    $price_accrued = $contract['price_accrued'];
    
                    if(!empty($this->_params['data']['price_promotion_percent'])) {
                        $price_promotion = $numberFormat->formatToNumber($this->_params['data']['price_promotion_percent']) / 100 * $price;
                    }
                    if(!empty($this->_params['data']['price_promotion_price'])) {
                        $price_promotion = $price_promotion + $numberFormat->formatToNumber($this->_params['data']['price_promotion_price']);
                    }
    
                    $price_total = $price - $price_promotion;
                    $price_owed = $price_total - $price_paid + $price_accrued;
    
                    $this->_params['data']['price'] = $price;
                    $this->_params['data']['price_promotion'] = $price_promotion;
                    $this->_params['data']['price_promotion_percent'] = $price_promotion_percent;
                    $this->_params['data']['price_promotion_price'] = $price_promotion_price;
                    $this->_params['data']['price_total'] = $price_total;
                    $this->_params['data']['price_owed'] = $price_owed;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    $this->flashMessenger()->addMessage('Cập nhật dữ liệu thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['caption']    = 'Sửa ưu đãi';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Sửa ghi chú
    public function editNoteAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $numberFormat = new \ZendX\Functions\Number();
        $myForm = new \Admin\Form\Contract\EditNote($this->getServiceLocator(), $this->_params);
    
        if(!empty($this->_params['data']['id'])) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contract_options = !empty($contract['options']) ? unserialize($contract['options']) : array();
            $contract = array_merge($contract, $contract_options);
            $contract['date'] = $dateFormat->formatToView($contract['date']);
    
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
    
            $myForm->setData($contract);
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\EditNote($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item'] = $contract;
                    $this->_params['contact'] = $contact;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    $this->flashMessenger()->addMessage('Cập nhật dữ liệu thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['caption']    = 'Sửa ghi chú';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Xóa hợp đồng
    public function deleteAction() {
        $item = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->params('id')));
    
        if(empty($item)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        if($this->getRequest()->isPost()) {
            // Xóa hoa đồng
            $this->_params['item'] = $item;
            $contract_delete = $this->getTable()->deleteItem($this->_params, array('task' => 'delete-item'));
    
            $this->flashMessenger()->addMessage('Xóa hợp đồng thành công');
    
            $this->goRoute();
        }
    
    
        $this->_viewModel['item']               = $item;
        $this->_viewModel['contact']            = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
        $this->_viewModel['user']               = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['sale_group']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $this->_viewModel['sale_branch']        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $this->_viewModel['sale_source_group']  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-group')), array('task' => 'cache'));
        $this->_viewModel['sex']                = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $this->_viewModel['product']            = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']            = 'Hợp đồng - Xóa';
        return new ViewModel($this->_viewModel);
    }
    
    // Sửa thông tin khách hàng
    public function contactEditAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $myForm = new \Admin\Form\Contract\Contact($this->getServiceLocator(), $this->_params);
    
        if(!empty($this->_params['data']['id'])) {
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact_options = !empty($contact['options']) ? unserialize($contact['options']) : array();
            $contact = array_merge($contact, $contact_options);
            $contact['birthday'] = !empty($contact['birthday']) ? $dateFormat->formatToView($contact['birthday']) : null;
    
            $myForm->setData($contact);
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\Contact($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['item'] = $contact;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($this->_params, array('task' => 'edit-item'));
    
                    $this->flashMessenger()->addMessage('Cập nhật dữ liệu thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']             = $myForm;
        $this->_viewModel['item']               = $contact;
        $this->_viewModel['location_district']  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'location-district')), array('task' => 'cache'));
        $this->_viewModel['caption']            = 'Cập nhật thông tin khách hàng';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Thêm đơn hàng
    public function billAddAction() {
        $dateFormat = new \ZendX\Functions\Date();
        
        if(!empty($this->_params['data']['id'])) {
            $contract   = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
            $contact['birthday'] = !empty($contact['birthday']) ? $dateFormat->formatToView($contact['birthday']) : null;
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        
        if($this->getRequest()->isPost()){
            $myForm = new \Admin\Form\Contract\Bill($this->getServiceLocator());
            $myForm->setData($this->_params['data']);
            
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\Bill(array('data' => $this->_params['data'], 'contract' => $contract, 'contact' => $contact)));
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract']  = $contract;
                    $this->_params['contact']   = $contact;
                    
                    if(!empty($this->_params['data']['paid_price']) || !empty($this->_params['data']['accrued_price'])) {
                        // Thêm đơn hàng
                        $bill = $this->getServiceLocator()->get('Admin\Model\BillTable')->saveItem($this->_params, array('task' => 'add-item'));
                        
                        // Cập nhật lại thông tin thanh toán hợp đồng
                        $number = new \ZendX\Functions\Number();
                        
                        $price_paid     = $contract['price_paid'] + $number->formatToNumber($this->_params['data']['paid_price']);
                        $price_accrued  = $contract['price_accrued'] + $number->formatToNumber($this->_params['data']['accrued_price']);
                        $price_owed     = $contract['price_total'] - $price_paid + $price_accrued;
                        
                        $arrContract = array();
                        $arrContract['id'] = $contract['id'];
                        $arrContract['price_paid'] = $price_paid;
                        $arrContract['price_accrued'] = $price_accrued;
                        $arrContract['price_owed'] = $price_owed;
                        $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem(array('data' => $arrContract), array('task' => 'edit-item'));
                    }
            
                    $this->flashMessenger()->addMessage('Thêm đơn hàng thành công');
                    echo 'success';
                    return $this->response;
                }
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['caption']    = 'Thêm đơn hàng';
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['contact']    = $contact;
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        
        return $viewModel;
    }
    
    // Sửa đơn hàng
    public function billEditAction() {
        $dateFormat = new \ZendX\Functions\Date();
        
        if(!empty($this->_params['data']['id'])) {
            $item       = $this->getServiceLocator()->get('Admin\Model\BillTable')->getItem(array('id' => $this->_params['data']['id']));
            $contract   = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $item['contract_id']));
            $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
        
        if($this->getRequest()->isPost()){
            $myForm = new \Admin\Form\Contract\BillEdit($this->getServiceLocator());
            $myForm->setInputFilter(new \Admin\Filter\Contract\BillEdit(array('data' => $this->_params['data'], 'item' => $item)));
            if ($item['type'] == 'Thu') {
                $caption = 'Sửa phiếu thu';
                $message = 'Sửa phiếu thu thành công';
            } elseif ($item['type'] == 'Chi') {
                $caption = 'Sửa phiếu chi';
                $message = 'Sửa phiếu chi thành công';
            }
            
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract']  = $contract;
                    $this->_params['contact']   = $contact;
                    $this->_params['item']      = $item;
                    
                    $result = $this->getServiceLocator()->get('Admin\Model\BillTable')->saveItem($this->_params, array('task' => 'contract-edit-item'));
            
                    $this->flashMessenger()->addMessage($message);
                    echo 'success';
                    return $this->response;
                }
            } else {
                $item_options = !empty($item['options']) ? unserialize($item['options']) : array();
                $item = array_merge($item, $item_options);
                $item['date'] = $dateFormat->formatToView($item['date']);
                
                $myForm->setData($item);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['caption']    = $caption;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['contract']   = $contract;
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        
        return $viewModel;
    }
    
    // Xóa đơn hàng
    public function billDeleteAction() {
        if(!empty($this->_params['data']['id'])) {
            $item       = $this->getServiceLocator()->get('Admin\Model\BillTable')->getItem(array('id' => $this->_params['data']['id']));
            $contract   = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $item['contract_id']));
            $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            $myForm = new \Admin\Form\Contract\BillDelete($this->getServiceLocator());
            $myForm->setInputFilter(new \Admin\Filter\Contract\BillDelete($this->_params));
            $myForm->setData($item);
            
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract']  = $contract;
                    $this->_params['contact']   = $contact;
                    $this->_params['item']      = $item;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\BillTable')->deleteItem($this->_params, array('task' => 'contract-delete-item'));
    
                    $this->flashMessenger()->addMessage('Xóa đơn hàng thành công');
                    echo 'success';
                    return $this->response;
                }
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']         = $myForm;
        $this->_viewModel['caption']        = 'Xóa đơn hàng';
        $this->_viewModel['item']           = $item;
        $this->_viewModel['contract']       = $contract;
        $this->_viewModel['bill_type']      = array('paid' => 'Thu', 'accrued' => 'Chi', 'surcharge' => 'Phụ phí');
        $this->_viewModel['paid_type']      = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "bill-type-paid" ), "order" => array("ordering" => "ASC", "created" => "ASC", "name" => "ASC"), "view"  => array( "key" => "id", "value" => "name", "sprintf" => "%s" ) ), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $this->_viewModel['accrued_type']   = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "bill-type-accrued" ), "order" => array("ordering" => "ASC", "created" => "ASC", "name" => "ASC"), "view"  => array( "key" => "id", "value" => "name", "sprintf" => "%s" ) ), array('task' => 'cache'));
        $this->_viewModel['surcharge_type'] = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "bill-type-surcharge" ), "order" => array("ordering" => "ASC", "created" => "ASC", "name" => "ASC"), "view"  => array( "key" => "id", "value" => "name", "sprintf" => "%s" ) ), array('task' => 'cache'));
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Thêm vật phẩm
    public function matterAddAction() {
        if(!empty($this->_params['data']['id'])) {
            $contract   = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
            $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            $myForm = new \Admin\Form\Contract\Matter($this->getServiceLocator());
            $myForm->setInputFilter(new \Admin\Filter\Contract\Matter(array('data' => $this->_params['data'], 'route' => $this->_params['route'], 'contract' => $contract, 'contact' => $contact)));
            $myForm->setData($this->_params['data']);
    
            if($this->_params['data']['modal'] == 'success') {
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract']  = $contract;
                    $this->_params['contact']   = $contact;
                    // Cập nhật vật phẩm vào hợp đồng
                    $update = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'add-matter'));
    
                    // Thêm vật phẩm vào danh sách
                    $result = $this->getServiceLocator()->get('Admin\Model\MatterTable')->saveItem($this->_params, array('task' => 'add-item'));
    
                    $this->flashMessenger()->addMessage('Thêm vật phẩm thành công');
                    echo 'success';
                    return $this->response;
                }
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['caption']    = 'Thêm vật phẩm';
        $this->_viewModel['items']      = $items;
        $this->_viewModel['contract']   = $contract;
        $this->_viewModel['contact']    = $contact;
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Xóa vật phẩm
    public function matterDeleteAction() {
        if(!empty($this->_params['data']['id'])) {
            $item       = $this->getServiceLocator()->get('Admin\Model\MatterTable')->getItem(array('id' => $this->_params['data']['id']));
            $contract   = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $item['contract_id']));
            $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            $myForm = new \Admin\Form\Contract\MatterDelete($this->getServiceLocator());
            $myForm->setInputFilter(new \Admin\Filter\Contract\MatterDelete($this->_params));
    
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    $this->_params['data']      = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract']  = $contract;
                    $this->_params['contact']   = $contact;
                    $this->_params['item']      = $item;
    
                    // Cập nhật vật phẩm vào hợp đồng
                    $update = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'delete-matter'));
    
                    // Xóa khỏi danh sách
                    $result = $this->getServiceLocator()->get('Admin\Model\MatterTable')->deleteItem($this->_params, array('task' => 'contract-delete-item'));
    
                    $this->flashMessenger()->addMessage('Xóa vật phẩm thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($item);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']         = $myForm;
        $this->_viewModel['caption']        = 'Xóa vật phẩm';
        $this->_viewModel['item']           = $item;
        $this->_viewModel['matter']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'matter')), array('task' => 'cache'));
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    // Chuyển người quản lý
    public function changeUserAction(){
        if($this->getRequest()->isXmlHttpRequest()) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']), null);
            $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $contract['contact_id']), null);
            if(!empty($contract)) {
                if($this->getRequest()->isPost()){
                    $bill = $this->getServiceLocator()->get('Admin\Model\BillTable')->listItem(array('data' => array('contract_id' => $contract['id'])), array('task' => 'list-all'));
                    
                    $this->_params['user'] = $this->getServiceLocator()->get('Admin\Model\UserTable')->getItem(array('id' => $this->_params['data']['user_id']));
                    $this->_params['contract'] = $contract;
                    $this->_params['contact'] = $contact;
                    $this->_params['bill'] = $bill;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'change-user'));
                }
            }
            
            return $this->response;
        } else {
            if($this->getRequest()->isPost()){
                $myForm = new \Admin\Form\Contract\ChangeUser($this->getServiceLocator(), $this->_params);
                
                if($this->getRequest()->isPost()){
                    $items = $this->getServiceLocator()->get('Admin\Model\ContractTable')->listItem(array('ids' => $this->_params['data']['cid']), array('task' => 'list-item-multi'));
                }
                
                $this->_viewModel['myForm']	                = $myForm;
                $this->_viewModel['caption']                = 'Hợp đồng - Chuyển quyền quản lý';
                $this->_viewModel['items']                  = $items;
                $this->_viewModel['user']                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
                $this->_viewModel['sale_group']             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
                $this->_viewModel['sale_branch']            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
                $this->_viewModel['product']                = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
            } else {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => $this->_params['controller'], 'action' => 'index'));
            }
        }
    
        return new ViewModel($this->_viewModel);
    }
    
    // Chuyển nhượng
    public function transferAction() {
        $dateFormat = new \ZendX\Functions\Date();
        $myForm = new \Admin\Form\Contract\Transfer($this->getServiceLocator(), $this->_params);
    
        if(!empty($this->_params['data']['id'])) {
            $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        if($this->getRequest()->isPost()){
            if($this->_params['data']['modal'] == 'success') {
                $myForm->setInputFilter(new \Admin\Filter\Contract\Transfer($this->_params));
                $myForm->setData($this->_params['data']);
    
                if($myForm->isValid()){
                    $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                    $this->_params['contract'] = $contract;
    
                    $result = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'transfer'));
    
                    $this->flashMessenger()->addMessage('Cập nhật dữ liệu thành công');
                    echo 'success';
                    return $this->response;
                }
            } else {
                $myForm->setData($this->_params['data']);
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $this->_viewModel['myForm']             = $myForm;
        $this->_viewModel['location_district']  = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'location-district')), array('task' => 'cache'));
        $this->_viewModel['caption']            = 'Chuyển nhượng hợp đồng';
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function printAction() {
        $item       = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('id' => $this->_params['route']['id']));
        $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
    
        if(empty($item)) {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        $this->_viewModel['item']                       = $item;
        $this->_viewModel['contact']                    = $contact;
        $this->_viewModel['user']                       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['payment']                    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'payment')), array('task' => 'cache'));
        $this->_viewModel['product']                    = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['company_department']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'company-department')), array('task' => 'cache'));
        
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function printMultiAction() {
        $items      = $this->getServiceLocator()->get('Admin\Model\ContractTable')->listItem(array('ids' => $this->_params['data']['cid']), array('task' => 'list-print-multi'));
        $contact    = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
        if(empty($items)) {
            return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
        }
    
        $this->_viewModel['items']                      = $items;
        $this->_viewModel['contact']                    = $contact;
        $this->_viewModel['user']                       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['vat']                        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'vat')), array('task' => 'cache'));
        $this->_viewModel['payment']                    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'payment')), array('task' => 'cache'));
        $this->_viewModel['product']                    = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['company_department']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'company-department')), array('task' => 'cache'));
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
    public function exportAction() {
        $dateFormat             = new \ZendX\Functions\Date();
        
        $items                  = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false));
        $user                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $sale_group             = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
        $sale_branch            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
        $contract_type          = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contract-type')), array('task' => 'cache'));
        $payment                = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'payment')), array('task' => 'cache'));
        $sale_contact_type      = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-contact-type')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'object'));
        $sale_history_action    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-action')), array('task' => 'cache'));
        $sale_history_result    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-history-result')), array('task' => 'cache'));
        $sale_source_group      = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-group')), array('task' => 'cache'));
        $sale_source_access     = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-source-access')), array('task' => 'cache'));
        $location_city          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
        $location_district      = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
        $product                = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
        
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
        
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
        
        // Data Export
        $arrData = array(
            array('field' => 'date', 'title' => 'Ngày', 'type' => 'date', 'format' => 'd/m/Y'),
            array('field' => 'date', 'title' => 'Tháng', 'type' => 'date', 'format' => 'm'),
            array('field' => 'contact_phone', 'title' => 'Điện thoại', 'type' => 'phone'),
            array('field' => 'contact_name', 'title' => 'Họ tên', 'type' => 'contact_name'),
            array('field' => 'contact_email', 'title' => 'Email', 'type' => 'contact_email'),
            array('field' => 'contact_location_city_id', 'title' => 'Tỉnh thành', 'type' => 'data_source', 'data_source' => $location_city),
            array('field' => 'contact_location_district_id', 'title' => 'Quận/huyện', 'type' => 'data_source', 'data_source' => $location_district),
            array('field' => 'address', 'title' => 'Địa chỉ', 'type' => 'address'),
            array('field' => 'payment', 'title' => 'Phương thức thanh toán', 'type' => 'data_source', 'data_source' => $payment),
            array('field' => 'product', 'title' => 'Sản phẩm', 'type' => 'data_product', 'data_serialize_product' => 'options'),
            array('field' => 'product', 'title' => 'Chiết khấu đơn hàng', 'type' => 'data_discount', 'data_serialize_discount' => 'options'),
            array('field' => 'product', 'title' => 'Thành tiền', 'type' => 'data_total', 'data_serialize_total' => 'options'),
            array('field' => 'user_id', 'title' => 'Người quản lý', 'type' => 'data_source', 'data_source' => $user, 'data_source_field' => 'name'),
        );
        
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
        
        // Set document properties
        $objPHPExcel->getProperties()->setCreator($this->_userInfo->getUserInfo('name'))
        							 ->setLastModifiedBy($this->_userInfo->getUserInfo('username'))
        							 ->setTitle("Export");
        
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
        
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $dateFormat->formatToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        $value = $data['data_source'][$item[$data['field']]][$field];
                        break;
                   	case 'data_serialize':
                        $data_serialize = $item[$data['data_serialize_field']] ? unserialize($item[$data['data_serialize_field']]) : array();
                        $value = $data_serialize[$data['field']];
                        
                        if(!empty($data['data_source'])) {
                            $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                            $value = $data['data_source'][$data_serialize[$data['field']]][$field];
                        }
                        if(!empty($data['data_date_format'])) {
                            $value = $dateFormat->formatToView($data_serialize[$data['field']], $data['data_date_format']);
                        }
                        break;
                    case 'data_product':
                        $data_serialize_product = $item[$data['data_serialize_product']] ? unserialize($item[$data['data_serialize_product']]) : array();
                        $value_pro = $data_serialize_product[$data['field']];
                        $value = '';
                        foreach ($value_pro as $key_product => $value_product){
                            $value .= 'Sản phẩm: '.$product[$value_product['product_id']]['name']. ' - ' . ' Số lượng: ' . ' ( ' . $value_product['numbers'] .' ) ' . ' - ' . ' Giá: ' . number_format($value_product['price']) .PHP_EOL;
                        }
                        break;
                    case 'data_discount':
                        $data_serialize_discount = $item[$data['data_serialize_discount']] ? unserialize($item[$data['data_serialize_discount']]) : array();
                        $value_dis = $data_serialize_discount[$data['field']];
                        $value = '';
                        foreach ($value_dis as $key_discount => $value_discount){
                            $value .= number_format($value_discount['discount']);
                        }
                        break;
                    case 'data_total':
                        $data_serialize_total = $item[$data['data_serialize_total']] ? unserialize($item[$data['data_serialize_total']]) : array();
                        $value_total = $data_serialize_total[$data['field']];
                        $value = '';
                        foreach ($value_total as $key_total => $value_tot){
                            $total += $value_tot['total'];
                            $value = number_format($total);
                        }
                        break;
                    default:
                        $value = $item[$data['field']];
                }
                
                //$objPHPExcel->getActiveSheet()->getStyle('H5')->getAlignment()->setWrapText(true);
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
            $startRow++;
        }
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
        
        return $this->response;
    }
    
    // Nhập đữ liệu từ file excel
    public function importAction() {
        $myForm = new \Admin\Form\Contract\Import($this->getServiceLocator(), $this->_params);
        $myForm->setInputFilter(new \Admin\Filter\Contract\Import($this->_params));

        $this->_viewModel['caption']    = 'Import đơn hàng';
        $this->_viewModel['myForm']     = $myForm;
        
        $viewModel = new ViewModel($this->_viewModel);
    
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()){
                $item = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('code' => $this->_params['data']['ct_code']), array('task' => 'by-code'));
                if(empty($item)) {
                    // Lấy contact theo số điện thoại
                    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('phone' => $this->_params['data']['ct_phone']), array('task' => 'by-phone'));
                    $this->_params['data']['contact_id'] = !empty($contact) ? $contact['id'] : null;

                    // Lấy user theo mã nhân viên.
                    $user = $this->getServiceLocator()->get('Admin\Model\UserTable')->getItem(array('code' => $this->_params['data']['ct_create_uid']), array('task' => 'by-code'));
                    $this->_params['data']['create_by'] = !empty($user) ? $user['id'] : null;

                    // Lấy product.
                    $product = $this->getServiceLocator()->get('Admin\Model\ProductTable')->getItem(array('code' => $this->_params['data']['ct_pro_code']), array('task' => 'by-code'));
                    $this->_params['data']['ct_pro_id']     = !empty($product) ? $product[0]['id'] : null;
                    $this->_params['data']['ct_pro_unit']   = !empty($product) ? $product[0]['unit_product'] : null;

                    $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'import-insert'));

                    echo 'Hoàn thành';
                }
                else{
                    echo 'Tồn tại';
                }
                
                return $this->response;
            }
        } else {
            if($this->getRequest()->isPost()){
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    if(!empty($this->_params['data']['file_import']['tmp_name'])){
                        $upload         = new \ZendX\File\Upload();
                        $file_import    = $upload->uploadFile('file_import', PATH_FILES . '/import/', array());
                    }
                    $viewModel->setVariable('file_import', $file_import);
                    $viewModel->setVariable('import', true);
                     
                    require_once PATH_VENDOR . '/Excel/PHPExcel/IOFactory.php';
                    $objPHPExcel = \PHPExcel_IOFactory::load(PATH_FILES . '/import/'. $file_import);
                     
                    $sheetData = $objPHPExcel->getActiveSheet(1)->toArray(null, true, true, true);
                    $viewModel->setVariable('sheetData', $sheetData);
                }
            }
        }
        return $viewModel;
    }
    
    // Cập nhật contract từ file excel giao vận.
    public function importUpdateAction() {
        $myForm = new \Admin\Form\Contract\Import($this->getServiceLocator(), $this->_params);
        $myForm->setInputFilter(new \Admin\Filter\Contract\Import($this->_params));

        $this->_viewModel['caption']    = 'Import đơn hàng';
        $this->_viewModel['myForm']     = $myForm;
        
        $viewModel = new ViewModel($this->_viewModel);
    
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()){
                $item = $this->getServiceLocator()->get('Admin\Model\ContractTable')->getItem(array('code' => $this->_params['data']['code']), array('task' => 'by-code'));
                if(empty($item)) {
                    echo "Không tồn tại";
                }
                else{
                    $this->_params['data']['id'] = $item['id'];
                    $contract = $this->getServiceLocator()->get('Admin\Model\ContractTable')->saveItem($this->_params, array('task' => 'import-update'));
                    echo "Hoàn thành";
                }
                
                return $this->response;
            }
        } else {
            if($this->getRequest()->isPost()){
                $myForm->setData($this->_params['data']);
                if($myForm->isValid()){
                    if(!empty($this->_params['data']['file_import']['tmp_name'])){
                        $upload         = new \ZendX\File\Upload();
                        $file_import    = $upload->uploadFile('file_import', PATH_FILES . '/import/', array());
                    }
                    $viewModel->setVariable('file_import', $file_import);
                    $viewModel->setVariable('import', true);
                     
                    require_once PATH_VENDOR . '/Excel/PHPExcel/IOFactory.php';
                    $objPHPExcel = \PHPExcel_IOFactory::load(PATH_FILES . '/import/'. $file_import);
                     
                    $sheetData = $objPHPExcel->getActiveSheet(1)->toArray(null, true, true, true);
                    $viewModel->setVariable('sheetData', $sheetData);
                }
            }
        }
        return $viewModel;
    }
}


