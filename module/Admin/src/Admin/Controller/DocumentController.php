<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;
use Zend\Db\TableGateway\TableGateway;

class DocumentController extends ActionController {
    
    public function init() {
        // Cấu hình dynamic
        $dynamic = $this->getServiceLocator()->get('Admin\Model\DynamicTable')->getItem(array('code' => $this->_params['route']['slug']), array('task' => 'code'));
        if(empty($dynamic['option'])) {
            die('Lỗi đường dẫn. Vui lòng liên hệ admin');
        } else {
            $dynamic_option = $dynamic['option'];
            eval("\$this->_params[\"configs\"] = $dynamic_option;");
        }
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\DocumentTable';
        $this->_options['formName'] = 'formAdminDocument';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        $configs = $this->_params['configs'];
        
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option = intval($data['pagination_option']);
    
            $ssFilter->order_by         = $data['order_by'];
            $ssFilter->order            = $data['order'];
    
            $ssFilter->filter_status    = $data['filter_status'];
            $ssFilter->filter_keyword   = $data['filter_keyword'];
        }
    
        return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'index'));
    }
    
    public function indexAction() {
        $configs = $this->_params['configs'];
        $adapter = $this->getServiceLocator()->get('dbConfig');
        
        $myForm	= new \Admin\Form\Search\Document($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));

        foreach ($configs['list']['fields'] AS $field) {
            if(!empty($field['data_source'])) {
		        $tableGateway = new TableGateway(TABLE_PREFIX . $field['data_source']['table'], $adapter, null);
		        $table        = new \Admin\Model\DocumentTable($tableGateway);
		        $service      = $table->setServiceLocator($this->getServiceLocator());
		        $task         = $field['data_source']['task'] ? $field['data_source']['task'] : 'cache';
		        $data_source  = $table->listItem($field['data_source'], array('task' => $task));
		        
		        $this->_viewModel['data_source'][$field['data_source']['table']] = $data_source;
		    }
        }
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']    = $configs['general']['title']['value'] . ' - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $configs    = $this->_params['configs'];
        
        $myForm     = new \Admin\Form\Document($this->getServiceLocator(), $this->_params['configs']);
        $myForm->setInputFilter(new \Admin\Filter\Document(array('configs' => $this->_params['configs'])));
        
        $task = 'add-item';
        $caption = $configs['general']['title']['value'] . ' - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Document(array('id' => $this->_params['data']['id'], 'configs' => $this->_params['configs'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = $configs['general']['title']['value'] . ' - Sửa';
            }
        }
        
        if($this->_params['route']['code'] == 'copy') {
            $caption = $configs['general']['title']['value'] . ' - Copy';
            $task = 'add-item';
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'form'));
                } else if($controlAction == 'save') {
                    return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'form', 'id' => $result));
                } else {
                    return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'index'));
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function statusAction() {
        $configs    = $this->_params['configs'];
        
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params, array('task' => 'change-status'));
        } else {
            return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'index'));
        }
    
        return $this->response;
    }
    
    public function deleteAction() {
        $configs    = $this->_params['configs'];
        
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid'])) {
                $result = $this->getTable()->deleteItem($this->_params, array('task' => 'delete-item'));
                $message = 'Xóa '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'index'));
    }
    
    public function orderingAction() {
        $configs    = $this->_params['configs'];
        
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid']) && !empty($this->_params['data']['ordering'])) {
                $result = $this->getTable()->changeOrdering($this->_params, array('task' => 'change-ordering'));
                $message = 'Sắp xếp '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        return $this->redirect()->toRoute('routeAdminDocument/default', array('slug' => $configs['code'], 'action' => 'index'));
    }
}

/* array(
    "code"  => "history-action",
    "general" => array(
        "title" => array(
            "value" => "Lịch sử chăm sóc - Hành động",
        )
    ),
    "list" => array(
        "general" => array(
            "showIndex" => array(
                "value" => true,
            ),
            "showCheckbox" => array(
                "value" => true,
            ),
            "showControl" => array(
                "value" => true,
            ),
        ),
        "pagination" => array(
            "active" => array(
                "value" => true,
            ),
            "itemCountPerPage" => array(
                "value" => 50,
            ),
            "pageRange" => array(
                "value" => 5,
            ),
            "options" => array(
                "value" => array(10, 20, 50, 100, 200, 500, 1000),
            ),
        ),
        "fields" => array(
            array(
                "caption"       => "Tên",
                "name"          => "name",
            ),
            array(
                "caption"       => "Thứ tự",
                "name"          => "ordering",
                "type"          => "text",
                "attributes"    => array(
                    "class" => "col-80"
                )
            ),
        ),
    ),
    "form" => array(
        "general" => array(
        ),
        "fields" => array(
            array(
                "caption"       => "Tên",
                "name"          => "name",
                "type"          => "text",
                "attributes"    => array(
                    "class"			=> "form-control",
                    "id"			=> "name",
                    "placeholder"	=> "Nhập tên"
                ),
                "validators"      => array(
                    "require"       => 1
                )
            ),
            array(
                "caption"       => "Tỉnh thành",
                "name"          => "location_city_id",
                "type"          => "select",
                "attributes"	=> array(
                    "class"		=> "form-control select2 select2_basic",
                ),
                "options"		=> array(
                    "empty_option"	=> "- Chọn -",
                    "value_options"	=> array(),
                    "data_source" => array(
                        "table" => "location_city",
                        "where" => array(
                            "location_country_id" => "1471336155-76f0-224d-ke95-l103t2b7z988"
                        ),
                        "order" => array("ordering" => "ASC", "name" => "ASC"),
                        "view"  => array(
                            "key" => "id",
                            "value" => "name,ordering",
                            "sprintf" => "%s - %s"
                        )
                    )
                ),
                "validators"      => array(
                    "require"       => 1
                )
            ),
            array(
                "caption"       => "Thứ tự",
                "name"			=> "ordering",
    		    "type"			=> "text",
    		    "attributes"	=> array(
    		        "value"         => 255,
    		        "class"			=> "form-control",
    		        "id"			=> "ordering",
    		        "placeholder"	=> "Thứ tự"
    		    )
            ),
            array(
                "caption"       => "Trạng thái",
                "name"			=> "status",
    			"type"			=> "select",
    			"attributes"	=> array(
    				"class"		=> "form-control select2 select2_basic",
    			),
    		    "options"		=> array(
    		        "value_options"	=> array( 1	=> "Hiển thị", 0 => "Không hiển thị"),
    		    )
            )
        ),
    )
) */