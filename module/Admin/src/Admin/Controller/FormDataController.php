<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class FormDataController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\FormDataTable';
        $this->_options['formName'] = 'formAdminFormData';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        //$this->_params['ssFilter']['filter_form']       = !empty($ssFilter->filter_form) ? $ssFilter->filter_form : $this->_params['route']['id'];
        $this->_params['ssFilter']['filter_form']       = $ssFilter->filter_form;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
    
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
    
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
            $ssFilter->filter_form          = $data['filter_form'];
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $ssFilter	= new Container(__CLASS__);
        $aclInfo    = new \ZendX\System\UserInfo();
        $this->_params['groupInfo'] = $aclInfo->getGroupInfo();
        $this->_params['permissionInfo'] = $aclInfo->getPermission();
        
        // Lấy danh sách các form.
        $listForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->listItem($this->_params, array('task' => 'cache'));
        
        /* if(!empty($this->_params['route']['id'])) {
            $currentForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $this->_params['route']['id']));
            $ssFilter->filter_form = $currentForm['id'];
        } */
        
        /* if(empty($ssFilter->filter_form)) {
            $currentForm = current($listForm);
            $ssFilter->filter_form = $currentForm['id'];
        } */
        
        $this->_params['ssFilter']['filter_form'] = $ssFilter->filter_form;
        
        $currentForm = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $ssFilter->filter_form));
        /* if(empty($currentForm)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
        } */
        
        $myForm	= new \Admin\Form\Search\FormData($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	        = $myForm;
        $this->_viewModel['items']          = $items;
        $this->_viewModel['listForm']       = $listForm;
        $this->_viewModel['count']          = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']           = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['currentForm']    = $currentForm;
        $this->_viewModel['caption']        = 'Đăng ký - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $myForm			= $this->getForm();
        
        $task = 'add-item';
        $caption = 'Đăng ký - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\FormData(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Đăng ký - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function historyAction() {
    	$myForm = new \Admin\Form\History($this->getServiceLocator());
    
    	if(!empty($this->_params['data']['id'])) {
    		$contact = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->getItem(array('id' => $this->_params['data']['id']));
    		if(!empty($contact['history'])) {
    			$history = unserialize($contact['history']);
    			$contact['history_1'] = $history['history_1'];
    			$contact['history_2'] = $history['history_2'];
    			$contact['history_3'] = $history['history_3'];
    		}
    		$myForm->setData($contact);
    	} else {
    		return $this->redirect()->toRoute('routeAdmin/type', array('controller' => 'notice', 'action' => 'not-found', 'type' => 'modal'));
    	}
    
    	if($this->getRequest()->isPost()){
    		if($this->_params['data']['modal'] == 'success') {
    			$myForm->setInputFilter(new \Admin\Filter\History($this->_params));
    			$myForm->setData($this->_params['data']);
    
    			if($myForm->isValid()){
	    			$this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
	    			$this->_params['item'] = $contact;
    
    				// Update lịch sử chăm sóc cho khách hàng
    				$contact = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->saveItem($this->_params, array('task' => 'add-history'));
    
    				$this->flashMessenger()->addMessage('Thêm lịch sử chăm sóc thành công');
    				echo 'success';
    				return $this->response;
    			}
    		} else {
    			$myForm->setData($this->_params['data']);
    		}
    	} else {
    		return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
    	}
    
    	$this->_viewModel['myForm']     = $myForm;
    	$this->_viewModel['contact']    = $contact;
    	$this->_viewModel['caption']    = 'Thêm lịch sử chăm sóc';
    
    	$viewModel = new ViewModel($this->_viewModel);
    	$viewModel->setTerminal(true);
    
    	return $viewModel;
    }
    
	public function insightAction() {
        $this->_viewModel['params'] = $this->_params;
        return new ViewModel($this->_viewModel);
    }
    
    public function companyProblemAction() {
        $ssFilter = new Container(__CLASS__ . str_replace('-', '_', $this->_params['action']));
        if($this->getRequest()->isPost()) {
            // Lấy giá trị post từ filter
            $this->_params['data'] = $this->getRequest()->getPost()->toArray();

            // Dữ liệu gốc
            $option = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->listItem(array( "where" => array( "form_id" => "1521774912-0w53-z2d3-1027-sukeg89775b3" )),  array('task' => 'list-cache'));

            // Format lại toàn bộ dữ liệu theo vị trí chức vụ
            $arrData = array();
            foreach ($option AS $val){
                if(!empty($val['option'])) {
                    $options = @unserialize($val['option']);
                    foreach ($options as $key => $value){
                        $arrData[$value]['total']++;
                    }
                }
            } 
            arsort($arrData);
            $arrCategory = array();
            $seriesColumn = array('name' => 'Vấn đề', 'name' => array());
            $totalAll = 0;
     
            foreach ($arrData AS $key => $item) {
              
                // Dữ liệu ra bảng
                $totalAll += (float)$item['total'];
                $xhtmlItems .= '<tr data-id="'. $key .'">
                                   <td>' .$key . '</td>
                                   <td>' . (float)$item['total'] . '</td>
                               </tr>';
    
                // Dữ liệu ra biểu đồ
                $arrCategory[] = $key;
                $seriesColumn['name'][] = (float)$item['total'];
            }
            
            $xhtmlItems .= '<tr>
                                <td><b>Tổng</b></td>
                                <td class="mask_currency">' . $totalAll . '</td>
                                <td><b>'. $xhtmlColumn.'</b></td>
                             </tr>';
    
            // Trả kết quả ra ngoài view
            $result['reportTable'] = '<thead><tr><th>Tên vấn đề</th><th>Tổng</th></tr></thead><tbody>'. $xhtmlItems .'</tbody> ';
            $result['reportChart'] = array(
                'categories'    => $arrCategory,
                'series'        => array($seriesColumn)
            );
            
            echo json_encode($result);
            return $this->response;
    
        } else {
            // Khai báo giá trị ngày tháng
            $default_date_begin = date('01/01/Y');
            $default_date_end   = date('31/12/Y');
    
            $ssFilter->report                       = $ssFilter->report ? $ssFilter->report : array();
            $ssFilter->report['date_begin']         = $ssFilter->report['date_begin'] ? $ssFilter->report['date_begin'] : $default_date_begin;
            $ssFilter->report['date_end']           = $ssFilter->report['date_end'] ? $ssFilter->report['date_end'] : $default_date_end;
    
            // Set giá trị cho form
            $myForm = new \Admin\Form\Search\Report($this->getServiceLocator(), $ssFilter->report);
            $myForm->setData($ssFilter->report);
    
            $this->_viewModel['params']         = $this->_params;
            $this->_viewModel['myForm']         = $myForm;
            $this->_viewModel['caption']        = 'Báo cáo vấn đề';
        }
    
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
    
        return $viewModel;
    }
    
	
    public function exportAction() {
        $date               = new \ZendX\Functions\Date();
        $items              = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false));
    
        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'created', 'title' => 'Ngày tạo', 'format' => 'd/m/Y'),
            array('field' => 'name', 'title' => 'Tên'),
            array('field' => 'email', 'title' => 'Email'),
            array('field' => 'phone', 'title' => 'Số điện thoại'),
            array('field' => 'address', 'title' => 'Địa chỉ'),
            
        );
        
        if($items['form_id'] == '1521774912-0w53-z2d3-1027-sukeg89775b3') {
            $arrData[] = array('field' => 'workshop', 'title' => 'Thương hiệu lãnh đạo', 'type' => 'json', 'key' => 0);
            $arrData[] = array('field' => 'workshop', 'title' => 'Mật mã tài lãnh đạo', 'type' => 'json', 'key' => 1);
            $arrData[] = array('field' => 'workshop', 'title' => 'Bản đồ chiến lược nhân sự', 'type' => 'json', 'key' => 2);
        }
    
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $date->fomartToView($item[$data['field']], $formatDate);
                        break;
                    case 'json':
                        $tmp = @unserialize($item[$data['field']]);
                        if ($tmp !== false) {
                            $key = (int)$data['key'];
                            $value = trim($tmp[$key]);
                            if(!empty($value)) {
                                $value = explode('|', $value);
                                $value = trim($value[1]);
                            }
                        } else {
                            $value = $item[$data['field']];
                        }
                        break;
                    default:
                        $value = $item[$data['field']];
                }
    
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
    
            $startRow++;
        }
    
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Form.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
    
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }
    //End
}
