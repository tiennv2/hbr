<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;

class IndexController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\UserTable';
        $this->_options['formName'] = 'formAdminUser';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'post-item', 'action' => 'index'));
    }
    
    public function deleteCacheAction() {
        $cache = $this->getServiceLocator()->get('cache');
        $cache = $cache->clearExpired();
    }
}
