<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class MenuController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\MenuTable';
        $this->_options['formName'] = 'formAdminMenu';
        
        // Thiết lập session filter
        $ssFilter	= new Container(__CLASS__);
        $this->_params['ssFilter']['filter_code'] = $ssFilter->filter_code ? $ssFilter->filter_code : 'Menu';
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 100;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
        }
    
        return $this->redirect()->toRoute('routeAdminNested/default', array( 'controller' => $this->_params['controller'], 'action' => 'index'));
    }
    
    public function indexAction() {
        $ssFilter = new Container(__CLASS__);
        $ssFilter->filter_code = $this->_params['route']['id'] ? $this->_params['route']['id'] : $ssFilter->filter_code;
        $this->_params['ssFilter']['filter_code'] = $ssFilter->filter_code;
        
        $node = $this->getTable()->getItem(array('code' => $ssFilter->filter_code), array('task' => 'code'));
        $this->_params['node'] = $node;
        if(empty($node)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        $count = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $count;
        $this->_viewModel['node']       = $node;
        $this->_viewModel['level']      = $this->getServiceLocator()->get('Admin\Model\MenuTable')->itemInSelectbox($this->_params, array('task' => 'list-level'));
        $this->_viewModel['caption']    = $node['name'] .' - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function addAction() {
        $ssFilter = new Container(__CLASS__);
        $myForm = $this->getForm();
        
        if(empty($ssFilter->filter_code) || empty($this->_params['route']['type']) || empty($this->_params['route']['reference'])) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'add-item'));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');

                if($controlAction == 'save-new') {
                    return $this->redirect()->toRoute('routeAdminNested/add', array(
                        'controller' => $this->_params['controller'],
                        'type' => $this->params()->fromRoute('type'),
                        'reference' => $this->params()->fromRoute('reference')
                    ));
                } else if($controlAction == 'save') {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'edit',
                        'id' => $result
                    ));
                } else {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'index',
                    ));
                }
            }
        }
        
        $nodeReference = $this->getTable()->getItem(array('id' => $this->_params['route']['reference']));
        if(empty($nodeReference)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
    
        $caption = 'Menu - Thêm mới';
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function editAction() {
        $myForm = $this->getForm();
    
        if(!empty($this->_params['route']['id'])) {
            $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Menu(array('id' => $item['id'])));
                $myForm->bind($item);
            } else {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'edit-item'));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save') {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'edit',
                        'id' => $result
                    ));
                } else {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'index',
                    ));
                }
            }
        }
        
        $caption = 'Menu - Sửa';
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function copyAction() {
        $myForm			= $this->getForm();
    
        if(!empty($this->_params['route']['id'])) {
            $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\Menu(array('id' => $item['id'])));
                $myForm->bind($item);
            } else {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['route']['reference'] = $item['id'];
                $this->_params['route']['type'] = 'after';
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'add-item'));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save') {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'edit',
                        'id' => $result
                    ));
                } else {
                    return $this->redirect()->toRoute('routeAdminNested/default', array(
                        'controller' => $this->_params['controller'],
                        'action' => 'index',
                    ));
                }
            }
        }
        
        $caption = 'Menu - Copy';
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function moveAction() {
        $ssFilter = new Container(__CLASS__);
        
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()) {
                $this->getTable()->moveItem($this->_params['data']);
                $message = 'Thứ tự phần tử đã được cập nhật thành công';
                $this->flashMessenger()->addMessage($message);
            }
            
            return $this->response;
        }
        
        if(!empty($this->_params['route']['id'])) {
            $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
            if(empty($item)) {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
            }
            
            $parent_node = $this->getServiceLocator()->get('Admin\Model\MenuTable')->getItem(array('code' => $ssFilter->filter_code), array('task' => 'code'));
            $list_node = $this->getServiceLocator()->get('Admin\Model\MenuTable')->itemInSelectbox(array('node' => $parent_node), array('task' => 'form-category'));
            $level_start = $list_node[0]['level'] + 1;
            unset($list_node[0]);
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        if($this->getRequest()->isPost()){
            $controlAction = $this->_params['data']['control-action'];
            
            switch ($this->_params['data']['type']) {
                case 'left':
                    $this->getTable()->moveLeft($item['id'], $this->_params['data']['parent']);
                    break;
                case 'before':
                    $this->getTable()->moveBefore($item['id'], $this->_params['data']['parent']);
                    break;
                case 'after':
                    $this->getTable()->moveAfter($item['id'], $this->_params['data']['parent']);
                    break;
                default:
                    $this->getTable()->moveRight($item['id'], $this->_params['data']['parent']);
                    break;
            }
            
            $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');

            if($controlAction == 'save') {
                return $this->redirect()->toRoute('routeAdminNested/default', array(
                    'controller' => $this->_params['controller'],
                    'action' => 'edit',
                    'id' => $item['id']
                ));
            } else {
                return $this->redirect()->toRoute('routeAdminNested/default', array(
                    'controller' => $this->_params['controller'],
                    'action' => 'index',
                ));
            }
        }
        
        $caption = 'Menu - Di chuyển';
        
        $this->_viewModel['item']       = $item;
        $this->_viewModel['list_node']  = \ZendX\Functions\CreateArray::create($list_node, array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => $level_start));
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function deleteAction() {
        if(!empty($this->_params['route']['id'])) {
            $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
            if(empty($item)) {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        if($this->getRequest()->isPost()){
            $controlAction = $this->_params['data']['control-action'];
            
            switch ($this->_params['data']['type']) {
                case 'only':
                    $this->getTable()->removeNodeOnly($item['id']);
                    break;
                case 'branch':
                default:
                    $this->getTable()->removeBranch($item['id']);
                    break;
            }
            
            $this->flashMessenger()->addMessage('Dữ liệu đã được xóa thành công');

            if($controlAction == 'save') {
                return $this->redirect()->toRoute('routeAdminNested/default', array(
                    'controller' => $this->_params['controller'],
                    'action' => 'edit',
                    'id' => $item['id']
                ));
            } else {
                return $this->redirect()->toRoute('routeAdminNested/default', array(
                    'controller' => $this->_params['controller'],
                    'action' => 'index',
                ));
            }
        }
        
        $caption = 'Menu - Xóa';
        
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function statusAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params['data'], array('task' => 'change-status'));
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'no-access'));
        }
        
        return $this->response;
    }

    public function orderingAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid']) && !empty($this->_params['data']['ordering'])) {
                $result = $this->getTable()->changeOrdering($this->_params['data'], array('task' => 'change-ordering'));
                $message = 'Sắp xếp '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        return $this->redirect()->toRoute('routeAdminNested/default', array(
            'controller' => $this->_params['controller'],
            'action' => 'index',
        ));
    }
}
