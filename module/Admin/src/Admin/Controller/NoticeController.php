<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;

class NoticeController extends ActionController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function noAccessAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            echo 'no-access';
            return $this->response;
        }
    }
    
    public function notFoundAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            echo 'not-found';
            return $this->response;
        }
    }
}
