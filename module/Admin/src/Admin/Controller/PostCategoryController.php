<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class PostCategoryController extends ActionController {
    
    public function init() {

        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\PostCategoryTable';
        $this->_options['formName'] = 'formAdminPostCategory';
        
        // Thiết lập session filter
        $ssFilter	= new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'left';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'ASC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_parent']     = $ssFilter->filter_parent;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 200;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }

    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
            
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
    
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
            $ssFilter->filter_level         = $data['filter_level'];
            $ssFilter->filter_parent        = $data['filter_parent'];
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\PostCategory($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        $count = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $count;
        $this->_viewModel['type']       = array( 'default'	=> 'Bài viết', 'course' => 'Khóa học', 'course_long' => 'Chuỗi khóa học', 'teacher' => 'Giảng viên', 'product' => 'Sản phẩm', 'gallery' => 'Album ảnh', 'video' => 'Video', 'file' => 'Tài liệu');
        $this->_viewModel['caption']    = 'Danh mục bài viết - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $ssFilter   = new Container(__CLASS__ . 'Form');
        $myForm     = $this->getForm();
    
        $task = 'add-item';
        $caption = 'Danh mục - Thêm mới';
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\PostCategory(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Danh mục - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                
                // Lưu lại category đã chọn
                $ssData['parent'] = $this->_params['data']['parent'];
                $ssData['type'] = $this->_params['data']['type'];
                $ssData['layout'] = $this->_params['data']['layout'];
                $ssFilter->data = $ssData;
                
                // Check tồn tại
                $check = $this->getTable()->getItem(array('alias' => $this->_params['data']['alias']), array('task' => 'alias'));
                if(!empty($check) && $check['id'] != $this->_params['data']['id']) {
                    $parent = $this->getTable()->getItem(array('id' => $this->_params['data']['parent']));
                    $this->_params['data']['alias'] = $parent['alias'] .'-'. $this->_params['data']['alias'];
                }
                
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');

                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = $caption;
        $this->_viewModel['ssData']     = $ssFilter->data;
        return new ViewModel($this->_viewModel);
    }
    
    public function statusAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params['data'], array('task' => 'change-status'));
        } else {
            $this->goRoute();
        }
        
        return $this->response;
    }
    
    public function deleteAction() {
        if(!empty($this->_params['route']['id'])) {
            $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
            if(empty($item)) {
                return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        if($this->getRequest()->isPost()){
            $controlAction = $this->_params['data']['control-action'];
            
            switch ($this->_params['data']['type']) {
                case 'only':
                    $this->getTable()->removeNodeOnly($item['id']);
                    break;
                case 'branch':
                default:
                    $this->getTable()->removeBranch($item['id']);
                    break;
            }
            
            $this->flashMessenger()->addMessage('Dữ liệu đã được xóa thành công');
            $this->goRoute();
        }
        
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = 'Danh mục bài viết - Xóa';
        return new ViewModel($this->_viewModel);
    }

    public function orderingAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid']) && !empty($this->_params['data']['ordering'])) {
                $result = $this->getTable()->changeOrdering($this->_params['data'], array('task' => 'change-ordering'));
                $message = 'Sắp xếp '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        $this->goRoute();
    }
    
    public function moveAction() {
        if($this->getRequest()->isPost()) {
            $this->getTable()->moveItem($this->_params['data']);
            $message = 'Thứ tự phần tử đã được cập nhật thành công';
            $this->flashMessenger()->addMessage($message);
        }
    
        $this->goRoute();
    }
    
    public function refreshAction() {
        $items = $this->getTable()->refresh($this->_params, array('task' => 'list'));
        
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()) {
                $this->getTable()->refresh($this->_params, array('task' => 'update'));
                echo 'Update thành công';
            }
            
            return $this->response;
        }
        
        $this->_viewModel['items']      = $items;
        $this->_viewModel['type']       = array( 'default'	=> 'Bài viết', 'product' => 'Sản phẩm', 'gallery' => 'Album ảnh', 'video' => 'Video', 'file' => 'Tài liệu');
        $this->_viewModel['caption']    = 'Danh mục bài viết - Danh sách';
        return new ViewModel($this->_viewModel);
    }
}
