<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class ProductCartController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ProductCartTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray());
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
         // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->filter_keyword   = $data['filter_keyword'];
            $ssFilter->filter_status    = $data['filter_status'];
        }
    
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\ProductCart($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items      = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['items']              = $items;
        $this->_viewModel['count']              = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['bank']               = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'bank')), array('task' => 'cache'));
        $this->_viewModel['myForm']             = $myForm;
        $this->_viewModel['caption']            = 'Danh sách đặt hàng';
        return new ViewModel($this->_viewModel);
    }
    public function exportAction() {
        $date               = new \ZendX\Functions\Date();
        $items              = $this->getTable()->listItem($this->_params, array('task' => 'list-item', 'paginator' => false));
        $product            = $this->getServiceLocator()->get('Admin\Model\PostItemTable')->countItem(null, array('task' => 'list-item'));

        //Include PHPExcel
        require_once PATH_VENDOR . '/Excel/PHPExcel.php';
    
        // Config
        $config = array(
            'sheetData' => 0,
            'headRow' => 1,
            'startRow' => 2,
            'startColumn' => 0,
        );
    
        // Column
        $arrColumn = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ','BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ');
    
        // Data Export
        $arrData = array(
            array('field' => 'created', 'title' => 'Ngày tạo',  'format' => 'd/m/Y'),
            array('field' => 'name', 'title' => 'Tên'),
            array('field' => 'email', 'title' => 'Email'),
            array('field' => 'phone', 'title' => 'Số điện thoại'),
            array('field' => 'address', 'title' => 'Địa chỉ'),
            array('field' => 'product', 'title' => 'Sản phẩm', 'type' => 'book'),
        );
        
        // Create new PHPExcel object
        $objPHPExcel = new \PHPExcel();
    
        // Set document properties
//         $objPHPExcel->getProperties()->setCreator($this->_userInfo->getUserInfo('fullname'))
//         ->setLastModifiedBy($this->_userInfo->getUserInfo('username'))
//         ->setTitle("Export");
    
        // Dữ liệu tiêu đề cột
        $startColumn = $config['startColumn'];
        foreach ($arrData AS $key => $data) {
            $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $config['headRow'], $data['title']);
            $objPHPExcel->getActiveSheet()->getStyle($arrColumn[$startColumn] . $config['headRow'])->getFont()->setBold(true);
            $startColumn++;
        }
    
        // Dữ liệu data
        $startRow = $config['startRow'];
        foreach ($items AS $item) {
            $startColumn = $config['startColumn'];
            foreach ($arrData AS $key => $data) {
                switch ($data['type']) {
                    case 'date':
                        $formatDate = $data['format'] ? $data['format'] : 'd/m/Y';
                        $value = $date->fomartToView($item[$data['field']], $formatDate);
                        break;
                    case 'data_source':
                        $field = $data['data_source_field'] ? $data['data_source_field'] : 'name';
                        //$value = $data['data_source'][$item[$data['field']]][$field] ;
                        break;
                    case 'book':
                        $source = unserialize($item[$data['field']]);
                        $value = '';
                        foreach ($source as $key_source => $value_source){
                            $value .= ', '. $value_source['product']['name'] .' ('. $value_source['amount'] .')';
                        }
                        
                        $value = substr($value, 2);
                        break;  
                    default:
                        $value = $item[$data['field']];
                }
                
                $objPHPExcel->setActiveSheetIndex($config['sheetData'])->setCellValue($arrColumn[$startColumn] . $startRow, $value);
                $startColumn++;
            }
    
            $startRow++;
        }
    
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Export.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
    
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
    
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    
        return $this->response;
    }
    //End
}
