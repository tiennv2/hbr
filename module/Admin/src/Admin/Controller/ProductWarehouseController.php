<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class ProductWarehouseController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ProductWarehouseTable';
        $this->_options['formName'] = 'formAdminProductWarehouse';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'ordering';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'ASC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
    
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
    
            $ssFilter->pagination_option    = intval($data['pagination_option']);
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
        }
    
        $this->goRoute();
    }

    public function indexAction() {
        $ssFilter = new Container(__CLASS__);
        $ssFilter->currentPageNumber = $this->_paginator['currentPageNumber'];
        $myForm = new \Admin\Form\Search\Contact($this->getServiceLocator(), $this->_params['ssFilter']);
        $myForm = $this->getForm();
        $myForm->setData($this->_params['ssFilter']);
        $items = $this->getServiceLocator()->get('Admin\Model\ProductWarehouseTable')->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']         = $myForm;
        $this->_viewModel['items']          = $items;
        $this->_viewModel['count']          = $this->getServiceLocator()->get('Admin\Model\ProductWarehouseTable')->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']           = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['product_batch']  = $this->getServiceLocator()->get('Admin\Model\ProductBatchTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['product']        = $this->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']        = 'Xuất kho - Danh sách';
        return new ViewModel($this->_viewModel);
    }

    public function viewAction()
    {
        $myForm = new \Admin\Form\Search\ProductWarehouse($this->getServiceLocator(), $this->_params['ssFilter']);
        $myForm->setData($this->_params['ssFilter']);

        $this->_params['id'] = $this->params('id');
        $items = $this->getServiceLocator()->get('Admin\Model\ProductWarehouseTable')->listItem($this->_params, array('task' => 'list-product'));

        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['batch_id']   = $this->params('id');
        $this->_viewModel['caption']    = 'Lô Sản phẩm - Danh sách sản phẩm trong lô';
        return new ViewModel($this->_viewModel);
    }

    public function addProductAction()
    {
        $myForm = new \Admin\Form\ProductWarehouse\AddProduct($this->getServiceLocator(), array());
        $task = 'add-item';
        $caption = 'Sản phẩm - Thêm mới';
        $item = array();
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $myForm->setInputFilter(new \Admin\Filter\ProductWarehouse\AddProduct(array('id' => $this->_params['data']['id'], 'data' => $this->_params['data'], 'route' => $this->_params['route'])));
                $myForm->setData($this->_params['data']);
                $this->_params['product_batch'] = $this->params('id');
                
                $this->getServiceLocator()->get('Admin\Model\ProductWarehouseTable')->saveItem($this->_params, array('task' => 'add-item'));
                
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'add-product', 'id' => $this->params('id')));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'add-product', 'id' => $this->params('id')));
                } else {
                    $this->goRoute(array('action' => 'view', 'id' => $this->params('id')));
                }
            }
        }
        
        $this->_viewModel['batch_id'] = $this->params('id');
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }

    
    public function addAction()
    {
        $myForm = new \Admin\Form\ProductWarehouse\Add($this->getServiceLocator(), array());
        
        $task = 'add-item';
        $caption = 'Sản phẩm - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\ProductWarehouse\Add(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Sản phẩm - Sửa';
            }
        }
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                
                
                if ($task == 'add-item'){
                    $result = $this->getTable()->saveItem($this->_params, array('task' => 'add-item'));
                    $number = $this->getServiceLocator()->get('Admin\Model\ProductBatchTable')->saveItem($this->_params, array('task' => 'update-export'));
                } else {
                    $result = $this->getTable()->saveItem($this->_params, array('task' => 'edit-item'));
                    $number = $this->getServiceLocator()->get('Admin\Model\ProductBatchTable')->saveItem($this->_params, array('task' => 'update-export-number'));
                }
                
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'add'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'add', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']     = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }

    public function formAction() {
        $myForm = $this->getForm();
        
        $task = 'add-item';
        $caption = 'Sản phẩm - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\ProductBatch(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Sản phẩm - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
}
