<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
/* use Zend\Form\FormInterface;
use ZendX\System\UserInfo; */

class TransportController extends ActionController {
    
    public function init() {
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\ContractTable';
        $this->_options['formName'] = 'formAdminTransport';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['filter_keyword']             = !empty($ssFilter->filter_keyword) ? $ssFilter->filter_keyword : null;
        $this->_params['ssFilter']['filter_date_begin']          = !empty($ssFilter->filter_date_begin) ? $ssFilter->filter_date_begin : null; //date('d/m/Y')
        $this->_params['ssFilter']['filter_date_end']            = !empty($ssFilter->filter_date_end) ? $ssFilter->filter_date_end : null;
        $this->_params['ssFilter']['transport_service']          = !empty($ssFilter->transport_service) ? $ssFilter->transport_service : null; //date('d/m/Y')
        $this->_params['ssFilter']['transport_status']           = !empty($ssFilter->transport_status) ? $ssFilter->transport_status : null;
        $this->_params['ssFilter']['transport_type']            = !empty($ssFilter->transport_type) ? $ssFilter->transport_type : null;
        $this->_params['ssFilter']['order_by']                   = !empty($ssFilter->order_by) ? $ssFilter->order_by : null;
        $this->_params['ssFilter']['order']                      = !empty($ssFilter->order) ? $ssFilter->order : null;
        
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage']   = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber']  = $this->params()->fromRoute('page', 1);
        $this->_params['paginator']             = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray());

        // lấy dữ liệu từ url
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
        
        // Lấy trạng thái
        $this->_list_status = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-status')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'alias'));

    }
    
    public function filterAction() {
        $ssFilter	= new Container(__CLASS__);
        $data = $this->_params['data'];
    
        if($this->getRequest()->isPost()) {
            $ssFilter['filter_keyword']         = !empty($data['filter_keyword']) ? $data['filter_keyword'] : null;
            $ssFilter['filter_date_begin']      = !empty($data['filter_date_begin']) ? $data['filter_date_begin'] : null;
            $ssFilter['filter_date_end']        = !empty($data['filter_date_end']) ? $data['filter_date_end'] : null;
            $ssFilter['transport_service']      = !empty($data['transport_service']) ? $data['transport_service'] : null;
            $ssFilter['transport_status']       = !empty($data['transport_status']) ? $data['transport_status'] : null;
            $ssFilter['transport_type']         = !empty($data['transport_type']) ? $data['transport_type'] : null;
            $ssFilter['order_by']               = !empty($data['order_by']) ? $data['order_by'] : null;
            $ssFilter['order']                  = !empty($data['order']) ? $data['order'] : null;
            $ssFilter->pagination_option        = intval($data['pagination_option']);
            $ssFilter->order_by                 = $data['order_by'];
        }
        
        $this->goRoute();
    }
    
    // giao diện chính
    public function indexAction() {
        $ssFilter = new Container(__CLASS__);
        $ssFilter->currentPageNumber = $this->_paginator['currentPageNumber'];
        
        $myForm	= new \Admin\Form\Transport($this);
        $myForm->setData($this->_params['ssFilter']);
        
        // lấy dữ liệu
        $items                = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        $users                = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $transport_service    = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-service')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
        $transport_status     = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-status')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
        $transport_type       = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-type')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));

        $this->_viewModel['myForm']	                = $myForm;
        $this->_viewModel['items']                  = $items;
        $this->_viewModel['users']                  = $users;
        $this->_viewModel['transport_service']      = $transport_service;
        $this->_viewModel['transport_status']       = $transport_status;
        $this->_viewModel['transport_type']         = $transport_type;
        $this->_viewModel['count']                  = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['location_city']          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
        $this->_viewModel['location_district']      = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));

        // $this->_viewModel['user']                   = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']                = 'Vận chuyển - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    //add, giao diện sau khi nhấn nút tạo đơn giao vận
    public function addAction(){
        $ssFilter = new Container(__CLASS__);
        $this->_viewModel = array();
        $myForm	= new \Admin\Form\Transport\Add($this);
        //set data to form
        $dataForm = array();
        
        //load giao diện nếu request phù hợp
        if ($this->getRequest()->isPost()){
            if ($this->_params['data']['task'] == 'add'){
                $dataForm['list_id'] = json_encode($this->_params['data']['list-id']);
                $myForm->setData($dataForm);
            }
            if ($this->_params['data']['type'] == 'submit'){
                return $this->response;
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        $this->_viewModel['caption'] = 'Tạo vận đơn';
        $this->_viewModel['params']                 = $this->_params;
        $this->_viewModel['myForm']	                = $myForm;
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    // view controller, không còn cần thiết
    public function viewAction() {
        $item = $this->getTable()->getItem(array('id' => $this->_params['route']['id']));
        if(empty($item)) {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        $contact_info = $this->getServiceLocator()->get('Admin\Model\ContactTable')->getItem(array('id' => $item['contact_id']));
    
        $this->_viewModel['item']    	            = $item;
        $this->_viewModel['contact']                = is_array($contact_info) && count($contact_info)? $contact_info:null;
        $this->_viewModel['location_city']          = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache'));
        $this->_viewModel['location_district']      = $this->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 2), array('task' => 'cache'));
        $this->_viewModel['caption']                = 'Xem thông tin chi tiết đơn hàng';
        
        $viewModel = new ViewModel($this->_viewModel);
        return $viewModel;
    }
    
    
    // load tùy chọn cho vận đơn
    public function optionsAction(){
        $ssFilter = new Container(__CLASS__);
        $this->_viewModel = array();
        $myForm	= null;
        $myView = null;
        //set data to form
        $dataForm = array();
        
        //load giao diện nếu request phù hợp
        if ($this->getRequest()->isPost()){
            if ($this->_params['data']['type'] == 'add'){
                $myForm	= new \Admin\Form\Transport\Add($this);
                $dataForm['list_id'] = json_encode($this->_params['data']['list-id']);
                $myForm->setData($dataForm);
                $myView = 'admin/transport/add';
                $this->_viewModel['caption'] = 'Tạo vận đơn';
            }
            if ($this->_params['data']['type'] == 'edit-range'){
                $myForm	= new \Admin\Form\Transport\Edit($this);
                $dataForm['list_id'] = json_encode($this->_params['data']['list-id']);
                $myForm->setData($dataForm);
                $myView = 'admin/transport/edit-range';
                $this->_viewModel['caption'] = 'Tạo vận đơn';
            }
        } else {
            return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        
        $this->_viewModel['params']                 = $this->_params;
        $this->_viewModel['myForm']	                = $myForm;
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTemplate($myView);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    
    //sửa trạng thái giao vận
    public function editStatusAction(){
        $ssFilter = new Container(__CLASS__);
        $this->_viewModel = array();
        $myForm	= new \Admin\Form\Transport\Edit($this);
        $check = 0;
        
        if ($this->getRequest()->isPost()){
            if ($this->_params['data']['task'] == 'view'){
                $dataForm['list_id'] = json_encode($this->_params['data']['list_id']);
                $myForm->setData($dataForm);
            }
            if ($this->_params['data']['task'] == 'submit'){
                $list_id = json_decode($this->_params['data']['list_id'], true);
                
                // gán trạng thái
                if (in_array($this->_params['data']['transport_status'], $this->_list_status)){
                    $status = $this->_params['data']['transport_status'];
                } elseif (!in_array($this->_params['data']['transport_status'], $this->_list_status) && !empty($this->_params['data']['transport_status_text'])) {
                    $status = $this->_params['data']['transport_status_text'];
                } else {
                    $status = 100;
                }
                
                if(is_array($list_id) && count($list_id)){
                    $list_transport = $this->getTable()->listItem($list_id, array('task' => 'list-by-id'));
                    if (is_array($list_transport) && count($list_transport)){
                        foreach ($list_transport as $key => $val){
                            $val['transport_status'] = $status;
                            $check = $this->getTable()->saveItem($val, array('task' => 'update-status-local'));
                        }
                    }
                }
                
                echo json_encode(array('check' => 1));
                return $this->response;
            }
        }
        
        $this->_viewModel['caption'] = 'Sửa trạng thái vận đơn trên hệ thống';
        $this->_viewModel['params']  = $this->_params;
        $this->_viewModel['myForm']	 = $myForm;
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    //sửa trạng thái giao vận
    public function editTypeAction(){
        $ssFilter = new Container(__CLASS__);
        $this->_viewModel = array();
        $myForm	= new \Admin\Form\Transport\Edit($this);
        $check = 0;
        
        if ($this->getRequest()->isPost()){
            if ($this->_params['data']['task'] == 'view'){
                $dataForm['list_id'] = json_encode($this->_params['data']['list_id']);
                $myForm->setData($dataForm);
            }
            if ($this->_params['data']['task'] == 'submit'){
                $list_id = json_decode($this->_params['data']['list_id'], true);
                
                // gán trạng thái
                if (in_array($this->_params['data']['transport_status'], $this->_list_status)){
                    $status = $this->_params['data']['transport_status'];
                } elseif (!in_array($this->_params['data']['transport_status'], $this->_list_status) && !empty($this->_params['data']['transport_status_text'])) {
                    $status = $this->_params['data']['transport_status_text'];
                } else {
                    $status = 100;
                }
                
                if(is_array($list_id) && count($list_id)){
                    $list_transport = $this->getTable()->listItem($list_id, array('task' => 'list-by-id'));
                    if (is_array($list_transport) && count($list_transport)){
                        foreach ($list_transport as $key => $val){
                            $val['transport_status'] = $status;
                            $check = $this->getTable()->saveItem($val, array('task' => 'update-status-local'));
                        }
                    }
                }
                
                echo json_encode(array('check' => 1));
                return $this->response;
            }
        }
        
        $this->_viewModel['caption'] = 'Sửa trạng thái vận đơn trên hệ thống';
        $this->_viewModel['params']  = $this->_params;
        $this->_viewModel['myForm']	 = $myForm;
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    //lấy dữ liệu cần qua ajax
    public function getDataAction(){
        $dataRes = array(
            'error' => false,
            'data' => array()
        );
        //load giao diện nếu request phù hợp
        if ($this->getRequest()->isPost()){
            $arrData = $this->_params['data'];
            
            // lấy danh sách nhà vận chuyển
            if ($arrData['type'] == 'get-transport-service'){
                $dataRes['data']['get'] = \ZendX\Functions\CreateArray::create($this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-service')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
                
                //format data to select2
                if (is_array($dataRes['data']['get']) && count($dataRes['data']['get'])){
                    $tmp = '';
                    $tmp .= '<option value="">- Chọn đơn vị vận chuyển -</option>';
                    
                    foreach ($dataRes['data']['get'] as $key => $val){
                        $tmp .= '<option value="'. $key .'">'. $val .'</option>';
                    }
                    
                    $dataRes['data']['html'] = $tmp;
                    unset($tmp);
                }
            }
        } else {
            $dataRes['error'] = true;
        }
        echo json_encode($dataRes);
        return $this->response;
    }
    
    public function sendDataAction(){
        //load giao diện nếu request phù hợp
        if ($this->getRequest()->isPost()){
            
        }
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}
















