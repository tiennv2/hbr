<?php

namespace Admin\Controller;

use ZendX\Controller\ActionController;
use ZendX\System\UserInfo;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;

class UserController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Admin\Model\UserTable';
        $this->_options['formName'] = 'formAdminUser';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']          = !empty($ssFilter->order_by) ? $ssFilter->order_by : 'login_time';
        $this->_params['ssFilter']['order']             = !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']     = $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_user_group'] = $ssFilter->filter_user_group;
        $this->_params['ssFilter']['filter_keyword']    = $ssFilter->filter_keyword;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : $this->_paginator['itemCountPerPage'];
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        
        if($this->getRequest()->isPost()) {
            $ssFilter	= new Container(__CLASS__);
            $data = $this->_params['data'];
            
            $ssFilter->pagination_option    = intval($data['pagination_option']);
            
            $ssFilter->order_by             = $data['order_by'];
            $ssFilter->order                = $data['order'];
            
            $ssFilter->filter_status        = $data['filter_status'];
            $ssFilter->filter_user_group    = $data['filter_user_group'];
            $ssFilter->filter_keyword       = $data['filter_keyword'];
        }
        
        $this->goRoute();
    }
    
    public function indexAction() {
        $myForm	= new \Admin\Form\Search\User($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $items      = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));

        $this->_viewModel['myForm']	            = $myForm;
        $this->_viewModel['items']              = $items;
        $this->_viewModel['count']              = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']               = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['user_group']         = $this->getServiceLocator()->get('Admin\Model\UserGroupTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']            = 'Người dùng - Danh sách';
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $myForm			= $this->getForm();
    
        $task = 'add-item';
        $caption = 'Người dùng - Thêm mới';
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            $item['user_group_id'] = explode(',', $item['user_group_id']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Admin\Filter\User(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Người dùng - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
            
            $controlAction = $this->_params['data']['control-action'];
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function statusAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->getTable()->changeStatus($this->_params, array('task' => 'change-status'));
        } else {
            $this->goRoute();
        }
        
        return $this->response;
    }
    
    public function deleteAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid'])) {
                $result = $this->getTable()->deleteItem($this->_params, array('task' => 'delete-item'));
                $message = 'Xóa '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
        
        $this->goRoute();
    }

    public function orderingAction() {
        if($this->getRequest()->isPost()) {
            if(!empty($this->_params['data']['cid']) && !empty($this->_params['data']['ordering'])) {
                $result = $this->getTable()->changeOrdering($this->_params, array('task' => 'change-ordering'));
                $message = 'Sắp xếp '. $result .' phần tử thành công';
                $this->flashMessenger()->addMessage($message);
            }
        }
    
        $this->goRoute();
    }
    
    public function changePasswordAction() {
        $userInfo = new UserInfo();
        $userInfo = $userInfo->getUserInfo();
        
        if($this->getRequest()->isPost()){
            $post   = $this->_params['data'];
            $errors = array();
            $flag   = true;
            
            if(md5($post['password_current']) != $userInfo['password']) {
                $errors[] = '<b>Mật khẩu cũ:</b> không chính xác';
                $flag = false;
            }
            
            $validator = new \Zend\Validator\Regex('/^(?=.*[a-zA-Z])(?=.*\d)[\S\W\D]{8,}$/');
            if (!$validator->isValid($post['password_new'])) {
                $errors[] = '<b>Mật khẩu mới:</b> phải lớn hơn 8 ký tự và bao gồm cả chữ và số';
                $flag = false;
            }
            
            if(md5($post['password_new']) != md5($post['password_confirm'])) {
                $errors[] = '<b>Xác nhận mật khẩu mới:</b> không đúng';
                $flag = false;
            }
            
            if($flag == true){
                $this->flashMessenger()->addMessage('Mật khẩu của bạn đã được đổi thành công. Vui lòng đăng nhập lại bằng mật khẩu mới');
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'change-password'));
    
                $this->goRoute(array('action' => 'logout'));
            }
        }
    
        $this->_viewModel['errors']      = $errors;
        $this->_viewModel['caption']    = 'Đổi mật khẩu';
        return new ViewModel($this->_viewModel);
    }
    
    public function updatePasswordAction() {
        $this->setLayout('empty');
        
        $userInfo = new UserInfo();
        $userInfo = $userInfo->getUserInfo();
        
        if($userInfo['password_status'] != 1) {
            $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'index', 'action' => 'index'));
        }
        
        if($this->getRequest()->isPost()){
            $post   = $this->_params['data'];
            $errors = array();
            $flag   = true;
            
            $validator = new \Zend\Validator\Regex('/^(?=.*[a-zA-Z])(?=.*\d)[\S\W\D]{8,}$/');
            if (!$validator->isValid($post['password_new'])) {
                $errors[] = '<b>Mật khẩu mới:</b> phải lớn hơn 8 ký tự và bao gồm cả chữ và số';
                $flag = false;
            }
            
            if(md5($post['password_new']) != md5($post['password_confirm'])) {
                $errors[] = '<b>Xác nhận mật khẩu mới:</b> không đúng';
                $flag = false;
            }
            
            if($flag == true){
                $this->flashMessenger()->addMessage('Mật khẩu của bạn đã được đổi thành công. Vui lòng đăng nhập lại bằng mật khẩu mới');
                $result = $this->getTable()->saveItem($this->_params, array('task' => 'update-password'));
    
                $this->goRoute(array('action' => 'logout'));
            }
        }
    
        $this->_viewModel['errors']      = $errors;
        $this->_viewModel['caption']    = 'Yêu cầu đổi mật khẩu mới';
        return new ViewModel($this->_viewModel);
    }
    
    public function loginAction() {
        $this->setLayout('login');
        
        if($this->identity()) {
            $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'index', 'action' => 'index'));
        }
    
        $this->_options['formName'] = 'formAdminLogin';
        $myForm = $this->getForm();
    
        $authService = $this->getServiceLocator()->get('MyAuth');
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
    
                $paramLogin = array(
                    'username' => $this->_params['data']['username'],
                    'password' => $this->_params['data']['password'],
                );
                if($authService->login($paramLogin) == true) {
                    $userId = $this->identity()->id;
                    $groupId = $this->identity()->user_group_id;
    
                    $userTable = $this->getServiceLocator()->get('Admin\Model\UserTable');
                    $groupTable = $this->getServiceLocator()->get('Admin\Model\UserGroupTable');
                    $permissionTable = $this->getServiceLocator()->get('Admin\Model\UserPermissionTable');
                    
                    $data['user'] = $userTable->getItem(array('id' => $userId));
                    $data['group'] = $groupTable->listItem(array('ids' => $groupId), array('task' => 'multi-id'));
                    
                    $data['permission']['role'] = 'role_'. $data['user']['id'];
                    $data['permission']['privileges'] = $permissionTable->listItem($data['group'], array('task' => 'list-privileges'));

                    $userInfo = new UserInfo();
                    $userInfo->storeInfo($data);
                    
                    // Cập nhật thông tin đăng nhập
                    $userTable->saveItem(array('data' => array('id' => $userId)), array('task' => 'update-login'));
    
                    if($data['user']['password_status'] == 1) {
                        $this->redirect()->toUrl($this->_params['moduleRoute'] .'/user/update-password');
                    } else {
                        $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'index', 'action' => 'index'));
                    }
                }
            }
        }
    
        $this->_viewModel['myForm'] = $myForm;
        $this->_viewModel['msgError'] = $authService->getError();
        return new ViewModel($this->_viewModel);
    }
    
    public function logoutAction() {
        $authService = $this->getServiceLocator()->get('MyAuth')->logout();
    
        $session = new Container();
        $session->getManager()->getStorage()->clear();
    
        return $this->redirect()->toRoute('routeAdmin/default', array('controller' => 'user', 'action' => 'login'));
    }
}
