<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Bill extends InputFilter {
	
	public function __construct($options = null){
	    $dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	    $optionId      = $options['id'];
	    $optionData    = $options['data'];
	    $optionRoute   = $options['route'];
	    
		// Phone
	    /* if(!empty($optionId)) {
	        $excludePhone = "id != '". $optionId ."' AND phone = '". $optionData['phone'] ."' AND store IS NULL";
	    } else {
	        $excludePhone = "phone = '". $optionData['phone'] ."' AND store IS NULL";
	    } */
	    if(empty($options['id'])) {
    		$this->add(array(
    			'name'		=> 'phone',
    			'required'	=> true,
    			'validators'	=> array(
    				array(
    					'name'		=> 'NotEmpty',
    				    'options'	=> array(
    				        'messages'	=> array(
    				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    				        )
    				    ),
    					'break_chain_on_failure'	=> true
    				),
    			    array(
    			        'name'		=> 'Regex',
    			        'options'	=> array(
    			            'pattern'   => '/^([0]{1})([0-9]{9,10})+$/',
    			            'messages'	=> array(
    			                \Zend\Validator\Regex::NOT_MATCH => 'Không đúng định dạng số điện thoại'
    			            )
    			        ),
    			        'break_chain_on_failure'	=> true
    			    ),
    			    array(
    			        'name'		=> 'DbNoRecordExists',
    			        'options'	=> array(
    			            'table'   => TABLE_CONTACT,
    			            'field'   => 'phone',
    			            'adapter' => $dbAdapter,
    			            'exclude' => null,
    			            'messages'	=> array(
    			                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Đã có người quản lý'
    			            )
    			        ),
    			        'break_chain_on_failure'	=> true
    			    )
    			)
    		));
	    }
		
		// Name
		$this->add(array(
			'name'		=> 'name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
		
		// Sex
		$this->add(array(
		    'name'		=> 'sex',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		// Source Group
		$this->add(array(
		    'name'		=> 'source_group_id',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		// Source Channel
		$this->add(array(
		    'name'		=> 'source_channel_id',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		// Student School
		$this->add(array(
		    'name'		=> 'student_school_id',
		    'required'	=> false
		));

		// Student Major
		$this->add(array(
		    'name'		=> 'student_major_id',
		    'required'	=> false
		));

		// Student Class
		$this->add(array(
		    'name'		=> 'student_class_id',
		    'required'	=> false
		));
		
		// Email
		$this->add(array(
		    'name'		=> 'email',
		    'required'	=> false,
		    'validators'	=> array(
		        array(
		            'name'		=> 'EmailAddress',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\EmailAddress::INVALID_FORMAT => 'Không đúng định dạng email',
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		
		// Birthday Year
		$this->add(array(
		    'name'		=> 'birthday_year',
		    'required'	=> false
		));
		
		// Birthday Year
		$this->add(array(
		    'name'		=> 'location_city_id',
		    'required'	=> false
		));
		
		// History Time Return
		$this->add(array(
		    'name'		=> 'history_time_return',
		    'required'	=> false,
		    'validators'	=> array(
		        array(
			        'name'		=> 'Regex',
			        'options'	=> array(
			            'pattern'   => '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})+$/',
			            'messages'	=> array(
			                \Zend\Validator\Regex::NOT_MATCH => 'Không đúng định dạng ngày tháng: dd/mm/yyyy'
			            )
			        ),
			        'break_chain_on_failure'	=> true
			    ),
		    )
		));
		
		// History Action
		if(($optionRoute['code'] == 'history') || !empty($optionData['history_action_id']) || !empty($optionData['history_purpose_id']) || !empty($optionData['history_result_id']) || !empty($optionData['history_time_return']) || !empty($optionData['history_content'])) {
    		$this->add(array(
    		    'name'		=> 'history_action_id',
    		    'required'	=> true,
    		    'validators'	=> array(
    		        array(
    		            'name'		=> 'NotEmpty',
    		            'options'	=> array(
    		                'messages'	=> array(
    		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    		                )
    		            ),
    		            'break_chain_on_failure'	=> true
    		        )
    		    )
    		));
    		
    		$this->add(array(
    		    'name'		=> 'history_purpose_id',
    		    'required'	=> true,
    		    'validators'	=> array(
    		        array(
    		            'name'		=> 'NotEmpty',
    		            'options'	=> array(
    		                'messages'	=> array(
    		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    		                )
    		            ),
    		            'break_chain_on_failure'	=> true
    		        )
    		    )
    		));
    		
    		$this->add(array(
    		    'name'		=> 'history_result_id',
    		    'required'	=> true,
    		    'validators'	=> array(
    		        array(
    		            'name'		=> 'NotEmpty',
    		            'options'	=> array(
    		                'messages'	=> array(
    		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    		                )
    		            ),
    		            'break_chain_on_failure'	=> true
    		        )
    		    )
    		));
    		
    		$this->add(array(
    		    'name'		=> 'history_content',
    		    'required'	=> true,
    		    'validators'	=> array(
    		        array(
    		            'name'		=> 'NotEmpty',
    		            'options'	=> array(
    		                'messages'	=> array(
    		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    		                )
    		            ),
    		            'break_chain_on_failure'	=> true
    		        )
    		    )
    		));
		} else {
		    $this->add(array(
		        'name'		=> 'history_action_id',
		        'required'	=> false
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_purpose_id',
		        'required'	=> false
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_result_id',
		        'required'	=> false
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_content',
		        'required'	=> false
		    ));
		}
	}
}