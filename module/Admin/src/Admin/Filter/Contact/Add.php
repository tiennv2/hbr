<?php
namespace Admin\Filter\Contact;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Add extends InputFilter {
	
	public function __construct($options = null){
	    $userInfo      = new \ZendX\System\UserInfo();
	    
	    $dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	    $optionId      = $options['id'];
	    $optionData    = $options['data'];
		$optionRoute   = $options['route'];
	    
		// Phone
	    if(!empty($optionId)) {
	        $excludePhoneMessages = $optionData['phone'] .' đã tồn tại trên hệ thống';
	        $excludePhone = "id != '". $optionId ."' AND phone = '". $optionData['phone'] ."'";
	    } else {
	        $excludePhoneMessages = $optionData['phone'] .' đã tồn tại trên hệ thống';
	        $excludePhone = "phone = '". $optionData['phone'] ."'";
	    }
		$this->add(array(
			'name'		=> 'phone',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure' => true
				),
			    array(
			        'name'		=> 'Regex',
			        'options'	=> array(
			            'pattern'   => '/^([0-9]{10})+$/',
			            'messages'	=> array(
			                \Zend\Validator\Regex::NOT_MATCH => 'Không đúng định dạng số điện thoại'
			            )
			        ),
			        'break_chain_on_failure' => true
			    ),
			)
		));
		
	    
		// Name
		$this->add(array(
			'name'		=> 'name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
		
		// Name
		$this->add(array(
		    'name'		=> 'email',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		// Sex
		$this->add(array(
		    'name'		=> 'sex',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		// Tuổi
		$this->add(array(
		    'name'		=> 'birthday_year',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		//type c3
		if($optionData['type_c3'] == 'c3-cod'){
			$this->add(array(
				'name'		=> 'address',
				'required'	=> true,
				'validators'	=> array(
					array(
						'name'		=> 'NotEmpty',
						'options'	=> array(
							'messages'	=> array(
								\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
							)
						),
						'break_chain_on_failure'	=> true
					)
				)
			));
		}

		// Email
		$this->add(array(
		    'name'		=> 'email',
		    'required'	=> false,
		));

		//phone 2
		$this->add(array(
		    'name'		=> 'phone_2',
		    'required'	=> false,
		));

		//phone 3
		$this->add(array(
		    'name'		=> 'phone_3',
		    'required'	=> false,
        ));
		// Nghề nghiệp
		$this->add(array(
		    'name'		=> 'job',
		    'required'	=> false,
		));

		// Birthday Year
		$this->add(array(
		    'name'		=> 'birthday_year',
		    'required'	=> false
		));
		
		// Tỉnh thành
		$this->add(array(
		    'name'		=> 'location_city_id',
		    'required'	=> false
		));
		
		// Quận huyện
		$this->add(array(
		    'name'		=> 'location_district_id',
		    'required'	=> false
		));

		// Tỉnh thành 2
		$this->add(array(
		    'name'		=> 'location_city_id_2',
		    'required'	=> false
		));
		
		// Quận huyện 2
		$this->add(array(
		    'name'		=> 'location_district_id_2',
		    'required'	=> false
		));

		// Tỉnh thành
		$this->add(array(
		    'name'		=> 'location_city_id_3',
		    'required'	=> false
		));
		
		// Quận huyện
		$this->add(array(
		    'name'		=> 'location_district_id_3',
		    'required'	=> false
		));
		
		// Sản phẩm quan tâm
		$this->add(array(
		    'name'		=> 'product_id',
		    'required'	=> false
		));

		$this->add(array(
		    'name'		=> 'delivery-status',
		    'required'	=> false
		));

	}
}

