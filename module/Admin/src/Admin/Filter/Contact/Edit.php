<?php
namespace Admin\Filter\Contact;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Edit extends InputFilter {
	
	public function __construct($options = null){
	    
	$dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	$optionId      = $options['id'];
	$optionData    = $options['data'];
	$optionRoute   = $options['route'];
	// Họ và tên
	$this->add(array(
		'name'		=> 'name',
		'required'	=> true,
		'validators'	=> array(
			array(
				'name'		=> 'NotEmpty',
			    'options'	=> array(
			        'messages'	=> array(
			            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
			        )
			    ),
				'break_chain_on_failure'	=> true
			)
		)
	));

	// giới tính
	$this->add(array(
	    'name'		=> 'sex',
	    'required'	=> true,
	    'validators'	=> array(
	        array(
	            'name'		=> 'NotEmpty',
	            'options'	=> array(
	                'messages'	=> array(
	                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
	                )
	            ),
	            'break_chain_on_failure'	=> true
	        )
	    )
	));

	// Birthday Year
	$this->add(array(
	    'name'		=> 'birthday_year',
	    'required'	=> false,
	));
	
	// Birthday Year
	$this->add(array(
	    'name'		=> 'birthday',
	    'required'	=> false,
	));

	//Số điện thoại
	$this->add(array(
		'name'		=> 'phone',
		'required'	=> true,
		'validators'	=> array(
			array(
				'name'		=> 'NotEmpty',
			    'options'	=> array(
			        'messages'	=> array(
			            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
			        )
			    ),
				'break_chain_on_failure' => true
			),
		    array(
		        'name'		=> 'Regex',
		        'options'	=> array(
		            'pattern'   => '/^([0-9]{10})+$/',
		            'messages'	=> array(
		                \Zend\Validator\Regex::NOT_MATCH => 'Không đúng định dạng số điện thoại'
		            )
		        ),
		        'break_chain_on_failure' => true
		    ),
		)
	));

	//số điện thoại 2	
	$this->add(array(
	    'name'		=> 'phone_2',
	    'required'	=> false,
    ));
	
	//số điện thoại 3
	$this->add(array(
	    'name'		=> 'phone_3',
	    'required'	=> false,
    ));

	// email
	$this->add(array(
	    'name'		=> 'email',
	    'required'	=> false
	));

	// email 2
	$this->add(array(
	    'name'		=> 'email_2',
	    'required'	=> false
	));

	// địa chỉ
	$this->add(array(
	    'name'		=> 'address',
	    'required'	=> true,
	    'validators'	=> array(
	        array(
	            'name'		=> 'NotEmpty',
	            'options'	=> array(
	                'messages'	=> array(
	                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
	                )
	            ),
	            'break_chain_on_failure'	=> true
	        )
	    )
	));

	// Tỉnh thành
	$this->add(array(
	    'name'		=> 'location_city_id',
	    'required'	=> false
	));
	
	// Quận huyện
	$this->add(array(
	    'name'		=> 'location_district_id',
	    'required'	=> false
	));

	// địa chỉ 2
	$this->add(array(
	    'name'		=> 'address_2',
	    'required'	=> false
	));

	// Tỉnh thành 2
	$this->add(array(
	    'name'		=> 'location_city_id_2',
	    'required'	=> false
	));
	
	// Quận huyện 2
	$this->add(array(
	    'name'		=> 'location_district_id_2',
	    'required'	=> false
	));

	// địa chỉ 3
	$this->add(array(
	    'name'		=> 'address_3',
	    'required'	=> false
	));

	// Tỉnh thành 3
	$this->add(array(
	    'name'		=> 'location_city_id_3',
	    'required'	=> false
	));
	
	// Quận huyện 3
	$this->add(array(
	    'name'		=> 'location_district_id_3',
	    'required'	=> false
	));
	
	// ghi chú
	$this->add(array(
	    'name'		=> 'note',
	    'required'	=> false
	));
    
	// nhóm bệnh
	$this->add(array(
	    'name'		=> 'disease_group',
	    'required'	=> false
	));

	// thời gian bệnh
	$this->add(array(
	    'name'		=> 'disease_time',
	    'required'	=> false
	));

	// thời gian bệnh 
	$this->add(array(
	    'name'		=> 'symptom_sick',
	    'required'	=> false
	));

	// tiền sử bệnh
	$this->add(array(
	    'name'		=> 'prehistoric_sick',
	    'required'	=> false
	));

	// bệnh kèm theo
	$this->add(array(
	    'name'		=> 'sick_attach',
	    'required'	=> false
	));

	// đối tượng sử dụng
	$this->add(array(
	    'name'		=> 'objects_used',
	    'required'	=> false
	));

	// liều dùng tư vấn
	$this->add(array(
	    'name'		=> 'dosage',
	    'required'	=> false
	));

	// tên người nhận hàng
	$this->add(array(
	    'name'		=> 'contract_name',
	    'required'	=> false
	));
	if(!empty($optionData['contract_product']['product_id'][0])){
		//Địa chỉ nhận hàng
		$this->add(array(
		    'name'		=> 'contract_address',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		//Số điện thoại người nhận
		$this->add(array(
		    'name'		=> 'contract_phone',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));

		//Số điện thoại người nhận
		$this->add(array(
		    'name'		=> 'payment',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
	}else{
		// List địa chỉ
		$this->add(array(
		    'name'		=> 'contract_address',
		    'required'	=> false
		));

		// List số điện thoại
		$this->add(array(
		    'name'		=> 'contract_phone',
		    'required'	=> false
		));

		// phương thức thanh toán
		$this->add(array(
		    'name'		=> 'payment',
		    'required'	=> false
		));
	}
	

	// Sản phẩm quan tâm
	$this->add(array(
	    'name'		=> 'product_id',
	    'required'	=> false
	));

	// số hộp
	$this->add(array(
	    'name'		=> 'product_number',
	    'required'	=> false
	));

	// trạng thái gọi
	$this->add(array(
	    'name'		=> 'call_status',
	    'required'	=> false
	));

	//Trạng thái chăm sóc
	if(!empty($optionData['history_status_id'])) {
		$this->add(array(
			'name'		=> 'history_status_id',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				)
			)
		));

		// Trạng thái gọi
		$this->add(array(
			'name'		=> 'call_status',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				)
			)
		));

		}else{
			$this->add(array(
				'name'		=> 'history_status_id',
				'required'	=> false,
			));
			
			// Ngày hẹn gọi lại
			$this->add(array(
				'name'		=> 'history_return',
				'required'	=> false,
			));

			// Level chăm sóc
			$this->add(array(
				'name'		=> 'level',
				'required'	=> false,
			));

			// Nhóm bệnh
			$this->add(array(
				'name'		=> 'sick_group',
				'required'	=> false,
			));

			// Thời gian bệnh
			$this->add(array(
				'name'		=> 'sick_time_id',
				'required'	=> false,
			));
		}	
	}
}