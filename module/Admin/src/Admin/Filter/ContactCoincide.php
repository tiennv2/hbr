<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class ContactCoincide extends InputFilter {
	
	public function __construct($options = null){
	    $userInfo      = new \ZendX\System\UserInfo();
	    
	    $dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	    $optionId      = $options['id'];
	    $optionData    = $options['data'];
	    $optionRoute   = $options['route'];
	    
	    
		// Name
		$this->add(array(
			'name'		=> 'name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
		
		// Nếu
		if($optionData['type'] != 'lost') {
		    $this->add(array(
		        'name'		=> 'lost_id',
		        'required'	=> false,
		    ));
		} else {
		    $this->add(array(
		        'name'		=> 'lost_id',
		        'required'	=> true,
		        'validators'	=> array(
		            array(
		                'name'		=> 'NotEmpty',
		                'options'	=> array(
		                    'messages'	=> array(
		                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                    )
		                ),
		                'break_chain_on_failure'	=> true
		            )
		        )
		    ));
		}

		// Check nhập lịch sử chăm sóc
		if(!empty($optionData['history_action_id']) || !empty($optionData['history_result_id']) || !empty($optionData['history_return']) || !empty($optionData['history_content'])) {
		    $this->add(array(
		        'name'		=> 'history_action_id',
		        'required'	=> true,
		        'validators'	=> array(
		            array(
		                'name'		=> 'NotEmpty',
		                'options'	=> array(
		                    'messages'	=> array(
		                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                    )
		                ),
		                'break_chain_on_failure'	=> true
		            )
		        )
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_result_id',
		        'required'	=> true,
		        'validators'	=> array(
		            array(
		                'name'		=> 'NotEmpty',
		                'options'	=> array(
		                    'messages'	=> array(
		                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                    )
		                ),
		                'break_chain_on_failure'	=> true
		            )
		        )
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_content',
		        'required'	=> true,
		        'validators'	=> array(
		            array(
		                'name'		=> 'NotEmpty',
		                'options'	=> array(
		                    'messages'	=> array(
		                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                    )
		                ),
		                'break_chain_on_failure'	=> true
		            )
		        )
		    ));
		    
		    if($optionData['type'] != 'lost') {
    		    $this->add(array(
    		        'name'		=> 'history_return',
    		        'required'	=> true,
    		        'validators'	=> array(
    		            array(
    		                'name'		=> 'NotEmpty',
    		                'options'	=> array(
    		                    'messages'	=> array(
    		                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    		                    )
    		                ),
    		                'break_chain_on_failure'	=> true
    		            )
    		        )
    		    ));
		    }
		} else {
		    $this->add(array(
		        'name'		=> 'history_action_id',
		        'required'	=> false,
		    ));
		    
		    $this->add(array(
		        'name'		=> 'history_result_id',
		        'required'	=> false,
		    ));
		}
	}
}