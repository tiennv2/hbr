<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class ContactCourse extends InputFilter {
	
	public function __construct($options = null){
	    $exclude = "course_id = '". $options['course_id'] ."' AND contact_id = '". $options['contact_id'] ."'";
	    if(!empty($options['id'])) {
	        $exclude = "id != '". $options['id'] ."' AND course_id = '". $options['course_id'] ."' AND contact_id = '". $options['contact_id'] ."'";
	    }
	   
		// Khóa học
		$this->add(array(
			'name'		=> 'course_id',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
			    array(
			        'name'		=> 'DbNoRecordExists',
			        'options'	=> array(
			            'table'   => TABLE_CONTACT_COURSE,
			            'field'   => 'course_id',
			            'adapter' => GlobalAdapterFeature::getStaticAdapter(),
			            'exclude' => $exclude,
			            'messages'	=> array(
			                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Khách hàng đã đăng ký mua khóa học'
			            )
			        ),
			        'break_chain_on_failure'	=> true
			    )
			)
		));
	    
		// Phương thức thanh toán
		$this->add(array(
			'name'		=> 'payment_id',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
	}
}