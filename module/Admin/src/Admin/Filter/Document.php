<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;

class Document extends InputFilter {
	
	public function __construct($options = null){
	    $configs = $options['configs'];
	    
	    $exclude = null;
	    if(!empty($options['id'])) {
	        $exclude = array(
	            'field' => 'id',
	            'value' => $options['id']
	        );
	    }
	    
	    foreach ($configs['form']['fields'] AS $field) {
	        $required = ($field['validators']['require'] == 1) ? true : false;
	        $validators = array();
	        
	        if($required == true) {
	            $validators[] = array(
	                'name'		=> 'NotEmpty',
	                'options'	=> array(
	                    'messages'	=> array(
	                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
	                    )
	                ),
	                'break_chain_on_failure'	=> true
	            );
	        }
	        
    	    $this->add(
    	        array(
        	        'name'		   => $field['name'],
        	        'required'	   => $required,
        	        'validators'   => $validators
        	    )
	        );
    	    
    	    /* array(
    	        array(
    	            'name'		=> 'NotEmpty',
    	            'options'	=> array(
    	                'messages'	=> array(
    	                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
    	                )
    	            ),
    	            'break_chain_on_failure'	=> true
    	        ),
    	        array(
    	         'name'		=> 'DbNoRecordExists',
    	            'options'	=> array(
    	                'table'   => TABLE_LOCATION_COUNTRY,
    	                'field'   => 'code',
    	                'adapter' => GlobalAdapterFeature::getStaticAdapter(),
    	                'exclude' => $exclude,
    	                'messages'	=> array(
    	                    \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Đã tồn tại'
    	                )
    	            ),
    	            'break_chain_on_failure'	=> true
    	        )
    	    ) */
	    }
	}
}