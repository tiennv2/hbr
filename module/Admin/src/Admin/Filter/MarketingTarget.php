<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class MarketingTarget extends InputFilter {
	
	public function __construct($options = null){
	    $exclude = null;
	    // $requirePassword = true;
	    if(!empty($options['id'])) {
	        $exclude = array(
		        'field' => 'id',
		        'value' => $options['id']
		    );
	        // $requirePassword = false;
	    }
		
		// me
	    $this->add(array(
			'name'		=> 'name', 
	        'required'	=> true,
	        'validators'	=> array(
	            array(
	                'name'		=> 'NotEmpty',
	                'options'	=> array(
	                    'messages'	=> array(
	                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Tên chiến dịch không được để trống'
	                    )
	                ),
	                'break_chain_on_failure'	=> true
				),
	        )
		));
		
		// month
	 //    $this->add(array(
	 //        'name'		=> 'month',
	 //        'required'	=> true,
	 //        'validators'	=> array(
	 //            array(
	 //                'name'		=> 'NotEmpty',
	 //                'options'	=> array(
	 //                    'messages'	=> array(
	 //                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Phải chọn tháng không được để trống'
	 //                    )
	 //                ),
	 //                'break_chain_on_failure'	=> true
	 //            ),
	 //        )
		// ));
		
		// year
	    // $this->add(array(
	    //     'name'		=> 'year',
	    //     'required'	=> true,
	    //     'validators'	=> array(
	    //         array(
	    //             'name'		=> 'NotEmpty',
	    //             'options'	=> array(
	    //                 'messages'	=> array(
	    //                     \Zend\Validator\NotEmpty::IS_EMPTY => 'Phải chọn năm không được để trống'
	    //                 )
	    //             ),
	    //             'break_chain_on_failure'	=> true
	    //         ),
	    //     )
	    // ));
	}
}