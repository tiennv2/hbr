<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
// use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Transport extends InputFilter {
    
    public function __construct($options = null){
        $exclude = null;
        if(!empty($options['id'])) {
            $exclude = array(
                'field' => 'id',
                'value' => $options['id']
            );
        }
        
        // name
        /*$this->add(array(
            'name'      => 'name',
            'required'  => true,
            'validators'    => array(
                array(
                    'name'      => 'NotEmpty',
                    'options'   => array(
                        'messages'  => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
                        )
                    ),
                    'break_chain_on_failure'    => true
                ),
            )
        ));*/

        // mã đơn hàng trên hệ thống
        $this->add(array(
            'name'      => 'contract_code',
            'required'  => true,
            'validators'    => array(
                array(
                    'name'      => 'NotEmpty',
                    'options'   => array(
                        'messages'  => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
                        )
                    ),
                    'break_chain_on_failure'    => true
                ),
            )
        ));

        // đơn vị vận chuyển
        $this->add(array(
            'name'      => 'transport_service',
            'required'  => true,
            'validators'    => array(
                array(
                    'name'      => 'NotEmpty',
                    'options'   => array(
                        'messages'  => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
                        )
                    ),
                    'break_chain_on_failure'    => true
                ),
            )
        ));
    }
}