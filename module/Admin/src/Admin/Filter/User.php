<?php
namespace Admin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class User extends InputFilter {
	
	public function __construct($options = null){
	    $exclude = null;
	    $requirePassword = true;
	    if(!empty($options['id'])) {
	        $exclude = array(
		        'field' => 'id',
		        'value' => $options['id']
		    );
	        $requirePassword = false;
	    }
	    
	    // Fullname
	    $this->add(array(
	        'name'		=> 'fullname',
	        'required'	=> true,
	        'validators'	=> array(
	            array(
	                'name'		=> 'NotEmpty',
	                'options'	=> array(
	                    'messages'	=> array(
	                        \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
	                    )
	                ),
	                'break_chain_on_failure'	=> true
	            ),
	        )
	    ));
	    
		// Username
		$this->add(array(
			'name'		=> 'username',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
				array(
					'name'		=> 'DbNoRecordExists',
					'options'	=> array(
						'table'   => TABLE_USER,
						'field'   => 'username',
						'adapter' => GlobalAdapterFeature::getStaticAdapter(),
					    'exclude' => $exclude,
					    'messages'	=> array(
					        \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Đã tồn tại'
					    )
					),
					'break_chain_on_failure'	=> true
				)
			)
		));
		
		// Email
		$this->add(array(
		    'name'		=> 'email',
		    'required'	=> false,
		    'validators'	=> array(
		        array(
		            'name'		=> 'EmailAddress',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\EmailAddress::INVALID_FORMAT      => 'Không đúng định dạng email',
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        ),
		        array(
					'name'		=> 'DbNoRecordExists',
					'options'	=> array(
						'table'   => TABLE_USER,
						'field'   => 'email',
						'adapter' => GlobalAdapterFeature::getStaticAdapter(),
					    'exclude' => $exclude,
					    'messages'	=> array(
					        \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Đã tồn tại'
					    )
					),
					'break_chain_on_failure'	=> true
				)
		    )
		));
		
		// Phone
		$this->add(array(
		    'name'		=> 'phone',
		    'required'	=> false,
		    'validators'	=> array(
		        array(
		            'name'		=> 'DbNoRecordExists',
		            'options'	=> array(
		                'table'   => TABLE_USER,
		                'field'   => 'phone',
		                'adapter' => GlobalAdapterFeature::getStaticAdapter(),
		                'exclude' => $exclude,
		                'messages'	=> array(
		                    \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Đã tồn tại'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		
		// Password
		$this->add(array(
		    'name'		=> 'password',
		    'required'	=> $requirePassword,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
		
		// Group
		$this->add(array(
		    'name'		=> 'user_group_id',
		    'required'	=> true,
		    'validators'	=> array(
		        array(
		            'name'		=> 'NotEmpty',
		            'options'	=> array(
		                'messages'	=> array(
		                    \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống',
		                )
		            ),
		            'break_chain_on_failure'	=> true
		        )
		    )
		));
	}
}