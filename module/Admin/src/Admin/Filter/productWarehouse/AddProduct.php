<?php
namespace Admin\Filter\ProductWarehouse;

use Zend\InputFilter\InputFilter;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class AddProduct extends InputFilter {
	
	public function __construct($options = null){
	    $userInfo      = new \ZendX\System\UserInfo();
	    
	    $dbAdapter     = GlobalAdapterFeature::getStaticAdapter();
	    $optionId      = $options['id'];
	    $optionData    = $options['data'];
		$optionRoute   = $options['route'];
	    

		// Name
		$this->add(array(
			'name'		=> 'product_id',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
	}
}

