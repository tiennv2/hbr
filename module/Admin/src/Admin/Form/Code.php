<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class Code extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Khóa học
		$this->add(array(
		    'name'			=> 'course_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'list-all')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Phân loại mã kích hoạt
		$this->add(array(
		    'name'			=> 'document_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "code-type" ), "order" => array("ordering" => "ASC", "name" => "ASC"), "view"  => array( "key" => "id", "value" => "name", "sprintf" => "%s" ) ), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Số lượng
		$this->add(array(
			'name'			=> 'number',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_number',
			),
		));
		
		// Người yêu cầu
		$this->add(array(
			'name'			=> 'requester',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
			),
		));
		
		// Ngày yêu cầu
		$this->add(array(
			'name'			=> 'request_date',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control date-picker',
			    'value'     => date('d/m/Y')
			),
		));
	}
}