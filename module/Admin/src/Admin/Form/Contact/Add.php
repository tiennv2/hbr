<?php
namespace Admin\Form\Contact;
use \Zend\Form\Form as Form;

class Add extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Phone
		$this->add(array(
		    'name'			=> 'phone',
		    'type'			=> 'Text',
		    'attributes'	=> array(
				'class'		   => 'form-control mask_phone',
				'autocomplete' => 'off',
				'placeholder'	=> 'Số điện thoại',
		    ),
		));
		
		// Phone 2
		$this->add(array(
		    'name'			=> 'phone_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control mask_interger',
		        'placeholder'	=> 'Số điện thoại',
		    ),
		));
		
		// Phone 3
		$this->add(array(
		    'name'			=> 'phone_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control mask_interger',
		        'placeholder'	=> 'Số điện thoại',
		    ),
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder'	=> 'Họ và tên',
			),
		));
		
		// Sex
		$this->add(array(
		    'name'			=> 'sex',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
		    )
		));
		
		// Email
		$this->add(array(
		    'name'			=> 'email',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'placeholder'	=> 'Email',
		    ),
		));

		//Birthday
		$this->add(array(
		    'name'			=> 'birthday',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'Ngày sinh',
		        'autocomplete'  => 'off'
		    )
		));
		
		// Birthday Year
		$birthday_year = array();
		for ($i = date('Y') - 10; $i >= 1950; $i--) {
		    $birthday_year[$i] = $i;
		}
		$this->add(array(
		    'name'			=> 'birthday_year',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> $birthday_year,
		    )
		));
		
		// Tỉnh thành
		$this->add(array(
		    'name'			=> 'location_city_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
		    ),
		));
		
		// Quận huyện
		$this->add(array(
		    'name'			=> 'location_district_id',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));

		// phường xã
		$this->add(array(
		    'name'			=> 'location_town_id',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));

		// Tỉnh thành 2
		$this->add(array(
		    'name'			=> 'location_city_id_2',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
		    ),
		));
		
		// Quận huyện 2
		$this->add(array(
		    'name'			=> 'location_district_id_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));

		// Phường xã 2
		$this->add(array(
		    'name'			=> 'location_town_id_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));
		
		// Tỉnh thành 3
		$this->add(array(
		    'name'			=> 'location_city_id_3',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
		    ),
		));
		
		// Quận huyện 3
		$this->add(array(
		    'name'			=> 'location_district_id_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));
		
		// Phường xã
		$this->add(array(
		    'name'			=> 'location_town_id_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_LOCATIONS,
		        'data-id'             => 'code',
		        'data-text'           => 'name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'parent',
		        'data-parent-name'    => 'parent',
		    ),
		));

		// Address
		$this->add(array(
		    'name'			=> 'address',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Email 2
		$this->add(array(
		    'name'			=> 'email_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'placeholder'	=> 'Email',
		    ),
		));

		// Address 2
		$this->add(array(
		    'name'			=> 'address_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Address 3
		$this->add(array(
		    'name'			=> 'address_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Ghi chú
		$this->add(array(
		    'name'			=> 'note',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'placeholder'	=> 'Ghi chú',
		    )
		));
		
		// Nghề nghiệp
		$this->add(array(
		    'name'			=> 'job',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		        'placeholder'	 => 'Nghề nghiệp'
		    ),
		));
		
		// Sản phẩm quan tâm
		$this->add(array(
		    'name'			=> 'product_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\FormTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));

		// Tình trạng đơn hàng
		$this->add(array(
		    'name'			=> 'delivery-status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'delivery-status')), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
	}
}