<?php
namespace Admin\Form\Contact;
use \Zend\Form\Form as Form;

class ChangeUser extends Form {
	
	public function __construct($sm, $params = NULL){
		parent::__construct();
		
		$userInfo = new \ZendX\System\UserInfo();
		$userInfo = $userInfo->getUserInfo();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Người quản lý mới
		$user = $sm->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
		$sale_group = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
		$sale_branch = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
		$company_department = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'company-department')), array('task' => 'cache'));
		$user_data = array();
		
	    foreach ($user AS $key => $val) {
	        $data_val = array(
	            'id' => $val['id'],
	            'username' => $val['username'],
	            'name' => $val['name'],
	            'company_department_name' => $company_department[$val['company_department_id']]['name'],
	            'sale_group_name' => $sale_group[$val['sale_group_id']]['name'],
	            'sale_branch_name' => $sale_branch[$val['sale_branch_id']]['name'],
	        );
	        if(!empty($userInfo['sale_branch_id'])) {
	            if($val['sale_branch_id'] == $userInfo['sale_branch_id']) {
	                if(!empty($userInfo['sale_group_id'])) {
	                    if($val['sale_group_id'] == $userInfo['sale_group_id']) {
	                        $user_data[] = $data_val;
	                    }
	                } else {
	                    $user_data[] = $data_val;
	                }
	            }
	        } else {
	            $user_data[] = $data_val;
	        }
	    }
		$this->add(array(
		    'name'			=> 'user_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'     => 'form-control select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::createSelect($user_data, array('key' => 'id', 'value' => 'username, name, company_department_name, sale_group_name, sale_branch_name')),
		    ),
		));
		
		// Hidden để lưu danh sách id của liên hệ cần chuyển
		$this->add(array(
		    'name'			=> 'contact_ids',
		    'type'			=> 'Hidden',
		));
	}
}