<?php
namespace Admin\Form\Contact;
use \Zend\Form\Form as Form;

class Edit extends Form {
    
    public function __construct($sm, $options = null){
        parent::__construct();

        $optionContact = $options['contact'];

        // FORM Attribute
        $this->setAttributes(array(
            'action'    => '',
            'method'    => 'POST',
            'class'     => 'horizontal-form',
            'role'      => 'form',
            'name'      => 'adminForm',
            'id'        => 'adminForm',
        ));
        
        // Id
        $this->add(array(
            'name'          => 'id',
            'type'          => 'Hidden',
        ));
        
        
        // Modal
        $this->add(array(
            'name'          => 'modal',
            'type'          => 'Hidden',
            'attributes'    => array(
                'value'     => 'success',
            ),
        ));
        
        // Name
        $this->add(array(
            'name'          => 'name',
            'type'          => 'Text',
            'attributes'    => array(
                'class'     => 'form-control',
                'placeholder'   => 'Họ và tên',
            ),
        ));

        // giới tính
        $this->add(array(
            'name'          => 'sex',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
            )
        ));

        // ngày tháng năm sinh
        $birthday_year = array();
        for ($i = date('Y') - 10; $i >= 1950; $i--) {
            $birthday_year[$i] = $i;
        }
        $this->add(array(
            'name'          => 'birthday_year',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => $birthday_year,
            )
        ));

        //năm sinh
        $this->add(array(
            'name'          => 'birthday',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control date-picker',
                'placeholder'   => 'Ngày sinh',
                'autocomplete'  => 'off'
            )
        ));

        // Phone
        $this->add(array(
            'name'          => 'phone',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control mask_phone',
                'placeholder'   => 'Số điện thoại',
            ),
        ));

        // Phone 2
        $this->add(array(
            'name'          => 'phone_2',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control mask_interger',
                'placeholder'   => 'Số điện thoại',
            ),
        ));
        
        // Phone 3
        $this->add(array(
            'name'          => 'phone_3',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control mask_interger',
                'placeholder'   => 'Số điện thoại',
            ),
        ));

        // Email
        $this->add(array(
            'name'          => 'email',
            'type'          => 'Text',
            'attributes'    => array(
                'class'     => 'form-control',
                'placeholder'   => 'Email',
            ),
        ));

        // Email 2
        $this->add(array(
            'name'          => 'email_2',
            'type'          => 'Text',
            'attributes'    => array(
                'class'     => 'form-control',
                'placeholder'   => 'Email',
            ),
        ));

        // Address
        $this->add(array(
            'name'          => 'address',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'placeholder'   => 'Địa chỉ'
            )
        ));

        // Tỉnh thành
        $this->add(array(
            'name'          => 'location_city_id',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            ),
        ));

        // Quận huyện
        $this->add(array(
            'name'          => 'location_district_id',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => 'name',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));

        // phường xã
        $this->add(array(
            'name'          => 'location_town_id',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));

        // Address 2
        $this->add(array(
            'name'          => 'address_2',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'placeholder'   => 'Địa chỉ'
            )
        ));

        // Tỉnh thành 2
        $this->add(array(
            'name'          => 'location_city_id_2',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            ),
        ));
        
        // Quận huyện 2
        $this->add(array(
            'name'          => 'location_district_id_2',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));
        
        // Phường xã 3
        $this->add(array(
            'name'          => 'location_town_id_2',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));
        
        // Address 3
        $this->add(array(
            'name'          => 'address_3',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'placeholder'   => 'Địa chỉ'
            )
        ));

        // Tỉnh thành 3
        $this->add(array(
            'name'          => 'location_city_id_3',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            ),
        ));
        
        // Quận huyện 3
        $this->add(array(
            'name'          => 'location_district_id_3',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));

        // Phường xã 3
        $this->add(array(
            'name'          => 'location_town_id_3',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));
        
        // Ghi chú
        $this->add(array(
            'name'          => 'note',
            'type'          => 'Textarea',
            'attributes'    => array(
                'class'     => 'form-control',
                'placeholder'   => 'Ghi chú',
            )
        ));

        // Nhóm bệnh
        $this->add(array(
            'name'          => 'disease_group',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Nhóm bệnh -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "disease-group" )), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
            ),
        ));

        // Thời gian bệnh
        $this->add(array(
            'name'          => 'disease_time',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "disease-time" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // Triệu chứng bệnh
        $this->add(array(
            'name'          => 'symptom_sick',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control',
                'placeholder'   => 'Triệu chứng bệnh',
            ),
        ));

        // Tiền sử bệnh
        $this->add(array(
            'name'          => 'prehistoric_sick',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control',
                'placeholder'   => 'Tiền sử bệnh',
            ),
        ));

        // Bệnh kèm theo
        $this->add(array(
            'name'          => 'sick_attach',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control',
                'placeholder'   => 'Bệnh kèm theo',
            ),
        ));
        
        // Đối tượng sử dụng
        $this->add(array(
            'name'          => 'objects_used',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control',
                'placeholder'   => 'Đối tượng sử dụng',
            ),
        ));

        // Liều dùng tư vấn
        $this->add(array(
            'name'          => 'dosage',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Liều dùng -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "dosage" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // Đơn hàng - Tên người nhận
        $this->add(array(
            'name'          => 'contract_name',
            'type'          => 'Text',
            'attributes'    => array(
                'class'     => 'form-control',
                'value'     => $optionContact['name'],
            ),
        ));

        // Đơn hàng - Số điện thoại
        $arrPhone = array($optionContact['phone'] => $optionContact['phone']);
        if(!empty($optionContact['phone_2'])) $arrPhone[$optionContact['phone_2']] = $optionContact['phone_2'];
        if(!empty($optionContact['phone_3'])) $arrPhone[$optionContact['phone_3']] = $optionContact['phone_3'];
        $this->add(array(
            'name'          => 'contract_phone',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => $arrPhone
            ),
        ));

        // Đơn hàng - Địa chỉ
        $arrAddress = array($optionContact['address'] .'_'. $optionContact['location_district_id'] .'_'. $optionContact['location_city_id'] => $optionContact['address']);
        if(!empty($optionContact['address_2'])) $arrAddress[$optionContact['address_2'] .'_'. $optionContact['location_district_id_2'] .'_'. $optionContact['location_city_id_2']] = $optionContact['address_2'];
        if(!empty($optionContact['address_3'])) $arrAddress[$optionContact['address_3'] .'_'. $optionContact['location_district_id_3'] .'_'. $optionContact['location_city_id_3']] = $optionContact['address_3'];
        $this->add(array(
            'name'          => 'contract_address',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => $arrAddress
            ),
        ));

        //Danh sách sản phẩm
        $this->add(array(
            'name'          => 'product_id',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\FormTable')->listItem(array( "where" => array( "status" => 1 )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // Số sản phẩm
        $this->add(array(
            'name'          => 'product_number',
            'type'          => 'number',
            'attributes'    => array(
                'class'         => 'form-control',
                'placeholder'   => 'Số hộp',
            )
        ));

        // Phương thức thanh toán
        $this->add(array(
            'name'          => 'payment',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "payment" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // Modal
        $this->add(array(
            'name'          => 'user_id',
            'type'          => 'Hidden',
            'attributes'    => array(
                'value'     => 'success',
            ),
        ));

        // Người lên đơn hàng
        $this->add(array(
            'name'          => 'user_id',
            'type'          => 'Hidden',
            'attributes'    => array(
                'class'     => 'form-control',
                'placeholder'   => 'Họ và tên',
            ),
        ));

        // Ngày hẹn gọi lại
        $this->add(array(
            'name'          => 'history_return',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control date-picker',
                'placeholder'   => 'dd/mm/yyyy'
            )
        ));

        // Trạng thái chăm sóc
        $this->add(array(
            'name'          => 'history_status_id',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "history-status" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // Trạng thái gọi
        $this->add(array(
            'name'          => 'call_status',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "call-status" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
            ),
        ));

        // lý do từ chối
        $this->add(array(
            'name'          => 'reason_refusing',
            'type'          => 'Text',
            'attributes'    => array(
                'class'          => 'form-control',
                'placeholder'   => 'Lý do từ chối',
            ),
        ));

        // Level khách hàng
        $this->add(array(
            'name'          => 'level',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "contact-level" )), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
            ),
        ));

        // History Content - Nội dung/Ghi chú
        $this->add(array(
            'name'          => 'history_content',
            'type'          => 'Textarea',
            'attributes'    => array(
                'class'         => 'form-control',
                'placeholder'   => 'Ghi chú',
            )
        ));

    }
}