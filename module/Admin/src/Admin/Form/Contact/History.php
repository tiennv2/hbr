<?php
namespace Admin\Form\Contact;
use \Zend\Form\Form as Form;

class History extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'		=> 'success',
		    ),
		));
		
		// Trạng thái chăm sóc
		$this->add(array(
		    'name'			=> 'history_status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "history-status" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// History Action - Hành động
		$this->add(array(
		    'name'			=> 'history_action_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-action" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Result - Kết quả
		$this->add(array(
		    'name'			=> 'history_result_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-result" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Content - Nội dung/Ghi chú
		$this->add(array(
		    'name'			=> 'history_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    )
		));
		
		// History Time Return 
		$this->add(array(
		    'name'			=> 'history_return',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'dd/mm/yyyy'
		    )
		));
		
		// Contact level
		$this->add(array(
		    'name'			=> 'level',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "contact-level" )), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
		    )
		));
		
		// Nghề nghiệp
		$this->add(array(
		    'name'			=> 'job',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Trạng thái level chăm sóc
		$this->add(array(
		    'name'			=> 'level',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "contact-level" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Lý do từ chối
		$this->add(array(
		    'name'			=> 'reason_refuse',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Nhóm bệnh
		$this->add(array(
		    'name'			=> 'sick_group',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Thời gian bệnh
		$this->add(array(
		    'name'			=> 'sick_time',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "disease-time" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Thói quen sinh hoạt
		$this->add(array(
		    'name'			=> 'living_habits',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Bệnh kèm theo
		$this->add(array(
		    'name'			=> 'sick_attach',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Thông tin cuộc gọi
		$this->add(array(
		    'name'			=> 'call_info',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// History Action - Hành động
		$this->add(array(
		    'name'			=> 'history_action_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-action" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Result - Kết quả
		$this->add(array(
		    'name'			=> 'history_result_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-result" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Content - Nội dung/Ghi chú
		$this->add(array(
		    'name'			=> 'history_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    )
		));
		
		// Ngày hẹn gọi lại
		$this->add(array(
		    'name'			=> 'history_return',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'dd/mm/yyyy'
		    )
		));
		
		// Bệnh kèm theo
		$this->add(array(
		    'name'			=> 'sick_attach',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Liều dùng tư vấn
		$this->add(array(
		    'name'			=> 'dosage',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Triệu chứng bệnh
		$this->add(array(
		    'name'			=> 'symptom_sick',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Tiền sử bệnh
		$this->add(array(
		    'name'			=> 'prehistoric_sick',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Đối tượng sử dụng
		$this->add(array(
		    'name'			=> 'objects_used',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
	}
}