<?php
namespace Admin\Form\Contact;
use \Zend\Form\Form as Form;

class Import extends Form {
	
	public function __construct($sm, $params){
		parent::__construct();
		
		$userInfo = new \ZendX\System\UserInfo();
		$userInfo = $userInfo->getUserInfo();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
			'enctype'		=> 'multipart/form-data'
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// File import
		$this->add(array(
			'name'			=> 'file_import',
			'type'			=> 'File',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
		));
		
		// Nguồn khách hàng
		// $this->add(array(
		//     'name'			=> 'marketing_channel_id',
		//     'type'			=> 'Select',
		//     'attributes'	=> array(
		//         'class'		=> 'form-control select2 select2_basic',
		//     ),
		//     'options'		=> array(
		//         'empty_option'	=> '- Chọn -',
		//         'disable_inarray_validator' => true,
		//         'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'marketing-channel')), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		//     )
		// ));
		
		// Phân loại khách hàng
		// $this->add(array(
		//     'name'			=> 'type',
		//     'type'			=> 'Select',
		//     'attributes'	=> array(
		//         'class'		=> 'form-control select2 select2_basic',
		//     ),
		//     'options'		=> array(
		//         'empty_option'  => '- Chọn -',
		//         'disable_inarray_validator' => true,
		//         'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-contact-type" )), array('task' => 'cache-public')), array('key' => 'alias', 'value' => 'name')),
		//     )
		// ));
		
		// Người quản lý mới
		// $sale_group_id = $userInfo['sale_group_id'];
		// $sale_group_ids = !empty($userInfo['sale_group_ids']) ? explode(',', $userInfo['sale_group_ids']) : null;
		// $user = $sm->get('Admin\Model\UserTable')->listItem($params, array('task' => 'cache'));
		// $sale_group = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-group')), array('task' => 'cache'));
		// $sale_branch = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sale-branch')), array('task' => 'cache'));
		// $company_department = $sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'company-department')), array('task' => 'cache'));
		// $user_data = array();
		// foreach ($user AS $key => $val) {
		//     $data_val = array(
		//         'id' => $val['id'],
		//         'username' => $val['username'],
		//         'name' => $val['name'],
		//         'company_department_name' => $company_department[$val['company_department_id']]['name'],
		//         'sale_group_name' => $sale_group[$val['sale_group_id']]['name'],
		//         'sale_branch_name' => $sale_branch[$val['sale_branch_id']]['name'],
		//     );
		//     if(!empty($userInfo['sale_branch_id'])) {
		//         if($val['sale_branch_id'] == $userInfo['sale_branch_id']) {
		//             if(!empty($userInfo['sale_group_id'])) {
		//                 if($val['sale_group_id'] == $userInfo['sale_group_id']) {
		//                     $user_data[] = $data_val;
		//                 }
		//             } else {
		//                 $user_data[] = $data_val;
		//             }
		//         }
		//     } else {
		//         $user_data[] = $data_val;
		//     }
		// }
		// $this->add(array(
		//     'name'			=> 'user_id',
		//     'type'			=> 'Select',
		//     'attributes'	=> array(
		//         'class'     => 'form-control select2_basic',
		//     ),
		//     'options'		=> array(
		//         'empty_option'	=> '- Chọn -',
		//         'disable_inarray_validator' => true,
		//         'value_options'	=> \ZendX\Functions\CreateArray::createSelect($user_data, array('key' => 'id', 'value' => 'username, name, company_department_name, sale_group_name, sale_branch_name')),
		//     ),
		// ));
		
		// Submit
		$this->add(array(
		    'name'			=> 'submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Bắt đầu import',
		        'class'		=> 'btn btn-sm green',
		        'style'     => 'border: 1px solid #35aa47;'
		    ),
		));
	}
}