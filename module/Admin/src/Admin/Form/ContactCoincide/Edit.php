<?php
namespace Admin\Form\ContactCoincide;
use \Zend\Form\Form as Form;

class Edit extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'		=> 'success',
		    ),
		));
		
		
	// Phone
		$this->add(array(
		    'name'			=> 'phone',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control mask_phone',
		    ),
		));
		
		// Phone 2
		$this->add(array(
		    'name'			=> 'phone_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control mask_phone',
		    ),
		));
		
		// Phone 3
		$this->add(array(
		    'name'			=> 'phone_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control mask_phone',
		    ),
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
		));
		
		// Sex
		$this->add(array(
		    'name'			=> 'sex',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
		    )
		));
		
		// Email
		$this->add(array(
		    'name'			=> 'email',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		));
		
		// Birthday Year
		$birthday_year = array();
		for ($i = date('Y') - 10; $i >= 1950; $i--) {
		    $birthday_year[$i] = $i;
		}
		$this->add(array(
		    'name'			=> 'birthday_year',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> $birthday_year,
		    )
		));
		
		// Tỉnh thành
		$this->add(array(
		    'name'			=> 'location_city_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options' => \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            
		    ),
		));
        // Quận huyện
        $this->add(array(
            'name'          => 'location_district_id',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => 'name',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));

		// Address
		$this->add(array(
		    'name'			=> 'address',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Address 2
		$this->add(array(
		    'name'			=> 'address_2',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Address 3
		$this->add(array(
		    'name'			=> 'address_3',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Địa chỉ'
		    )
		));
		
		// Nguồn khách hàng
		$this->add(array(
		    'name'			=> 'contact_source',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'contact-source')), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Nguồn biết đến
		$this->add(array(
		    'name'			=> 'marketing_channel',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'marketing-channel')), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Ghi chú
		$this->add(array(
		    'name'			=> 'note',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    )
		));
		
		
		// Sản phẩm quan tâm
		$this->add(array(
		    'name'			=> 'product_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "product-interest" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Nghề nghiệp
		$this->add(array(
		    'name'			=> 'job',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Trạng thái level chăm sóc
		$this->add(array(
		    'name'			=> 'level',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "contact-level" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Lý do từ chối
		$this->add(array(
		    'name'			=> 'reason_refuse',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Nhóm bệnh
		$this->add(array(
		    'name'			=> 'sick_group',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Thời gian bệnh
		$this->add(array(
		    'name'			=> 'sick_time',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "disease-time" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Thói quen sinh hoạt
		$this->add(array(
		    'name'			=> 'living_habits',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Bệnh kèm theo
		$this->add(array(
		    'name'			=> 'sick_attach',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Thông tin cuộc gọi
		$this->add(array(
		    'name'			=> 'call_info',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// History Action - Hành động
		$this->add(array(
		    'name'			=> 'history_action_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-action" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Result - Kết quả
		$this->add(array(
		    'name'			=> 'history_result_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "sale-history-result" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// History Content - Nội dung/Ghi chú
		$this->add(array(
		    'name'			=> 'history_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    )
		));
		
		// Ngày hẹn gọi lại
		$this->add(array(
		    'name'			=> 'history_return',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'dd/mm/yyyy'
		    )
		));
		
		// Bệnh kèm theo
		$this->add(array(
		    'name'			=> 'sick_attach',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Liều dùng tư vấn
		$this->add(array(
		    'name'			=> 'dosage',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Triệu chứng bệnh
		$this->add(array(
		    'name'			=> 'symptom_sick',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Tiền sử bệnh
		$this->add(array(
		    'name'			=> 'prehistoric_sick',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
		// Đối tượng sử dụng
		$this->add(array(
		    'name'			=> 'objects_used',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		     => 'form-control',
		    ),
		));
		
	}
}