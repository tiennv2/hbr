<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class Contract extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		/* // Product type
		$this->add(array(
		    'name'			=> 'product_type',
		    'type'			=> 'Hidden',
		)); */
		
		// Phone
		$this->add(array(
		    'name'			=> 'phone',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control mask_phone',
		    ),
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'disabled'  => 'disabled'
			),
		));
		
		// Giới tính
		$this->add(array(
		    'name'			=> 'sex',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'disabled'  => 'disabled'
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'sex')), array('task' => 'cache')), array('key' => 'alias', 'value' => 'name')),
		    )
		));
		
		// Email
		$this->add(array(
		    'name'			=> 'email',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'disabled'  => 'disabled'
		    ),
		));
		
		// Tỉnh thành
        $this->add(array(
            'name'          => 'location_city_id',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chọn -',
                'disable_inarray_validator' => true,
                'value_options' => \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\LocationsTable')->listItem(array('level' => 1), array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            ),
        ));

        // Quận huyện
        $this->add(array(
            'name'          => 'location_district_id',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => 'name',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));

        // phường xã
        $this->add(array(
            'name'          => 'location_town_id',
            'type'          => 'Text',
            'attributes'    => array(
                'class'               => 'form-control select2 select2_advance',
                'value'               => '',
                'data-table'          => TABLE_LOCATIONS,
                'data-id'             => 'code',
                'data-text'           => 'name',
                'data-parent'         => '',
                'data-parent-field'   => 'parent',
                'data-parent-name'    => 'parent',
            ),
        ));
		
		// Địa chỉ
		$this->add(array(
		    'name'			=> 'address',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'disabled'  => 'disabled'
		    )
		));
		
		// Ngày hợp đồng
		$this->add(array(
		    'name'			=> 'date',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		    => 'form-control date-picker not-push',
		        'placeholder'	=> 'dd/mm/yyyy',
		        'value'         => date('d/m/Y')
		    )
		));
		
		/* // Sản phẩm
		$this->add(array(
		    'name'			=> 'product_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic not-push',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\ProductTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		)); */
		
		// Phương thức thanh toán
		$this->add(array(
		    'name'          => 'payment',
		    'type'          => 'Select',
		    'attributes'    => array(
		        'class'     => 'form-control select2 select2_basic',
		    ),
		    'options'       => array(
		        'empty_option'  => '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options' => \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "payment" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
	}
}