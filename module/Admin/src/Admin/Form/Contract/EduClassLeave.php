<?php
namespace Admin\Form\Contract;
use \Zend\Form\Form as Form;

class EduClassLeave extends Form {
	
	public function __construct($sm){
	    $dateFormat = new \ZendX\Functions\Date();
	    
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'     => 'success',
		    )
		));
		
		// Contract Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Số buổi đã học
		$this->add(array(
		    'name'			=> 'leave_sessions',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		    => 'form-control mask_number',
		    )
		));
		
		// Nghỉ học từ ngày
		$this->add(array(
		    'name'			=> 'leave_date',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		    => 'form-control date-picker',
		        'placeholder'	=> 'dd/mm/yyyy',
		        'value'         => date('d/m/Y'),
		        'data-value'    => date('d/m/Y'),
		    )
		));
		
		// Nguyên nhân/Lý do nghỉ học
		$this->add(array(
		    'name'			=> 'leave_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'		    => 'form-control',
		    )
		));
	}
}