<?php
namespace Admin\Form\Contract;
use \Zend\Form\Form as Form;

class EduClassLeaveCancel extends Form {
	
	public function __construct($sm){
	    $dateFormat = new \ZendX\Functions\Date();
	    
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'     => 'success',
		    )
		));
		
		// Contract Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Nguyên nhân/Lý do hủy nghỉ học
		$this->add(array(
		    'name'			=> 'leave_cancel_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'		    => 'form-control',
		    )
		));
	}
}