<?php
namespace Admin\Form\Contract;
use \Zend\Form\Form as Form;

class EduClassReserveCancel extends Form {
	
	public function __construct($sm){
	    $dateFormat = new \ZendX\Functions\Date();
	    
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'     => 'success',
		    )
		));
		
		// Contract Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Nguyên nhân/Lý do hủy bảo lưu
		$this->add(array(
		    'name'			=> 'reserve_cancel_content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'		    => 'form-control',
		    )
		));
	}
}