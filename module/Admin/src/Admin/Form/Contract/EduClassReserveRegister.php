<?php
namespace Admin\Form\Contract;
use \Zend\Form\Form as Form;

class EduClassReserveRegister extends Form {
	
	public function __construct($sm, $params = null){
	    $dateFormat = new \ZendX\Functions\Date();
	    
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'     => 'success',
		    )
		));
		
		// Contract Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Lớp học
		$this->add(array(
		    'name'			=> 'edu_class_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\EduClassTable')->listItem(array('status' => 1, 'not_id' => $params['edu_class_id']), array('task' => 'list-all')), array('key' => 'id', 'value' => 'name,student_total,student_max', 'sprintf' => '%s (%s/%s)')),
		    )
		));
	}
}