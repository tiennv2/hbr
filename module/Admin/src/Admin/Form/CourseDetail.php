<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class CourseDetail extends Form {
	
	public function __construct($sm = null, $params = null){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Video
		$this->add(array(
		    'name'			=> 'video',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Nhập tên video hoặc link youtube',
		    )
		));
		
		// Hình ảnh đại diện
		$this->add(array(
			'name'			=> 'image',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'image',
				'placeholder'	=> 'Chọn hình ảnh',
			),
			'options'		=> array(
				'type'	    => 'open-file',
				'group'	    => 'images',
			),
		));
		
		// Group
		$this->add(array(
		    'name'			=> 'group_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\CourseGroupTable')->listItem(array('course_item_id' => $params['ssFilter']['filter_item']), array('task' => 'list-all')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Description
		$this->add(array(
			'name'			=> 'description',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'description',
			)
		));
		
		// Content
		$this->add(array(
			'name'			=> 'content',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'content',
			)
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control',
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
	}
}