<?php
namespace Admin\Form\FormData;
use \Zend\Form\Form as Form;

class Import extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
			'enctype'		=> 'multipart/form-data'
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// File import
		$this->add(array(
			'name'			=> 'file_import',
			'type'			=> 'File',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
		));
		
		// Form
		$aclInfo    = new \ZendX\System\UserInfo();
		//$params['groupInfo'] = $aclInfo->getGroupInfo();
		$params['permissionInfo'] = $aclInfo->getPermissionListInfo();
		$this->add(array(
		    'name'			=> 'form_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		    	'empty_option'	=> '- Chọn form -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\FormTable')->listItem($params, array('task' => 'list-form')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Submit
		$this->add(array(
		    'name'			=> 'submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Bắt đầu import',
		        'class'		=> 'btn btn-sm green',
		        'style'     => 'border: 1px solid #35aa47;'
		    ),
		));
	}
}