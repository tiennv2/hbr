<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class History extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Modal
		$this->add(array(
		    'name'			=> 'modal',
		    'type'			=> 'Hidden',
		    'attributes'	=> array(
		        'value'		=> 'success',
		    ),
		));
		
		// Kết quả chăm sóc
		$this->add(array(
			'name'			=> 'history_1',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'placeholder'	=> ''
			)
		));
		
		// Kết quả chăm sóc
		$this->add(array(
			'name'			=> 'history_2',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'placeholder'	=> ''
			)
		));
		
		// Kết quả chăm sóc
		$this->add(array(
			'name'			=> 'history_3',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'placeholder'	=> ''
			)
		));
	}
}