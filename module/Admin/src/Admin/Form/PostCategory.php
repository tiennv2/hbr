<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class PostCategory extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'placeholder'	=> 'Để trống để hệ thống lấy tự động',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Parent
		$list_node = $sm->getServiceLocator()->get('Admin\Model\PostCategoryTable')->itemInSelectbox(null, array('task' => 'form-category'));
		$list_node[0]['level'] = $list_node[0]['level'] + 1;
		$list_node[0]['name'] = '- Danh mục gốc -';
		$level_start = 1;
		$this->add(array(
		    'name'			=> 'parent',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'value_options'	=> \ZendX\Functions\CreateArray::create($list_node, array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => $level_start)),
		    ),
		));
		
		// Image
		$this->add(array(
		    'name'			=> 'image',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
		
		// Type
		$this->add(array(
			'name'			=> 'type',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
		    'options'		=> array(
		        'value_options'	=> array( 'default'	=> 'Bài viết', 'course' => 'Khóa học', 'course_long' => 'Chuỗi khóa học', 'teacher' => 'Giảng viên', 'product' => 'Sản phẩm', 'gallery' => 'Album ảnh', 'video' => 'Video', 'file' => 'Tài liệu', 'online' => 'Khóa học online'),
		    )
		));
		
		// Layout
		$template = new \ZendX\System\Template();
		$this->add(array(
		    'name'			=> 'layout',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'value_options'	=> $template->getLayout(),
		    )
		));

		// Description
		$this->add(array(
		    'name'			=> 'description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'description',
		    )
		));
		
		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));
		
		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));
		
		// Meta Title
		$this->add(array(
		    'name'			=> 'meta_title',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_title',
		    )
		));
		
		// Meta Keywords
		$this->add(array(
		    'name'			=> 'meta_keywords',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_keywords',
		    )
		));
		
		// Meta Description
		$this->add(array(
		    'name'			=> 'meta_description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_description',
		    )
		));
		
		// Box Hot
		$this->add(array(
		    'name'			=> 'box_hot',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Highlight
		$this->add(array(
		    'name'			=> 'box_highlight',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Home
		$this->add(array(
		    'name'			=> 'box_home',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Footer
		$this->add(array(
		    'name'			=> 'box_footer',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Left
		$this->add(array(
		    'name'			=> 'box_left',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Right
		$this->add(array(
		    'name'			=> 'box_right',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
	}
}