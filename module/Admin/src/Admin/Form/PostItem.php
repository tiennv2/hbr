<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class PostItem extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Title
		$this->add(array(
			'name'			=> 'title',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
			)
		));
		
		// Post Category
		$this->add(array(
		    'name'			=> 'post_category_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listItem(null, array('task' => 'list-all')), array('key' => 'id', 'value' => 'name', 'level' => true, 'level_start' => 1)),
		    ),
		));
		
		// Hình ảnh đại diện
		$this->add(array(
		    'name'			=> 'image',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Hình ảnh cover
		$this->add(array(
		    'name'			=> 'image_cover',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'image_cover',
		        'placeholder'	=> 'Chọn hình ảnh',
		    ),
		    'options'		=> array(
		        'type'	    => 'open-file',
		        'group'	    => 'images',
		    ),
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'ordering',
		        'placeholder'	=> 'Thứ tự'
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));

		$this->add(array(
			'name'			=> 'target',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => '_self',
			),
		    'options'		=> array(
		        'value_options'	=> array( "_self" => "Tại trang", "_blank" => "Tab mới"),
		    )
		));
		
		// Layout
		$template = new \ZendX\System\Template();
		$this->add(array(
		    'name'			=> 'layout',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Tự động theo danh mục',
		        'value_options'	=> $template->getLayout(),
		    )
		));
		
		// Description
		$this->add(array(
		    'name'			=> 'description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'description',
		    )
		));
		
		// Content
		$this->add(array(
		    'name'			=> 'content',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'content',
		    )
		));
		
		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));
		
		// Meta Title
		$this->add(array(
		    'name'			=> 'meta_title',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_title',
		    )
		));
		
		// Meta Keywords
		$this->add(array(
		    'name'			=> 'meta_keywords',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_keywords',
		    )
		));
		
		// Meta Description
		$this->add(array(
		    'name'			=> 'meta_description',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_description',
		    )
		));
		
		// Box Hot
		$this->add(array(
		    'name'			=> 'box_hot',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Highlight
		$this->add(array(
		    'name'			=> 'box_highlight',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Home
		$this->add(array(
		    'name'			=> 'box_home',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Footer
		$this->add(array(
		    'name'			=> 'box_footer',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Left
		$this->add(array(
		    'name'			=> 'box_left',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Box Right
		$this->add(array(
		    'name'			=> 'box_right',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 0,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 0 => 'Không', 1 => 'Có'),
		    )
		));
		
		// Sản phẩm
		$this->add(array(
			'name'			=> 'product_price',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_currency',
			)
		));
		
		// Giảm giá theo %
		$this->add(array(
			'name'			=> 'product_sale_percent',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_percent',
			)
		));
		
		// Giảm giá theo giá tiền
		$this->add(array(
			'name'			=> 'product_sale_price',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_currency',
			)
		));
		
		// Năm xuất bản
		$product_year = array();
		for ($i = date('Y'); $i >= date('Y') - 50; $i--) {
		    $product_year[$i] = $i;
		}
		$this->add(array(
		    'name'			=> 'product_year',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> $product_year,
		    )
		));
		
		// Mã khóa học
		$this->add(array(
		    'name'			=> 'course_code',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		    )
		));
		
		// Diễn giả
		$this->add(array(
		    'name'			=> 'course_teacher_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Tác giả sách
		$this->add(array(
		    'name'			=> 'book_teacher_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Chuỗi
		$this->add(array(
		    'name'			=> 'course_list_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\PostItemTable')->listItem(array('category_type' => 'course_long'), array('task' => 'list-all')), array('key' => 'id', 'value' => 'name')),
	        )
		));
		
		// Giá khóa học
		$this->add(array(
		    'name'			=> 'course_price',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control mask_currency',
		    )
		));
		
		// Đối tượng tham gia
		$this->add(array(
		    'name'			=> 'course_target',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'course_target',
		    )
		));
		
		// Lợi ích tham gia
		$this->add(array(
		    'name'			=> 'course_benefit',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'course_content',
		    )
		));
		
		// Chính sách ưu đãi
		$this->add(array(
		    'name'			=> 'course_policy',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'course_content',
		    )
		));
	}
}