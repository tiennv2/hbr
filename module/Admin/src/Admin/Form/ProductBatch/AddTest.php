<?php
namespace Admin\Form\ProductBatch;
use \Zend\Form\Form as Form;

class AddTest extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'id'		=> 'name',
			    'placeholder'	=> 'Tên lô sản phẩm',
			),
		));
		
		// Status
		$this->add(array(
		    'name'			=> 'code',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 1,
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 'kiem nghiem'	=> 'Kiểm nghiệm'),
		    )
		));

		// form
		$this->add(array(
		    'name'			=> 'product_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\ProductTable')->listItem(array('where' => array('status' => 1)), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Name
		$this->add(array(
		    'name'			=> 'number',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control mask_currency',
		        'id'		=> 'name',
		        'placeholder'	=> 'Số lượng',
		    ),
		));
		
		// Hạn sử dụng
		$this->add(array(
		    'name'			=> 'expdate',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		  => 'form-control date-picker',
		        'placeholder' => 'dd/mm/yyyy',
		    )
		));
		
		// Người giao
		$this->add(array(
		    'name'			=> 'deliver',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		        'id'		=> 'name',
		        'placeholder'	=> 'Người giao',
		    ),
		));

		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'     => 255,
		        'class'		=> 'form-control mask_interger',
		    )
		));

		// Status
		/* $this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		)); */
	}
}