<?php
namespace Admin\Form\ProductWarehouse;
use \Zend\Form\Form as Form;

class Add extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Lô Xuât
		$this->add(array(
		    'name'			=> 'product_batch',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\ProductBatchTable')->listItem(array('where' => array('status' => 1)), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'number',
		    'type'			=> 'Number',
		    'attributes'	=> array(
		        'placeholder'	=> 'Số lượng xuât',
		        'min'           => 0,
		        'class'		    => 'form-control mask_interger',
		    )
		));

		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'     => 255,
		        'class'		=> 'form-control mask_interger',
		    )
		));
	}
}