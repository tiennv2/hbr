<?php
namespace Admin\Form\ProductWarehouse;
use \Zend\Form\Form as Form;

class AddProduct extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		

		// Tên sản phẩm
		$this->add(array(
			'name'			=> 'product_id',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'id'		=> 'name',
			    'placeholder'	=> 'Sản phẩm',
			),
		));

		// Số lượng sản phẩm
		$this->add(array(
			'name'			=> 'total',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'id'		=> 'name',
			    'placeholder'	=> 'Số lượng sản phẩm',
			),
		));

		// Số lượng sản phẩm
		$this->add(array(
			'name'			=> 'price',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'id'		=> 'name',
			    'placeholder'	=> 'Giá sản phẩm',
			),
		));

		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'     => 255,
		        'class'		=> 'form-control mask_interger',
		    )
		));

		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));

	}
}