<?php
namespace Admin\Form\ProductWarehouse;
use \Zend\Form\Form as Form;

class Add extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	    => '',
			'method'	    => 'POST',
			'class'		    => 'horizontal-form',
			'role'		    => 'form',
			'name'		    => 'adminForm',
			'id'		    => 'adminForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));

		// Tên sản phẩm
		$this->add(array(
			'name'			=> 'product_batch',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
			    'id'		=> 'name',
			    'placeholder'	=> 'Lô sản phẩm',
			),
		));
	}
}