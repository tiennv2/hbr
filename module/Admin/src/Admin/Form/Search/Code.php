<?php
namespace Admin\Form\Search;
use \Zend\Form\Form as Form;

class Code extends Form{
    
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Keyword
		$this->add(array(
		    'name'			=> 'filter_keyword',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'placeholder'   => 'Từ khóa',
		        'class'			=> 'form-control input-sm',
		        'id'			=> 'filter_keyword',
		    ),
		));
		
		// Ngày tạo
		$this->add(array(
		    'name'			=> 'filter_created',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'placeholder'   => 'Ngày tạo',
		        'class'			=> 'form-control date-picker',
		        'id'			=> 'filter_created',
		    ),
		));
		
		// Status
		$this->add(array(
		    'name'			=> 'filter_status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Trạng thái -',
		        'value_options'	=> array( 0	=> 'Chưa sử dụng', 1 => 'Đã sử dụng'),
		    )
		));
		
		// Vận chuyển
		$this->add(array(
		    'name'			=> 'filter_send',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Vận chuyển -',
		        'value_options'	=> array( 0	=> 'Chưa gửi', 1 => 'Đã gửi'),
		    )
		));
		
		// Khóa học
		$this->add(array(
		    'name'			=> 'filter_course_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Khóa học -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'list-all')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Phân loại mã kích hoạt
		$this->add(array(
		    'name'			=> 'filter_document_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Loại -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\DocumentTable')->listItem(array( "table" => "document", "where" => array( "code" => "code-type" ), "order" => array("ordering" => "ASC", "name" => "ASC"), "view"  => array( "key" => "id", "value" => "name", "sprintf" => "%s" ) ), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Submit
		$this->add(array(
		    'name'			=> 'filter_submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Tìm',
		        'class'		=> 'btn btn-sm green',
		    ),
		));
		
		// Order
		$this->add(array(
		    'name'			=> 'order',
		    'type'			=> 'Hidden',
		));
		
		// Order By
		$this->add(array(
		    'name'			=> 'order_by',
		    'type'			=> 'Hidden',
		));
	}
}