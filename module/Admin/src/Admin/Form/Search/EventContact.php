<?php
namespace Admin\Form\Search;
use \Zend\Form\Form as Form;

class EventContact extends Form{
    
	public function __construct($sm, $params = NULL){ 
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Keyword
		$this->add(array(
		    'name'			=> 'filter_keyword',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'placeholder'   => 'Từ khóa',
		        'class'			=> 'form-control input-sm',
		        'id'			=> 'filter_keyword',
		    ),
		));
		
		// Status
		$this->add(array(
		    'name'			=> 'filter_status',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Trạng thái -',
		        'value_options'	=> array( 
		            'call'            => 'Gọi điện', 
		            'listen'          => 'Nghe máy',
		            'no_listen'       => 'Không nghe máy',
		            'busy'            => 'Bận',
		            'wrong_number'    => 'Sai số',
		            'sms'             => 'SMS',
		            'mail'            => 'Mail',
		            'agree'           => 'Đồng ý',
		            'confirm'         => 'Xác thực',
		            'ticket'          => 'Nhận vé',
		            'join'            => 'Tham gia',
		            'contract'        => 'Hợp đồng',
		        ),
		    )
		));

		// Người dùng
		$this->add(array(
		    'name'			=> 'filter_company_branch',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Cơ sở -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\CompanyBranchTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));
		
		// Đội nhóm
		$this->add(array(
		    'name'			=> 'filter_company_group',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Đội nhóm -',
		        'value_options'	=> \ZendX\Functions\CreateArray::createSelect($sm->get('Admin\Model\CompanyGroupTable')->listItem(array('data' => array('company_branch_id' => $params['filter_company_branch'])), array('task' => 'list-all')), array('key' => 'id', 'value' => 'name,parent_name', 'symbol' => ' - ')),
		    )
		));
		
		// nhân viên
		$this->add(array(
		    'name'			=> 'filter_user',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Nhân viên -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\UserTable')->listItem(array('data' => array('company_group_id' => $params['filter_company_group'],'company_branch_id' => $params['filter_company_branch'])), array('task' => 'list-all')), array('key' => 'id', 'value' => 'fullname')),
		    )
		));
		
		// Submit
		$this->add(array(
		    'name'			=> 'filter_submit',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Tìm',
		        'class'		=> 'btn btn-sm green',
		    ),
		));
		
		// Xóa
		$this->add(array(
		    'name'			=> 'filter_reset',
		    'type'			=> 'Submit',
		    'attributes'	=> array(
		        'value'     => 'Xóa',
		        'class'		=> 'btn btn-sm red',
		    ),
		));
		
		// Order
		$this->add(array(
		    'name'			=> 'order',
		    'type'			=> 'Hidden',
		));
		
		// Order By
		$this->add(array(
		    'name'			=> 'order_by',
		    'type'			=> 'Hidden',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
	}
}