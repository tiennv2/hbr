<?php
namespace Admin\Form\Search;
use \Zend\Form\Form as Form;

class Report extends Form{
    
	public function __construct($sm, $params = null){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Ngày bắt đầu
		$this->add(array(
		    'name'			=> 'date_begin',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'Ngày bắt đầu'
		    )
		));
		
		// Ngày kết thúc
		$this->add(array(
		    'name'			=> 'date_end',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'Ngày kết thúc'
		    )
		));
		
		// Phân ngày
		$this->add(array(
			'name'			=> 'date_type',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
				'value'     => 'created'
			),
			'options'		=> array(
				'value_options'	=> array('created' => 'Ngày nhập', 'date' => 'Ngày chứng từ'),
			)
		));
		
		// So sánh Ngày bắt đầu
		$this->add(array(
		    'name'			=> 'ss_date_begin',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'Ngày bắt đầu'
		    )
		));
		
		// So sánh Ngày kết thúc
		$this->add(array(
		    'name'			=> 'ss_date_end',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control date-picker',
		        'placeholder'	=> 'Ngày kết thúc'
		    )
		));
		
		// So sánh Checkbox
		$this->add(array(
		    'name'			=> 'ss_checkbox',
		    'type'			=> 'Checkbox',
		    'options' => array(
		        'attributes'	=> array(
		            'class'		=> 'checkbox-inline',
		        ),
		    )
		));
		
		// Phân loại nhóm
		$this->add(array(
		    'name'			=> 'company_group_type',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		        'value'     => 'me'
		    ),
		    'options'		=> array(
		        'value_options'	=> array('me' => 'Chỉ những đội quản lý', 'all' => 'Tất cả các đội'),
		    )
		));
		
		/* // Cơ sở
		$this->add(array(
		    'name'			=> 'company_branch_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Cơ sở -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\CompanyBranchTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		)); */
		
		// Đội nhóm
		$this->add(array(
		    'name'			=> 'company_group_id',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'		          => 'form-control select2 select2_advance',
		        'value'               => '',
		        'data-table'          => TABLE_COMPANY_GROUP,
		        'data-id'             => 'id',
		        'data-text'           => 'name,parent_name',
		        'data-parent'         => '',
		        'data-parent-field'   => 'company_branch_id',
		    ),
		));
		
		/* // Thành phố
		$this->add(array(
		    'name'			=> 'location_city_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        //'empty_option'	=> '- Chọn -',
		        //'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\LocationCityTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		)); */
		
		// Nhân viên
		$this->add(array(
		    'name'			=> 'user_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Nhân viên -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\UserTable')->listItem(array('data' => array('company_branch_id' => $params['company_branch_id'], 'company_group_id' => $params['company_group_id'])), array('task' => 'list-all')), array('key' => 'id', 'value' => 'fullname')),
		    )
		));
		
		/* // Event id
		$this->add(array(
		    'name'			=> 'event_id',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Sự kiện -',
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('Admin\Model\EventTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		)); */
		
		// Submit
		$this->add(array(
		    'name'			=> 'submit',
		    'type'			=> 'Button',
		    'options' => array(
                'label' => 'Lọc',
            ),
		    'attributes'	=> array(
		        'class'		=> 'btn btn-sm green',
		    ),
		));
	}
}