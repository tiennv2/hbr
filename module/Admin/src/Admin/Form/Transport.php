<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class Transport extends Form {
    
    public function __construct($sm){
        parent::__construct();
        
        // FORM Attribute
        $this->setAttributes(array(
            'action'    => '',
            'method'    => 'POST',
            'class'     => 'horizontal-form',
            'role'      => 'form',
            'name'      => 'adminForm',
            'id'        => 'adminForm',
            'enctype'   => 'multipart/form-data'
        ));
        
        // Id
        $this->add(array(
            'name'          => 'id',
            'type'          => 'Hidden',
        ));

        // Mã đơn hàng trên hệ thống
        $this->add(array(
            'name'          => 'contract_code',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'id'            => 'name',
                'placeholder'   => 'Mã đơn hàng',
            ),
        ));

        // Mã vận đơn
        $this->add(array(
            'name'          => 'tracking_code',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'id'            => 'tracking_code',
                'placeholder'   => 'Mã vận đơn',
            ),
        ));
        
        // filter_keyword
        $this->add(array(
            'name'          => 'filter_keyword',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control',
                'id'            => 'filter_keyword',
                'placeholder'   => 'Từ khóa',
            ),
        ));

        // filter_date_begin
        $this->add(array(
            'name'          => 'filter_date_begin',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control date-picker',
                'id'            => 'filter_date_begin',
                'placeholder'   => 'Ngày bắt đầu',
            ),
        ));
        
        // filter_date_end
        $this->add(array(
            'name'          => 'filter_date_end',
            'type'          => 'Text',
            'attributes'    => array(
                'class'         => 'form-control date-picker',
                'id'            => 'filter_date_end',
                'placeholder'   => 'Ngày kết thúc',
            ),
        ));
        
        /* // order_by
        $this->add(array(
            'name'          => 'order_by',
            'type'          => 'Select',
            'attributes'    => array(
                'class'         => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Sắp xếp bởi -',
                'value_options' => array('id' => 'ID', 'tracking_code' => 'Mã giao vận', 'created' => 'Ngày tháng'),
            ),
        ));
        
        // order
        $this->add(array(
            'name'          => 'order',
            'type'          => 'Select',
            'attributes'    => array(
                'class'         => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Chiều sắp xếp -',
                'value_options' => array('DESC' => 'Giảm dần', 'ASC' => 'Tăng dần'),
            ),
        )); */

        // filter_submit
        $this->add(array(
            'name'          => 'filter_submit',
            'type'          => 'Submit',
            'attributes'    => array(
                'class'         => 'btn btn-sm btn-success',
                'value'         => 'Tìm'
            ),
        ));

        // filter_reset
        $this->add(array(
            'name'          => 'filter_reset',
            'type'          => 'Submit',
            'attributes'    => array(
                'class'         => 'btn btn-sm btn-danger',
                'value'         => 'Xóa'
            ),
        ));

        // Nhà cung cấp dịch vụ vận chuyển
        $list_transport = \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-service')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
        $this->add(array(
            'name'          => 'transport_service',
            'type'          => 'Select',
            'attributes'    => array(
                'class'         => 'form-control select2 select2_basic',
                'id'            => 'transport_service'
            ),
            'options'       => array(
                'disable_inarray_validator' => true,
                'empty_option'  => '- Chọn đơn vị vận chuyển -',
                'value_options' => $list_transport,
            ),
        ));

        // trạng thái vận chuyển
        $list_status = \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-status')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
        $this->add(array(
            'name'          => 'transport_status',
            'type'          => 'Select',
            'attributes'    => array(
                'class'         => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Trạng thái đơn -',
                'value_options' => $list_status,
            ),
        ));
        
        // trạng thái vận chuyển viết tay
        $this->add(array(
            'name'          => 'transport_status_text',
            'type'          => 'Text',
            'attributes'    => array(
                'id'            => 'transport_status_text',
                'class'         => 'form-control',
            ),
        ));

        // Loại hình vận chuyển
        $list_type = \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'transport-type')), array('task' => 'cache')),  array('key' => 'alias', 'value' => 'name'));
        $this->add(array(
            'name'          => 'transport_type',
            'type'          => 'Select',
            'attributes'    => array(
                'id'            => 'transport_type',
                'class'         => 'form-control select2 select2_basic',
            ),
            'options'       => array(
                'empty_option'  => '- Loại đơn -',
                'value_options' => $list_type,
            ),
        ));
        
        // Nhóm quyền truy cập
        /* $this->add(array(
            'name'          => 'permission_ids',
            'type'          => 'MultiCheckbox',
            'options'       => array(
                'label_attributes' => array(
                    'class'     => 'checkbox-inline',
                ),
                'value_options' => \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\PermissionTable')->listItem(null, array('task' => 'cache')), array('key' => 'code', 'value' => 'name')),
            ),
        )); */
        
        // Status
        $this->add(array(
            'name'          => 'status',
            'type'          => 'Select',
            'attributes'    => array(
                'class'     => 'form-control select2 select2_basic',
                'value'     => 1,
            ),
            'options'       => array(
                'empty_option'  => '- Trạng thái đơn -',
                'value_options' => $list_transport,
            ),
        ));
    }
}