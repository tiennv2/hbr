<?php
namespace Admin\Form;
use \Zend\Form\Form as Form;

class User extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		    'enctype'   => 'multipart/form-data'
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Fullname
		$this->add(array(
		    'name'			=> 'fullname',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'fullname',
		        'placeholder'	=> 'Họ và tên',
		    ),
		));
		
		// Username
		$this->add(array(
			'name'			=> 'username',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'username',
				'placeholder'	=> 'Tên đăng nhập',
			),
		));
		
		// Email
		$this->add(array(
		    'name'			=> 'email',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'email',
		        'placeholder'	=> 'Email',
		    ),
		));
		
		// Phone
		$this->add(array(
		    'name'			=> 'phone',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'phone',
		        'placeholder'	=> 'Điện thoại',
		    ),
		));
		
		// Password
		$this->add(array(
		    'name'			=> 'password',
		    'type'			=> 'Password',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'password',
		        'placeholder'	=> 'Mật khẩu',
		    ),
		));
		
		// Group
		$this->add(array(
		    'name'			=> 'user_group_id',
		    'type'			=> 'MultiCheckbox',
		    'options'		=> array(
		        'label_attributes' => array(
		            'class'		=> 'checkbox-inline',
		        ),
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\UserGroupTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control mask_number',
		        'id'			=> 'ordering',
		        'placeholder'	=> 'Thứ tự'
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
		
		// Password Status
		$this->add(array(
			'name'			=> 'password_status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Có', 0 => 'Không'),
		    )
		));
		
		// Active Code
		$this->add(array(
			'name'			=> 'active_code',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			    'value'     => 1,
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Có', 0 => 'Không'),
		    )
		));
		
		// Sign
		$this->add(array(
		    'name'			=> 'sign',
		    'type'			=> 'Textarea',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'sign',
		    )
		));
	}
}