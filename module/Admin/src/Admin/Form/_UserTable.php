<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Where;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class _UserTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {
	
    protected $tableGateway;
	protected $userInfo;
	protected $serviceLocator;
	
	public function __construct(TableGateway $tableGateway) {
	    $this->tableGateway	= $tableGateway;
	    $this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator() {
	    return $this->serviceLocator;
	}
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_permission']) && $ssFilter['filter_permission'] != '') {
	                $select -> where -> literal('FIND_IN_SET(\''. $ssFilter['filter_permission'] .'\', permission_ids)');
	            }
	            
	            if(!empty($ssFilter['filter_company_branch'])) {
	                $select -> where -> equalTo('company_branch_id', $ssFilter['filter_company_branch']);
	            }
	            
	            if(!empty($ssFilter['filter_company_department'])) {
	                $select -> where -> equalTo('company_department_id', $ssFilter['filter_company_department']);
	            }
	            
	            if(!empty($ssFilter['filter_company_position'])) {
	                $select -> where -> equalTo('company_position_id', $ssFilter['filter_company_position']);
	            }
	            
	            if(!empty($ssFilter['filter_sale_branch'])) {
	                $select -> where -> equalTo('sale_branch_id', $ssFilter['filter_sale_branch']);
	            }
	            
	            if(!empty($ssFilter['filter_sale_group'])) {
	                $select -> where -> equalTo('sale_group_id', $ssFilter['filter_sale_group']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select -> where -> NEST
                    			     -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> OR
                    			     -> like('username', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> OR
                    			     -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> OR
                    			     -> like('email', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> UNNEST;
				}
				
				if($this->userInfo->getUserInfo('id') != '1111111111111111111111') {
				    $select -> where -> notEqualTo('id', '1111111111111111111111');
				}
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $paginator = $arrParam['paginator'];
	            $ssFilter  = $arrParam['ssFilter'];
	             
	            if(!isset($options['paginator']) || $options['paginator'] == true) {
	    			$select -> limit($paginator['itemCountPerPage'])
	    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
	    
	            if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
	                $select -> order(array(TABLE_USER .'.'. $ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
	            }
	    
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo(TABLE_USER .'.status', $ssFilter['filter_status']);
	            }
	    
	            if(isset($ssFilter['filter_permission']) && $ssFilter['filter_permission'] != '') {
	                $select -> where -> literal('FIND_IN_SET(\''. $ssFilter['filter_permission'] .'\', permission_ids)');
	            }
	            
	            if(!empty($ssFilter['filter_company_branch'])) {
	                $select -> where -> equalTo('company_branch_id', $ssFilter['filter_company_branch']);
	            }
	            
	            if(!empty($ssFilter['filter_company_department'])) {
	                $select -> where -> equalTo('company_department_id', $ssFilter['filter_company_department']);
	            }
	            
	            if(!empty($ssFilter['filter_company_position'])) {
	                $select -> where -> equalTo('company_position_id', $ssFilter['filter_company_position']);
	            }
	            
	            if(!empty($ssFilter['filter_sale_branch'])) {
	                $select -> where -> equalTo('sale_branch_id', $ssFilter['filter_sale_branch']);
	            }
	            
	            if(!empty($ssFilter['filter_sale_group'])) {
	                $select -> where -> equalTo('sale_group_id', $ssFilter['filter_sale_group']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
	                $select -> where -> NEST
                	                 -> like(TABLE_USER .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> OR
                	                 -> like(TABLE_USER .'.username', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> OR
                	                 -> like(TABLE_USER .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> OR
                	                 -> like(TABLE_USER .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> UNNEST;
	            }
	    
	            if($this->userInfo->getUserInfo('id') != '1111111111111111111111') {
	                $select -> where -> notEqualTo('id', '1111111111111111111111');
	            }
	        });
	            	
	    }
	    
	    if($options['task'] == 'list-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $arrData  = $arrParam['data'];
	            $arrRoute = $arrParam['route'];
	             
	            $select -> order(array('name' => 'ASC'));
	             
	            if(!empty($arrData['sale_group_id'])) {
	                $select -> where -> equalTo('sale_group_id', $arrData['sale_group_id']);
	            }

	            if(!empty($arrData['mkt_group_id'])) {
	                $select -> where -> equalTo('mkt_group_id', $arrData['mkt_group_id']);
	            }

	            if(!empty($arrData['group_id'])) {
	                $select -> where -> NEST
	                				 -> equalTo('sale_group_id', $arrData['group_id'])
	                				 -> OR
	                				 -> like('sale_group_ids', '%'. $arrData['group_id'] .'%')
	                				 -> OR
	                				 -> equalTo('mkt_group_id', $arrData['group_id'])
	                				 -> UNNEST;
	            }
	             
	            if(!empty($arrData['sale_branch_id'])) {
	                $select -> where -> equalTo('sale_branch_id', $arrData['sale_branch_id']);
	            }
	             
	            if(!empty($arrData['company_position_id'])) {
	                $select -> where -> equalTo('company_position_id', $arrData['company_position_id']);
	            }
	             
	            if(!empty($arrData['status'])) {
	                $select -> where -> equalTo('status', $arrData['status']);
	            }
	             
	            if($this->userInfo->getUserInfo('id') != '1111111111111111111111') {
	                $select -> where -> notEqualTo('id', '1111111111111111111111');
	            }
	        })->toArray();
	    }

	    if ($options['task'] == 'list-sale') {
	        $result    = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	        	$select -> order('name ASC');
	        	$select -> where -> equalTo('status', 1);
	            $select -> where -> equalTo('company_department_id', 'sale');
	        })->toArray();
	    }
	    
	    if ($options['task'] == 'list-marketing') {
	        $result    = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	        	$select -> order('name ASC');
	            $select -> where -> equalTo('status', 1);
	        	$select -> where -> equalTo('company_department_id', 'marketing');
	        })->toArray();
	    }
	    
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'AdminUser';
	        if(!empty($arrParam['company_position_id'])) {
	            $cache_key .= 'Position'. $arrParam['company_position_id'];
	        }
	        $result = $cache->getItem($cache_key);
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(TABLE_USER .'.name ASC');
	                
	                if(!empty($arrParam['company_position_id'])) {
	                    $select -> where -> equalTo('company_position_id', $arrParam['company_position_id']);
	                }
	            });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	            $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'cache-code') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'AdminUserCode';
	        if(!empty($arrParam['company_position_id'])) {
	            $cache_key .= 'Position'. $arrParam['company_position_id'];
	        }
	        $result = $cache->getItem($cache_key);
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(TABLE_USER .'.name ASC');
	                 
	                if(!empty($arrParam['company_position_id'])) {
	                    $select -> where -> equalTo('company_position_id', $arrParam['company_position_id']);
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'code', 'value' => 'object'));
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'AdminUserStatus';
	        $result = $cache->getItem($cache_key);
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(TABLE_USER .'.name ASC')
	                        -> where -> equalTo(TABLE_USER .'.status', 1);
	            });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	            $cache->setItem($cache_key, $result);
	        }
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->toArray();
		}
		
		if($options['task'] == 'by-username') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
			    $select->where->equalTo('username', $arrParam['username']);
			})->toArray();
		}
		
		if($options['task'] == 'by-code') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('code', $arrParam['code']);
		    })->toArray();
		}
	
		return current($result);
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $arrItem  = $arrParam['item'];
	     
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
	    $permission_ids    = $arrData['permission_ids'] ? implode(',', $arrData['permission_ids']) : '';
	    $sale_group_ids = $arrData['sale_group_ids'] ? implode(',', $arrData['sale_group_ids']) : '';
	    $mkt_group_ids = $arrData['mkt_group_ids'] ? implode(',', $arrData['mkt_group_ids']) : '';

		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                    	=> $id,
				'name'                  	=> $arrData['name'],
				'username'              	=> $arrData['username'],
				'code'                  	=> $arrData['code'],
				'password'              	=> md5($arrData['password']),
				'email'                 	=> $arrData['email'],
				'phone'                 	=> $arrData['phone'],
				'status'                	=> $arrData['status'],
				'created'               	=> date('Y-m-d H:i:s'),
				'created_by'            	=> $this->userInfo->getUserInfo('id'),
				'permission_ids'        	=> $permission_ids,
				'company_branch_id'     	=> $arrData['company_branch_id'],
				'company_department_id' 	=> $arrData['company_department_id'],
				'company_position_id'   	=> $arrData['company_position_id'],
				'sale_branch_id'        	=> $arrData['sale_branch_id'],
				'sale_level'				=> $arrData['sale_level'],
				'sale_group_id'         	=> $arrData['sale_group_id'],
				'sale_group_ids'        	=> $sale_group_ids,
				'mkt_group_id'          	=> $arrData['mkt_group_id'],
				'mkt_group_ids'         	=> $mkt_group_ids,
			);
			
			$arrOptions = array('password_status' => $arrData['password_status'], 'layout' => $arrData['layout']);
			$data['options'] = serialize($arrOptions);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'                  	=> $arrData['name'],
				'username'              	=> $arrData['username'],
			    'code'                  	=> $arrData['code'],
				'email'                 	=> $arrData['email'],
			    'phone'                 	=> $arrData['phone'],
				'status'                	=> $arrData['status'],
			    'permission_ids'        	=> $permission_ids,
			    'company_branch_id'     	=> $arrData['company_branch_id'],
			    'company_department_id' 	=> $arrData['company_department_id'],
			    'company_position_id'   	=> $arrData['company_position_id'],
			    'sale_branch_id'        	=> $arrData['sale_branch_id'],
			    'sale_level'				=> $arrData['sale_level'],
				'sale_group_id'         	=> $arrData['sale_group_id'],
			    'sale_group_ids'        	=> $sale_group_ids,
			    'mkt_group_id'          	=> $arrData['mkt_group_id'],
				'mkt_group_ids'         	=> $mkt_group_ids,
			);
			
			if(!empty($arrData['password'])) {
			    $data['password'] = md5($arrData['password']);
			}

			$arrOptions = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
			$arrOptions['password_status'] = $arrData['password_status'];
			$arrOptions['layout']          = $arrData['layout'];
			
			$data['options'] = serialize($arrOptions);
			
			$this->tableGateway->update($data, array('id' => $arrData['id']));
			return $arrData['id'];
		}
		
		if($options['task'] == 'change-password') {
		    $data	= array(
		        'password' => md5($arrData['password_new']),
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $this->userInfo->getUserInfo('id')));
		    return $this->userInfo->getUserInfo('id');
		}
		
		if($options['task'] == 'update-password') {
		    $item = $this->getItem(array('id' => $this->userInfo->getUserInfo('id')));
		    $options = unserialize($item['options']);
		    $options['password_status'] = 0;
		    
		    $data	= array(
		        'password'    => md5($arrData['password_new']),
		        'options'     => serialize($options)
		    );
		    
		    $this->tableGateway->update($data, array('id' => $this->userInfo->getUserInfo('id')));
		    return $this->userInfo->getUserInfo('id');
		}
		
		if($options['task'] == 'update-login') {
		    $data	= array(
		        'login_ip'        => $_SERVER['REMOTE_ADDR'],
		        'login_time'      => date('Y-m-d H:i:s'),
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $this->userInfo->getUserInfo('id')));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'delete-item') {
	        $where = new Where();
	        $where -> in('id', $arrData['cid']);
	        $where -> notEqualTo('id', '1111111111111111111111');
	        $this -> tableGateway -> delete($where);
	        
	        return count($arrData['cid']);
	    }
	
	    return false;
	}
	
    public function changeStatus($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-status') {
	        if(!empty($arrData['cid'])) {
    	        $data = array( 
    	            'status' => !$arrData['status']
    	        );
    	        
    	        $where = new Where();
    	        $where -> in('id', $arrData['cid']);
    			$this -> tableGateway -> update($data, $where);
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrData['cid'] AS $id) {
                $data	= array( 'ordering'	=> $arrData['ordering'][$id] );
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrData['cid']);
	    }
	    return false;
	}
}