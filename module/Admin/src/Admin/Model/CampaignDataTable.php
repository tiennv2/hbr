<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class CampaignDataTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                if(!empty($ssFilter['filter_form'])) {
                	$select -> where->equalTo('form_id', $ssFilter['filter_form']);
                }
                
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		$arrData  = $arrParam['data'];
	    $arrItem  = $arrParam['item'];
	    $arrRoute = $arrParam['route'];
	    
	    $gid      = new \ZendX\Functions\Gid();

		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                
    			$select -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			if(!empty($ssFilter['filter_form'])) {
    				$select -> where->equalTo('form_id', $ssFilter['filter_form']);
    			}
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    			
    		});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'Form';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'get-by-phone') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('form_id', $arrParam['form_id'])
                                 -> equalTo('phone', $arrParam['phone'])
                                 -> greaterThanOrEqualTo('created', date('Y-m-d'));
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier(array( array('HTML.AllowedElements', '') ));
        $gid      = new \ZendX\Functions\Gid();
	    $id       = $gid->getId();
	    
	    if($options['task'] == 'add-item'){
	        $gid               = new \ZendX\Functions\Gid();
	        $data               = array(
	           'id'            => $id,
	           'birthday'      => $arrData['birthday'],
	           'position'      => $arrData['position'],
	           'seniority'     => $arrData['seniority'],
	           'field'         => $arrData['field'],
	           'scale'         => $arrData['scale'],
	           'job'           => $arrData['job'],
	           'revenue'       => $arrData['revenue'],
	           'problems'      => implode(', ' , $arrData['problems']),
	           'number_course' => $arrData['number'],
	           'source'        => implode(',' , $arrData['source']),
	           'reason'        => implode(',' , $arrData['reason']),
	           'channel'       => implode(',' , $arrData['channel']),
	           'budget_study'  => $arrData['budget_study'],
	           'budget_invest' => $arrData['budget_invest'],
	           'partner'       => $arrData['partner'],
	           'content'       => $arrData['content'],
	           'campaign_code' => $arrData['campaign_code'],
	           'name'          => $arrData['name'],
	           'phone'         => $arrData['phone'],
	           'city'          => $arrData['city'],
	           'email'         => $arrData['email'],
	        
	        );
	        $result = $this->tableGateway->insert($data);
	    }
	    return $result;
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}