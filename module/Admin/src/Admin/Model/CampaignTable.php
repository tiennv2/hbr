<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use ZendX\Functions\Date;

class CampaignTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result = $this->defaultCount($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->defaultList($arrParam, null);
		}
		
		if($options['task'] == 'list-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
				$select -> where -> equalTo('status', 1);
    		});
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('alias', $arrParam['alias']);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image             = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $image_course      = new \ZendX\Functions\Thumbnail($arrData['image_course']);
	    $filter            = new \ZendX\Filter\Purifier();
	    $alias             = new \ZendX\Filter\CreateAlias();
	    $gid               = new \ZendX\Functions\Gid();
	    
	    // Xử lý thêm nhiều hình ảnh
	    $images = '';
	    if(!empty($arrData['images'])) {
	        for ($i = 0; $i < count($arrData['images']['url']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['images']['url'][$i]);
	    
	            $images['url'][]      = $image_multi->getFull();
	            $images['medium'][]   = $image_multi->getMedium();
	            $images['thumb'][]    = $image_multi->getThumb();
	            $images['name'][]     = $arrData['images']['name'][$i];
	        }
	    }
	    
	    // Xử lý thêm nhiều video
	    $videos = '';
	    if(!empty($arrData['videos'])) {
	        for ($i = 0; $i < count($arrData['videos']['id']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['videos']['image'][$i]);
	    
	            $videos['image'][]    = $image_multi->getFull() ? $image_multi->getFull() : $arrData['videos']['image'][$i];
	            $videos['medium'][]   = $image_multi->getMedium() ? $image_multi->getMedium() : $arrData['videos']['image'][$i];
	            $videos['thumb'][]    = $image_multi->getThumb() ? $image_multi->getThumb() : $arrData['videos']['image'][$i];
	            $videos['name'][]     = $arrData['videos']['name'][$i];
	            $videos['id'][]       = $arrData['videos']['id'][$i];
	        }
	    }
	    $date = new Date();
		if($options['task'] == 'add-item') {
			$id = $arrData['id'] ? $arrData['id'] : $gid->getId();
			$data	= array(
			    'id'                 => $id,
			    'name'               => $arrData['name'],
			    'alias'              => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
			    'status'             => $arrData['status'],
			    'ordering'           => $arrData['ordering'],
			    'layout'             => $arrData['layout'],
			    'created'            => @date('Y-m-d H:i:s'),
			    'created_by'         => $this->userInfo->getUserInfo('id'),
			    'code'               => $arrData['code'],
			);
			$data['alias'] .= '_khaosat';
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    
			$data	= array(
				'name'               => $arrData['name'],
				'alias'              => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
				'status'             => $arrData['status'],
			    'ordering'           => $arrData['ordering'],
			    'layout'             => $arrData['layout'],
				'code'          	 => $arrData['code'],
			);
			$data['alias'] .= '_khaosat';
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}