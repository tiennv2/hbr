<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class CodeTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $date      = new \ZendX\Functions\Date();
                
                $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(id)')));
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_send']) && $ssFilter['filter_send'] != '') {
    			    $select->where->equalTo('send', $ssFilter['filter_send']);
    			}
    			
    			if(!empty($ssFilter['filter_course_id'])) {
    			    $select->where->equalTo('course_id', $ssFilter['filter_course_id']);
    			}
    			
    			if(!empty($ssFilter['filter_created'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo('created', $date->formatToData($ssFilter['filter_created']) .' 00:00:00')
                    			     -> and
                    			     -> lessThanOrEqualTo('created', $date->formatToData($ssFilter['filter_created']) .' 23:59:59')
                    			     -> UNNEST;
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->equalTo('code', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
    	    
    	    return $result->count;
	    }
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                $date      = new \ZendX\Functions\Date();
                
    			$select -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_send']) && $ssFilter['filter_send'] != '') {
    			    $select->where->equalTo('send', $ssFilter['filter_send']);
    			}
    			
    			if(!empty($ssFilter['filter_course_id'])) {
    			    $select->where->equalTo('course_id', $ssFilter['filter_course_id']);
    			}
    			
    			if(!empty($ssFilter['filter_created'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo('created', $date->formatToData($ssFilter['filter_created']) .' 00:00:00')
                    			     -> and
                    			     -> lessThanOrEqualTo('created', $date->formatToData($ssFilter['filter_created']) .' 23:59:59')
                    			     -> UNNEST;
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->equalTo('code', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    			
    		});
		}
		
		if($options['task'] == 'list-export') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $date      = new \ZendX\Functions\Date();
                
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
                
    			if(isset($ssFilter['filter_send']) && $ssFilter['filter_send'] != '') {
    			    $select->where->equalTo('send', $ssFilter['filter_send']);
    			}
    			
    			if(!empty($ssFilter['filter_course_id'])) {
    			    $select->where->equalTo('course_id', $ssFilter['filter_course_id']);
    			}
    			
    			if(!empty($ssFilter['filter_created'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo('created', $date->fomartToData($ssFilter['filter_created']) .' 00:00:00')
                    			     -> and
                    			     -> lessThanOrEqualTo('created', $date->fomartToData($ssFilter['filter_created']) .' 23:59:59')
                    			     -> UNNEST;
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->equalTo('code', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    		});
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'by-code-status') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> join(TABLE_DOCUMENT, TABLE_DOCUMENT .'.id='. TABLE_CODE. '.document_id', array('code_type' => 'type'), 'inner')
                        -> where -> equalTo(TABLE_CODE .'.code', $arrParam['code'])
								 -> equalTo(TABLE_CODE .'.status', 0)
								 -> greaterThan(TABLE_CODE .'.total', 0);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $gid     = new \ZendX\Functions\Gid();
	    $number   = new \ZendX\Functions\Number();
	    $filter   = new \ZendX\Filter\Purifier();
	    
		if($options['task'] == 'add-item') {
			if($arrData['number'] > 0) {
			    for ($i = 0; $i < $arrData['number']; $i++) {
			        $id      = $gid->getId();
			        $code    = $gid->getLetter(0, 1) . date('d'). $gid->getString(0, 5) . date('m'); 
        			$data    = array(
        				'id'            => $id,
        				'code'          => $code,
        				'status'        => 0,
        				'send'          => 0,
        				'course_id'     => $arrData['course_id'],
        				'document_id'   => $arrData['document_id'],
        				'created'       => date('Y-m-d H:i:s'),
        				'created_by'    => $this->userInfo->getUserInfo('id'),
        			);
        			
        			$this->tableGateway->insert($data);
			    }
			    
			    // Thêm report
			    $arrParam['data']['name'] = 'Yêu cầu tạo mã kích hoạt';
			    $code_report = $this->getServiceLocator()->get('Admin\Model\CodeReportTable')->saveItem($arrParam, array('task' => 'add-item'));
			}
			
			return $id;
		}
		
		if($options['task'] == 'update-status') {
			$id = $arrData['id'];
			$status = 0;
			$total = $arrData['total'] - 1;
			if($arrData['total'] == 1) {
				$status = 1;
			}
		    $data	= array(
		        'status'        => $status,
		        'total'        => $total,
		    );
		    $this->tableGateway->update($data, array('id' => $id));
		
		    return $id;
		}
		
		if($options['task'] == 'update-send') {
		    $id = $arrData['id'];
		    $data	= array(
		        'send'        => 1,
		    );
		    $this->tableGateway->update($data, array('id' => $id));
		
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	    
	    if($options['task'] == 'update-onoff') {
	        $arrData  = $arrParam['data'];
	        $arrRoute = $arrParam['route'];
	        
	        $data	= array( 
	                       $arrData['field'] => $arrData['value']
	                   );
	        
			$this->tableGateway->update($data, array('id' => $arrData['id']));
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}