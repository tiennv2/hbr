<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class ContactCoincideTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
        if($options['task'] == 'list-item') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter   = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                $number     = new \ZendX\Functions\Number();
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id = '. TABLE_CONTACT_COINCIDE .'.contact_id', array(), 'inner');
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $filter_keyword = trim($ssFilter['filter_keyword']);
                    if(strlen($number->formatToPhone($filter_keyword)) == 10) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.phone', $number->formatToPhone($filter_keyword));
                    } elseif(strlen($number->formatToNumber($filter_keyword)) == 4) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.birthday_year', $filter_keyword);
                    } elseif (filter_var($filter_keyword, FILTER_VALIDATE_EMAIL)) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.email', $filter_keyword);
                    } else {
                        $select -> where -> NEST
                                         -> like(TABLE_CONTACT.'.name', '%'. $filter_keyword .'%')
                                         -> UNNEST;
                    }
                }

                if(!empty($ssFilter['filter_history_status'])) {
                    if($ssFilter['filter_history_status'] == 'yes') {
                        $select -> where -> isNotNull('history_created');
                    }
                    if($ssFilter['filter_history_status'] == 'no') {
                        $select -> where -> isNull('history_created');
                    }
                    if($ssFilter['filter_history_status'] == 'return') {
                        $select -> where -> NEST
                                         -> lessThan('history_return', date('Y-m-d'))
                                         -> UNNSET;
                    }
                }
            })->current();
        }
        
        return $result->count;
    }
    
    public function listItem($arrParam = null, $options = null){
        if($options['task'] == 'list-item') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator  = $arrParam['paginator'];
                $ssFilter   = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                $number     = new \ZendX\Functions\Number();
                
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id = '. TABLE_CONTACT_COINCIDE .'.contact_id',
                    array(
                        'contact_marketer_id'       => 'marketer_id',
                        'contact_type'              => 'type',
                        'contact_user_id'           => 'user_id',
                    ), 'inner');
                
                if(!isset($options['paginator']) || $options['paginator'] == true) {
                    $select -> limit($paginator['itemCountPerPage'])
                            -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
                
               if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $filter_keyword = trim($ssFilter['filter_keyword']);
                    if(strlen($number->formatToPhone($filter_keyword)) == 10) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.phone', $number->formatToPhone($filter_keyword));
                    } elseif(strlen($number->formatToNumber($filter_keyword)) == 4) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.birthday_year', $filter_keyword);
                    } elseif (filter_var($filter_keyword, FILTER_VALIDATE_EMAIL)) {
                        $select -> where -> equalTo(TABLE_CONTACT.'.email', $filter_keyword);
                    } else {
                        $select -> where -> NEST
                                         -> like(TABLE_CONTACT.'.name', '%'. $filter_keyword .'%')
                                         -> UNNEST;
                    }
                }
                
                if(!empty($ssFilter['filter_date_begin']) && !empty($ssFilter['filter_date_end'])) {
                    $select -> where -> NEST
                                     -> greaterThanOrEqualTo(TABLE_CONTACT . '.created', $date->formatToData($ssFilter['filter_date_begin']))
                                     -> AND
                                     -> lessThanOrEqualTo(TABLE_CONTACT .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59')
                                     -> UNNEST;
                } elseif (!empty($ssFilter['filter_date_begin'])) {
                    $select -> where -> greaterThanOrEqualTo(TABLE_CONTACT .'.created', $date->formatToData($ssFilter['filter_date_begin']));
                } elseif (!empty($ssFilter['filter_date_end'])) {
                    $select -> where -> lessThanOrEqualTo(TABLE_CONTACT .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59');
                }

                if(!empty($ssFilter['filter_contact_source'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.contact_source', $ssFilter['filter_contact_source']);
                }
                
                if(!empty($ssFilter['filter_marketing_channel'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.marketing_channel_id', $ssFilter['filter_marketing_channel']);
                }

                if(!empty($ssFilter['filter_contact_type'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.type', $ssFilter['filter_contact_type']);
                }

                if(!empty($ssFilter['filter_user_id'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.user_id', $ssFilter['filter_user_id']);
                }
                
                if(!empty($ssFilter['filter_product_interest'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.product_id', $ssFilter['filter_product_interest']);
                }

                if(!empty($ssFilter['filter_location_city'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.location_city_id', $ssFilter['filter_location_city']);
                }

                if(!empty($ssFilter['filter_location_district'])) {
                    $select -> where -> equalTo(TABLE_CONTACT.'.location_district_id', $ssFilter['filter_location_district']);
                }

                // $select -> group(TABLE_CONTACT.'.phone');
            });
        }
        
        return $result;
    }
    
    public function getItem($arrParam = null, $options = null){
    
        if($options == null) {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('id', $arrParam['id']);
            })->toArray();
        }
        
        if($options['task'] == 'by-phone') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('phone', $arrParam['phone']);
            })->toArray();
        }
        
        if($options['task'] == 'by-contact') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('contact_id', $arrParam['contact_id']);
                $select -> order(array('created DESC'));
            })->toArray();
        }
    
        return current($result);
    }
    
    public function saveItem($arrParam = null, $options = null){
        $arrData        = $arrParam['data'];
        $arrItem        = $arrParam['item'];
        $arrRoute       = $arrParam['route'];
        
        $dateFormat     = new \ZendX\Functions\Date();
        $filter         = new \ZendX\Filter\Purifier();
        $gid            = new \ZendX\Functions\Gid();
        
        // Thêm mới liên hệ - NamNV
        if($options['task'] == 'add-item') {
            $id = $gid->getId();
            
            // Xác định những phần tử lưu vào options
            $item_options = array(
                'name'                      => $arrData['name'],
                'email'                     => $arrData['email'],
                'job'                       => $arrData['job'],
                'phone'                     => $arrData['phone'],
                'phone_2'                   => $arrData['phone_2'],
                'phone_3'                   => $arrData['phone_3'],
                'address'                   => $arrData['address'],
                'address_2'                 => $arrData['address_2'],
                'address_3'                 => $arrData['address_3'],
                'location_city_id'          => $arrData['location_city_id'],
                'location_district_id'      => $arrData['location_district_id'],
                'birthday'                  => $arrData['birthday'] ? $arrData['birthday'] : null,
                'birthday_year'             => $arrData['birthday_year'] ? $arrData['birthday_year'] : null,
                'product_id'                => $arrData['product_id'] ? $arrData['product_id'] : null,
                'marketing_channel_id'      => $arrData['marketing_channel_id'],
                'sex'                       => $arrData['sex'],
                'marketing_team_id'         => $arrData['marketing_team_id'],
                'note'                      => $arrData['note'],
                'level'                     => 'l1',
                'type'                      => 'new',
                'type_c3'                   => $arrData['type_c3'],
                'sale_branch_id'            => $arrData['sale_branch_id'] ? $arrData['sale_branch_id'] : $this->userInfo->getUserInfo('sale_branch_id'),
                'sale_group_id'             => $arrData['sale_group_id'] ? $arrData['sale_group_id'] : $this->userInfo->getUserInfo('sale_group_id'),
                'created'                   => date('Y-m-d H:i:s'),
            );
            
            $data   = array(
                'id'                    => $id,
                'contact_id'            => $arrParam['contact_id'],
                'options'               => !empty($item_options) ? serialize($item_options) : null,
                'created'               => date('Y-m-d H:i:s'),
                'created_by'            => $this->userInfo->getUserInfo('id'),
            );
            
            $this->tableGateway->insert($data);
            
            return $id;
        }

        // Đăng ký lại khách hàng kho - NamNV
        if($options['task'] == 'register-store') {
            $id = $arrItem['id'];
            $data   = array(
                'date'              => date('Y-m-d H:i:s'),
                'store'             => null,
                'user_id'           => $this->userInfo->getUserInfo('id'),
                'sale_branch_id'    => $this->userInfo->getUserInfo('sale_branch_id'),
                'sale_group_id'     => $this->userInfo->getUserInfo('sale_group_id'),
            );
            
            if(!empty($arrData['source_group_id'])) {
                $data['source_group_id'] = $arrData['source_group_id'];
            }
            
            $this->tableGateway->update($data, array('id' => $id));
            
            // Thêm lịch sử hệ thống
            if(!empty($id)) {
                $arrCheckLogs = array('source_group_id', 'source_known_id', 'user_id', 'sale_branch_id', 'sale_group_id');
                $arrCheckResult = array();
                foreach ($arrCheckLogs AS $field) {
                    if($data[$field] != $arrItem[$field]) {
                        if(isset($data[$field])) {
                            $arrCheckResult[$field] = $data[$field];
                        }
                    }
                }
            
                if(!empty($arrCheckResult)) {
                    $arrParamLogs = array(
                        'data' => array(
                            'title'          => 'Liên hệ',
                            'phone'          => $arrItem['phone'],
                            'name'           => $arrItem['name'],
                            'action'         => 'Nhập kho',
                            'contact_id'     => $arrItem['id'],
                            'options'        => $arrCheckResult
                        )
                    );
                    $logs = $this->getServiceLocator()->get('Admin\Model\LogsTable')->saveItem($arrParamLogs, array('task' => 'add-item'));
                }
            }
            
            return $id;
        }
        
        // Chuyển người quản lý - NamNV
        if($options['task'] == 'change-user') {
            $arrUser = $arrParam['user'];
            
            $contact_ids = explode(',', $arrData['contact_ids']);
            if(count($contact_ids) > 0) {
                $data = array(
                    'date'            => date('Y-m-d H:i:s'),
                    'store'           => null,
                    'user_id'         => $arrUser['id'],
                    'sale_branch_id'  => $arrUser['sale_branch_id'],
                    'sale_group_id'   => $arrUser['sale_group_id'],
                );
                $where = new Where();
                $where->in('id', $contact_ids);
                $this->tableGateway->update($data, $where);
                
                // Thêm lịch sử hệ thống
                $arrCheckResult = array(
                    'contact_ids'     => $contact_ids,
                    'user_id'         => $arrUser['id'],
                    'sale_branch_id'  => $arrUser['sale_branch_id'],
                    'sale_group_id'   => $arrUser['sale_group_id'],
                );
                $arrParamLogs = array(
                    'data' => array(
                        'title'          => 'Liên hệ',
                        'phone'          => null,
                        'name'           => null,
                        'action'         => 'Chuyển quản lý',
                        'contact_id'     => null,
                        'options'        => $arrCheckResult
                    )
                );
                $logs = $this->getServiceLocator()->get('Admin\Model\LogsTable')->saveItem($arrParamLogs, array('task' => 'add-item'));
            }
                
            return count($contact_ids);
        }
        
        // Chuyển người quản lý khi chuyển hợp đồng - NamNV
        if($options['task'] == 'contract-change-user') {
            if(!empty($arrData['contract_id'])) {
                $data = array(
                    'store'           => null,
                    'user_id'         => $arrData['user_id'],
                    'sale_branch_id'  => $arrData['sale_branch_id'],
                    'sale_group_id'   => $arrData['sale_group_id'],
                );
                $where = new Where();
                $where->equalTo('id', $arrData['id']);
                $this->tableGateway->update($data, $where);
            }
                
            return count($contact_ids);
        }
        
        // Chuyển người quản lý khi chuyển nhượng hợp đồng
        if($options['task'] == 'contract-transfer') {
            if(!empty($arrData['id'])) {
                $id = $arrData['id'];
                $data = array(
                    'store'                 => null,
                    'date'                  => date('Y-m-d H:i:s'),
                    'user_id'               => $arrData['user_id'],
                    'company_branch_id'     => $arrData['company_branch_id'],
                    'company_group_id'      => $arrData['company_group_id'],
                );
                $where = new Where();
                $where->equalTo('id', $id);
                $this->tableGateway->update($data, $where);
            }
             
            return $id;
        }
        
        //Thêm lịch sử chăm sóc
        /*if($options['task'] == 'add-history') {
            $id = $arrData['contact_coincide_id'];
            $options = $arrItem['history'] ? unserialize($arrItem['history']) : array();
            
            if (!empty($arrData['history_content'])){
                $options[] = array(
                    'history_action_id'       => $arrData['history_action_id'],
                    'history_result_id'       => $arrData['history_result_id'],
                    'history_content'         => $arrData['history_content'],
                    'created_by'              => $this->userInfo->getUserInfo('id'),
                    'created'                 => date('Y-m-d H:i:s'),
                    'history_return'          => $arrData['history_return'] ? $dateFormat->formatToData($arrData['history_return']) : '',
                );
            }
            $data['history'] = serialize($options);
            $this->tableGateway->update($data, array('id' => $id));
            return $id;
        }
        */
        // Đổi mật khẩu - NamNV
        /*if($options['task'] == 'change-password') {
            $item_options = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
            if(isset($arrData['password_status'])) {
                $item_options['password_status'] = $arrData['password_status'];
            }
        
            $id = $arrData['id'];
            $data = array(
                'password'    => md5($arrData['password']),
                'options'     => serialize($item_options),
            );
            
            $this->tableGateway->update($data, array('id' => $id));
        
            // Thêm lịch sử hệ thống
            $arrParamLogs = array(
                'data' => array(
                    'title'          => 'Liên hệ',
                    'phone'          => $arrItem['phone'],
                    'name'           => $arrItem['name'],
                    'action'         => 'Đổi mật khẩu',
                    'contact_id'     => $arrItem['id'],
                    'options'        => null
                )
            );
            $logs = $this->getServiceLocator()->get('Admin\Model\LogsTable')->saveItem($arrParamLogs, array('task' => 'add-item'));
        
            return $id;
        }
        */
        // Import - Insert
        if($options['task'] == 'import-insert') {
            $id = $gid->getId();
            
            // Xác định những phần tử lưu vào options
            $item_options = array();
            $item_options['password_status'] = 1;
            if(!empty($arrData['address'])) {
                $item_options['address'] = $arrData['address'];
            }
            if(!empty($arrData['facebook'])) {
                $item_options['facebook'] = $arrData['facebook'];
            }
            if(!empty($arrData['subject_id'])) {
                $item_options['subject_id'] = $arrData['subject_id'];
            }
            if(!empty($arrData['school_id'])) {
                $item_options['school_id'] = $arrData['school_id'];
            }
            if(!empty($arrData['major_id'])) {
                $item_options['major_id'] = $arrData['major_id'];
            }
            if(!empty($arrData['note'])) {
                $item_options['note'] = $arrData['note'];
            }
            if(!empty($arrData['product_ids'])) { // Sản phẩm đã mua
                $item_options['product_ids'] = $arrData['product_ids'];
            }
            if(!empty($arrData['history_action_id'])) {
                $item_options['history_created_by']   = $this->userInfo->getUserInfo('id');
                $item_options['history_action_id']    = $arrData['history_action_id'];
                $item_options['history_result_id']    = $arrData['history_result_id'];
                $item_options['history_content']      = $arrData['history_content'];
                $item_options['history_count']        = !empty($item_options['history_count']) ? $item_options['history_count'] + 1 : 1;
            }
            if(!empty($arrData['test_ielts_listen'])) {
                $item_options['test_ielts_listen'] = $arrData['test_ielts_listen'];
            }
            if(!empty($arrData['test_ielts_speak'])) {
                $item_options['test_ielts_speak'] = $arrData['test_ielts_speak'];
            }
            if(!empty($arrData['test_ielts_read'])) {
                $item_options['test_ielts_read'] = $arrData['test_ielts_read'];
            }
            if(!empty($arrData['test_ielts_write'])) {
                $item_options['test_ielts_write'] = $arrData['test_ielts_write'];
            }
            $data   = array(
                'id'                    => $id,
                'date'                  => date('Y-m-d H:i:s'),
                'name'                  => $arrData['name'],
                'phone'                 => !empty($arrData['phone']) ? $arrData['phone'] : date('dmYHis'),
                'email'                 => $arrData['email'],
                'password'              => md5('12345678'),
                'sex'                   => $arrData['sex'] ? $arrData['sex'] : 'khong-xac-dinh',
                'source_group_id'       => $arrData['source_group_id'],
                'birthday_year'         => $arrData['birthday_year'] ? $arrData['birthday_year'] : null,
                'location_city_id'      => $arrData['location_city_id'],
                'location_district_id'  => $arrData['location_district_id'],
                'product_id'            => $arrData['product_id'],
                'type'                  => $arrData['type'],
                'lost_id'               => null,
                'product_id'            => $arrData['product_id'] ? $arrData['product_id'] : null,
                'contract_total'        => $arrData['contract_total'] ? $arrData['contract_total'] : 0,
                'history_created'       => $arrData['history_action_id'] ? date('Y-m-d H:i:s') : null,
                'history_return'        => $arrData['history_return'] ? $dateFormat->formatToData($arrData['history_return'], 'Y-m-d') : null,
                'store'                 => null,
                'user_id'               => $arrData['user_id'] ? $arrData['user_id'] : $this->userInfo->getUserInfo('id'),
                'sale_branch_id'        => $arrData['sale_branch_id'] ? $arrData['sale_branch_id'] : $this->userInfo->getUserInfo('sale_branch_id'),
                'sale_group_id'         => $arrData['sale_group_id'] ? $arrData['sale_group_id'] : $this->userInfo->getUserInfo('sale_group_id'),
                'options'               => !empty($item_options) ? serialize($item_options) : null,
                'created'               => date('Y-m-d H:i:s'),
                'created_by'            => $this->userInfo->getUserInfo('id'),
            );
            if($data['type'] == 'lost') {
                $data['lost_id'] = $arrData['lost_id'];
            }
            $this->tableGateway->insert($data);
            
            // Thêm lịch sử hệ thống
            if(!empty($id)) {
                $arrParamLogs = array(
                    'data' => array(
                        'title'          => 'Liên hệ',
                        'phone'          => $arrData['phone'],
                        'name'           => $arrData['name'],
                        'action'         => 'Import',
                        'contact_id'     => $id,
                        'options'        => array(
                            'source_group_id'        => $data['source_group_id'],
                            'user_id'                => $data['user_id'],
                            'sale_branch_id'         => $data['sale_branch_id'],
                            'sale_group_id'          => $data['sale_group_id'],
                            'location_city_id'       => $data['location_city_id'],
                            'location_district_id'   => $data['location_district_id'],
                        )
                    ) 
                );
                $logs = $this->getServiceLocator()->get('Admin\Model\LogsTable')->saveItem($arrParamLogs, array('task' => 'add-item'));
            }
            
            return $id;
        }
        
        // Import - Update
        if($options['task'] == 'import-update') {
            $id = $arrData['id'];
            $data = array();
            // Xác định những phần tử lưu vào options
            $item_options = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
            if(isset($arrData['password_status'])) {
                $item_options['password_status']  = $arrData['password_status'];
            }
            if(isset($arrData['address'])) {
                $item_options['address']  = $arrData['address'];
            }
            if(isset($arrData['facebook'])) {
                $item_options['facebook']  = $arrData['facebook'];
            }
            if(isset($arrData['note'])) {
                $item_options['note']  = $arrData['note'];
            }
            if(isset($arrData['product_id'])) { // Sản phẩm quan tâm
                $item_options['product_id']  = $arrData['product_id'];
            }
            if(isset($arrData['product_ids'])) { // Sản phẩm đã mua
                $item_options['product_ids']  = $arrData['product_ids'];
            }
        
            // Dữ liệu bảng
            if(!empty($arrData['date'])) {
                $data['date'] = $dateFormat->formatToData($arrData['date'], 'Y-m-d H:i:s');
            }
            if(!empty($arrData['name'])) {
                $data['name'] = $arrData['name'];
            }
            if(!empty($arrData['phone'])) {
                $data['phone'] = $arrData['phone'];
            }
            if(isset($arrData['email'])) {
                $data['email'] = $arrData['email'];
            }
            if(!empty($arrData['password'])) {
                $data['password'] = md5($arrData['password']);
            }
            if(!empty($arrData['sex'])) {
                $data['sex'] = $arrData['sex'];
            }
            if(!empty($arrData['source_group_id'])) {
                $data['source_group_id'] = $arrData['source_group_id'];
            }
            if(isset($arrData['birthday'])) {
                $data['birthday'] = !empty($arrData['birthday']) ? $dateFormat->formatToData($arrData['birthday'], 'Y-m-d') : null;
                $data['birthday_year'] = !empty($arrData['birthday']) ? $dateFormat->formatToData($arrData['birthday'], 'Y') : null;
            }
            if(isset($arrData['birthday_year'])) {
                $data['birthday_year'] = !empty($arrData['birthday_year']) ? $arrData['birthday_year'] : null;
            }
            if(isset($arrData['location_city_id'])) {
                $data['location_city_id'] = $arrData['location_city_id'];
            }
            if(isset($arrData['location_district_id'])) {
                $data['location_district_id'] = $arrData['location_district_id'];
            }
            if(!empty($arrData['type'])) {
                $data['type'] = $arrData['type'];
                if($data['type'] == 'lost') {
                    $data['lost_id'] = $arrData['lost_id'];
                } else {
                    $data['lost_id'] = null;
                }
            }
            if(isset($arrData['contract_total'])) {
                $data['contract_total'] = !empty($arrData['contract_total']) ? $arrData['contract_total'] : null;
            }
            if(!empty($arrData['history_created'])) {
                $data['history_created'] = $dateFormat->formatToData($arrData['history_created'], 'Y-m-d H:i:s');
            }
            if(!empty($arrData['history_return'])) {
                $data['history_return'] = $dateFormat->formatToData($arrData['history_return'], 'Y-m-d');
            }
            if(!empty($arrData['store'])) {
                if($arrData['store'] == 'null') {
                    $data['store'] = null;
                } else {
                    $data['store'] = $dateFormat->formatToData($arrData['store'], 'Y-m-d H:i:s');
                }
            }
            if(!empty($arrData['user_id'])) {
                $data['user_id'] = $arrData['user_id'];
            }
            if(!empty($arrData['sale_branch_id'])) {
                $data['sale_branch_id'] = $arrData['sale_branch_id'];
            }
            if(!empty($arrData['sale_group_id'])) {
                $data['sale_group_id'] = $arrData['sale_group_id'];
            }
            if(!empty($item_options)) {
                $data['options'] = serialize($item_options);
            }
        
            $this->tableGateway->update($data, array('id' => $id));
        
            return $id;
        }
        
        // Cập nhật lại thông tin chăm sóc cuối cùng vào liên hệ - NamNV
        if($options['task'] == 'import-history') {
            $item_options = !empty($arrItem['options']) ? unserialize($arrItem['options']) : array();
            $item_options['history_created_by']       = $arrItem['user_id'];
            $item_options['history_action_id']        = $arrData['history_action_id'];
            $item_options['history_result_id']        = $arrData['history_result_id'];
            $item_options['history_content']          = $arrData['history_content'];
            $item_options['history_count']            = !empty($item_options['history_count']) ? $item_options['history_count'] + 1 : 1;
        
            $id = $arrData['id'];
            $data   = array(
                'history_created'     => $arrData['history_created'] ? $dateFormat->formatToData($arrData['history_created'], 'Y-m-d H:i:s') : date('Y-m-d H:i:s'),
                'history_return'      => $arrData['history_return'] ? $dateFormat->formatToData($arrData['history_return']) : null,
                'options'             => serialize($item_options),
            );
            $this->tableGateway->update($data, array('id' => $id));
            return $id;
        }
    }

    public function deleteItem($arrParam = null, $options = null){
        if($options == null) {
            $arrData  = $arrParam['data'];
            $arrRoute = $arrParam['route'];
            $where = new Where();
            $where->equalTo('id', $arrData['id']);
            $this->tableGateway->delete($where);

            return $result;
        }
        if ($options == 'delete-items') {
            $arrData  = $arrParam['item'];
            $arrRoute = $arrParam['route'];
            
            $where = new Where();
            $where->equalTo('contact_id', $arrData['contact_id']);
            $this->tableGateway->delete($where);
            
            $result = count($arrData['contact_id']);
        
            return $result;
        }

        if ($options == 'delete-coincide') {
            $where = new Where();
            $where->in('id', $arrParam);
            $this->tableGateway->delete($where);
            return true;
        }
    }

    public function changeContactCoincide($arrParam = null, $options = null){
        $where = new Where();
        $contact_ids = array();

        foreach ($arrParam as $item) {
            $result                  = $this->tableGateway->select(array('id' => $item))->current();
            $newUser['contact_id']   = $result->contact_id;
            $newUser['new_user_id']  = $result->created_by;
            $this->getServiceLocator()->get('Admin\Model\ContactTable')->changeContactUser($newUser);
            $contact_ids[] = $result->contact_id;
        }

        $where->equalTo('contact_id', $contact_ids);
        $this->tableGateway->delete($where);
        return true;
    }
    
    public function changeStatus($arrParam = null, $options = null){
        if($options['task'] == 'change-status') {
            $result = $this->defaultStatus($arrParam, null);
        }
         
        return $result;
    }
    
    public function changeOrdering($arrParam = null, $options = null){
        if($options['task'] == 'change-ordering') {
            $result = $this->defaultOrdering($arrParam, null);
        }
        return $result;
    }
}