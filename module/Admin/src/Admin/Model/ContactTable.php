<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class ContactTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $date      = new \ZendX\Functions\Date();
                
                $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT('. TABLE_CRM_CONTACT_COURSE .'.id)')));
                $select->join(TABLE_CRM_CONTACT, TABLE_CRM_CONTACT .'.id = '. TABLE_CRM_CONTACT_COURSE .'.contact_id',
                    array(), 'inner'
                );
                
                $select -> where -> equalTo(TABLE_CRM_CONTACT_COURSE .'.type', 0);
                $select -> where -> NEST
                                 -> notEqualTo(TABLE_CRM_CONTACT_COURSE .'.code_type', 'free')
                                 -> or
                                 -> isNull(TABLE_CRM_CONTACT_COURSE .'.code_type')
                                 -> UNNEST;
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.status', $ssFilter['filter_status']);
				}
				
				if(isset($ssFilter['filter_pending']) && $ssFilter['filter_pending'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.pending', $ssFilter['filter_pending']);
				}
				
				if(!empty($ssFilter['filter_course_id'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.course_id', $ssFilter['filter_course_id']);
				}
				
				if(!empty($ssFilter['filter_payment_id'])) {
				    if($ssFilter['filter_payment_id'] == 'code') {
				        $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.checked_by', 'system');
				    } else {
				        $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.payment_id', $ssFilter['filter_payment_id']);
				    }
				}
				
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like(TABLE_CRM_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
				if(!empty($ssFilter['filter_date_begin'])) {
				    $select->where->greaterThanOrEqualTo(TABLE_CRM_CONTACT_COURSE .'.created', $date->fomartToData($ssFilter['filter_date_begin']));
				}
				 
				if(!empty($ssFilter['filter_date_end'])) {
				    $select->where->lessThanOrEqualTo(TABLE_CRM_CONTACT_COURSE .'.created', $date->fomartToData($ssFilter['filter_date_end']) . ' 23:59:59');
				}
            })->current();
	    }
	    
	    if($options['task'] == 'list-free') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT('. TABLE_CRM_CONTACT_COURSE .'.id)')));
                $select->join(TABLE_CRM_CONTACT, TABLE_CRM_CONTACT .'.id = '. TABLE_CRM_CONTACT_COURSE .'.contact_id',
                    array(), 'inner'
                );
                
                $select -> where -> notEqualTo(TABLE_CRM_CONTACT_COURSE .'.type', 0);
                
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.status', $ssFilter['filter_status']);
				}
				
				if(!empty($ssFilter['filter_course_id'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.course_id', $ssFilter['filter_course_id']);
				}
				
				if(!empty($ssFilter['filter_type'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.type', $ssFilter['filter_type']);
				}
				
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like(TABLE_CRM_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.address', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                $date      = new \ZendX\Functions\Date();
                $select->join(TABLE_CRM_CONTACT, TABLE_CRM_CONTACT .'.id = '. TABLE_CRM_CONTACT_COURSE .'.contact_id', 
                    array(
                        'contact_name'      => 'name',
                        'contact_phone'     => 'phone',
                        'contact_email'     => 'email',
                        'contact_address'   => 'address',
                        'contact_sex'       => 'sex',
                        'contact_birthday'  => 'birthday',
                        'contact_location_city_id'  => 'location_city_id',
                        'contact_career_id'  => 'career_id',
                    ),
                    'inner'
                );
                
                $select -> where -> equalTo(TABLE_CRM_CONTACT_COURSE .'.type', 0);
                $select -> where -> NEST
                                 -> notEqualTo(TABLE_CRM_CONTACT_COURSE .'.code_type', 'free')
                                 -> or
                                 -> isNull(TABLE_CRM_CONTACT_COURSE .'.code_type')
                                 -> UNNEST;
                
			    if($options['paginator'] != false || !isset($options['paginator'])) {
                    $select -> order(array('created' => 'DESC'))
                        -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.status', $ssFilter['filter_status']);
				}
				
				if(isset($ssFilter['filter_pending']) && $ssFilter['filter_pending'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.pending', $ssFilter['filter_pending']);
				}
				
				if(!empty($ssFilter['filter_course_id'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.course_id', $ssFilter['filter_course_id']);
				}
				
				if(!empty($ssFilter['filter_payment_id'])) {
				    if($ssFilter['filter_payment_id'] == 'code') {
				        $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.checked_by', 'system');
				    } else {
				        $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.payment_id', $ssFilter['filter_payment_id']);
				    }
				}
				
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like(TABLE_CRM_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
				if(!empty($ssFilter['filter_date_begin'])) {
				    $select->where->greaterThanOrEqualTo(TABLE_CRM_CONTACT_COURSE .'.created', $date->fomartToData($ssFilter['filter_date_begin']));
				}
					
				if(!empty($ssFilter['filter_date_end'])) {
				    $select->where->lessThanOrEqualTo(TABLE_CRM_CONTACT_COURSE .'.created', $date->fomartToData($ssFilter['filter_date_end']) . ' 23:59:59');
				}
			});
		}
		
		if($options['task'] == 'list-free') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select->join(TABLE_CRM_CONTACT, TABLE_CRM_CONTACT .'.id = '. TABLE_CRM_CONTACT_COURSE .'.contact_id', 
                    array(
                        'contact_name'      => 'name',
                        'contact_phone'     => 'phone',
                        'contact_email'     => 'email',
                        'contact_address'   => 'address',
                    ),
                    'inner'
                );
                
                $select -> where -> notEqualTo(TABLE_CRM_CONTACT_COURSE .'.type', 0);
                
                if($options['paginator'] != false || !isset($options['paginator'])) {
                    $select -> order(array('created' => 'DESC'))
                            -> limit($paginator['itemCountPerPage'])
    				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.status', $ssFilter['filter_status']);
				}
				
				if(!empty($ssFilter['filter_course_id'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.course_id', $ssFilter['filter_course_id']);
				}
				
				if(!empty($ssFilter['filter_type'])) {
				    $select->where->equalTo(TABLE_CRM_CONTACT_COURSE .'.type', $ssFilter['filter_type']);
				}
				
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like(TABLE_CRM_CONTACT .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.email', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like(TABLE_CRM_CONTACT .'.address', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
			});
		}
		
		if($options['task'] == 'list-free-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select->join(TABLE_CRM_CONTACT, TABLE_CRM_CONTACT .'.id = '. TABLE_CRM_CONTACT_COURSE .'.contact_id', 
                    array(
                        'contact_name'      => 'name',
                        'contact_phone'     => 'phone',
                        'contact_email'     => 'email',
                        'contact_address'   => 'address',
                    ),
                    'inner'
                );
                
                $select -> where -> equalTo(TABLE_CRM_CONTACT .'.type', 'free');
			});
		}
		
		if($options['task'] == 'contact-course') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select -> join(TABLE_COURSE, TABLE_COURSE .'.id = '. TABLE_CRM_CONTACT_COURSE .'.course_id', array( 'course_name' => 'name', 'course_alias' => 'alias', 'course_price' => 'price', 'course_image' => 'image', 'course_image_medium' => 'image_medium' ), 'inner');
                
                $select -> order(array('created' => 'DESC'))
                		-> where->equalTo('contact_id', $arrParam['data']['contact_id']);
			});
		}
		
		if($options['task'] == 'my-course') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select -> join(TABLE_COURSE, TABLE_COURSE .'.id = '. TABLE_CRM_CONTACT_COURSE .'.course_id', array( 'course_name' => 'name', 'course_alias' => 'alias', 'course_price' => 'price', 'course_vote' => 'vote', 'course_image' => 'image', 'course_image_medium' => 'image_medium' ), 'inner')
		                -> join(TABLE_TEACHER, TABLE_TEACHER .'.id = '. TABLE_COURSE .'.teacher_id', array( 'teacher_name' => 'name', 'teacher_alias' => 'alias' ), 'inner');
		
		        $select -> order(array('created' => 'DESC'))
		                -> where -> equalTo(TABLE_CRM_CONTACT_COURSE .'.contact_id', $arrParam['data']['contact_id'])
		                         -> equalTo(TABLE_CRM_CONTACT_COURSE .'.status', 1);
		    })->toArray();
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}

		if($options['task'] == 'check-course') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select->where->equalTo('contact_id', $arrParam['contact_id'])
		                      ->equalTo('course_id', $arrParam['course_id']);
		    })->current();
		}
		
		if($options['task'] == 'check-course-access') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select->where->equalTo('contact_id', $arrParam['contact_id'])
                              ->equalTo('course_id', $arrParam['course_id'])
                              ->equalTo('status', 1)
                              ->equalTo('pending', 1);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData       = $arrParam['data'];
	    $arrRoute      = $arrParam['route'];
	    $arrContact    = $arrParam['contact'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $date     = new \ZendX\Functions\Date();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'contact_id'    => $arrContact['id'],
				'course_id'     => $arrData['course_id'],
				'payment_id'    => $arrData['payment_id'],
				'status'        => 1,
				'pending'       => 0,
				'day_number'    => 365,
				'created'       => date('Y-m-d H:i:s'),
			);
			
			if(!empty($arrData['type'])) {
			    $data['type'] = $arrData['type'];
			}
			
			$this->tableGateway->insert($data);
			return $id;
		}
	    
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          => $arrData['name'],
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'modified'      => date('Y-m-d H:i:s'),
				'modified_by'   => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'update-checked') {
		    $id = $arrData['id'];
			$data	= array(
				'pending'      => 1,
				'checked'      => date('Y-m-d H:i:s'),
				'checked_by'   => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'buy-course') {
		    $contact_course = $this->getItem(array('contact_id' => $arrData['contact_id'], 'course_id' => $arrData['course_id']), array('task' => 'check-course'));
		    if(!empty($contact_course)) {
		        if($contact_course['pending'] == 0) {
		            $id = $contact_course['id'];
		            $data	= array(
		                'payment_id'  => $arrData['payment_id'],
		                'created'     => date('Y-m-d H:i:s'),
		            );
		            	
		            $this->tableGateway->update($data, array('id' => $id));
		        }
		    } else {
    		    $id = $gid->getId();
    		    $data	= array(
    		        'id'            => $id,
    		        'contact_id'    => $arrData['contact_id'],
    		        'course_id'     => $arrData['course_id'],
    		        'payment_id'    => $arrData['payment_id'],
    		        'created'       => date('Y-m-d H:i:s'),
    		        'day_number'    => 365
    		    );
    		    
    		    if(!empty($arrData['type'])) {
    		        $data['type'] = $arrData['type'];
    		    }
    		    
    		    $this->tableGateway->insert($data);
		    }
		    
		    // Update thông tin của liên hệ
		    $arrParamContact = array();
		    $arrParamContact['data']['id'] = $arrData['contact_id'];
		    $arrParamContact['data']['name'] = $arrData['name'];
		    $arrParamContact['data']['phone'] = $arrData['phone'];
		    $arrParamContact['data']['address'] = $arrData['address'];
		    $contact = $this->getServiceLocator()->get('Admin\Model\ContactTable')->saveItem($arrParamContact, array('task' => 'update-item'));
		    
		    return $id;
		}
		
		if($options['task'] == 'buy-course-code-add') {
		    $id = $gid->getId();
		    $data	= array(
		        'id'            => $id,
		        'contact_id'    => $arrData['contact_id'],
		        'course_id'     => $arrData['course_id'],
		        'payment_id'    => null,
		        'status'        => 1,
		        'pending'       => 1,
		        'code'          => $arrData['code'],
		        'code_type'     => $arrParam['code']['code_type'],
		        'checked'       => date('Y-m-d H:i:s'),
		        'checked_by'    => 'system',
		        'created'       => date('Y-m-d H:i:s'),
		        'day_number'    => 365
		    );
		    
		    if(!empty($arrData['type'])) {
		        $data['type'] = $arrData['type'];
		    }
		    
		    $this->tableGateway->insert($data);
		    
		    return $id;
		}
		
		if($options['task'] == 'buy-course-code-update') {
		    $id = $arrData['id'];
		    $data	= array(
		        'status'        => 1,
		        'pending'       => 1,
		        'code'          => $arrData['code'],
		        'code_type'     => $arrParam['code']['code_type'],
		        'checked'       => date('Y-m-d H:i:s'),
		        'checked_by'    => 'system',
		    );
		    $this->tableGateway->update($data, array('id' => $id));
		    
		    return $id;
		}
		
		if($options['task'] == 'add-free') {
		    $id = $gid->getId();
		    $data	= array(
		        'id'            => $id,
		        'contact_id'    => $arrData['contact_id'],
		        'course_id'     => $arrData['course_id'],
		        'payment_id'    => null,
		        'code'          => 'langmaster',
		        'status'        => 1,
		        'pending'       => 1,
		        'day_number'    => $arrData['day_number'],
		        'day_end'       => $date->add(date('d/m/Y'), $arrData['day_number'], 'Y-m-d'),
		        'checked'       => date('Y-m-d H:i:s'),
		        'checked_by'    => 'system',
		        'created'       => date('Y-m-d H:i:s'),
		    );

		    if(!empty($arrData['type'])) {
		        $data['type'] = $arrData['type'];
		    }
		    
		    $this->tableGateway->insert($data);
		    return $id;
		}
		
		if($options['task'] == 'update-type') {
		    $id = $arrParam['id'];
		    $data	= array(
		        'type'          => 1,
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
		
		if($options['task'] == 'update-lesson') {
		    $id = $arrParam['data']['id'];
		    $item = $this->getItem(array('id' => $id));
		    $arrIdLesson = unserialize($item['lesson_id']);
		    $arrIdLesson[$arrParam['route']['id']]  = $arrParam['route']['id'];
 
 		    $data	= array(
		        'lesson_id'          => serialize($arrIdLesson),
		    );
		     
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}