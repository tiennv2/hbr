<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;


class CourseDetailTable extends DefaultTable {
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	        	if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
		
				if(!empty($ssFilter['filter_item'])) {
					$select -> where -> equalTo('item_id', $ssFilter['filter_item']);
				}
	        })->count();
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$paginator = $arrParam['paginator'];
				$ssFilter  = $arrParam['ssFilter'];
				$select -> join(TABLE_COURSE_ITEM, TABLE_COURSE_ITEM .'.id = '. TABLE_COURSE_DETAIL .'.item_id', array('item_name' => 'name'), 'inner')
				        -> limit($paginator['itemCountPerPage'])
						-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
		
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
					$select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
		
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
					$select -> where -> equalTo('status', $ssFilter['filter_status']);
				}
		
				if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
		
				if(!empty($ssFilter['filter_item'])) {
					$select -> where -> equalTo('item_id', $ssFilter['filter_item']);
				}
			});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'CourseDetail';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(array('created' => 'ASC'));
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
		if($options['task'] == 'user-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> join(TABLE_COURSE_GROUP, TABLE_COURSE_GROUP .'.id = '. TABLE_COURSE_DETAIL .'.group_id', array('group_name' => 'name'), 'inner')
				        -> where -> equalTo('item_id', $arrParam['data']['item_id']);
				$select -> order(array('ordering' => 'ASC', 'created' => 'ASC'));
			}) -> toArray();
		}

		if ($options['task'] == 'course-page') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select -> where -> equalTo('item_id', $arrParam['item_id']);
	        })->toArray();
	    }
	    
	    if($options['task'] == 'list-all') {
	        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select -> where -> equalTo('item_id', $arrParam['item_id']);
	            $select -> order(array('ordering' => 'ASC', 'name' => 'ASC', 'created' => 'ASC'));
	        });
	            	
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  	= $arrParam['data'];
	    $arrRoute 	= $arrParam['route'];
	    $arrCourse 	= $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem(array('id' => $arrParam['ssFilter']['filter_item']));;
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            	=> $id,
				'name'          	=> $arrData['name'],
				'alias'         	=> $arrData['alias'],
				'description'   	=> $arrData['description'],
				'content'       	=> $arrData['content'],
				'image'         	=> $image->getFull(),
				'image_medium'  	=> $image->getMedium(),
				'image_thumb'   	=> $image->getThumb(),
				'video'   			=> $arrData['video'],
				'ordering'      	=> $arrData['ordering'],
				'status'        	=> $arrData['status'],
			    'options'           => !empty($options) ? serialize($options) : null,
				'created'       	=> date('Y-m-d H:i:s'),
				'created_by'    	=> $this->userInfo->getUserInfo('id'),
				'category_id'   	=> $arrCourse['category_id'],
				'item_id'   		=> $arrCourse['id'],
			    'group_id'   		=> $arrData['group_id'],
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          	=> $arrData['name'],
			    'alias'         	=> $arrData['alias'],
				'description'   	=> $arrData['description'],
				'content'       	=> $arrData['content'],
				'image'         	=> $image->getFull(),
				'image_medium'  	=> $image->getMedium(),
				'image_thumb'   	=> $image->getThumb(),
				'video'      		=> $arrData['video'],
				'ordering'      	=> $arrData['ordering'],
				'status'        	=> $arrData['status'],
				'category_id'   	=> $arrCourse['category_id'],
				'item_id'   		=> $arrCourse['id'],
			    'group_id'   		=> $arrData['group_id'],
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'update-view') {
		    $id = $arrData['id'];
		    $data	= array(
		        'view' => new Expression('(`view` + ?)', 1),
		    );
		    $this->tableGateway->update($data, array('id' => $id));
		    
		    return $id;
		}
		
		if($options['task'] == 'add-contact-user') {
		    $id = $arrData['id'];
		    $option_item = $arrParam['item']['options']? unserialize($arrParam['item']['options']) : array();
		    
		    $option_item['view'][$arrData['contact_user_id']]['date'][date('Y-m-d')] = array(
		            'contact_user_id'     => $arrData['contact_user_id'],
		            'contact_user_name'   => $arrData['contact_user_name'],
	                'contact_user_email'  => $arrData['contact_user_email'],
		            'contact_user_phone'  => $arrData['contact_user_phone'],
		            'date_view'           => date('Y-m-d H:i:s'),
		            'seconds_view'        => $arrData['seconds_view'],
		    );

		    $data = array(
		        'options' => serialize($option_item)
		    );
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}