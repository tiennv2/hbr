<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class FormDataTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                if(!empty($ssFilter['filter_form'])) {
                	$select -> where->equalTo('form_id', $ssFilter['filter_form']);
                }
                
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		$arrData  = $arrParam['data'];
	    $arrItem  = $arrParam['item'];
	    $arrRoute = $arrParam['route'];
	    
	    $gid      = new \ZendX\Functions\Gid();

		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                
    			$select -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			if(!empty($ssFilter['filter_form'])) {
    				$select -> where->equalTo('form_id', $ssFilter['filter_form']);
    			}
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo('status', $ssFilter['filter_status']);
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    			
    		});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'Form';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    //TiênNV
	    if($options['task'] == 'list-cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'Admin'. $arrParam['where']['option'];
	        $result = $cache->getItem($cache_key);
	    
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                if(!empty($arrParam['order'])) {
	                    $select->order($arrParam['order']);
	                }
	                if(!empty($arrParam['where'])) {
	                    foreach ($arrParam['where'] AS $key => $value) {
	                        if(!empty($value)) {
	                            $select->where->equalTo($key, $value);
	                        }
	                    }
	                }
	            });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	             
	            $cache->setItem($cache_key, $result);
	        }
	    }
	    //end
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'get-by-phone') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('form_id', $arrParam['form_id'])
                                 -> equalTo('phone', $arrParam['phone'])
                                 -> greaterThanOrEqualTo('created', date('Y-m-d'));
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier(array( array('HTML.AllowedElements', '') ));
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'public-add') {
		    $contact = $this->getItem($arrData, array('task' => 'get-by-phone'));
		    
		    $data = array();
		    foreach ($arrData AS $key => $val) {
		        if(is_array($val)) {
		            $arrTmp = array();
		            foreach ($val AS $k => $v) {
		                $arrTmp[$k] = $filter->filter($v);
		            }
		            $value = serialize($arrTmp);
		        } else {
		            $value = $filter->filter($val);
		        }
		        $data[$key] = $value;
		    }
		    
		    if(!empty($arrData['form_new_id'])) {
		    	$data['form_id'] = $arrData['form_new_id'];
		    	unset($data['form_new_id']);
		    }
		    
		    if(!empty($contact)) {
		        $id = $contact['id'];
		        
		        $this->tableGateway->update($data, array('id' => $id));
		    } else {
    			$data['status']  = 0;
    			$data['created'] = date('Y-m-d H:i:s');
    			
    			$this->tableGateway->insert($data);
    			$id = $this->tableGateway->getLastInsertValue();
		    }
			return $id;
		}
	    
		if($options['task'] == 'public-update') {
		    $id = $arrData['form_data_id'];
			
			$data = array();
			
			if(!empty($arrData['company'])) {
			    $data['company'] = $arrData['company'];
			    $data['job']     = $arrData['job'];
			    $data['option']  = serialize($arrData['option']);
			    $data['content'] = $arrData['content'];
			}
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'add-history') {
			$id = $arrData['id'];
			
			$history = array(
				'history_1' => $arrData['history_1'],
				'history_2' => $arrData['history_2'],
				'history_3' => $arrData['history_3'],
			);
			
			$data = array(
				'history' => serialize($history)
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}