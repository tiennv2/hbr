<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class HistoryLevelTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')))
                        -> join(TABLE_CONTACT, TABLE_CONTACT .'.id = '. TABLE_HISTORY .'.contact_id', array(), 'inner');
                
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> equalTo(TABLE_CONTACT .'.phone', trim($ssFilter['filter_keyword']))
                                     -> OR
                                     -> equalTo(TABLE_CONTACT .'.email', trim($ssFilter['filter_keyword']))
                                     -> UNNEST;
                }
    			
	            if(!empty($ssFilter['filter_date_begin']) && !empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_begin']))
                    			     -> AND
                    			     -> lessThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59')
                    			     -> UNNEST;
    			} elseif (!empty($ssFilter['filter_date_begin'])) {
    			    $select -> where -> greaterThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_begin']));
    			} elseif (!empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> lessThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59');
    			}
    			
	            if(!empty($ssFilter['filter_sale_branch'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'sale_branch_id', $ssFilter['filter_sale_branch']);
    			}
    			
    			if(!empty($ssFilter['filter_sale_group'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'sale_group_id', $ssFilter['filter_sale_group']);
    			} else {
        			if(!empty($this->userInfo->getUserInfo('sale_group_ids'))){
        			    $select -> where -> in(TABLE_HISTORY .'.'. 'sale_group_id', explode(',', $this->userInfo->getUserInfo('sale_group_ids')));
        			}
    			}
    			
    			if(!empty($ssFilter['filter_user'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'user_id', $ssFilter['filter_user']);
    			}
    			
    			if(!empty($ssFilter['filter_action'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.action_id', $ssFilter['filter_action']);
    			}
    			
    			if(!empty($ssFilter['filter_result'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.result_id', $ssFilter['filter_result']);
    			}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                
                $select -> join(TABLE_CONTACT, TABLE_CONTACT .'.id = '. TABLE_HISTORY .'.contact_id', array('contact_name' => 'name', 'contact_phone' => 'phone', 'contact_email' => 'email', 'contact_birthday_year' => 'birthday_year', 'contact_location_city_id' => 'location_city_id', 'contact_location_district_id' => 'location_district_id'), 'inner')
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
			    if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> equalTo(TABLE_CONTACT .'.phone', trim($ssFilter['filter_keyword']))
                                     -> OR
                                     -> equalTo(TABLE_CONTACT .'.email', trim($ssFilter['filter_keyword']))
                                     -> UNNEST;
                }
    			
	            if(!empty($ssFilter['filter_date_begin']) && !empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_begin']))
                    			     -> AND
                    			     -> lessThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59')
                    			     -> UNNEST;
    			} elseif (!empty($ssFilter['filter_date_begin'])) {
    			    $select -> where -> greaterThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_begin']));
    			} elseif (!empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> lessThanOrEqualTo(TABLE_HISTORY .'.'. $ssFilter['filter_date_type'], $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59');
    			}
    			
    			if(!empty($ssFilter['filter_sale_branch'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'sale_branch_id', $ssFilter['filter_sale_branch']);
    			}
    			 
    			if(!empty($ssFilter['filter_sale_group'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'sale_group_id', $ssFilter['filter_sale_group']);
    			} else {
    			    if(!empty($this->userInfo->getUserInfo('sale_group_ids'))){
    			        $select -> where -> in(TABLE_HISTORY .'.'. 'sale_group_id', explode(',', $this->userInfo->getUserInfo('sale_group_ids')));
    			    }
    			}
    			 
    			if(!empty($ssFilter['filter_user'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.'. 'user_id', $ssFilter['filter_user']);
    			}
    			
    			if(!empty($ssFilter['filter_action'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.action_id', $ssFilter['filter_action']);
    			}
    			 
    			if(!empty($ssFilter['filter_result'])) {
    			    $select -> where -> equalTo(TABLE_HISTORY .'.result_id', $ssFilter['filter_result']);
    			}
    		});
		}
		
		if($options['task'] == 'cache') {
		    $cache = $this->getServiceLocator()->get('cache');
		    $cache_key = 'AdminContact';
		    $result = $cache->getItem($cache_key);
		    if (empty($result)) {
		        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		            $select -> order(TABLE_HISTORY_LEVEL .'created ASC');
		        });
	            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	            $cache->setItem($cache_key, $result);
		    }
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
		$arrRoute = $arrParam['route'];
	    
	    $date     = new \ZendX\Functions\Date();
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
		
		//Thêm lịch sử thay đổi level
		if($options['task'] == 'add-item') {
			$arrContact  = $arrParam['item'];

		    $id = $gid->getId();
		    $data	= array(
		        'id'                  => $id,
		        'contact_id'          => $arrContact['id'],
		        'level_left'          => $arrContact['level'],
		        'level_right'         => $arrData['level'],
		        'created'             => $arrContact['created'],
		        'modified'            => date('Y-m-d H:i:s'),
		        'modified_by'         => $this->userInfo->getUserInfo('id'),
		    );
		    $this->tableGateway->insert($data);
			
			// Thêm lịch sử hệ thống
	        $arrParamLogs = array(
	            'data' => array(
	                'title'          => 'Liên hệ',
	                'phone'          => $arrContact['phone'],
	                'name'           => $arrContact['name'],
	                'action'         => 'Thay đổi level',
	                'contact_id'     => $id,
	                'options'        => array(
	                    'level_left'    => $data['level_left'],
	                    'level_right'   => $data['level_right'],
	                    'created'       => $data['created'],
	                    'modified'  	=> $data['modified'],
	                    'modified_by'   => $data['modified_by'],
	                )
	            )
	        );
	        $logs = $this->getServiceLocator()->get('Admin\Model\LogsTable')->saveItem($arrParamLogs, array('task' => 'add-item'));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
	
	public function report($arrParam = null, $options = null){
	    if($options['task'] == 'date') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $arrData  = $arrParam['data'];
	            $arrRoute = $arrParam['route'];
	            $dateFormat = new \ZendX\Functions\Date();
	
	            $select -> columns(array('sale_branch_id', 'sale_group_id', 'user_id', 'contact_id', 'action_id', 'result_id'))
        	            -> where -> greaterThanOrEqualTo('created', $dateFormat->formatToData($arrData['date_begin']) .' 00:00:00')
        	                     -> lessThanOrEqualTo('created', $dateFormat->formatToData($arrData['date_end']) .' 23:59:59');
	             
	            if(!empty($arrData['sale_branch_id'])) {
	                $select -> where -> equalTo('sale_branch_id', $arrData['sale_branch_id']);
	            }
	        });
	    }
	    return $result;
	}
}