<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class LinkCheckingTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter   = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                $number     = new \ZendX\Functions\Number();
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
	            if(!empty($ssFilter['filter_date_begin']) && !empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo(TABLE_LINK_CHECKING . '.created', $date->formatToData($ssFilter['filter_date_begin']))
                    			     -> AND
                    			     -> lessThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59')
                    			     -> UNNEST;
    			} elseif (!empty($ssFilter['filter_date_begin'])) {
    			    $select -> where -> greaterThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_begin']));
    			} elseif (!empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> lessThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59');
				}
    			
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $paginator  = $arrParam['paginator'];
                $ssFilter   = $arrParam['ssFilter'];
                $date       = new \ZendX\Functions\Date();
                $number     = new \ZendX\Functions\Number();
                
                if(!isset($options['paginator']) || $options['paginator'] == true) {
	    			$select -> limit($paginator['itemCountPerPage'])
	    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
    			
			    if(!empty($ssFilter['filter_date_begin']) && !empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> NEST
                    			     -> greaterThanOrEqualTo(TABLE_LINK_CHECKING . '.created', $date->formatToData($ssFilter['filter_date_begin']))
                    			     -> AND
                    			     -> lessThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59')
                    			     -> UNNEST;
    			} elseif (!empty($ssFilter['filter_date_begin'])) {
    			    $select -> where -> greaterThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_begin']));
    			} elseif (!empty($ssFilter['filter_date_end'])) {
    			    $select -> where -> lessThanOrEqualTo(TABLE_LINK_CHECKING .'.created', $date->formatToData($ssFilter['filter_date_end']) . ' 23:59:59');
				}
    		});
		}
		
		if($options['task'] == 'cache') {
		    $cache = $this->getServiceLocator()->get('cache');
		    $cache_key = 'AdminLinkChecking';
		    $result = $cache->getItem($cache_key);
		    if (empty($result)) {
		        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		            $select -> order(TABLE_CONTACT .'.name ASC');
		        });
		            $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
		            $cache->setItem($cache_key, $result);
		    }
		}
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->toArray();
		}
		return current($result);
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData       = $arrParam['data'];
	    $arrItem       = $arrParam['item'];
	    $arrRoute      = $arrParam['route'];
	    
	    $dateFormat    = new \ZendX\Functions\Date();
	    $filter        = new \ZendX\Filter\Purifier();
	    $gid           = new \ZendX\Functions\Gid();
	    // Thêm mới liên hệ - NamNV
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                    => $id,
				'link'                  => $arrData['link'],
				'marketing_channel_id'  => $arrData['marketing_channel_id'],
				'marketer_id'           => $this->userInfo->getUserInfo('id'),
				'campaign'              => $arrData['campaign'],
				'type'                  => $arrData['type'],
				'content'               => $arrData['content'],
				'flexible_1'            => $arrData['flexible_1'],
				'flexible_2'            => $arrData['flexible_2'],
				'created'               => date('Y-m-d H:i:s'),
				'created_by'            => $this->userInfo->getUserInfo('id'),
			);

			$this->tableGateway->insert($data);
			
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    $data	= array(
		        'link'                  => $arrData['link'],
		        'marketing_channel_id'  => $arrData['marketing_channel_id'],
		        'marketer_id'           => $this->userInfo->getUserInfo('id'),
		        'campaign'              => $arrData['campaign'],
		        'type'                  => $arrData['type'],
		        'content'               => $arrData['content'],
		        'flexible_1'            => $arrData['flexible_1'],
		        'flexible_2'            => $arrData['flexible_2'],
		        'created'               => date('Y-m-d H:i:s'),
		        'created_by'            => $this->userInfo->getUserInfo('id'),
		    );
		    
		    $this->tableGateway->update($data, array('id' => $id));
		    	
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
}