<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class MarketingTargetTable extends DefaultTable {
    public function countItem($arrParam = null, $options = null){
        if($options == null) {
            $result = $this->tableGateway->select()->count();
        }
        
        if($options['task'] == 'list-item') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                $select -> where -> equalTo('type', 'campaign');
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                    -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like('address', '%'. $ssFilter['filter_keyword'] . '%')
                    -> UNNEST;
                }
            })->current();
        }
        
        if($options['task'] == 'list-target') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
                $ssFilter  = $arrParam['ssFilter'];
        
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
        
                $select -> where -> equalTo('type', 'target');
        
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                    -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like('address', '%'. $ssFilter['filter_keyword'] . '%')
                    -> UNNEST;
                }
            })->current();
        }
        
        return $result->count;
    }
    
    public function listItem($arrParam = null, $options = null){
        if($options['task'] == 'list-item') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
                
                if(!isset($options['paginator']) || $options['paginator'] == true) {
                    $select -> limit($paginator['itemCountPerPage'])
                    -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
                
                $select -> where -> equalTo('type', 'campaign');


                /*if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                    -> like(TABLE_MARKETING_TARGET .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like(TABLE_MARKETING_TARGET .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                    -> OR
                    -> like(TABLE_MARKETING_TARGET .'.address', '%'. $ssFilter['filter_keyword'] . '%')
                    -> UNNEST;
                }*/
            });
        }
        
        if($options['task'] == 'list-target') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
        
                if(!isset($options['paginator']) || $options['paginator'] == true) {
                    $select -> limit($paginator['itemCountPerPage'])
                    -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
        
                $select -> where -> equalTo('type', 'target');
        
        
                /*if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                 $select -> where -> NEST
                 -> like(TABLE_MARKETING_TARGET .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                 -> OR
                 -> like(TABLE_MARKETING_TARGET .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                 -> OR
                 -> like(TABLE_MARKETING_TARGET .'.address', '%'. $ssFilter['filter_keyword'] . '%')
                 -> UNNEST;
                 }*/
            });
        }
        
        if($options['task'] == 'get-all') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam){
                $date     = new \ZendX\Functions\Date();
                $arrData  = $arrParam['ssFilter'];
                if (!empty($arrParam['data']['type'])) {
                    $select -> where -> equalTo('type', $arrParam['data']['type']);
                }

                if (!empty($arrData['filter_date_begin']) && !empty($arrData['filter_date_end'])) {
                    $select -> where -> NEST
                    -> greaterThanOrEqualTo('time_start', $date->formatToData($arrData['filter_date_begin'], 'Y-m') . '-01')
                    -> AND
                    -> lessThanOrEqualTo('time_start', $date->formatToData($arrData['filter_date_end'], 'Y-m' . '-30'))
                    -> UNNEST;
                }
            });
        }
        return $result;
    }
    
    public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
		
		if($options['task'] == 'month-year') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> where -> equalTo('month', $arrParam['month'])
		                         -> equalTo('year', $arrParam['year']);
		    })->current();
		}
		
		return $result;
	}
    
    public function saveItem($arrParam = null, $options = null){
        $arrData  = $arrParam['data'];
        $arrRoute = $arrParam['route'];
        $arrItem  = $arrParam['item'];
        
        $image         = new \ZendX\Functions\Thumbnail($arrData['image']);
        $filter        = new \ZendX\Filter\Purifier();
        $gid           = new \ZendX\Functions\Gid();
        $dateFormat    = new \ZendX\Functions\Date();
        $number        = new \ZendX\Functions\Number();

        if($options['task'] == 'add-item') {
            $id = $gid->getId();
            
            $data = array(
                'id'                    => $id,
                'name'                  => $arrData['name'],
                'month'                 => $arrData['month'],
                'year'                  => $arrData['year'],
                'time_start'            => date('Y-m-d', strtotime('01'.'-'.$arrData['month'].'-'.$arrData['year'])),
                'options'               => serialize($arrData['options']),
                'type'                  => 'campaign',
                'created'               => date('Y-m-d H:i:s'),
                'created_by'            => $this->userInfo->getUserInfo('id'),
            );            
            $this->tableGateway->insert($data);
            return $id;
        }
        
        if($options['task'] == 'edit-item') {
            $id = $arrData['id'];
            
            $data = array(
                'name'                  => $arrData['name'],
                'month'                 => $arrData['month'],
                'year'                  => $arrData['year'],
                'options'               => serialize($arrData['options']),
            );
            
            $this->tableGateway->update($data, array('id' => $arrData['id']));
            return $arrData['id'];
        }
        
        if($options['task'] == 'add-target') {
            $id = $gid->getId();
             
            $params = array();
            if(!empty($arrData['params'])) {
                foreach ($arrData['params'] AS $key => $val) {
                    if(!empty($val)) {
                        foreach ($val AS $k => $v) {
                            $params[$key][$k] = $number->formatToData($v);
                        }
                    }
                }
            }
        
            $data	= array(
                'id'            => $id,
                'month'         => $arrData['month'],
                'year'          => $arrData['year'],
                'name'          => $arrData['name'],
                'options'       => serialize($params),
                'type'          => 'target',
                'time_start'    => date('Y-m-d', strtotime('01'.'-'.$arrData['month'].'-'.$arrData['year'])),
                'created'       => date('Y-m-d H:i:s'),
                'created_by'    => $this->userInfo->getUserInfo('id'),
            );
        
            $this->tableGateway->insert($data);
            return $id;
        }
        
        if($options['task'] == 'edit-target') {
            $id = $arrData['id'];
             
            $params = array();
            if(!empty($arrData['params'])) {
                foreach ($arrData['params'] AS $key => $val) {
                    if(!empty($val)) {
                        foreach ($val AS $k => $v) {
                            $params[$key][$k] = $number->formatToData($v);
                        }
                    }
                }
            }
             
            $data	= array(
                'month'        => $arrData['month'],
                'year'         => $arrData['year'],
                'name'         => $arrData['name'],
                'options'      => serialize($params),
            );
        
            $this->tableGateway->update($data, array('id' => $id));
            return $id;
        }
        
    }
    
    public function deleteItem($arrParam = null, $options = null){
        $arrData  = $arrParam['data'];
        $arrRoute = $arrParam['route'];
        
        if($options['task'] == 'delete-item') {
            $where = new Where();
            $where -> in('id', $arrData['cid']);
            $where -> notEqualTo('id', '1111111111111111111111');
            $this -> tableGateway -> delete($where);
            
            return count($arrData['cid']);
        }
        
        return false;
    }
}