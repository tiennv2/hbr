<?php

namespace Admin\Model;



use Zend\Db\Sql\Select;

use ZendX\Functions\Date;



class PageTable extends DefaultTable {



    public function countItem($arrParam = null, $options = null){

	    if($options['task'] == 'list-item') {

	        $result = $this->defaultCount($arrParam, null);

	    }

	    

	    return $result;

	}

	

	public function listItem($arrParam = null, $options = null){

		if($options['task'] == 'list-item') {

			$result	= $this->defaultList($arrParam, null);

		}

		

		if($options['task'] == 'list-all') {

			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){

				$select -> where -> equalTo('status', 1);

    		});

		}



		if ($options['task'] == 'list-page') {



            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){

                $ssSpeaker 	= $arrParam['speaker'];

                $ssCategory = $arrParam['category'];

                $ssLimit 	= (int)$arrParam['limit'] ? (int)$arrParam['limit'] : false;

                if($ssLimit){

                	$select -> limit($ssLimit);

                }



                if (!empty($ssSpeaker)) {

                    $select -> where -> like(TABLE_PAGE .'.course_options', '%'. $ssSpeaker .'%');

                }



                if (empty($ssSpeaker) && empty($ssCategory)) {

                    $select -> where -> notEqualTo(TABLE_PAGE .'.course_options', '');

                }



                if (!empty($ssCategory)) {

                    $select -> where -> like(TABLE_PAGE .'.course_options', '%'. $ssCategory .'%');

                }



                $select -> where -> equalTo('status', 1);



                $select->order(array('id' => 'DESC'));



            })->toArray();

        }

		

		return $result;

	}

	

	public function getItem($arrParam = null, $options = null){

	

		if($options == null) {

			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){

			    $select -> where -> equalTo('id', $arrParam['id']);

    		})->current();

		}

	

		if($options['task'] == 'get-by-alias') {

			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){

			    $select -> where -> equalTo('alias', $arrParam['alias']);

    		})->current();

		}

	

		return $result;

	}

	

	public function saveItem($arrParam = null, $options = null){

	    $arrData  = $arrParam['data'];

	    $arrRoute = $arrParam['route'];

	    

	    $image             = new \ZendX\Functions\Thumbnail($arrData['image']);

	    $image_course      = new \ZendX\Functions\Thumbnail($arrData['image_course']);

	    $filter            = new \ZendX\Filter\Purifier();

	    $alias             = new \ZendX\Filter\CreateAlias();

	    $gid               = new \ZendX\Functions\Gid();

	    

	    // Xử lý thêm nhiều hình ảnh

	    $images = '';

	    if(!empty($arrData['images'])) {

	        for ($i = 0; $i < count($arrData['images']['url']); $i++) {

	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['images']['url'][$i]);

	    

	            $images['url'][]      = $image_multi->getFull();

	            $images['medium'][]   = $image_multi->getMedium();

	            $images['thumb'][]    = $image_multi->getThumb();

	            $images['name'][]     = $arrData['images']['name'][$i];

	        }

	    }

	    

	    // Xử lý thêm nhiều video

	    $videos = '';

	    if(!empty($arrData['videos'])) {

	        for ($i = 0; $i < count($arrData['videos']['id']); $i++) {

	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['videos']['image'][$i]);

	    

	            $videos['image'][]    = $image_multi->getFull() ? $image_multi->getFull() : $arrData['videos']['image'][$i];

	            $videos['medium'][]   = $image_multi->getMedium() ? $image_multi->getMedium() : $arrData['videos']['image'][$i];

	            $videos['thumb'][]    = $image_multi->getThumb() ? $image_multi->getThumb() : $arrData['videos']['image'][$i];

	            $videos['name'][]     = $arrData['videos']['name'][$i];

	            $videos['id'][]       = $arrData['videos']['id'][$i];

	        }

	    }



	    $course_options = array();

	    $course_options							= $arrData['course_schedule'];

	    $course_options['course_code']			= $arrData['course_code'];

	    $course_options['course_category_id']	= $arrData['course_category_id'];

	    $course_options['course_teacher_id']	= $arrData['course_teacher_id'];

	    $course_options['course_price']			= $arrData['course_price'];



	    $course_options	= serialize($course_options);



	    $date = new Date();

		if($options['task'] == 'add-item') {

			$id = $arrData['id'] ? $arrData['id'] : $gid->getId();

			$data	= array(

			    'id'                 => $id,

			    'name'               => $arrData['name'],

			    'alias'              => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),

			    'status'             => $arrData['status'],

			    'ordering'           => $arrData['ordering'],

			    'layout'             => $arrData['layout'],

			    'form'             	 => $arrData['form'],

			    'description'        => $filter->filter($arrData['description']),

			    'content'            => $filter->filter($arrData['content']),

			    //'course_options'     => $course_options,

			    'image'              => $image->getFull(),

			    'image_medium'       => $image->getMedium(),

			    'image_thumb'        => $image->getThumb(),

			    'image_course'       => $image_course->getFull(),

			    'images'             => $images ? serialize($images) : '',

			    'videos'             => $videos ? serialize($videos) : '',

			    'created'            => @date('Y-m-d H:i:s'),

			    'created_by'         => $this->userInfo->getUserInfo('id'),

			    'meta_url'           => $arrData['meta_url'],

			    'meta_title'         => $arrData['meta_title'],

			    'meta_keywords'      => $arrData['meta_keywords'],

			    'meta_description'   => $arrData['meta_description'],

			    'box_hot'            => $arrData['box_hot'],

			    'box_highlight'      => $arrData['box_highlight'],

			    'box_home'           => $arrData['box_home'],

			    'box_footer'         => $arrData['box_footer'],

			    'box_left'           => $arrData['box_left'],

			    'box_right'          => $arrData['box_right'],

			    'form_id'            => $arrData['form_id'],

			);

			

			$this->tableGateway->insert($data);

			return $id;

		}

		

		if($options['task'] == 'edit-item') {

		    $id = $arrData['id'];

		    

			$data	= array(

				'name'               => $arrData['name'],

				'alias'              => !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),

				'status'             => $arrData['status'],

			    'ordering'           => $arrData['ordering'],

			    'layout'             => $arrData['layout'],

				'form'             	 => $arrData['form'],

			    'description'        => $filter->filter($arrData['description']),

			    'content'            => $filter->filter($arrData['content']),

			    //'course_options'     => $course_options,

			    'image'              => $image->getFull(),

			    'image_medium'       => $image->getMedium(),

			    'image_thumb'        => $image->getThumb(),

			    'image_course'       => $image_course->getFull(),

			    'images'             => $images ? serialize($images) : '',

			    'videos'             => $videos ? serialize($videos) : '',

			    'meta_url'           => $arrData['meta_url'],

			    'meta_title'         => $arrData['meta_title'],

			    'meta_keywords'      => $arrData['meta_keywords'],

			    'meta_description'   => $arrData['meta_description'],

			    'box_hot'            => $arrData['box_hot'],

			    'box_highlight'      => $arrData['box_highlight'],

			    'box_home'           => $arrData['box_home'],

			    'box_footer'         => $arrData['box_footer'],

			    'box_left'           => $arrData['box_left'],

			    'box_right'          => $arrData['box_right'],

				'form_id'          	 => $arrData['form_id'],

			);

			

			$this->tableGateway->update($data, array('id' => $id));

			return $id;

		}

	}

	

	public function deleteItem($arrParam = null, $options = null){

	    if($options['task'] == 'delete-item') {

	        $result = $this->defaultDelete($arrParam, null);

	    }

	

	    return $result;

	}

	

	public function changeStatus($arrParam = null, $options = null){

	    if($options['task'] == 'change-status') {

	        $result = $this->defaultStatus($arrParam, null);

	    }

	     

	    return $result;

	}

	

	public function changeOrdering($arrParam = null, $options = null){

	    if($options['task'] == 'change-ordering') {

	        $result = $this->defaultOrdering($arrParam, null);

	    }

	    return $result;

	}

}