<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class PaymentTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result = $this->defaultCount($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->defaultList($arrParam, null);
		}
		
	    if($options['task'] == 'list-all') {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'))
                       ->where->equalTo('status', 1);
            });
	    }
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'Payment';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'))
	                       ->where->equalTo('status', 1);
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiPayment';
	        $result = $cache->getItem($cache_key);
	    
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> where -> equalTo('status', 1);
	    
	                if(!empty($arrParam['order'])) {
	                    $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
	                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
	                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'name'          => $arrData['name'],
				'icon'          => $arrData['icon'],
			    'content'       => $filter->filter($arrData['content']),
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
				'modified'      => date('Y-m-d H:i:s'),
				'modified_by'   => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          => $arrData['name'],
			    'icon'          => $arrData['icon'],
			    'content'       => $filter->filter($arrData['content']),
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'modified'      => date('Y-m-d H:i:s'),
				'modified_by'   => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}