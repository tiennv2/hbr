<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Session\Container;

class PostItemTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
                }
                
                if(!empty($ssFilter['filter_category'])) {
                    $categorys = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
                    $ids = array();
                    foreach ($categorys AS $category) {
                        $ids[] = $category['id'];
                    }
                    if(count($ids) > 0) {
                        $select->where->in(TABLE_POST_ITEM .'.post_category_id', $ids);
                    }
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like(TABLE_POST_ITEM .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo(TABLE_POST_ITEM .'.id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $ssFilter  = $arrParam['ssFilter'];
                $ssSystem  = New Container('system');
                $paginator = $arrParam['paginator'];
                
    			$select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_LEFT )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
    			$select -> where -> equalTo(TABLE_POST_ITEM .'.language', $ssSystem->language);
    			
    			if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
    			    $select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
    			}
    			
    			if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
    			    $select->where->equalTo(TABLE_POST_ITEM .'.status', $ssFilter['filter_status']);
    			}
    			
    			if(!empty($ssFilter['filter_category'])) {
    			    $categorys = $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->listNodes(array('id' => $ssFilter['filter_category']), array('task' => 'list-branch'));
    			    $ids = array();
    			    foreach ($categorys AS $category) {
    			        $ids[] = $category['id'];
    			    }
    			    if(count($ids) > 0) {
    			        $select->where->in(TABLE_POST_ITEM .'.post_category_id', $ids);
    			    }
    			}
    			
    			if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like(TABLE_POST_ITEM .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo(TABLE_POST_ITEM .'.id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
    			
    		});
		}
		
		if($options['task'] == 'list-all') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> join( TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias', 'category_type' => 'type'), $select::JOIN_INNER );
		        $select -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);

		        if(!empty($arrParam['category_type'])) {
		        	$select -> where -> equalTo(TABLE_POST_CATEGORY .'.type', $arrParam['category_type']);
		        }
		    });
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssSystem = New Container('system');
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $date     = new \ZendX\Functions\Date();
	    $number   = new \ZendX\Functions\Number();
	    $gid      = new \ZendX\Functions\Gid();
	    $filter   = new \ZendX\Filter\Purifier();
	    $alias    = new \ZendX\Filter\CreateAlias();
	    
	    // Xử lý thêm nhiều hình ảnh
	    $images = '';
	    if(!empty($arrData['images'])) {
	        for ($i = 0; $i < count($arrData['images']['url']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['images']['url'][$i]);
	    
	            $images['url'][]      = $image_multi->getFull();
	            $images['medium'][]   = $image_multi->getMedium();
	            $images['thumb'][]    = $image_multi->getThumb();
	            $images['name'][]     = $arrData['images']['name'][$i];
	        }
	    }
	    
	    // Xử lý thêm nhiều video
	    $videos = '';
	    if(!empty($arrData['videos'])) {
	        for ($i = 0; $i < count($arrData['videos']['id']); $i++) {
	            $image_multi          = new \ZendX\Functions\Thumbnail($arrData['videos']['image'][$i]);
	    
	            $videos['image'][]    = $image_multi->getFull() ? $image_multi->getFull() : $arrData['videos']['image'][$i];
	            $videos['medium'][]   = $image_multi->getMedium() ? $image_multi->getMedium() : $arrData['videos']['image'][$i];
	            $videos['thumb'][]    = $image_multi->getThumb() ? $image_multi->getThumb() : $arrData['videos']['image'][$i];
	            $videos['name'][]     = $arrData['videos']['name'][$i];
	            $videos['id'][]       = $arrData['videos']['id'][$i];
	        }
	    }
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			
			$course_city = null;
			$course_option = null;
			if(!empty($arrData['course_option'])) {
				foreach ($arrData['course_option'] AS $key => $value) {
					if(!empty($value['date'])) {
						$course_city[] = $key;
						$course_option[$key] = $value;
					}
				}
			}
			
			$data	= array(
			    'id'                 	=> $id,
			    'name'               	=> $arrData['name'],
			    'alias'              	=> !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
			    'title'             	=> $arrData['title'],
			    'status'             	=> $arrData['status'],
			    'ordering'           	=> $arrData['ordering'],
			    'layout'             	=> $arrData['layout'],
			    'description'        	=> $arrData['description'],
			    'content'            	=> $arrData['content'],
			    'image'              	=> $image->getFull(),
			    'image_medium'       	=> $image->getMedium(),
			    'image_thumb'        	=> $image->getThumb(),
			    'image_cover'           => $arrData['image_cover'],
			    'images'             	=> $images ? serialize($images) : '',
			    'videos'             	=> $videos ? serialize($videos) : '',
			    'created'            	=> date('Y-m-d H:i:s'),
			    'created_by'         	=> $this->userInfo->getUserInfo('id'),
			    'meta_url'           	=> $arrData['meta_url'],
			    'meta_title'         	=> $arrData['meta_title'],
			    'meta_keywords'      	=> $arrData['meta_keywords'],
			    'meta_description'   	=> $arrData['meta_description'],
			    'box_hot'            	=> $arrData['box_hot'],
			    'box_highlight'      	=> $arrData['box_highlight'],
			    'box_home'           	=> $arrData['box_home'],
			    'box_footer'         	=> $arrData['box_footer'],
			    'box_left'           	=> $arrData['box_left'],
			    'box_right'          	=> $arrData['box_right'],
			    'post_category_id'   	=> $arrData['post_category_id'],
			    'product_price'   	 	=> $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null,
			    'product_sale_percent'	=> $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null,
			    'product_sale_price'	=> $arrData['product_sale_price'] ? $number->formatToData($arrData['product_sale_price']) : null,
			    'product_year'	        => $arrData['product_year'],
			    'course_code'	        => $arrData['course_code'],
			    'course_teacher_id'	    => $arrData['course_teacher_id'],
			    'course_city'	    	=> !empty($course_city) ? implode(',', $course_city) : null,
			    'course_option'	    	=> !empty($course_option) ? serialize($course_option) : null,
			    'course_price'   	 	=> $arrData['course_price'] ? $number->formatToData($arrData['course_price']) : null,
			    'course_target'   	 	=> $arrData['course_target'],
			    'course_benefit'   	 	=> $arrData['course_benefit'],
			    'course_policy'   	 	=> $arrData['course_policy'],
			    'course_list_id'   	 	=> $arrData['course_list_id'],
			    'target'   	 			=> $arrData['target'],
			    'language'              => $ssSystem->language
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    
			$course_city = null;
			$course_option = null;
			if(!empty($arrData['course_option'])) {
				foreach ($arrData['course_option'] AS $key => $value) {
					if(!empty($value['date'])) {
						$course_city[] = $key;
						$course_option[$key] = $value;
					}
				}
			}
		    
			$data	= array(
				'name'               	=> $arrData['name'],
				'alias'              	=> !empty($arrData['alias']) ? $arrData['alias'] : $alias->filter($arrData['name']),
				'title'             	=> $arrData['title'],
				'status'             	=> $arrData['status'],
			    'ordering'           	=> $arrData['ordering'],
			    'layout'             	=> $arrData['layout'],
			    'description'        	=> $arrData['description'],
			    'content'            	=> $arrData['content'],
			    'image'              	=> $image->getFull(),
			    'image_medium'       	=> $image->getMedium(),
			    'image_thumb'        	=> $image->getThumb(),
			    'image_cover'           => $arrData['image_cover'],
			    'images'             	=> $images ? serialize($images) : '',
			    'videos'             	=> $videos ? serialize($videos) : '',
				'modified'           	=> date('Y-m-d H:i:s'),
				'modified_by'        	=> $this->userInfo->getUserInfo('id'),
			    'meta_url'           	=> $arrData['meta_url'],
			    'meta_title'         	=> $arrData['meta_title'],
			    'meta_keywords'      	=> $arrData['meta_keywords'],
			    'meta_description'   	=> $arrData['meta_description'],
			    'box_hot'            	=> $arrData['box_hot'],
			    'box_highlight'      	=> $arrData['box_highlight'],
			    'box_home'           	=> $arrData['box_home'],
			    'box_footer'         	=> $arrData['box_footer'],
			    'box_left'           	=> $arrData['box_left'],
			    'box_right'          	=> $arrData['box_right'],
			    'post_category_id'   	=> $arrData['post_category_id'],
				'product_price'   	 	=> $arrData['product_price'] ? $number->formatToData($arrData['product_price']) : null,
			    'product_sale_percent'	=> $arrData['product_sale_percent'] ? $number->formatToData($arrData['product_sale_percent']) : null,
			    'product_sale_price'	=> $arrData['product_sale_price'] ? $number->formatToData($arrData['product_sale_price']) : null,
			    'product_year'	        => $arrData['product_year'],
			    'course_code'	        => $arrData['course_code'],
			    'course_teacher_id'	    => $arrData['course_teacher_id'],
			    'course_city'	    	=> !empty($course_city) ? implode(',', $course_city) : null,
			    'course_option'	    	=> !empty($course_option) ? serialize($course_option) : null,
			    'course_price'   	 	=> $arrData['course_price'] ? $number->formatToData($arrData['course_price']) : null,
			    'course_target'   	 	=> $arrData['course_target'],
			    'course_benefit'   	 	=> $arrData['course_benefit'],
			    'course_policy'   	 	=> $arrData['course_policy'],
			    'course_list_id'   	 	=> $arrData['course_list_id'],
			    'target'   	 			=> $arrData['target'],
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}