<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class ProductBatchTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result = $this->defaultCount($arrParam, null);

	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->defaultList($arrParam, null);
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'AdminProductBatch'. $arrParam['type'];
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                if(!empty($arrParam['type'])) {
	                    $select -> where -> equalTo('type', $arrParam['type']);
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
		
		if($options['task'] == 'by-id') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> where -> equalTo('id', $arrParam['id']);
		    })->toArray();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $date     = new \ZendX\Functions\Date();
	    $filter   = new \ZendX\Filter\Purifier();
	    $number   = new \ZendX\Functions\Number();
	    $gid      = new \ZendX\Functions\Gid();
	    $code     = new \ZendX\Filter\CreateAlias();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'code'          => $arrData['code'],
				'name'          => $arrData['name'],
				'branch_id'     => $arrData['branch_id'],
				'product_id'    => $arrData['product_id'],
				'number'        => $arrData['number'],
				'expdate'       => $arrData['expdate'] ? $date->formatToData($arrData['expdate']) : date('Y-m-d'),
				'deliver'       => $arrData['deliver'],
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
			);
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if ($options['task'] == 'update-code') {
		    $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> where -> equalTo('id', $arrParam['id']);
		    })->toArray();
		
		    $index = '';
		    foreach ($result as $value) {
		        $index = $value['index'];
		    }
		    if (strlen($index) <= 5) {
		        $i = 7 - strlen($index);
		        $data['code'] = substr_replace("1910",$index, $i);
		        $this->tableGateway->update($data, array('id' => $arrParam['id']));
		    }else{
		        $data['code'] = substr_replace("1910",$index, 2);
		        $this->tableGateway->update($data, array('id' => $arrParam['id']));
		
		    }
		    return true;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
			    'code'          => !empty($arrData['code']) ? $arrData['code'] : $code->filter($arrData['name']),
				'name'          => $arrData['name'],
			    'price'         => $arrData['price'] ? $number->formatToData($arrData['price']) : 0,
			    'type'          => $arrData['type'],
			    'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		if($options['task'] == 'update-export') {
		    $id = $arrData['product_batch'];
		   
		    $product_batch = $this->getServiceLocator()->get('Admin\Model\ProductBatchTable')->getItem(array('id' => $arrData['product_batch']));
		    $number_export = $product_batch['number_export'] + $arrData['number'];
		    
		    $data	= array(
		        'number_export'      => $number_export,
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
		
		if($options['task'] == 'update-export-number') {
		    $id = $arrData['product_batch'];
		    $product_batch = $this->getServiceLocator()->get('Admin\Model\ProductBatchTable')->getItem(array('id' => $arrData['product_batch']));
		    
		    $number        = $arrData['number'] - $product_batch['number_export'];
		    $number_export = $product_batch['number_export'] + $number;
		
		    $data	= array(
		        'number_export'      => $number_export,
		    );
		     
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}