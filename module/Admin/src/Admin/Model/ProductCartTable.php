<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;

class ProductCartTable extends DefaultTable {
	
	protected $tableGateway;
	protected $userInfo;
	
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway	= $tableGateway;
		$this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	
	public function countItem($arrParam = null, $options = null){
	   if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> or
                                     -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> or
                                     -> like('email', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> UNNEST;
                }
				
			})->current();
		}
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $paginator = $arrParam['paginator'];
                $ssFilter  = $arrParam['ssFilter'];
			    
                $select -> limit($paginator['itemCountPerPage'])
				        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
                        -> order('created DESC');
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
                    $select -> where -> NEST
                                     -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> or
                                     -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> or
                                     -> like('email', '%'. $ssFilter['filter_keyword'] . '%')
                                     -> UNNEST;
                }
				
			})->toArray();
		}
		
		return $result;
	}
	
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $ssFilter = new Container('myCart');
	    $productCart = $ssFilter->data;
	    
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'                => $id,
				'name'              => $arrData['name'],
				'email'             => $arrData['email'],
				'phone'             => $arrData['phone'],
				'address'           => $arrData['address'],
				'product'           => serialize($productCart),
				'note'              => $arrData['note'],
				'bank'              => $arrData['bank'],
				'discount'          => $arrData['discount'],
			    'created'           => date('Y-m-d H:i:s'),
			);
			$this->tableGateway->insert($data);
			return $id;
		}

		if($options['task'] == 'add-api') {
			$id = $gid->getId();
			foreach ($arrData['product'] as $key => $value) {
				$arrData['product'][$key] = (array)$value;
				$arrData['product'][$key]['product'] = (array)$arrData['product'][$key]['product'];
			}
			$data	= array(
				'id'                => $id,
				'name'              => $arrData['name'],
				'email'             => $arrData['email'],
				'phone'             => $arrData['phone'],
				'address'           => $arrData['address'],
				'product'           => !empty($arrData['product']) ? serialize($arrData['product']) : null,
				'note'              => $arrData['note'],
				'bank'              => null,
				'discount'          => null,
			    'created'           => date('Y-m-d H:i:s'),
			);
			$this->tableGateway->insert($data);
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}