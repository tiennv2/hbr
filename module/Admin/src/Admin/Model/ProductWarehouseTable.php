<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;

class ProductWarehouseTable extends DefaultTable {

    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result = $this->defaultCount($arrParam, null);
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->defaultList($arrParam, null);
		}

		if ($options['task'] == 'list-product') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
            	$select -> where -> equalTo('product_batch', $arrParam['id']);
    		});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'AdminProductWarehouse'. $arrParam['type'];
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                if(!empty($arrParam['type'])) {
	                    $select -> where -> equalTo('type', $arrParam['type']);
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->defaultGet($arrParam, array('by' => 'id'));
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  		= $arrParam['data'];
	    $arrRoute 		= $arrParam['route'];
	    $product_batch 	= $arrParam['product_batch'];
	    
	    $filter   = new \ZendX\Filter\Purifier();
	    $number   = new \ZendX\Functions\Number();
	    $gid      = new \ZendX\Functions\Gid();
	    $code     = new \ZendX\Filter\CreateAlias();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			
			$data	= array(
				'id'            => $id,
				'product_batch' => $arrData['product_batch'],
				'number' 	    => $arrData['number'],
				'status'        => 1,
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
			);
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    
			$data	= array(
				'product_batch' => $arrData['product_batch'],
				'number' 	    => $arrData['number'],
				'status'        => 1,
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}