<?php
namespace Admin\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class MarketingTargetTable extends DefaultTable {
    public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select -> where -> NEST
                    			     -> like('name', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> OR
                    			     -> like('phone', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> OR
                    			     -> like('address', '%'. $ssFilter['filter_keyword'] . '%')
                    			     -> UNNEST;
				}
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $paginator = $arrParam['paginator'];
	            $ssFilter  = $arrParam['ssFilter'];
	             
	            if(!isset($options['paginator']) || $options['paginator'] == true) {
	    			$select -> limit($paginator['itemCountPerPage'])
	    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
                }
	    
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
	                $select -> where -> NEST
                	                 -> like(TABLE_MARKETING_TARGET .'.name', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> OR
                	                 -> like(TABLE_MARKETING_TARGET .'.phone', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> OR
                	                 -> like(TABLE_MARKETING_TARGET .'.address', '%'. $ssFilter['filter_keyword'] . '%')
                	                 -> UNNEST;
	            }
	        });
	    }
	    
	    if($options['task'] == 'get-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $date     = new \ZendX\Functions\Date();
	            $arrData  = $arrParam['ssFilter'];
	            
	            if(!empty($arrData['date_begin']) && !empty($arrData['date_end'])) {
	                $select -> where -> NEST
	                -> greaterThanOrEqualTo('created', $date->formatToData($arrData['date_begin']) . ' 00:00:00')
	                -> AND
	                -> lessThanOrEqualTo('created', $date->formatToData($arrData['date_end']) . ' 23:59:59')
	                -> UNNEST;
	            }
	        });
	        
//             $result = \ZendX\Functions\CreateArray::create($result, array('key' => 'id', 'value' => 'object'));
	    }
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->toArray();
		}
		
		return current($result);
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    $arrItem  = $arrParam['item'];
	     
	    $image         = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter        = new \ZendX\Filter\Purifier();
		$gid           = new \ZendX\Functions\Gid();
		$dateFormat    = new \ZendX\Functions\Date();
		$number        = new \ZendX\Functions\Number();

		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			
			$option = array();
			if(!empty($arrData['params'])) {
			    foreach ($arrData['params'] AS $key => $val) {
			        if(!empty($val)) {
			            foreach ($val AS $k => $v) {
			                $option[$key][$k] = $number->formatToData($v);
			            }
			        }
			    }
			}
			
			$data = array(
				'id'                    => $id,
				'me'                  	=> str_replace(".00", "", $arrData['me']),
				'c1'               		=> str_replace(".00", "", $arrData['c1']),
				'c2'                 	=> str_replace(".00", "", $arrData['c2']),
			    'options'               => serialize($option),
				'created'               => date('Y-m-d H:i:s'),
				'created_by'            => $this->userInfo->getUserInfo('id'),
			);		
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
		    
		    $option = array();
		    if(!empty($arrData['params'])) {
		        foreach ($arrData['params'] AS $key => $val) {
		            if(!empty($val)) {
		                foreach ($val AS $k => $v) {
		                    $option[$key][$k] = $number->formatToData($v);
		                }
		            }
		        }
		    }
		    
			$data	= array(
				'id'                    => $id,
				'me'                  	=> $arrData['me'],
				'c1'               		=> $arrData['c1'],
				'c2'                 	=> $arrData['c2'],
			    'options'               => serialize($option),
			);
			
			$this->tableGateway->update($data, array('id' => $arrData['id']));
			return $arrData['id'];
		}
		
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'delete-item') {
	        $where = new Where();
	        $where -> in('id', $arrData['cid']);
	        $where -> notEqualTo('id', '1111111111111111111111');
	        $this -> tableGateway -> delete($where);
	        
	        return count($arrData['cid']);
	    }
	
	    return false;
	}
}