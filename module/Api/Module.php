<?php

namespace Api;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;
use Zend\Db\TableGateway\TableGateway;

class Module {
	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		$adapter = $e->getApplication()->getServiceManager()->get('dbConfig');
		GlobalAdapterFeature::setStaticAdapter($adapter);
	}
	
    public function getConfig() {
        return array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/router.config.php'
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            )
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'	=> array(
            	'Api\Model\SaleFormTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_FORM, $adapter, null);
            		return new \Api\Model\SaleFormTable($tableGateway);
            	},
            	'Api\Model\SaleFormDataTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_FORM_DATA, $adapter, null);
            		return new \Api\Model\SaleFormDataTable($tableGateway);
            	},
            	'Api\Model\SaleCompanyBranchTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_COMPANY_BRANCH, $adapter, null);
            		return new \Api\Model\SaleCompanyBranchTable($tableGateway);
            	},
            	'Api\Model\SaleDocumentTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_DOCUMENT, $adapter, null);
            		return new \Api\Model\SaleDocumentTable($tableGateway);
            	},
            	'Api\Model\PostCategoryTable'    => function ($sm) {
            	    $adapter = $sm->get('dbConfig');
            	    $tableGateway = new TableGateway(TABLE_POST_CATEGORY, $adapter, null);
            	    return new \Api\Model\PostCategoryTable($tableGateway);
            	},
            	'Api\Model\PostItemTable'    => function ($sm) {
            	    $adapter = $sm->get('dbConfig');
            	    $tableGateway = new TableGateway(TABLE_POST_ITEM, $adapter, null);
            	    return new \Api\Model\PostItemTable($tableGateway);
            	},
            	'Api\Model\SaleContactTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_CONTACT, $adapter, null);
            		return new \Api\Model\SaleContactTable($tableGateway);
            	},
            	'Api\Model\SaleContractTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_CONTRACT, $adapter, null);
            		return new \Api\Model\SaleContractTable($tableGateway);
            	},
            	'Api\Model\SaleBillTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_BILL, $adapter, null);
            		return new \Api\Model\SaleBillTable($tableGateway);
            	},
            	'Api\Model\OfflineTrainingClassTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_TRAINING_CLASS, $adapter, null);
            		return new \Api\Model\OfflineTrainingClassTable($tableGateway);
            	},
            	'Api\Model\OfflineProductTable'	=> function ($sm) {
            		$adapter = $sm->get('dbConfig');
            		$tableGateway = new TableGateway(TABLE_PRODUCT_CART, $adapter, null);
            		return new \Api\Model\OfflineProductTable($tableGateway);
            	},
            	'Api\Model\LocationCityTable'	=> function ($sm) {
            	    $adapter = $sm->get('dbConfig');
            	    $tableGateway = new TableGateway(TABLE_LOCATION_CITY, $adapter, null);
            	    return new \Api\Model\LocationCityTable($tableGateway);
            	},
            	'Api\Model\LocationDistrictTable'	=> function ($sm) {
            	    $adapter = $sm->get('dbConfig');
            	    $tableGateway = new TableGateway(TABLE_LOCATION_DISTRICT, $adapter, null);
            	    return new \Api\Model\LocationDistrictTable($tableGateway);
            	},
            	'Api\Model\LocationCountryTable'	=> function ($sm) {
            	    $adapter = $sm->get('dbConfig');
            	    $tableGateway = new TableGateway(TABLE_LOCATION_COUNTRY, $adapter, null);
            	    return new \Api\Model\LocationCountryTable($tableGateway);
            	},
            	'Api\Model\HbrFormTable'	=> function ($sm) {
                	$adapter = $sm->get('dbHbr');
                	$tableGateway = new TableGateway(TABLE_CRM_FORM, $adapter, null);
                	return new \Api\Model\HbrFormTable($tableGateway);
            	},
            	'Api\Model\HbrFormDataTable'	=> function ($sm) {
                	$adapter = $sm->get('dbHbr');
                	$tableGateway = new TableGateway(TABLE_HBR_FORM_DATA, $adapter, null);
                	return new \Api\Model\HbrFormDataTable($tableGateway);
            	},
            	'Api\Model\HbrContactTable'	=> function ($sm) {
                	$adapter = $sm->get('dbHbr');
                	$tableGateway = new TableGateway(TABLE_CRM_CONTACT, $adapter, null);
                	return new \Api\Model\HbrContactTable($tableGateway);
            	},
            	'Api\Model\HbrContractTable'	=> function ($sm) {
                	$adapter = $sm->get('dbHbr');
                	$tableGateway = new TableGateway(TABLE_CRM_CONTRACT, $adapter, null);
                	return new \Api\Model\HbrContractTable($tableGateway);
            	},
            	'Api\Model\HbrCourseTable'	=> function ($sm) {
                	$adapter = $sm->get('dbHbr');
                	$tableGateway = new TableGateway(TABLE_CRM_COURSE, $adapter, null);
                	return new \Api\Model\HbrCourseTable($tableGateway);
            	},
                'Api\Model\DocumentTable'    => function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_DOCUMENT, $adapter, null);
                    return new \Api\Model\DocumentTable($tableGateway);
                },
            	'Api\Model\HbrDocumentTable'	=> function ($sm) {
                	$adapter = $sm->get('dbConfig');
                	$tableGateway = new TableGateway(TABLE_DOCUMENT, $adapter, null);
                	return new \Api\Model\HbrDocumentTable($tableGateway);
            	},
                'Api\Model\HbrCategoryTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_POST_CATEGORY, $adapter, null);
                    return new \Api\Model\HbrCategoryTable($tableGateway);
                },
                'Api\Model\HbrPostItemTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_POST_ITEM, $adapter, null);
                    return new \Api\Model\HbrPostItemTable($tableGateway);
                },
                'Api\Model\HbrCourseCategoryTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_CATEGORY, $adapter, null);
                    return new \Api\Model\HbrCourseCategoryTable($tableGateway);
                },
                'Api\Model\HbrCourseItemTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_ITEM, $adapter, null);
                    return new \Api\Model\HbrCourseItemTable($tableGateway);
                },
                'Api\Model\HbrCourseDetailTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_DETAIL, $adapter, null);
                    return new \Api\Model\HbrCourseDetailTable($tableGateway);
                },
                'Api\Model\HbrCourseGroupTable'    => function ($sm) {
                    $adapter = $sm->get('dbConfig');
                    $tableGateway = new TableGateway(TABLE_COURSE_GROUP, $adapter, null);
                    return new \Api\Model\HbrCourseGroupTable($tableGateway);
                },
                'Api\Model\HbrContactCourseTable'    => function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_CONTACT_COURSE, $adapter, null);
                    return new \Api\Model\HbrContactCourseTable($tableGateway);
                },
            ),
        );
    }

    public function getFormElementConfig() {
        return array(
            'factories' => array(
            )
        );
    }
}
