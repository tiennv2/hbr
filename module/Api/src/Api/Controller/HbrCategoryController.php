<?php
namespace Api\Controller;

use ZendX\Controller\ApiController;

class HbrCategoryController extends ApiController {
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $_GET);
    
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
     
    //API Teacher ((Giảng viên))
    public function teacherAction(){
        $result = array();
        if(!empty($this->_params['data']['id'])) {
            $item = $this->getServiceLocator()->get('Api\Model\HbrDocumentTable')->getItem(array('id' => $this->_params['data']['id']));
            $result['id']               = $item['id'];
            $result['name']             = $item['name'];
            $result['alias']            = $item['alias'];
            $result['description']      = $item['description'];
            $result['content']          = html_entity_decode(strip_tags($item['content']));
            $result['image']            = DOMAIN . $item['image'];
            $result['image_medium']     = DOMAIN . $item['image_medium'];
            $result['image_thumb']      = DOMAIN . $item['image_thumb'];
        } else {
            $list_teacher = $this->getServiceLocator()->get('Api\Model\HbrDocumentTable')->listItem(array("where" => array( "code" => "teacher" ) ), array('task' => 'cache-status'));
            foreach ($list_teacher as $item) {
                $result[] = array(
                    'id'           => $item['id'],
                    'name'         => $item['name'],
                    'alias'        => $item['alias'],
                    'description'  => $item['description'],
                    'content'      => html_entity_decode(strip_tags($item['content'])),
                    'image'        => DOMAIN . $item['image'],
                    'image_medium' => DOMAIN . $item['image_medium'],
                    'image_thumb'  => DOMAIN . $item['image_thumb'],
                );
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API sách danh mục sách
    public function bookCategoryAction(){
        if(!empty($this->_params['data']['id'])) {
            $category = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            $category = current($this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem(array('type' => 'product'), array('task' => 'list-all')));
        }
        $branche = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
        $result = array();
        foreach ($branche AS $item) {
            $data                   = array();
            $data['id']             = $item['id'];
            $data['name']           = $item['name'];
            $data['alias']          = $item['alias'];
            $data['description']    = $item['description'];
            $data['image']          = DOMAIN . $item['image'];
            $data['image_thumb']    = DOMAIN . $item['image_thumb'];
            $data['image_medium']   = DOMAIN . $item['image_medium'];
            $data['parent']         = $item['parent'];
            $data['level']          = $item['level'];
            $data['left']           = $item['left'];
            $data['right']          = $item['right'];
            
            $result[] = $data;
        }

        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Danh sách sách
    public function bookAction(){
        if(!empty($this->_params['data']['id'])) {
            $category = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            $category = current($this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem(array('type' => 'product'), array('task' => 'list-all')));
        }
        $branche = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
        $arrParam = array(
            'branche' => $branche,
            'paginator' => array(
                'itemCountPerPage' => !empty($this->_params['data']['itemCountPerPage']) ? (int)$this->_params['data']['itemCountPerPage'] : 20,
                'currentPageNumber' => !empty($this->_params['data']['currentPageNumber']) ? (int)$this->_params['data']['currentPageNumber'] : 1,
            )
        );
        
        $items = $this->getServiceLocator()->get('Api\Model\PostItemTable')->listItem($arrParam, array('task' => 'list-item'));
        $result = array();
        if($items->count() > 0) {
            foreach ($items AS $item) {
                $data                           = array();
                $data['id']                     = $item['id'];
                $data['name']                   = $item['name'];
                $data['alias']                  = $item['alias'];
                $data['description']            = $item['description'];
                $data['post_category_id']       = $item['post_category_id'];
                $data['image']                  = DOMAIN . $item['image'];
                $data['image_thumb']            = DOMAIN . $item['image_thumb'];
                $data['image_medium']           = DOMAIN . $item['image_medium'];
                $data['product_price']          = $item['product_price'];
                $data['product_sale_percent']   = $item['product_sale_percent'];
                $data['product_sale_price']     = $item['product_sale_price'];
                $data['category_name']          = $item['category_name'];
                $data['category_alias']         = $item['category_alias'];
                
                $result[] = $data;
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Xem chi tiết sách
    public function bookItemAction(){
        $result = null;
        if(!empty($this->_params['data']['id'])) {
            $item = $this->getServiceLocator()->get('Api\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
            $result['id'] = $item['id'];
            $result['name'] = $item['name'];
            $result['alias'] = $item['alias'];
            $result['description'] = $item['description'];
            $result['content'] = html_entity_decode(strip_tags($item['content']));
            $result['image'] = DOMAIN . $item['image'];
            $result['image_medium'] = DOMAIN . $item['image_medium'];
            $result['image_thumb'] = DOMAIN . $item['image_thumb'];
            $result['product_price'] = $item['product_price'];
            $result['product_sale_percent'] = $item['product_sale_percent'];
            $result['product_sale_price'] = $item['product_sale_price'];
            $result['product_year'] = $item['product_year'];
            $result['post_category_id'] = $item['post_category_id'];
            $result['link_web'] = DOMAIN . '/view-book?book_id='. $item['id'];
        }

        echo json_encode($result, true);
        return $this->response;
    }
    
    public function courseCategoryAction(){
        if(!empty($this->_params['data']['id'])) {
            $category = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            $category = current($this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem(array('type' => 'course'), array('task' => 'list-all')));
        }
        $branche = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
        $result = array();
        foreach ($branche AS $key => $item) {
            if($key != 0) {
                $data                   = array();
                $data['id']             = $item['id'];
                $data['name']           = $item['name'];
                $data['alias']          = $item['alias'];
                $data['description']    = $item['description'];
                $data['image']          = DOMAIN . $item['image'];
                $data['image_thumb']    = DOMAIN . $item['image_thumb'];
                $data['image_medium']   = DOMAIN . $item['image_medium'];
                $data['parent']         = $item['parent'];
                $data['level']          = $item['level'];
                $data['left']           = $item['left'];
                $data['right']          = $item['right'];
                
                $result[] = $data;
            }
        }

        echo json_encode($result, true);
        return $this->response;
    }
    
    // API khóa ngắn hạn 
    public function listShortAction(){
        $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));

        if(!empty($this->_params['data']['id'])) {
            $category = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            $category = current($this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem(array('type' => 'course'), array('task' => 'list-all')));
        }
        $branche = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
        $arrParam = array(
            'branche' => $branche,
            'paginator' => array(
                'itemCountPerPage' => !empty($this->_params['data']['itemCountPerPage']) ? (int)$this->_params['data']['itemCountPerPage'] : 20,
                'currentPageNumber' => !empty($this->_params['data']['currentPageNumber']) ? (int)$this->_params['data']['currentPageNumber'] : 1,
            )
        );
        
        $items = $this->getServiceLocator()->get('Api\Model\PostItemTable')->listItem($arrParam, array('task' => 'list-item'));
        $result = array();
        if($items->count() > 0) {
            foreach ($items AS $item) {
                $course_option = !empty($item['course_option']) ? unserialize($item['course_option']) : array();

                $data                           = array();
                $data['id']                     = $item['id'];
                $data['name']                   = $item['name'];
                $data['alias']                  = $item['alias'];
                $data['description']            = $item['description'];
                $data['post_category_id']       = $item['post_category_id'];
                $data['image']                  = DOMAIN . $item['image'];
                $data['image_thumb']            = DOMAIN . $item['image_thumb'];
                $data['image_medium']           = DOMAIN . $item['image_medium'];
                $data['image_cover']            = DOMAIN . $item['image_cover'];
                $data['course_teacher_id']      = $item['course_teacher_id'];
                $data['course_teacher_name']    = $teacher[$item['course_teacher_id']]['name'];
                $data['course_location']        = array_values($course_option);
                $data['course_price']           = $item['course_price'];
                $data['course_list_id']         = $item['course_list_id'];
                $data['category_name']          = $item['category_name'];
                $data['category_alias']         = $item['category_alias'];
                $data['link_web']               = $item['meta_url'];

                $result[] = $data;
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Xem chi tiết khóa học ngắn hạn
    public function listShortItemAction(){
        $result = null;
        if(!empty($this->_params['data']['id'])) {
            $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
            
            $item = $this->getServiceLocator()->get('Api\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
            
            $course_option = !empty($item['course_option']) ? unserialize($item['course_option']) : array();
            
            $result['id']                   = $item['id'];
            $result['name']                 = $item['name'];
            $result['alias']                = $item['alias'];
            $result['description']          = $item['description'];
            $result['content']              = html_entity_decode(strip_tags($item['content']));
            $result['post_category_id']     = $item['post_category_id'];
            $result['image']                = DOMAIN . $item['image'];
            $result['image_thumb']          = DOMAIN . $item['image_thumb'];
            $result['image_medium']         = DOMAIN . $item['image_medium'];
            $result['image_cover']          = DOMAIN . $item['image_cover'];
            $result['course_teacher_id']    = $item['course_teacher_id'];
            $result['course_teacher_name']  = $teacher[$item['course_teacher_id']]['name'];
            $result['course_location']      = array_values($course_option);
            $result['course_price']         = $item['course_price'];
            $result['course_list_id']       = $item['course_list_id'];
            $result['course_target']        = html_entity_decode(strip_tags($item['course_target']));
            $result['course_benefit']       = html_entity_decode(strip_tags($item['course_benefit']));
            $result['course_policy']        = html_entity_decode(strip_tags($item['course_policy']));
            
            if(!empty($item['course_list_id'])) {
                $course_long = $this->getServiceLocator()->get('Api\Model\PostItemTable')->getItem(array('id' => $item['course_list_id']));
                $result['course_list_name'] = $course_long['name'];
            }
        }
    
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API khóa dài hạn
    public function listLongAction(){
        $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        
        if(!empty($this->_params['data']['id'])) {
            $category = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->getItem(array('id' => $this->_params['data']['id']));
        } else {
            $category = current($this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem(array('type' => 'course_long'), array('task' => 'list-all')));
        }
        $branche = $this->getServiceLocator()->get('Api\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
        $arrParam = array(
            'branche' => $branche,
            'paginator' => array(
                'itemCountPerPage' => !empty($this->_params['data']['itemCountPerPage']) ? (int)$this->_params['data']['itemCountPerPage'] : 20,
                'currentPageNumber' => !empty($this->_params['data']['currentPageNumber']) ? (int)$this->_params['data']['currentPageNumber'] : 1,
            )
        );
        
        $items = $this->getServiceLocator()->get('Api\Model\PostItemTable')->listItem($arrParam, array('task' => 'list-item'));
        $result = array();
        if($items->count() > 0) {
            foreach ($items AS $item) {
                $data                           = array();
                $data['id']                     = $item['id'];
                $data['name']                   = $item['name'];
                $data['alias']                  = $item['alias'];
                $data['description']            = $item['description'];
                $data['post_category_id']       = $item['post_category_id'];
                $data['image']                  = DOMAIN . $item['image'];
                $data['image_thumb']            = DOMAIN . $item['image_thumb'];
                $data['image_medium']           = DOMAIN . $item['image_medium'];
                $data['image_cover']            = DOMAIN . $item['image_cover'];
                $data['course_code']            = $item['course_code'];
                $data['course_teacher_id']      = $item['course_teacher_id'];
                $data['course_teacher_name']    = $teacher[$item['course_teacher_id']]['name'];
                $data['link_web']               = $item['meta_url'];

                $result[] = $data;
            }
        }

        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Xem chi tiết khóa học dài hạn
    public function listLongItemAction(){
        $result = null;
        if(!empty($this->_params['data']['id'])) {
            $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
    
            $item = $this->getServiceLocator()->get('Api\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
    
            $result['id']                   = $item['id'];
            $result['name']                 = $item['name'];
            $result['alias']                = $item['alias'];
            $result['description']          = $item['description'];
            $result['post_category_id']     = $item['post_category_id'];
            $result['image']                = DOMAIN . $item['image'];
            $result['image_thumb']          = DOMAIN . $item['image_thumb'];
            $result['image_medium']         = DOMAIN . $item['image_medium'];
            $result['image_cover']          = DOMAIN . $item['image_cover'];
            $result['course_teacher_id']    = $item['course_teacher_id'];
            $result['course_teacher_name']  = $teacher[$item['course_teacher_id']]['name'];
            $result['course_target']        = html_entity_decode(strip_tags($item['course_target']));
            $result['course_benefit']       = html_entity_decode(strip_tags($item['course_benefit']));
            $result['course_policy']        = html_entity_decode(strip_tags($item['course_policy']));
            $result['course_short']         = array();
    
            // Lấy danh sách khóa học con
            $items = $this->getServiceLocator()->get('Api\Model\PostItemTable')->listItem(array('course_list_id' => $item['id']), array('task' => 'by-course-list'));
            if(count($items) > 0) {
                foreach ($items AS $key => $val) {
                    $result['course_short'][] = array(
                        'id' => $val['id'],
                        'name' => $val['name']
                    );
                } 
            }
        }

        echo json_encode($result, true);
        return $this->response;
    }
    
    //API Home
    function homeAction() {
        $list_book              = $this->getServiceLocator()->get('Api\Model\HbrPostItemTable')->listItem(array('data' => array('post_category_id' => '1515844043-3tq3-3631-06y2-14583112p02k', 'limit' => 5)), array('task' => 'list-all'));
        $category               = $this->getServiceLocator()->get('Api\Model\HbrCategoryTable')->listItem(array('where' => array('type' => 'course')), array('task' => 'cache-status'));
        $list_short_course      = $this->getServiceLocator()->get('Api\Model\HbrPostItemTable')->listItem($this->_params, array('task' => 'cache-status'));
        $list_Online            = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->listItem(array('data' => array('status' => 1, 'limit' => 5)), array('task' => 'list-all'));
        $list_slide             = $this->getServiceLocator()->get('Api\Model\HbrDocumentTable')->listItem(array('data' => array('code' => 'ads-slideshow', 'limit' => 4)), array('task' => 'list-slide'));;
        $teacher                = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        
        $result = array();
        foreach ($list_slide AS $key => $value){
            $data['id']       = $value['id'];
            $data['name']     = $value['name'];
            $data['image']    = 'https://hbr.edu.vn/'.$value['image'];
            $data['url']      = $value['url'];
            $data['target']   = $value['target'];
            
            $result[$value['id']] = $data;
        }
        
        $result_online = array();
        foreach ($list_Online AS $key => $value_online){
            $data_online['id']             = $value_online['id'];
            $data_online['name']           = $value_online['name'];
            $data_online['alias']          = $value_online['alias'];
            $data_online['image']          = 'https://hbr.edu.vn/'.$value_online['image'];
            $data_online['image_thumb']    = 'https://hbr.edu.vn/'.$value_online['image_thumb'];
            $data_online['image_medium']   = 'https://hbr.edu.vn/'.$value_online['image_medium'];
            $data_online['description']    = $value_online['description'];
            $data_online['content']        = preg_replace("/[\n\r\t]/"," ",(html_entity_decode(strip_tags($value_online['content']))));
            $data_online['teacher']        = $teacher[$value_online['teacher']]['name'];
            $data_online['price']          = $value_online['price'];
            $data_online['price_sale']     = $value_online['price_sale'];
            $data_online['price_sale_percent']    = $value_online['price_sale_percent'];
            
            $result_online[$value_online['id']] = $data_online;
        }
        
        $result_book = array();
        foreach ($list_book AS $key => $value_book){
            $data_book['id']                     = $value_book['id'];
            $data_book['name']                   = $value_book['name'];
            $data_book['alias']                  = $value_book['alias'];
            $data_book['image']                  = 'https://hbr.edu.vn/'.$value_book['image'];
            $data_book['image_thumb']            = 'https://hbr.edu.vn/'.$value_book['image_thumb'];
            $data_book['image_medium']           = 'https://hbr.edu.vn/'.$value_book['image_medium'];
            $data_book['product_price']          = $value_book['product_price'];
            $data_book['product_sale_percent']   = $value_book['product_sale_percent'];
            $data_book['product_sale_price']     = $value_book['product_sale_price'];
            $data_book['teacher']                = $teacher[$value_book['book_teacher_id']]['name'];
            $data_book['content_teacher']        = preg_replace("/[\n\r\t]/"," ",(html_entity_decode(strip_tags($value_book['content_teacher']))));
            $data_book['content_book']           = preg_replace("/[\n\r\t]/"," ",(html_entity_decode(strip_tags($value_book['content_book']))));
            $data_book['description']            = $value_book['description'];
            $data_book['content']                = preg_replace("/[\n\r\t]/"," ",(html_entity_decode(strip_tags($value_online['$value_book']))));
    
            $result_book[$value_book['id']] = $data_book;
        }
        
        $result_short_course = array();
        foreach ($category AS $val){
            $id_category = $val['id'];
            $i = 0;
            foreach ($list_short_course AS $key => $value_short){
                if ($value_short['post_category_id'] == $id_category && $value_short['post_category_id'] != '1519790673-5000-i313-l081-c5a5x394n971'){
                    $i++;
                    $data_short['id']                 = $value_short['id'];
                    $data_short['name']               = $value_short['name'];
                    $data_short['alias']              = $value_short['alias'];
                    $data_short['teacher']            = $teacher[$value_short['course_teacher_id']]['name'];
                    $data_short['content']            = preg_replace("/[\n\r\t]/"," ",(html_entity_decode(strip_tags($value_short['content']))));
                    $data_short['description']        = $value_short['description'];
                    $data_short['post_category_name'] = $value_short[$value_short['post_category_id']]['name'];
                    $data_short['course_list_name']   = $list_short_course[$value_short['course_list_id']]['name'];
                    $data_short['image']              = 'https://hbr.edu.vn/'.$value_short['image'];
                    $data_short['image_thumb']        = 'https://hbr.edu.vn/'.$value_short['image_thumb'];
                    $data_short['image_medium']       = 'https://hbr.edu.vn/'.$value_short['image_medium'];
                    $course_options = $value_short['course_option'] ? unserialize($value_short['course_option']) : array();
                    foreach ($course_options AS $key => $options){
                        $data_short['city_name']    = $options['name'];
                        $data_short['date']         = $options['date'];
                        $data_short['time']         = $options['time'];
                        $data_short['address']      = $options['address'];
                    }
                    
                    $result_short_course[$value_short['id']] = $data_short;
                    if($i == 5) break;
                }
            }
        }
        
        
        echo json_encode(array(
            'slide' => $result,
            'course_online' => $result_online,
            'book_home' => $result_book,
            'course_short' => $result_short_course,
        ));
        
        return $this->response;
    }

    //API sach
    function bookHbrAction() {
        $list_book              = $this->getServiceLocator()->get('Api\Model\HbrPostItemTable')->listItem(array('data' => array('post_category_id' => '1515844043-3tq3-3631-06y2-14583112p02k', 'limit' => 5)), array('task' => 'list-all'));
        
        $result = array();
        foreach ($list_book AS $key => $value_book){
            $data_book['id']                     = $value_book['id'];
            $data_book['name']                   = $value_book['name'];
            $data_book['alias']                  = $value_book['alias'];
            $data_book['image']                  = 'https://hbr.edu.vn/'.$value_book['image'];
            $data_book['image_thumb']            = 'https://hbr.edu.vn/'.$value_book['image_thumb'];
            $data_book['image_medium']           = 'https://hbr.edu.vn/'.$value_book['image_medium'];
            $data_book['product_price']          = number_format($value_book['product_price']);
            $data_book['product_sale_percent']   = number_format($value_book['product_sale_percent']);
            $data_book['product_sale_price']     = number_format($value_book['product_sale_price']);
    
            $result[$value_book['id']] = $data_book;
        }
        
        
        echo json_encode($result, true);
        
        return $this->response;
    }

    //API get khóa dài hạn
    function courseInfoAction(){
        $course_id = $_POST['id'];
        $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(null, array('task' => 'list-teacher'));
        
        $result  = $this->getServiceLocator()->get('Api\Model\HbrPostItemTable')->getItem(array('id' => $course_id));
        echo json_encode($result, true);
        
        return $this->response;
    }
}














