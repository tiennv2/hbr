<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;
use ZendX\System\ContactInfo;

class HbrContactController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $_GET);
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    // API Khóa học online của tôi
    public function myCourseAction(){
        $result = null;
        if(!empty($this->_params['data']['contact_id']) && !empty($this->_params['data']['token'])) {
            $contact        = $this->getServiceLocator()->get('Api\Model\HbrContactTable')->getItem(array('data' => array('id' => $this->_params['data']['contact_id'])));
            $contact_course = $this->getServiceLocator()->get('Api\Model\HbrContactCourseTable')->listItem(array('contact_id' => $contact['id']), array('task' => 'list-all'));
            $teacher        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
            $course_item    = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
            
            $result = array();
            if($contact_course->count() > 0) {
                foreach ($contact_course AS $item) {
                    $products = !empty($item['product']) ? unserialize($item['product']) : array();
                    if(empty($products)) {
                        $products = array($item['course_id'] => array('product' => $course_item[$item['course_id']]));
                    }
                    
                    if(!empty($products)) {
                        foreach ($products AS $product) {
                            $course = $course_item[$product['product']['id']];
                            
                            $data                       = array();
                            $data['id']                 = $item['id'];
                            $data['course_id']          = $course['id'];
                            $data['name']               = $course['name'];
                            $data['alias']              = $course['alias'];
                            $data['description']        = $course['description'];
                            $data['image']              = DOMAIN . $course['image'];
                            $data['image_thumb']        = DOMAIN . $course['image_thumb'];
                            $data['image_medium']       = DOMAIN . $course['image_medium'];
                            $data['price']              = $product['product']['price'];
                            $data['price_sale']         = $product['product']['price_sale'];
                            $data['price_sale_percent'] = $product['product']['price_sale_percent'];
                            $data['teacher']            = $course['teacher'];
                            $data['teacher_name']       = $teacher[$course['teacher']]['name'];
                            $data['category_id']        = $course['category_id'];
                            $data['pending']            = !empty($item['pending']) ? 'Đã thanh toán' : 'Chờ thanh toán';
                            if($item['status'] == 0) {
                                $data['pending']        = 'Đã hủy';
                            }
                            $data['progress']           = 0; 
                    
                            $result[] = $data;
                        }
                    }
                }
            }
            
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Xem chi tiết Khóa học online của tôi
    public function myCourseDetailAction(){
        $result = null;
        if(!empty($this->_params['data']['id']) && !empty($this->_params['data']['contact_id']) && !empty($this->_params['data']['course_id'] && !empty($this->_params['data']['token']))) {
            $contact        = $this->getServiceLocator()->get('Api\Model\HbrContactTable')->getItem(array('data' => array('id' => $this->_params['data']['contact_id'])));
            $contact_course = $this->getServiceLocator()->get('Api\Model\HbrContactCourseTable')->getItem(array('id' => $this->_params['data']['id']));
            
            $error = 1;
            if($contact_course['pending'] == 0) {
                $error = 0;
                $result['error'] = 'Khóa học đang chờ thanh toán';
            }
            if($contact_course['status'] == 0) {
                $error = 0;
                $result['error'] = 'Khóa học đã bị hủy';
            }
            
            if($error == 1) {
                $teacher        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
                $course_item    = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
                
                $result = array();
                if(!empty($contact_course)) {
                    $item = $contact_course;
                    $products = !empty($item['product']) ? unserialize($item['product']) : array();
                    if(empty($products)) {
                        $products = array($item['course_id'] => array('product' => $course_item[$item['course_id']]));
                    }
                    
                    if(!empty($products)) {
                        $product = $products[$this->_params['data']['course_id']];
                        if(!empty($product)) {
                            $course = $course_item[$product['product']['id']];
                            
                            $result['id']                 = $item['id'];
                            $result['course_id']          = $course['id'];
                            $result['name']               = $course['name'];
                            $result['alias']              = $course['alias'];
                            $result['description']        = $course['description'];
                            $result['image']              = DOMAIN . $course['image'];
                            $result['image_thumb']        = DOMAIN . $course['image_thumb'];
                            $result['image_medium']       = DOMAIN . $course['image_medium'];
                            $result['price']              = $product['product']['price'];
                            $result['price_sale']         = $product['product']['price_sale'];
                            $result['price_sale_percent'] = $product['product']['price_sale_percent'];
                            $result['teacher']            = $course['teacher'];
                            $result['teacher_name']       = $teacher[$course['teacher']]['name'];
                            $result['category_id']        = $course['category_id'];
                            $result['pending']            = !empty($item['pending']) ? 'Đã thanh toán' : 'Chờ thanh toán';
                            if($item['status'] == 0) {
                                $result['pending']        = 'Đã hủy';
                            }
                            $result['progress']           = 0;
                            
                            $item_group = $this->getServiceLocator()->get('Api\Model\HbrCourseGroupTable')->listItem(array('course_item_id' => $course['id']), array('task' => 'list-all'));
                            $item_detail = $this->getServiceLocator()->get('Api\Model\HbrCourseDetailTable')->listItem(array('item_id' => $course['id']), array('task' => 'list-all'));
                            if(!empty($item_group)) {
                                $content = array();
                                foreach ($item_group AS $val_group) {
                                    $group = array();
                                    $group['group'] = mb_strtoupper($val_group['name']);
                                    if(!empty($item_detail)) {
                                        foreach ($item_detail AS $val_detail) {
                                            if($val_detail['group_id'] == $val_group['id']) {
                                                $link_video  = DOMAIN .'/xem-video?video_id='. $val_detail['id'];
                                                $link_video .= '&contract_id='. $this->_params['data']['id'];
                                                $link_video .= '&contact_id='. $this->_params['data']['contact_id'];
                                                $link_video .= '&course_id='. $this->_params['data']['course_id'];
                                                $link_video .= '&token='. $this->_params['data']['token'];
                                                $group['video'][] = array(
                                                    'name' => $val_detail['name'],
                                                    'link_web' => $link_video
                                                );
                                            }
                                        }
                                    }
                                    $content[] = $group;
                                }
                                $result['content'] = $content;
                            }
                            $result['hotline'] = $this->_settings['General.System.Hotline']['value'];
                        }
                    }
                }
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API lịch sử giao dich
    public function listOrderAction(){
        $userInfo = new ContactInfo();
        // $contact_Id = $userInfo->getContactInfo()['id'];
        $contact_Id = '1523853551-6l3d-6185-7526-9n7621674466';

        if(!empty($contact_Id)) {
            $contact_course     = $this->getServiceLocator()->get('Api\Model\HbrContactCourseTable')->listItem(array('contact_id' => $this->_params['data']['contact_id']), array('task' => 'list-order'));
            $courseItem         = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->listItem($this->_params, array('task' => 'cache-status'));
            $payment            = $this->getServiceLocator()->get('Admin\Model\PaymentTable')->listItem($this->_params, array('task' => 'cache-status'));

            $result = array();
            foreach ($contact_course AS $key => $value){
                if (!empty($value['course_id'])){
                    $data['id']                         = $value['id'];
                    $data['contact_id']                 = $value['contact_id'];
                    $data['course_id']                  = $value['course_id'];
                    $data['course_name']                = $courseItem[$value['course_id']]['name'];
                    $data['course_price']               = $courseItem[$value['course_id']]['price'];
                    $data['course_price_sale']          = $courseItem[$value['course_id']]['price_sale'];
                    $data['course_price_sale_percent']  = $courseItem[$value['course_id']]['price_sale_percent'];
                    $data['payment']                    = $payment[$value['payment_id']]['name'];
                    $data['pending']                    = $value['pending'];
                    $data['number']                     = $value['number'];
                    $data['code']                       = $value['code'];
                    $data['created']                    = $value['created'];
                    
                    $result[$value['course_id']] = $data;
                }

                if (!empty($value['product'])) {
                    $product                = unserialize($value['product']) ? unserialize($value['product']) : array();
                    
                    foreach ($product AS $key => $val) {
                        $data['id']                         = $value['id'];
                        $data['contact_id']                 = $value['contact_id'];
                        $data['course_id']                  = $courseItem[$val['product']['id']]['id'];
                        $data['course_name']                = $courseItem[$val['product']['id']]['name'];
                        $data['course_price']               = $courseItem[$val['product']['id']]['price'];
                        $data['course_price_sale']          = $courseItem[$val['product']['id']]['price_sale'];
                        $data['course_price_sale_percent']  = $courseItem[$val['product']['id']]['price_sale_percent'];
                        $data['payment']                    = $payment[$value['payment_id']]['name'];
                        $data['pending']                    = $value['pending'];
                        $data['number']                     = $value['number'];
                        $data['code']                       = $value['code'];
                        $data['created']                    = $value['created'];

                        $result[$val['product']['id']] = $data;
                    }
                }
            }
            echo json_encode($result, true);
        }
        return $this->response;
    }
    
    // API chương trình của tôi
    function listCourseAction () {
        if(!empty($this->_params['data']['contact_id'])) {
            $dateFormat = new \ZendX\Functions\Date();

            $course         = $this->getServiceLocator()->get('Api\Model\HbrCourseTable')->listItem($this->_params, array('task' => 'cache'));
            $contract       = $this->getServiceLocator()->get('Api\Model\HbrContractTable')->listItem(array('contact_id' => $this->_params['data']['contact_id']), array('task' => 'list-all'))->toArray();
            $result = array();
            foreach ($contract AS $key => $value){
                if($value['price_paid'] && $value['price_paid'] != 0){
                    $data['id']           = $value['id'];
                    $data['contact_id']   = $value['contact_id'];
                    $data['index']        = $value['index'];
                    $data['course_id']    = $value['course_id'];
                    $data['course_name']  = $course[$value['course_id']]['name'];
                    $data['public_date']  = !empty($value['public_date']) ? $dateFormat->formatToView($value['public_date']) : '';
                    $data['link_web']     = $value['course_id'] ? DOMAIN_ERP . '/api/course/detail/id/'.$value['course_id'] : '';
                    $data['image']        = ($course[$value['course_id']] && $course[$value['course_id']]['image']) ? DOMAIN_ERP_ROOT . $course[$value['course_id']]['image'] : '';
                    $data['image_medium'] = ($course[$value['course_id']] && $course[$value['course_id']]['image_medium']) ? DOMAIN_ERP_ROOT . $course[$value['course_id']]['image_medium'] : '';
                    $data['image_thumb']  = ($course[$value['course_id']] && $course[$value['course_id']]['image_thumb']) ? DOMAIN_ERP_ROOT . $course[$value['course_id']]['image_thumb'] : '';
                    $result[] = $data;
                }
            }
            
            echo json_encode($result, true);
        }
        return $this->response;
    }
    
    // API list video
    function listVideoAction(){
        $courseItem_id = $_GET['item_id'];
        
        $courseItem         = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->listItem(array('id' => $courseItem_id), array('task' => 'list-all-video'));
        $courseGroup        = $this->getServiceLocator()->get('Admin\Model\CourseGroupTable')->listItem(array('course_item_id' => $courseItem_id), array('task' => 'list-all'));
        $courseDetail       = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem(array('item_id' => $courseItem_id), array('task' => 'list-all'));
        
        $result = array();
       
        foreach ($courseItem AS $key => $item){
            foreach ($courseDetail AS $key => $value){
                if ($item['id'] == $value['item_id']){
                    $data['id']       = $value['id'];
                    $data['name']     = $value['name'];
                    $data['video']    = $value['video'].'.mp4';
                    $data['group']    = $courseGroup[$value['group_id']]['name'];
                    
                    $result[$value['id']] = $data;
                }
            }
        }
        echo json_encode($result, true);
        
        return $this->response;
    }
}
