<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;
use JWT\JWT;

class HbrCourseCategoryController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $_GET);
    
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    //API Danh mục khóa học online
    public function listAction(){
        $items = $this->getServiceLocator()->get('Api\Model\HbrCourseCategoryTable')->listItem($this->_params, array('task' => 'cache-status'));
        
        $result = array();
        if(count($items) > 0) {
            foreach ($items AS $key => $value){
                $data['id']             = $value['id'];
                $data['name']           = $value['name'];
                $data['description']    = $value['description'];
                $data['image']          = DOMAIN . $value['image'];
                $data['image_medium']   = DOMAIN . $value['image_medium'];
                $data['image_thumb']    = DOMAIN . $value['image_thumb'];
                
                $result[] = $data;
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Khóa học online
    public function listCourseAction(){
        $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        
        $arrParam = array(
            'data' => array(
                'category_id' => $this->_params['data']['id']
            ),
            'paginator' => array(
                'itemCountPerPage' => !empty($this->_params['data']['itemCountPerPage']) ? (int)$this->_params['data']['itemCountPerPage'] : 20,
                'currentPageNumber' => !empty($this->_params['data']['currentPageNumber']) ? (int)$this->_params['data']['currentPageNumber'] : 1,
            )
        );
        
        $items = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->listItem($arrParam, array('task' => 'list-item'));
        $result = array();
        if($items->count() > 0) {
            foreach ($items AS $item) {
                $data                       = array();
                $data['id']                 = $item['id'];
                $data['name']               = $item['name'];
                $data['alias']              = $item['alias'];
                $data['description']        = $item['description'];
                $data['image']              = DOMAIN . $item['image'];
                $data['image_thumb']        = DOMAIN . $item['image_thumb'];
                $data['image_medium']       = DOMAIN . $item['image_medium'];
                $data['price']              = $item['price'];
                $data['price_sale']         = $item['price_sale'];
                $data['price_sale_percent'] = $item['price_sale_percent'];
                $data['type']               = $item['type'];
                $data['teacher']            = $item['teacher'];
                $data['teacher_name']       = $teacher[$item['teacher']]['name'];
                $data['category_id']        = $item['category_id'];
                
                $result[] = $data;
            }
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API Xem chi tiết khóa học online
    public function courseItemAction(){
        $result = null;
        if(!empty($this->_params['data']['id'])) {
            $teacher = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
            $category = $this->getServiceLocator()->get('Api\Model\HbrCourseCategoryTable')->listItem(null, array('task' => 'cache'));
    
            $item = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->getItem(array('id' => $this->_params['data']['id']));
            $item_group = $this->getServiceLocator()->get('Api\Model\HbrCourseGroupTable')->listItem(array('course_item_id' => $item['id']), array('task' => 'list-all'));
            $item_detail = $this->getServiceLocator()->get('Api\Model\HbrCourseDetailTable')->listItem(array('item_id' => $item['id']), array('task' => 'list-all'));

            $result['id']                   = $item['id'];
            $result['name']                 = $item['name'];
            $result['alias']                = $item['alias'];
            $result['description']          = $item['description'];
            $result['category_id']          = $item['category_id'];
            $result['category_name']        = $category[$item['category_id']]['name'];
            $result['image']                = DOMAIN . $item['image'];
            $result['image_thumb']          = DOMAIN . $item['image_thumb'];
            $result['image_medium']         = DOMAIN . $item['image_medium'];
            $result['price']                = $item['price'];
            $result['price_sale']           = $item['price_sale'];
            $result['price_sale_percent']   = $item['price_sale_percent'];
            $result['type']                 = $item['type'];
            $result['teacher']              = $item['teacher'];
            $result['teacher_name']         = $teacher[$item['teacher']]['name'];
            $result['teacher_content']      = html_entity_decode(strip_tags($teacher[$item['teacher']]['content']));
    
            if(!empty($item_group)) {
                $content = '';
                foreach ($item_group AS $val_group) {
                    $content .= mb_strtoupper($val_group['name']) .'<br>';
                    if(!empty($item_detail)) {
                        foreach ($item_detail AS $val_detail) {
                            if($val_detail['group_id'] == $val_group['id']) {
                                $content .= '- '. $val_detail['name'] .'<br>';
                            }
                        }
                    }
                }
                $result['content'] = $content;
            }
            $result['hotline'] = $this->_settings['General.System.Hotline']['value'];
        }
        
        echo json_encode($result, true);
        return $this->response;
    }
    
    // API lịch sử giao dich
    public function listOrderAction(){
        $contact            = $this->getServiceLocator()->get('Api\Model\HbrContactTable')->getItem($this->_params, array('task' => 'login'));
        $contact_course     = $this->getServiceLocator()->get('Api\Model\HbrContactCourseTable')->listItem($this->_params, array('task' => 'cache-status'));
        $courseItem         = $this->getServiceLocator()->get('Api\Model\HbrCourseItemTable')->listItem($this->_params, array('task' => 'cache-status'));
        $payment            = $this->getServiceLocator()->get('Admin\Model\PaymentTable')->listItem($this->_params, array('task' => 'cache-status'));

        $result = array();
        foreach ($contact_course AS $key => $value){
            if (!empty($value['course_id'])){
                $data['id']                         = $value['id'];
                $data['contact_id']                 = $value['contact_id'];
                $data['course_id']                  = $value['course_id'];
                $data['course_image']               = 'https://hbr.edu.vn'.$courseItem[$value['course_id']]['image'];
                $data['course_image_medium']        = 'https://hbr.edu.vn'.$courseItem[$value['course_id']]['image_medium'];
                $data['course_image_thumb']         = 'https://hbr.edu.vn'.$courseItem[$value['course_id']]['image_thumb'];
                $data['payment_id']                 = $payment[$value['payment_id']]['name'];
                $data['pending']                    = $value['pending'];
                $data['code']                       = $value['code'];


                $result[$value['id']] = $data;
            }
        }
        echo json_encode($result, true);

        $result_product = array();
        foreach ($contact_course AS $key => $value){
            if (!empty($value['product'])){
                $product                = unserialize($value['product']) ? unserialize($value['product']) : array();
                foreach ($product as $key => $list) {
                    $data['id']                         = $value['id'];
                    $data['course_id']                  = $list['product']['id'];
                    $data['course_name']                = $list['product']['name'];
                    $data['course_description']         = $list['product']['description'];
                    $data['course_content']             = html_entity_decode(strip_tags($list['product']['content']));
                    $data['course_image']               = 'https://hbr.edu.vn'.$courseItem[$list['product']['id']]['image'];
                    $data['course_image_medium']        = 'https://hbr.edu.vn'.$courseItem[$list['product']['id']]['image_medium'];
                    $data['course_image_thumb']         = 'https://hbr.edu.vn'.$courseItem[$list['product']['id']]['image_thumb'];
                    $data['contact_id']                 = $value['contact_id'];
                    $data['code']                       = $value['code'];
                    $data['payment_id']                 = $payment[$value['payment_id']]['name'];
                    $data['pending']                    = $value['pending'];

                    $result_product = array([$list['product']['id']] => $data, token => $value['contact_id']);
                }
            }
        }
        echo json_encode($result_product, true);
        return $this->response;
    }
}
