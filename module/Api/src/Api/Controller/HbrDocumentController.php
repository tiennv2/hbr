<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;

class HbrDocumentController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    function listAction() {
    	if($this->getRequest()->isPost()) {
    	    $result = null;
    	    if(!empty($this->_params['data']['code'])) {
    		    $result = $this->getServiceLocator()->get('Api\Model\HbrDocumentTable')->listItem(array('where' => array('code' => 'company-size')), array('task' => 'cache-array'));
    	    }
    		echo json_encode($result);
    	}
    	return $this->response;
    }
    
    //API slide
    function slideAction() {
        $result = $this->getServiceLocator()->get('Api\Model\HbrDocumentTable')->listItem(array('where' => array('code' => 'ads-slideshow', 'status' => 1)), array('task' => 'cache-array'));
        echo json_encode($result, true);
    
        return $this->response;
    }
}