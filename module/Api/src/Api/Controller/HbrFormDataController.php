<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;

class HbrFormDataController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    function addAction() {
    	if($this->getRequest()->isPost()) {
    	    $number = new \ZendX\Functions\Number();
    	    $this->_params['data']['phone'] = $number->fomartToData($this->_params['data']['phone']);
    	    if(substr($this->_params['data']['phone'], 0, 1) != 0 && substr($this->_params['data']['phone'], 0, 1) != '0') {
    	        $this->_params['data']['phone'] = '0'. $this->_params['data']['phone'];
    	    }
    	    
            $this->_params['form'] = $this->getServiceLocator()->get('Api\Model\HbrFormTable')->getItem(array('id' => $this->_params['data']['form_id'])); 
            $filter = new \Api\Filter\SaleFormData($this->_params);
            
            $result = array();
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $result['save'] = $this->getServiceLocator()->get('Api\Model\HbrFormDataTable')->saveItem($this->_params, array('task' => 'add-item'));
            }
            
            echo json_encode($result);
        }
        
    	return $this->response;
    }
    
    function updateAction() {
    	if($this->getRequest()->isPost()) {
    	    $result = array();
    	    
    	    $number = new \ZendX\Functions\Number();
    	    $this->_params['data']['phone'] = $number->fomartToData($this->_params['data']['phone']);
    	    if(substr($this->_params['data']['phone'], 0, 1) != 0 && substr($this->_params['data']['phone'], 0, 1) != '0') {
    	        $this->_params['data']['phone'] = '0'. $this->_params['data']['phone'];
    	    }
    	    
            $this->_params['form'] = $this->getServiceLocator()->get('Api\Model\HbrFormTable')->getItem(array('id' => $this->_params['data']['form_id'])); 
            $this->_params['form_data'] = $this->getServiceLocator()->get('Api\Model\HbrFormDataTable')->getItem(array('id' => $this->_params['data']['form_data_id']));
            if(empty($this->_params['form_data']['id'])) {
                $result['error'] = array('form_data_id' => 'Không tìm thấy form_data_id');
            } else {
                $filter = new \Api\Filter\SaleFormData($this->_params);
                if(!empty($filter->getError())) {
                    $result['error'] = $filter->getError();
                } else {
                    $result['save'] = $this->getServiceLocator()->get('Api\Model\HbrFormDataTable')->saveItem($this->_params, array('task' => 'update-item'));
                }
            }
            
            echo json_encode($result);
        }
        
    	return $this->response;
    }
    
    /* API form đăng ký tư vấn */
    function formConsultAction(){
        $form_id = array(
            id => '1554715663819492433kdl',
        );
        echo json_encode($form_id);
        return $this->response;
    }

    /* API form đăng ký tư vấn theo yêu cầu */
    function formCustomizingConsultAction(){
        $company_problem = $this->getServiceLocator()->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-problem" )), array('task' => 'cache'));
        $list_problems = [];
        foreach ($company_problem as $problem) {
            $list_problems[] = [
                'id'    => $problem['id'],
                'name'  => $problem['name']
            ];
        }
        $result = [
            'form_id'       => '1554717825ru860nm80z45',
            'list_problem'  => $list_problems
        ];
        echo json_encode($result);
        return $this->response;
    }
    
}