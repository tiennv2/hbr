<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;

class LocationDistrictController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    function listAction() {
    	if($this->getRequest()->isPost()) {
    	    $result = null;
    	    if(!empty($this->_params['data']['location_city_id'])) {
		        $result = $this->getServiceLocator()->get('Api\Model\LocationDistrictTable')->listItem($this->_params, array('task' => 'list-all'));
    	    }
    
    		echo json_encode($result);
    	}
    
    	return $this->response;
    }
}