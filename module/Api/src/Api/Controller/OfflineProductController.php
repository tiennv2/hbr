<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;
use Zend\Session\Container;

class OfflineProductController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    function productCartAction(){
        $result = array('result' => 'error', 'msg' => 'Đặt hàng không thành công');
        $information  = (array)json_decode($this->_params['data']['information']);
        
        if($this->getRequest()->isPost()){
            $information  = (array)json_decode($this->_params['data']['information']);
            if(!empty($information)){
                $error = null;
                if(!is_array($information['product'])) {
                    $error = "Sản phẩm phải ở dạng mảng";
                }
                if(empty($error)) {
                    $arr_params = array(
                        'data' => $information
                    );
                    if ($this->getServiceLocator()->get('Admin\Model\ProductCartTable')->saveItem($arr_params, array('task' => 'add-api'))){
                        $result = array('result' => 'success', 'msg' => 'Đặt hàng thành công');
                    }
                } else {
                    $result = array('result' => 'error', 'msg' => $error);
                }
            }
        }
        echo json_encode($result);
        return $this->response;
    }
}