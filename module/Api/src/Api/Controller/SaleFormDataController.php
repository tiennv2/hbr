<?php

namespace Api\Controller;

use ZendX\Controller\ApiController;

class SaleFormDataController extends ApiController {
    
    public function init() {
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    function addAction() {
    	if($this->getRequest()->isPost()) {
    	    
    	    $number = new \ZendX\Functions\Number();
    	    $this->_params['data']['phone'] = $number->fomartToData($this->_params['data']['phone']);
    	    if(substr($this->_params['data']['phone'], 0, 1) != 0 && substr($this->_params['data']['phone'], 0, 1) != '0') {
    	        $this->_params['data']['phone'] = '0'. $this->_params['data']['phone'];
    	    }
    	    
            $this->_params['form'] = $this->getServiceLocator()->get('Api\Model\SaleFormTable')->getItem(array('id' => $this->_params['data']['form_id'])); 
            $filter = new \Api\Filter\SaleFormData($this->_params);
            
            $result = array();
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $result['save'] = $this->getServiceLocator()->get('Api\Model\SaleFormDataTable')->saveItem($this->_params, array('task' => 'add-item'));
                $options['fromName']    = 'Tiếng Anh giao tiếp Langmaster';
                $options['to']          = $this->_params['data']['email']; //sale2.tooltechvietnam@gmail.com
                $options['toName']      = 'Xác nhận';
                $options['subject']     = 'Langmaster xác nhận đăng ký tư vấn thành công - ' . @date('d/m/Y');
                $options['content']     = '
                   <h2 style="text-align: center;">Xác nhận đăng ký tư vấn thành công</h2>
                   <h1 style="text-align: center;">KHOÁ HỌC TIẾNG ANH OFFLINE LANGMASTER</h1>
                   <p>Cảm bạn bạn đã quan tâm tới chương trình học Tiếng Anh trực tiếp tại Langmaster – Langmaster English Offline Course.</p>
                   <p>Chậm nhất trong vòng 5h (giờ làm việc) bạn sẽ nhận được điện thoại từ chuyên viên tư vấn khóa học của Langmaster để tư vấn, hướng dẫn và hỗ trợ đăng kí khóa học.</p>
                   <p>Bạn vui lòng chờ điện thoại từ chuyên viên tư vấn của Langmaster nhé!</p>
                   <p>Langmaster rất mong được đồng hành và giúp bạn tiến bộ, thành công trong việc học tiếng anh. Vì vậy, bạn hãy chú ý điện thoại nhé!</p>
                   <p>Rất cảm ơn sự ủng hộ của bạn!</p>
                   <p style="text-align: center;"><b>Chi tiết xin liên hệ hotline:</b></p>
                   <p>- Langmaster khu vực Hồ Tùng Mậu - Hoàng Quốc Việt: <a href="tel:+84962195439">0962195439</a></p>
                   <p>- Langmaster khu vực Trần Đại Nghĩa - Đại Cồ Việt: <a href="tel:+84962152228">0962152228‬</a></p>
                   <p>- Langmaster khu vực Nguyễn Lương Bằng - Nguyễn Xiển: <a href="tel:+84962244496">0962244496</a></p>
               ';
                $mailObj = new \ZendX\Mail\Mail();
                $mailObj->sendMail($options);
            }
            
            echo json_encode($result);
        }
        
        
    	return $this->response;
    }
}














