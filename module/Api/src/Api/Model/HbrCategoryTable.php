<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HbrCategoryTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
	public function listItem($arrParam = null, $options = null){
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiHbrCategory';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> columns(array('id', 'name', 'ordering', 'status'));
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCategoryStatus';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                //$select -> columns(array('id', 'name', 'status'));
	            	$select -> where -> equalTo('status', 1)
	            	        -> where -> equalTo('type', 'course');
	            	
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'cache-teacher') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCategoryTeacher';
	        $result = $cache->getItem($cache_key);
	    
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> where -> equalTo('status', 1)
	                -> where -> equalTo('type', 'teacher');
	    
	                if(!empty($arrParam['order'])) {
	                    $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('name' => 'ASC'));
	                }
	            });
	                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
	                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'list-book') {
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> where -> equalTo('status', 1)
	                -> where -> equalTo('type', 'product');
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	        }
	    }
	    
	    if($options['task'] == 'cache-course') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCategoryBook';
	        $result = $cache->getItem($cache_key);
	    
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> where -> equalTo('status', 1)
	                -> where -> equalTo('type', 'course');
	            });
	                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
	                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if ($options['task'] == 'list-offline') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select -> where -> equalTo('parent', $arrParam['parent']);
	        })->toArray();
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
}