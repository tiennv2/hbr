<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HbrContractTable extends AbstractTableGateway implements ServiceLocatorAwareInterface{
	
    protected $tableGateway;
	protected $userInfo;
	protected $serviceLocator;
	
	public function __construct(TableGateway $tableGateway) {
	    $this->tableGateway	= $tableGateway;
	    $this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator() {
	    return $this->serviceLocator;
	}
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $arrData  = $arrParam['data'];
	            $arrRoute = $arrParam['route'];
	            
	            if(!empty($arrRoute['id'])) {
                    $select->where->equalTo('id', $arrRoute['id']);
	            }
	            
	            if(!empty($arrData['data-parent-field'])) {
	                $select->where->equalTo($arrData['data-parent-field'], $arrData['data-parent']);
	            }
	        })->count();
	    }
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $arrData  = $arrParam['data'];
	            $arrRoute = $arrParam['route'];
	             
	            if(!empty($arrData['id'])) {
	                $select->where->equalTo('id', $arrData['id']);
	            }
	             
	            if(!empty($arrData['data-parent-field'])) {
	                $select->where->equalTo($arrData['data-parent-field'], $arrData['data-parent']);
	            }
	            
	            if(!empty($arrData['data-where'])) {
	                foreach ($arrData['data-where'] AS $key => $value) {
	                    $select->where->equalTo($key, $value);
	                }
	            }
	        })->toArray();
	    }
	    
	    if($options['task'] == 'list-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            if(!empty($arrParam['contact_id'])) {
	                $select -> where -> equalTo('contact_id', $arrParam['contact_id']);
	            }
	        });
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$arrData = $arrParam['data'];
				
				if(!empty($arrData['fields'])) {
					$select -> columns($arrData['fields']);
				};
				
				$select -> where -> equalTo('id', $arrData['id']);
			})->current();
		}
		
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
		$arrData  = $arrParam['data'];
		$arrRoute = $arrParam['route'];
		
	    $filter   = new \ZendX\Filter\Purifier(array(array('HTML.AllowedElements', '')));
	    $gid      = new \ZendX\Functions\Gid();
	    $date     = new \ZendX\Functions\Date();
	    $number	  = new \ZendX\Functions\Number();
	    
	    if($options['task'] == 'add-item') {
	    	$id = $gid->getId();
	    	$data = null;
	    	if(!empty($arrData['name'])) {
	    	    $data['name'] = trim($filter->filter($arrData['name']));
	    	}
	    	if(!empty($arrData['phone'])) {
	    	    $data['phone'] = $number->dauSo($number->formatToPhone($arrData['phone']), 10);
	    	}
	    	if(!empty($arrData['email'])) {
	    	    $data['email'] = strtolower(trim($filter->filter($arrData['email'])));
	    	}
	    	if(!empty($arrData['sex'])) {
	    	    $data['sex'] = $filter->filter($arrData['sex']);
	    	}
	    	if(!empty($arrData['avatar'])) {
	    	    $data['avatar'] = $filter->filter($arrData['avatar']);
	    	}
	    	if(!empty($arrData['source_id'])) {
	    	    $data['source_id'] = $filter->filter($arrData['source_id']);
	    	}
	    	if(!empty($arrData['company_name'])) {
	    	    $data['company_name'] = $arrData['company_name'];
	    	}
	    	if(!empty($arrData['company_position'])) {
	    	    $data['company_position'] = $arrData['company_position'];
	    	}
	    	if(!empty($arrData['company_size'])) {
	    	    $data['company_size'] = $arrData['company_size'];
	    	}
	    	if(!empty($arrData['company_career'])) {
	    	    $data['company_career'] = $arrData['company_career'];
	    	}
	    	if(!empty($arrData['company_problem'])) {
	    	    $data['company_problem'] = implode(',',$arrData['company_problem']);
	    	}
	    	if(!empty($arrData['birthday'])) {
	    	    $data['birthday'] = $date->fomartToData($filter->filter($arrData['birthday']));
	    	}
	    	if(!empty($arrData['birthday_year'])) {
	    	    $data['birthday_year'] = $number->fomartToData($filter->filter($arrData['birthday_year']));
	    	}
	    	if(!empty($arrData['location_city_id'])) {
	    	    $data['location_city_id'] = $filter->filter($arrData['location_city_id']);
	    	}
	    	if(!empty($arrData['location_district_id'])) {
	    	    $data['location_district_id'] = $filter->filter($arrData['location_district_id']);
	    	}
	    	if(!empty($arrData['address'])) {
	    	    $data['address'] = $filter->filter($arrData['address']);
	    	}
	    	if(!empty($arrData['facebook'])) {
	    	    $data['facebook'] = $filter->filter($arrData['facebook']);
	    	}
	    	if(!empty($data)) {
	    	    $data['id'] = $id;
	    	    $data['password'] = $arrData['password'] ? md5($arrData['password']) : md5('12345678');
	    	    $data['password_status'] = 1;
	    	    $data['status'] = 1;
	    	    $data['type'] = 'new';
	    	    $data['created'] = date('Y-m-d H:i:s');
	    	    
	    	    $this->tableGateway->insert($data);
	    	    
	    	    return $id;
	    	}
	    	
	    	return null;
	    }
	    
	    if($options['task'] == 'update-item') {
	    	$arrContact  = $arrParam['contact'];
	    	
	    	$id = $arrContact['id'];
	    	$data = array();
	    		
	    	if(!empty($arrData['name'])) {
	    		$data['name'] = trim($filter->filter($arrData['name']));
	    	}
	    	if(!empty($arrData['phone'])) {
	    		$data['phone'] = $number->dauSo($number->formatToPhone($arrData['phone']), 10);
	    	}
	    	if(!empty($arrData['email'])) {
	    		$data['email'] = strtolower(trim($filter->filter($arrData['email'])));
	    	}
	    	if(!empty($arrData['sex'])) {
	    		$data['sex'] = $filter->filter($arrData['sex']);
	    	}
	    	if(!empty($arrData['avatar'])) {
	    		$data['avatar'] = $filter->filter($arrData['avatar']);
	    	}
	    	if(!empty($arrData['source_id'])) {
	    		$data['source_id'] = $filter->filter($arrData['source_id']);
	    	}
	    	if(!empty($arrData['company_name'])) {
	    	    $data['company_name'] = $arrData['company_name'];
	    	}
	    	if(!empty($arrData['company_position'])) {
	    	    $data['company_position'] = $arrData['company_position'];
	    	}
	    	if(!empty($arrData['company_size'])) {
	    	    $data['company_size'] = $arrData['company_size'];
	    	}
	    	if(!empty($arrData['company_career'])) {
	    	    $data['company_career'] = $arrData['company_career'];
	    	}
	    	if(!empty($arrData['company_problem'])) {
	    	    $data['company_problem'] = implode(',',$arrData['company_problem']);
	    	}
	    	if(!empty($arrData['birthday'])) {
	    	    $data['birthday'] = $date->fomartToData($filter->filter($arrData['birthday']));
	    	}
	    	if(!empty($arrData['birthday_year'])) {
	    	    $data['birthday_year'] = $number->fomartToData($filter->filter($arrData['birthday_year']));
	    	}
	    	if(!empty($arrData['location_city_id'])) {
	    	    $data['location_city_id'] = $filter->filter($arrData['location_city_id']);
	    	}
	    	if(!empty($arrData['location_district_id'])) {
	    	    $data['location_district_id'] = $filter->filter($arrData['location_district_id']);
	    	}
	    	if(!empty($arrData['address'])) {
	    	    $data['address'] = $filter->filter($arrData['address']);
	    	}
	    	if(!empty($arrData['facebook'])) {
	    	    $data['facebook'] = $filter->filter($arrData['facebook']);
	    	}
	    	if(!empty($data)) {
	    		$this->tableGateway->update($data, array('id' => $id));
    	    	return $id;
	    	}
	    	return null;
	    }
	    
	    if($options['task'] == 'update-data') {
	        $id = $arrParam['id'];
	        $data = $arrParam['data'];
	        	
	        $this->tableGateway->update($data, array('id' => $id));
	        return $id;
	    }
	}
}