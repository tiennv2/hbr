<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HbrCourseDetailTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
	public function listItem($arrParam = null, $options = null){
		
	    if($options['task'] == 'list-all') {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select -> order(array('ordering' => 'ASC', 'name' => 'ASC'));
                $select -> where -> equalTo('status', 1);
                
                if(!empty($arrParam['item_id'])) {
                    $select -> where -> equalTo('item_id', $arrParam['item_id']);
                }
            })->toArray();
	    }
	    
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCourseDetail';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCourseDetailStatus';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> join(TABLE_COURSE_ITEM, TABLE_COURSE_ITEM .'.id='. TABLE_COURSE_DETAIL .'.item_id', array('item_name' => 'name', 'item_image' => 'image', 'item_image_thumb' => 'image_thumb', 'item_image_medium' => 'image_medium' ), 'inner');
	            	$select -> where -> equalTo(TABLE_COURSE_DETAIL .'.status', 1);
	            	
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array(TABLE_COURSE_DETAIL .'.ordering' => 'ASC', TABLE_COURSE_DETAIL .'.name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
}