<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HbrCourseItemTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
        if($options['task'] == 'list-item') {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> equalTo('status', 1);
    
                if(!empty($arrParam['data']['category_id'])) {
                    $select -> where -> equalTo('category_id', $arrParam['data']['category_id']);
				}
				
				if(!empty($arrParam['data']['type'])) {
                    $select -> where -> equalTo('type', $arrParam['data']['type']);
                }
            })->current();
        }
         
        return $result->count;
    }
    
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $paginator = $arrParam['paginator'];
	            	
	            $select -> limit($paginator['itemCountPerPage'])
	                    -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
	    
                $select -> order(array('ordering' => 'ASC', 'created' => 'DESC'));
                $select -> where -> equalTo('status', 1);

	            if(!empty($arrParam['data']['category_id'])) {
                    $select -> where -> equalTo('category_id', $arrParam['data']['category_id']);
				}
				
				if(!empty($arrParam['data']['type'])) {
                    $select -> where -> equalTo('type', $arrParam['data']['type']);
                }
	        });
	    }
	    
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCourseItem';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> columns(array('id', 'code', 'name', 'ordering', 'status'));
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiCourseItemStatus';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            	$select -> where -> equalTo('status', 1);
	            	
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	   if ($options['task'] == 'list-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $limit = $arrParam['data']['limit'] ? $arrParam['data']['limit'] : 5;
	            $select -> limit($limit)-> where -> equalTo('status', 1);
	            $select -> where -> equalTo('status', $arrParam['data']['status']);
	        })->toArray();
	    }
	    
	   if($options['task'] == 'list-all-video') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            if(!empty($arrParam['id'])) {
	                $select -> where -> equalTo(TABLE_COURSE_ITEM.'.id', $arrParam['id']);
	            }
	        });
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
}