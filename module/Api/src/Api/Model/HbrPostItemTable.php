<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class HbrPostItemTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
	public function listItem($arrParam = null, $options = null){
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiPost';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> columns(array('id', 'name', 'ordering', 'status'));
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                } else {
	                    $select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
	    if($options['task'] == 'cache-status') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'ApiPostStatus';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                //$select -> columns(array('id', 'name', 'ordering', 'status'));
	            	$select -> where -> equalTo('status', 1);
	            	
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }

	    if ($options['task'] == 'list-teacher') {
	    	$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('post_category_id', $arrParam['post_category_id']);
    		})->toArray();
	    }
	    
	    if ($options['task'] == 'list-book') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select -> where -> equalTo('post_category_id', $arrParam['post_category_id']);
	        })->toArray();
	    }
	    
	    if ($options['task'] == 'list-all') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $limit = $arrParam['data']['limit'] ? $arrParam['data']['limit'] : 5;
	            $select -> limit($limit)-> where -> equalTo('status', 1);
	            $select -> order(array('created' => 'DESC'));
	            $select -> where -> equalTo('post_category_id', $arrParam['data']['post_category_id']);
	        })->toArray();
	    }
	    
	   if($options['task'] == 'list-category') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            if(!empty($arrParam['id'])) {
	                $select -> where -> equalTo(TABLE_POST_ITEM.'.id', $arrParam['id']);
	            }
	        });
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            if(!empty($arrParam['course_list_id'])) {
	                $select -> where -> equalTo('status', 1);
	                $select -> where -> equalTo(TABLE_POST_ITEM.'.course_list_id', $arrParam['course_list_id']);
	            }
	        });
	    }
	    
	    if ($options['task'] == 'list-course-short') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select  -> limit(5)
	                     -> where -> equalTo('status', 1);
	        })->toArray();
	    }
		
		return $result;
	}


	public function getItem($arrParam = null, $options = null){
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> where -> equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
}