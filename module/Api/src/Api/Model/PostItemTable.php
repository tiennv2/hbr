<?php
namespace Api\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class PostItemTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $ssSystem  = New Container('system');
	            
	            $categories = array();
                foreach ($arrParam['branche'] AS $branche) {
                    $categories[] = $branche['id'];
                }
                
                $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
                $select -> where -> in(TABLE_POST_ITEM .'.post_category_id', $categories);
                $select -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
            })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $ssSystem  = New Container('system');
			    
			    $paginator  = $arrParam['paginator'];
                $categories = array();
                foreach ($arrParam['branche'] AS $branche) {
                    $categories[] = $branche['id'];
                }
                
    			$select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
    			        -> limit($paginator['itemCountPerPage'])
    			        -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage'])
    			        -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'))
    			        -> where -> in(TABLE_POST_ITEM .'.post_category_id', $categories);
    			
			    $select -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
    		});
		}
		
		if($options['task'] == 'list-item-box') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $ssSystem  = New Container('system');
		        
		        $limit = $arrParam['limit'] ? $arrParam['limit'] : 1;
		        
	            $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
		                -> limit($limit)
	                    -> where -> equalTo('c.type', $arrParam['type'])
	                    -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
	            
	            if(!empty($arrParam['box'])) {
	                $select -> where -> equalTo(TABLE_POST_ITEM .'.'. $arrParam['box'], 1);
	            }
	            
	            if(!empty($arrParam['random'] == true)){
	                $select -> order(new \Zend\Db\Sql\Expression('RAND()'));
	            } else {
	                $select -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'));
	            }
		    });
		}
		
		if($options['task'] == 'list-category-box') {
		    
		    $postCategoryTable = $this->getServiceLocator()->get('Post\Model\PostCategoryTable');
		    $categories = $postCategoryTable->listItem($arrParam, $options);
		    
		    $result = array();
		    if(!empty($categories)) {
    		    foreach ($categories AS $category) {
    		        $childs = $category['childs'];
    		        $ids = array();
    		        
    		        if(count($childs) > 0) {
    		            foreach ($childs AS $child) {
    		                $ids[] = $child['id'];
    		            }
    		        }
    		        
    		        $category['items'] = $this->tableGateway->select(function (Select $select) use ($arrParam, $options, $ids){
    		            $limit = $arrParam['limit'] ? $arrParam['limit'] : 1;
    		            
    		            $select -> join(TABLE_POST_CATEGORY, TABLE_POST_ITEM . '.post_category_id = '. TABLE_POST_CATEGORY .'.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
            		            -> limit($limit)
            		            -> where -> in(TABLE_POST_ITEM .'.post_category_id', $ids)
    		                    -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
    		            
    		            if(!empty($arrParam['random'] == true)){
    		                $select -> order(new \Zend\Db\Sql\Expression('RAND()'));
    		            } else {
    		                $select -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'));
    		            }
    		            
    		        })->toArray();
    		        
    		        $result[] = $category;
    		    }
		    }
		}
		
		if($options['task'] == 'list-item-involve') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $item     = $arrParam['item'];
		        $category = $arrParam['category'];
		        $settings = $arrParam['settings'];
		        $limit    = $settings['Post.'. ucfirst($category['type']) .'.ItemInvolve'] ? intval($settings['Post.'. ucfirst($category['type']) .'.ItemInvolve']['value']) : 5;
		        
		        $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
        		        -> limit($limit)
        		        -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'))
        		        -> where -> equalTo(TABLE_POST_ITEM .'.post_category_id', $item['post_category_id'])
        		                 -> notEqualTo(TABLE_POST_ITEM .'.id', $item['id'])
		                         -> equalTo(TABLE_POST_ITEM .'.status', 1);
		        
		        if($category['type'] != 'product' && $category['type'] != 'course') {
        		     $select -> where -> lessThan(TABLE_POST_ITEM .'.created', $item['created']);
		        }
		    })->toArray();
		}
		
		if($options['task'] == 'item-addCart') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> where -> equalTo('id', $arrParam);
		    })->toArray();
		}
		
		if($options['task'] == 'by-course-list') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select -> where -> equalTo('course_list_id', $arrParam['course_list_id']);
		    })->toArray();
		}
		
		if($options['task'] == 'list-course') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $ssSystem  = New Container('system');
		        $dateFormat = new \ZendX\Functions\Date();
		
		        $currentPageNumber = $arrParam['page'] ? (int)$arrParam['page'] : 1;
		        $itemCountPerPage = $arrParam['number'] ? (int)$arrParam['number'] : 3;
		
		        $select -> limit($itemCountPerPage)
		                -> offset(($currentPageNumber - 1) * $itemCountPerPage);
		        
		        $select -> join( array('c' => TABLE_POST_CATEGORY), TABLE_POST_ITEM . '.post_category_id = c.id', array('category_id' => 'id', 'category_index' => 'index', 'category_name' => 'name', 'category_alias' => 'alias'), $select::JOIN_INNER )
        		        -> where -> equalTo('c.type', 'course')
        		        -> where -> equalTo(TABLE_POST_ITEM .'.status', 1);
		        
		        $select -> order(array(TABLE_POST_ITEM .'.ordering' => 'ASC', TABLE_POST_ITEM .'.created' => 'DESC'));
		        
		        if(!empty($arrParam['course_city_id'])) {
		            $select -> where -> like(TABLE_POST_ITEM .'.course_city', '%'. $arrParam['course_city_id'] .'%');
		        }
		    });
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null || $options['task'] == 'get-by-id') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('alias', $arrParam['alias']);
    		})->current();
		}
	
		return $result;
	}
}