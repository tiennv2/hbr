<?php

namespace Online;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Module {
	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
		
		$adapter = $e->getApplication()->getServiceManager()->get('dbConfig');
		GlobalAdapterFeature::setStaticAdapter($adapter);
	}
	
    public function getConfig() {
        return array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/router.config.php'
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            )
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'	=> array(
                // Khai báo các Model
                'Online\Model\CourseCategoryTable'    => function ($sm) {
                $adapter = $sm->get('dbConfig');
                $tableGateway = new TableGateway(TABLE_COURSE_CATEGORY, $adapter, null);
                return new \Online\Model\CourseCategoryTable($tableGateway);
                },
                'Online\Model\CourseItemTable'    => function ($sm) {
                $adapter = $sm->get('dbConfig');
                $tableGateway = new TableGateway(TABLE_COURSE_ITEM, $adapter, null);
                return new \Online\Model\CourseItemTable($tableGateway);
                },
                'Online\Model\CourseDetailTable'    => function ($sm) {
                $adapter = $sm->get('dbConfig');
                $tableGateway = new TableGateway(TABLE_COURSE_DETAIL, $adapter, null);
                return new \Online\Model\CourseDetailTable($tableGateway);
                },
                // Khai báo xác thực đăng nhập
                'AuthenticateService'	=> function ($sm) {
                    $dbTableAdapter = new \Zend\Authentication\Adapter\DbTable($sm->get('dbConfig'), TABLE_USER, 'username', 'password', 'MD5(?)');
                    $dbTableAdapter->getDbSelect()->where->equalTo('status', 1)
                                                  ->where->equalTo('active_code', 1);
                    
                    $authenticateServiceObj = new \Zend\Authentication\AuthenticationService(null, $dbTableAdapter);
                    return $authenticateServiceObj;
                },
                'MyAuth'	=> function ($sm) {
                    return new \ZendX\System\Authenticate($sm->get('AuthenticateService'));
                },
            ),
            'invokables' => array(
                'Zend\Authentication\AuthenticationService' => 'Zend\Authentication\AuthenticationService',
            ),
        );
    }

    public function getFormElementConfig() {
        return array(
            'factories' => array(
                'formOnlineCourseCategory' => function($sm) {
                $myForm = new \Online\Form\CourseCategory($sm);
                $myForm->setInputFilter(new \Online\Filter\CourseCategory());
                return $myForm;
                },
                'formOnlineCourseItem' => function($sm) {
                $myForm = new \Online\Form\CourseItem($sm);
                $myForm->setInputFilter(new \Online\Filter\CourseItem());
                return $myForm;
                },
                'formOnlineCourseDetail' => function($sm) {
                $myForm = new \Online\Form\CourseDetail($sm);
                $myForm->setInputFilter(new \Online\Filter\CourseDetail());
                return $myForm;
                },
            )
        );
    }

    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'xViewElementError'	    => '\ZendX\View\Helper\ElementError',
                'xViewElementErrors'	=> '\ZendX\View\Helper\ElementErrors',
                'xViewInfoPrice'	    => '\ZendX\View\Helper\InfoPrice',
                'xFormSelect'           => '\ZendX\Form\View\Helper\FormSelect',
                'xFormHidden'           => '\ZendX\Form\View\Helper\FormHidden',
                'xFormInput'            => '\ZendX\Form\View\Helper\FormInput',
                'xFormButton'           => '\ZendX\Form\View\Helper\FormButton',
                'linkOnline'            => '\ZendX\View\Helper\Url\LinkAdmin',
                'linkOnlineSort'        => '\ZendX\View\Helper\Url\LinkAdminSort',
                'linkOnlineStatus'      => '\ZendX\View\Helper\Url\LinkAdminStatus',
                'linkOnlineHtml'        => '\ZendX\View\Helper\Url\LinkAdminHtml',
                'blkMenu'               => '\Block\Menu',
                'blkBreadcrumb'         => '\Block\Breadcrumb',
            )
        );
    }
}
