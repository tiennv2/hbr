<?php

// Route Online
$routeOnline = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/xonline',
        'defaults' => array (
            '__NAMESPACE__' => 'Online\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/id/:id[/code/:code]]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9_-]*',
                    'action' 		=> '[a-zA-Z0-9_-]*',
                    'id' 		    => '[a-zA-Z0-9_-]*',
                    'code' 		    => '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array ()
            )
        ),
        'paginator' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/:controller/index/page/:page[/]',
                'constraints' => array (
                    'controller'    => '[a-zA-Z0-9_-]*',
                    'page'          => '[0-9]*'
                ),
                'defaults' => array ()
            )
        )
    )
);

// Route Online Nested
$routeOnlineNested = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/online-nested',
        'defaults' => array (
            '__NAMESPACE__' => 'Online\Controller',
            'controller' 	=> 'Nested',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'add' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/:type[/:reference]]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9-]*',
                    'action' 		=> '[a-zA-Z0-9-]*',
                    'type' 	        => '[a-zA-Z0-9]*',
                    'reference' 	=> '[a-zA-Z0-9-]*'
                ),
                'defaults' => array (
                    'action'        => 'add'
                )
            )
        ),
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/:id]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9-]*',
                    'action' 		=> '[a-zA-Z0-9-]*',
                    'id' 		    => '[a-zA-Z0-9-.]*'
                ),
                'defaults' => array (
                )
            )
        ),
        'paginator' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/:controller/index/page/:page[/]',
                'constraints' => array (
                    'controller'    => '[a-zA-Z0-9-]*',
                    'page'          => '[0-9]*'
                ),
                'defaults' => array ()
            )
        )
    )
);

// Route Online Document
$routeOnlineDocument = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/online-document',
        'defaults' => array (
            '__NAMESPACE__' => 'Online\Controller',
            'controller' 	=> 'Document',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:slug[/:action[/id/:id[/code/:code]]]][/]',
                'constraints' => array (
                    'slug' 	        => '[a-zA-Z0-9-]*',
                    'action' 		=> '[a-zA-Z0-9-]*',
                    'id' 		    => '[a-zA-Z0-9-]*',
                    'code' 		    => '[a-zA-Z0-9-]*',
                ),
                'defaults' => array (
                    'controller' => 'document'
                )
            )
        ),
        'paginator' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/:slug/index/page/:page[/]',
                'constraints' => array (
                    'slug' 	        => '[a-zA-Z0-9-]*',
                    'page'          => '[0-9]*'
                ),
                'defaults' => array (
                    'controller' => 'document'
                )
            )
        )
    )
);

$routeCategory = array(
    'type' => 'Literal',
    'options' => array (
        'route' => '/chu-de',
        'defaults' => array (
            '__NAMESPACE__'     => 'Post\Controller',
            'controller'        => 'Index',
            'action'            => 'category',
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array (
            'type'      => 'Regex',
            'options'   => array (
                'regex' => '/(?<alias>[a-zA-Z0-9-_]*)(/)?',
                'defaults' => array (
                    'controller'    => 'Index',
                    'action'        => 'category',
                ),
                'spec'      => '/%alias%',
            )
        ),
    )
);

return array (
    'router' => array(
        'routes' => array(
            'routeOnline'            => $routeOnline,
            'routeOnlineNested'      => $routeOnlineNested,
            'routeOnlineDocument'    => $routeOnlineDocument,
            'routeCategory'          => $routeCategory,
        ),
    )
);