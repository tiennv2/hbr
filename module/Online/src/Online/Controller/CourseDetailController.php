<?php

namespace Online\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Form\FormInterface;
use Zend\Db\TableGateway\TableGateway;

class CourseDetailController extends ActionController {
    
    public function init() {
        
        // Thiết lập options
        $this->_options['tableName'] = 'Online\Model\CourseDetailTable';
        $this->_options['formName'] = 'formOnlineCourseDetail';
        
        // Thiết lập session filter
        $ssFilter = new Container(__CLASS__);
        $this->_params['ssFilter']['order_by']       	= !empty($ssFilter->order_by) ? $ssFilter->order_by : 'created';
        $this->_params['ssFilter']['order']          	= !empty($ssFilter->order) ? $ssFilter->order : 'DESC';
        $this->_params['ssFilter']['filter_status']		= $ssFilter->filter_status;
        $this->_params['ssFilter']['filter_keyword']   	= $ssFilter->filter_keyword;
        $this->_params['ssFilter']['filter_item']   	= $ssFilter->filter_item;
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = !empty($ssFilter->pagination_option) ? $ssFilter->pagination_option : 50;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = $this->getRequest()->getPost()->toArray();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function filterAction() {
        $ssFilter	= new Container(__CLASS__);
        
        if($this->getRequest()->isPost()) {
            $data = $this->_params['data'];
            
            $ssFilter->pagination_option = intval($data['pagination_option']);
            
            $ssFilter->order_by         = $data['order_by'];
            $ssFilter->order            = $data['order'];
            
            $ssFilter->filter_status    = $data['filter_status'];
            $ssFilter->filter_keyword   = $data['filter_keyword'];
        }
        if(!empty($this->_params['route']['id'])) {
          	$ssFilter->filter_item = $this->_params['route']['id'];
        }
        
        $this->goRoute(array('route' => 'routeOnline/default'));
        
        return $this->response;
    }
    
    public function indexAction() {
        $myForm	= new \Online\Form\Search\CourseDetail($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
       	//return $this->response;
        $items = $this->getTable()->listItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['items']     	= $items;
        $this->_viewModel['count']      = $this->getTable()->countItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['user']       = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['category']   = $this->getServiceLocator()->get('Online\Model\CourseCategoryTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['item']    	= $this->getServiceLocator()->get('Online\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['caption']    = 'Chi tiết khóa học - '. $this->_viewModel['item'][$this->_params['ssFilter']['filter_item']]['name'];
        return new ViewModel($this->_viewModel);
    }
    
    public function formAction() {
        $myForm	= $this->getForm();
        
        $course = $this->getServiceLocator()->get('Online\Model\CourseItemTable')->getItem(array('id' => $this->_params['ssFilter']['filter_item']));
        
        $task = 'add-item';
        $caption = 'Chi tiết khóa học - '. $course['name'] .' - Thêm mới';
        $item = array();
        if(!empty($this->params('id'))) {
            $this->_params['data']['id'] = $this->params('id');
            $item = $this->getTable()->getItem($this->_params['data']);
            if(!empty($item)) {
                $myForm->setInputFilter(new \Online\Filter\CourseDetail(array('id' => $this->_params['data']['id'])));
                $myForm->bind($item);
                $task = 'edit-item';
                $caption = 'Chi tiết khóa học - '. $course['name'] .' - Sửa';
            }
        }
    
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
    
            $controlAction = $this->_params['data']['control-action'];
            $permission = $this->_params['data']['permission'];
            
            if($myForm->isValid()){
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['course'] = $course;
                $result = $this->getTable()->saveItem($this->_params, array('task' => $task));
    
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
    
                if($controlAction == 'save-new') {
                    $this->goRoute(array('action' => 'form'));
                } else if($controlAction == 'save') {
                    $this->goRoute(array('action' => 'form', 'id' => $result));
                } else {
                    $this->goRoute();
                }
            }
        }
    
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['item']       = $item;
        $this->_viewModel['caption']    = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function viewersAction(){
        $myForm	= new \Online\Form\Search\CourseDetail($this->getServiceLocator());
        $myForm->setData($this->_params['ssFilter']);
        
        $this->_viewModel['myForm']	    = $myForm;
        $viewers = $this->getServiceLocator()->get('Online\Model\CourseDetailTable')->getItem(array('id' => $this->_params['route']['id']));
        $this->_viewModel['detail_item']    	= $this->getServiceLocator()->get('Online\Model\CourseDetailTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['detail']             = $viewers;
        $this->_viewModel['caption']            = 'Danh sách người xem video - '. $viewers['name'];
        return new ViewModel($this->_viewModel);
    }
}
