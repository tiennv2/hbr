<?php
namespace Online\Form;
use \Zend\Form\Form as Form;

class CourseItem extends Form {
	
	public function __construct($sm = null){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'onlineForm',
			'id'		=> 'onlineForm',
		));
		
		// Id
		$this->add(array(
		    'name'			=> 'id',
		    'type'			=> 'Hidden',
		));
		
		// Name
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'name',
				'placeholder'	=> 'Nhập tên',
			    'onchange'      => 'javascript:createAlias(this, \'#alias\');'
			)
		));
		
		// Alias
		$this->add(array(
		    'name'			=> 'alias',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'alias',
		        'onchange'      => 'javascript:createAlias(\'#name\', \'#alias\');'
		    )
		));
		
		// Danh mục
		$this->add(array(
			'name'			=> 'category_id',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
			'options'		=> array(
				'empty_option'	=> '- Chọn -',
				'disable_inarray_validator' => true,
				'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Online\Model\CourseCategoryTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
			),
		));
		
		// Hình ảnh đại diện
		$this->add(array(
			'name'			=> 'image',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'image',
				'placeholder'	=> 'Chọn hình ảnh',
			),
			'options'		=> array(
				'type'	    => 'open-file',
				'group'	    => 'images',
			),
		));

		// Teacher
		$this->add(array(
		    'name'			=> 'teacher',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'empty_option'	=> '- Chọn -',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    )
		));

		// Meta Url
		$this->add(array(
		    'name'			=> 'meta_url',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'class'			=> 'form-control',
		        'id'			=> 'meta_url',
		    )
		));

		// Giá
		$this->add(array(
			'name'			=> 'price',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_currency',
			)
		));
		
		// Giảm giá theo %
		$this->add(array(
			'name'			=> 'price_sale_percent',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_percent',
			)
		));
		
		// Giảm giá theo giá tiền
		$this->add(array(
			'name'			=> 'price_sale',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control mask_currency',
			)
		));

		// Phân loại
		$this->add(array(
		    'name'			=> 'type',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control select2 select2_basic',
		    ),
		    'options'		=> array(
		        'value_options'	=> array( 'default' => 'Mặc định', 'free' => 'Tặng'),
		    )
		));
		
		// Description
		$this->add(array(
			'name'			=> 'description',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'description',
			)
		));
		
		// Content
		$this->add(array(
			'name'			=> 'content',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'content',
			)
		));
		
		// ID khóa học bên CRM được phép truy cập
		$this->add(array(
			'name'			=> 'course_id',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'		=> 'form-control',
			)
		));
		
		// Địa điểm
		$this->add(array(
			'name'			=> 'location',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'location',
			)
		));
		
		// Tài liệu chuẩn bị
		$this->add(array(
			'name'			=> 'document',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'document',
			)
		));
		
		// Liên kết thành viên
		$this->add(array(
			'name'			=> 'networking',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'networking',
			)
		));
		
		// Tiện ích
		$this->add(array(
			'name'			=> 'utility',
			'type'			=> 'Textarea',
			'attributes'	=> array(
				'class'			=> 'form-control',
				'id'			=> 'utility',
			)
		));
		
		// Đường dẫn giới thiệu đến khóa học
		$this->add(array(
			'name'			=> 'url',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'			=> 'form-control',
			)
		));
		
		// Ordering
		$this->add(array(
		    'name'			=> 'ordering',
		    'type'			=> 'Text',
		    'attributes'	=> array(
		        'value'         => 255,
		        'class'			=> 'form-control',
		    )
		));
		
		// Status
		$this->add(array(
			'name'			=> 'status',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
		    'options'		=> array(
		        'value_options'	=> array( 1	=> 'Hiển thị', 0 => 'Không hiển thị'),
		    )
		));
	}
}