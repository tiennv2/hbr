<?php
namespace Online\Model;

use Zend\Db\Sql\Select;

class CourseCategoryTable extends DefaultTable {
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];

	            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		            $select    	-> order(array('ordering' => 'ASC'));
		        })->toArray();

	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
	        })->count();
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$paginator = $arrParam['paginator'];
				$ssFilter  = $arrParam['ssFilter'];

				$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		            $select    	-> order(array('ordering' => 'ASC'));
		        })->toArray();
				 
				$select -> limit($paginator['itemCountPerPage'])
						-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
		
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
					$select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
		
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
					$select -> where -> equalTo('status', $ssFilter['filter_status']);
				}
		
				if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
			});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'CourseCategory';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(array('ordering' => 'ASC', 'name' => 'ASC'));
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
		return $result;
	}

	public function itemInSelectbox($arrParam = null, $options = null){
	    if($options['task'] == 'list-level') {
	        $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select    -> columns(array('id', 'level'))
	                       -> order(array('level' => 'DESC'))
	                       -> limit(1);
	        })->current();
	        
	        $result = array();
	        if(!empty($items)) {
	            for ($i = 1; $i <= $items->level; $i++) {
	                $result[$i] = 'Level ' . $i;
	            }
	        }
	    }
	    
	    if($options['task'] == 'form-category') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select    	-> order(array('ordering' => 'ASC'))
	            			-> order(array('left' => 'ASC'));
	        })->toArray();
	    }
	    
	    if($options['task'] == 'form-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select    	-> order(array('left' => 'ASC'))
	                       	-> where->greaterThan('level', 0);
	        })->toArray();
	    }
	
	    return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'name'          => $arrData['name'],
				'alias'         => $arrData['alias'],
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'created'       => date('Y-m-d H:i:s'),
				'created_by'    => $this->userInfo->getUserInfo('id'),
				'parent'            => $arrData['parent'],
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          => $arrData['name'],
			    'alias'         => $arrData['alias'],
				'ordering'      => $arrData['ordering'],
				'status'        => $arrData['status'],
				'parent'        => $arrData['parent'],
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}