<?php
namespace Online\Model;

use Zend\Db\Sql\Select;

class CourseItemTable extends DefaultTable {
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	        	if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
	        })->current();
	    }
	    
	    if($options['task'] == 'user-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	    		$select -> where -> equalTo('status', 1);
	            
	        	if(!empty($arrParam['data']['category_id'])) {
	    			$select -> where -> equalTo('category_id', $arrParam['data']['category_id']);
	    		}
	    		
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$paginator = $arrParam['paginator'];
				$ssFilter  = $arrParam['ssFilter'];
				 
				$select -> limit($paginator['itemCountPerPage'])
						-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
		
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
					$select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
		
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
					$select -> where -> equalTo('status', $ssFilter['filter_status']);
				}
		
				if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
		
			});
		}
		
		if($options['task'] == 'list-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> where -> equalTo('status', 1);
				$select	-> order(array('ordering' => 'ASC'));
				
				if(!empty($arrParam['category_id'])) {
					$select -> where -> equalTo('category_id', $arrParam['category_id']);
				}
			});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'CourseItem';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('ordering' => 'ASC', 'name' => 'ASC'))
	                       ->where->equalTo('status', 1);
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'user-item') {
	    	$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	    		$paginator = $arrParam['paginator'];
	    		$ssFilter  = $arrParam['ssFilter'];
	    		
	    		$select -> join(TABLE_COURSE_CATEGORY, TABLE_COURSE_CATEGORY .'.id='. TABLE_COURSE_ITEM .'.category_id', array('category_type' => 'type'), 'inner');
	    		
	    		if(!isset($options['paginator']) || $options['paginator'] == true) {
	    			$select -> limit($paginator['itemCountPerPage'])
	    					-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
	    		}
	    		$select -> where -> equalTo(TABLE_COURSE_ITEM .'.status', 1);
	    		
	    		$select	-> order(array(TABLE_COURSE_ITEM .'.ordering' => 'ASC'));
	    
	    		if(!empty($arrParam['data']['category_id'])) {
	    			$select -> where -> equalTo(TABLE_COURSE_ITEM .'.category_id', $arrParam['data']['category_id']);
	    		} else {
	    			$select -> where -> equalTo(TABLE_COURSE_CATEGORY .'.type', 'free');
	    		}
	    		
	    	});
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
		
		if($options['task'] == 'alias') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $select->where->equalTo('alias', $arrParam['alias']);
		    })->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $number   = new \ZendX\Functions\Number();
	    $filter   = new \ZendX\Filter\Purifier();
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'add-item') {
			$id = $gid->getId();
			$data	= array(
				'id'            		=> $id,
				'name'          		=> $arrData['name'],
				'alias'         		=> $arrData['alias'],
				'description'   		=> $arrData['description'],
				'content'       		=> $arrData['content'],
				'location'       		=> $arrData['location'],
				'document'       		=> $arrData['document'],
				'networking'       		=> $arrData['networking'],
				'utility'       		=> $arrData['utility'],
				'price'   	 			=> $arrData['price'] ? $number->formatToData($arrData['price']) : null,
			    'price_sale_percent'	=> $arrData['price_sale_percent'] ? $number->formatToData($arrData['price_sale_percent']) : null,
			    'price_sale'			=> $arrData['price_sale'] ? $number->formatToData($arrData['price_sale']) : null,
				'image'         		=> $image->getFull(),
				'image_medium'  		=> $image->getMedium(),
				'image_thumb'   		=> $image->getThumb(),
				'ordering'      		=> $arrData['ordering'],
				'status'        		=> $arrData['status'],
				'type'	        		=> $arrData['type'],
				'teacher'        		=> $arrData['teacher'],
				'meta_url'        		=> $arrData['meta_url'],
				'created'       		=> date('Y-m-d H:i:s'),
				'created_by'    		=> $this->userInfo->getUserInfo('id'),
				'category_id'   		=> $arrData['category_id'],
				'course_id'   			=> str_replace(' ', '', $arrData['course_id']),
				'url'   				=> $arrData['url'],
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		if($options['task'] == 'edit-item') {
		    $id = $arrData['id'];
			$data	= array(
				'name'          		=> $arrData['name'],
			    'alias'         		=> $arrData['alias'],
				'description'   		=> $arrData['description'],
				'content'       		=> $arrData['content'],
				'location'       		=> $arrData['location'],
				'document'       		=> $arrData['document'],
				'networking'       		=> $arrData['networking'],
				'utility'       		=> $arrData['utility'],
				'price'   	 			=> $arrData['price'] ? $number->formatToData($arrData['price']) : null,
			    'price_sale_percent'	=> $arrData['price_sale_percent'] ? $number->formatToData($arrData['price_sale_percent']) : null,
			    'price_sale'			=> $arrData['price_sale'] ? $number->formatToData($arrData['price_sale']) : null,
				'image'         		=> $image->getFull(),
				'image_medium'  		=> $image->getMedium(),
				'image_thumb'   		=> $image->getThumb(),
				'ordering'      		=> $arrData['ordering'],
				'status'        		=> $arrData['status'],
				'teacher'        		=> $arrData['teacher'],
				'type'	        		=> $arrData['type'],
				'meta_url'        		=> $arrData['meta_url'],
				'category_id'   		=> $arrData['category_id'],
				'course_id'   			=> str_replace(' ', '', $arrData['course_id']),
				'url'   				=> $arrData['url'],
			);
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
	public function deleteItem($arrParam = null, $options = null){
	    if($options['task'] == 'delete-item') {
	        $result = $this->defaultDelete($arrParam, null);
	    }
	
	    return $result;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    if($options['task'] == 'change-status') {
	        $result = $this->defaultStatus($arrParam, null);
	    }
	     
	    return $result;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    if($options['task'] == 'change-ordering') {
	        $result = $this->defaultOrdering($arrParam, null);
	    }
	    return $result;
	}
}