<?php

namespace Post;

return array (
	'controllers' => array(
		'invokables' => array(
			'Post\Controller\Api'		    => Controller\ApiController::class,
			'Post\Controller\Index'		    => Controller\IndexController::class,
			'Post\Controller\Notice'		=> Controller\NoticeController::class,
			'Post\Controller\Form'		    => Controller\FormController::class,
			'Post\Controller\Product'       => Controller\ProductController::class,
			'Post\Controller\Online'        => Controller\OnlineController::class,
		)
	),
    'view_manager' => array(
        'doctype'					=> 'HTML5',
        'template_path_stack'		=> array(__DIR__ . '/../view'),
    )
);


