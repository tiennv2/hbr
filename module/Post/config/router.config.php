<?php

$routeHome = array(
    'type' => 'Literal',
    'options' => array (
        'route' => '/',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
);

$routePostCategory = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<alias>[^\/]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Index',
            'action' 			=> 'category',
        ),
        'spec' 		=> '/%alias%',
    ),
);

$routePostPaginator = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<alias>[^\/]*)/page/(?<page>[0-9]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Index',
            'action' 			=> 'category',
        ),
        'spec' 		=> '/%alias%/page/%page%',
    ),
);

$routePostItem = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/(?<category_alias>[^\/]*)/(?<alias>[^\/]*).html',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Index',
            'action' 			=> 'item',
        ),
        'spec' 		=> '/%category_alias%/%alias%.html',
    ),
);

$routePost = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/post',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/id/:id]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9_-]*',
                    'action' 		=> '[a-zA-Z0-9_-]*',
                    'id' 		    => '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array ()
            )
        )
    )
);

$routeTeacher = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/teacher',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Teacher',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:alias][/]',
                'constraints' => array (
                    'alias' 	=> '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array (
                    'controller' => 'teacher',
                    'action' => 'item'
                )
            )
        )
    )
);

$routeLanguage = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/language/(?<code>[a-z]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Admin\Controller',
            'controller' 		=> 'api',
            'action' 			=> 'language',
        ),
        'spec' 		=> '/language/%code%',
    ),
);

$routeSearch = array(
    'type' 		=> 'Segment',
    'options' => array (
        'route' => '/search',
        'defaults' => array (
            '__NAMESPACE__' => 'Post\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'search'
        )
    ),
);

$routePageRegister = array(
	'type' 		=> 'Regex',
	'options' 	=> array(
		'regex' 	=> '/(?<alias>[^\/]*)/dang-ky(/)?',
		'defaults' 	=> array(
			'__NAMESPACE__' 	=> 'Post\Controller',
			'controller' 		=> 'Index',
			'action' 			=> 'page',
		),
		'spec' 		=> '/%alias%/dang-ky',
	),
);

$routePageThankyou = array(
    'type'      => 'Regex',
    'options'   => array(
        'regex'     => '/(?<alias>[^\/]*)/thankyou(/)?',
        'defaults'  => array(
            '__NAMESPACE__'     => 'Post\Controller',
            'controller'        => 'Index',
            'action'            => 'page',
        ),
        'spec'      => '/%alias%/thankyou',
    ),
);

$routePageSurvey = array(
    'type'      => 'Regex',
    'options'   => array(
        'regex'     => '/feedback/(?<course_id>[a-z0-9_-]*)/(?<contract_id>[a-z0-9_-]*)(/)?',
        'defaults'  => array(
            '__NAMESPACE__'     => 'Post\Controller',
            'controller'        => 'Index',
            'action'            => 'survey',
        ),
        'spec'      => '/feedback/%course_id%/%contract_id%',
    ),
);

$routeKhaoSat = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/khao-sat-in-house/(?<code>[a-z0-9_-]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'index',
            'action' 			=> 'category',
            'alias'             => 'khao-sat-in-house'
        ),
        'spec' 		=> '/khao-sat-in-house/%code%',
    ),
);

$routeCourse = array(
    'type' => 'Literal',
    'options' => array (
        'route' => '/khoa-hoc-online',
        'defaults' => array (
            '__NAMESPACE__'     => 'Post\Controller',
            'controller'        => 'Online',
            'action'            => 'index',
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/id/:id]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9_-]*',
                    'action' 		=> '[a-zA-Z0-9_-]*',
                    'id' 		    => '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array ()
            )
        ),
        'category' => array (
            'type'      => 'Regex',
            'options'   => array (
                'regex' => '/(?<alias>[a-zA-Z0-9-_]*)(/)?',
                'defaults' => array (
                    'controller'    => 'Online',
                    'action'        => 'category',
                ),
                'spec'      => '/%alias%/',
            )
        ),
        'detail' => array (
            'type'      => 'Regex',
            'options'   => array (
                'regex' => '/(?<category_alias>[a-zA-Z0-9-_]*)/(?<alias>[a-zA-Z0-9-_]*)(/)?',
                'defaults' => array (
                    'controller'    => 'Online',
                    'action'        => 'detail',
                ),
                'spec'      => '/%category_alias%/%alias%',
            )
        ),
    )
);

$routeBuy = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/khoa-hoc-online/mua-khoa-hoc/(?<alias>[a-zA-Z0-9-_]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Online',
            'action' 			=> 'buy',
        ),
        'spec' 		=> '/khoa-hoc-online/mua-khoa-hoc/%alias%',
    ),
);

$routeCourseCart = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/khoa-hoc-online/course-cart',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Online',
            'action' 			=> 'course-cart',
        ),
        'spec' 		=> '/khoa-hoc-online/course-cart',
    ),
);

$routeSuccess = array(
    'type' 		=> 'Regex',
    'options' 	=> array(
        'regex' 	=> '/khoa-hoc-online/thankyou/(?<alias>[a-zA-Z0-9-_]*)(/)?',
        'defaults' 	=> array(
            '__NAMESPACE__' 	=> 'Post\Controller',
            'controller' 		=> 'Online',
            'action' 			=> 'success',
            'alias'             => 'success'
        ),
        'spec' 		=> '/khoa-hoc-online/thankyou/%alias%',
    ),
);

return array (
    'router' => array(
        'routes' => array(
            'routePostCategory'     => $routePostCategory,
            'routePostPaginator'    => $routePostPaginator,
            'routePostItem'         => $routePostItem,
            'routeHome'             => $routeHome,
            'routePost'             => $routePost,
            'routeTeacher'          => $routeTeacher,
            'routeLanguage'         => $routeLanguage,
            'routeSearch'           => $routeSearch,
            'routePageRegister'     => $routePageRegister,
            'routeKhaoSat'          => $routeKhaoSat,
            'routePageThankyou'     => $routePageThankyou,
            'routeCourse'           => $routeCourse,
            'routeBuy'              => $routeBuy,
            'routeSuccess'          => $routeSuccess,
            'routeCourseCart'       => $routeCourseCart,
            'routePageSurvey'       => $routePageSurvey,
        ),
    )
);