<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;

class ApiController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/default');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function listCourseAction() {

        $this->_viewModel['items'] = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params['data'], array('task' => 'list-course'));

        $this->_viewModel['teacher'] = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    public function productNewAction() {
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    public function productHotAction() {
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
    public function productHighlightAction() {
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}
