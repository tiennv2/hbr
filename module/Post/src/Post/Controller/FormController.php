<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;

class FormController extends ActionController {
    
    public function init() {
        $this->setLayout('template/default/full');
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function addAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $this->_params['form'] = $this->getServiceLocator()->get('Admin\Model\FormTable')->getItem(array('id' => $this->_params['data']['form_id'])); 
            $filter = new \Post\Filter\FormRegister($this->_params);
            
            $result = array();
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $result['save'] = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->saveItem($this->_params, array('task' => 'public-add'));
            }
            
            echo json_encode($result);
        } else {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        
        return $this->response;
    }
    
    public function updateAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            $form_data_id = $this->_params['data']['form_data_id'];
            if(empty($form_data_id)) {
                $result['error']['form_data_id'] = 'Không tìm thấy phần tử cần sửa';
            } else {
                $result['save'] = $this->getServiceLocator()->get('Admin\Model\FormDataTable')->saveItem($this->_params, array('task' => 'public-update'));
            }
            
            echo json_encode($result);
        } else {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        
        return $this->response;
    }
}
