<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use ZendX\System\ContactInfo;

class IndexController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/default');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute(), $_GET);
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author' => $this->_settings['General.Meta.Author']['value'],
            'image' => $this->_settings['General.Meta.Image']['image'],
            'title' => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->setLayout('layout/home');
        
        return new ViewModel($this->_viewModel);
    }
    
    public function categoryAction() {
        // Lấy thông tin cấu hình module
        $settingPost = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'Post'), array('task' => 'cache-by-code')); // Sau này không cần
      
        // Lấy thông tin phần tử
        $item       = $this->getServiceLocator()->get('Admin\Model\PageTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        if(!empty($item)) {
            if($item['alias'] == 'dang-ky') {
                $userInfo = new \ZendX\System\ContactInfo();
                if(!empty($userInfo->getContactInfo())) {
                    return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                }
            }
	        // Thiết lập layout hiển thị
	        if(!empty($item['layout'])) {
	        	$this->setLayout('template/'. $item['layout']);
	        }
	        
	        $this->_viewModel['item']       = $item;
        } else {
	        // Lấy thông tin danh mục
	        $category = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
	        
	        // Kiểm tra nếu không tồn tại thì đến trang thông báo
	        if(empty($category) || $category['status'] == 0) {
	            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
	        }
	        
	        // Thiết lập layout hiển thị
	        if(!empty($category['layout'])) {
	            $this->setLayout('layout/'. $category['layout']);
	        }
	        
	        // Lấy danh sách breadcrumb
	        $breadcrumb = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-breadcrumb'));
	        $urlHelper  = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
	        foreach ($breadcrumb AS $key => $val) {
	            $link   = $urlHelper('routePostCategory', array('alias' => $val['alias']), true);
	            $name   = $val['name'];
	            $this->_params['breadcrumb'][] = array(
	                'url' => $link,
	                'name' => $name
	            );
	        }
	        
	        // Lấy danh sách node trên nhánh
	        $this->_params['branche'] = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-branch'));
	        
	        // Thiết lập lại thông số phân trang khi đọc được cấu hình module
	        $wordFirst = new \ZendX\Functions\WordFirst();
	        $codeType = $wordFirst->upperFirst($category['type']);
	        $this->_paginator['itemCountPerPage'] = $settingPost['Post.'. $codeType .'.ItemCountPerPage'] ? intval($settingPost['Post.'. $codeType .'.ItemCountPerPage']['value']) : 10;
	        $this->_paginator['pageRange'] = $settingPost['Post.'. $codeType .'.PageRange'] ? intval($settingPost['Post.'. $codeType .'.PageRange']['value']) : 5;
	        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
	        $this->_params['paginator'] = $this->_paginator;

            $this->_params['speaker'] = $this->params()->fromPost('speaker');
	        
	        // Lấy danh sách dữ liệu

            if ($_POST['speaker']) {
                $items = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params, array('task' => 'list-item'));
                $count = $this->getServiceLocator()->get('Post\Model\PostItemTable')->countItem($this->_params, array('task' => 'list-item'));
            } else {

                $items = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params, array('task' => 'list-item'));
                $count = $this->getServiceLocator()->get('Post\Model\PostItemTable')->countItem($this->_params, array('task' => 'list-item'));

            }
	        
	        // Update lượt view
	        $ssFilter = new Container(__CLASS__);
	        $ssCategory = $ssFilter->category;
	        $ssCategory[$category['id']] = 1;
	        $ssFilter->category = $ssCategory;
	        if(empty($ssFilter->category[$category['id']])) {
	            $this->getServiceLocator()->get('Admin\Model\PostCategoryTable')->saveItem(array('data' => array('id' => $category['id'])), array('task' => 'update-view'));
	        }
	        
	        // Nếu chỉ có 1 phần tử thì hiển thị chi tiết luôn
	        if($count == 1) {
	            $this->_viewModel['item']   = $items->current();
	        }
	        
	        // Thiết lập Meta
	        $this->_params['meta']['image'] 		= $category['image_medium'];
	        $this->_params['meta']['title'] 		= $category['meta_title'] ? $category['meta_title'] : $category['name'];
	        $this->_params['meta']['description'] 	= $category['meta_description'];
	        $this->_params['meta']['keywords'] 		= $category['meta_keywords'];
	        $this->_params['meta']['category_id'] 	= $category['id'];
	        
	        $this->_viewModel['category']   = $category;
	        $this->_viewModel['items']      = $items;
	        $this->_viewModel['count']      = $count;
	        $this->_viewModel['teacher']    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        }
        
        $this->_viewModel['params'] = $this->_params;
        return new ViewModel($this->_viewModel);
    }
    
    public function itemAction() {
        // Lấy cấu hình trong admin
        $settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'Post'), array('task' => 'cache-by-code'));
        
        // Lấy thông tin cấu hình module
        $settingPost    = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'Post'), array('task' => 'cache-by-code'));
        
        // Lấy thông tin phần tử và danh mục
        $item           = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        $category       = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->getItem(array('id' => $item['post_category_id']), array('task' => 'get-by-id'));
        
        // Kiểm tra nếu không tồn tại thì đến trang thông báo
        if(empty($item) || empty($category) || $item['status'] == 0 || $category['status'] == 0) {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
        
        // Lấy bài viết liên quan
        $itemInvolve    = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem(array('item' => $item, 'category' => $category, 'settings' => $settings), array('task' => 'list-item-involve'));

        // Thiết lập layout hiển thị
        if(!empty($item['layout'])) {
            $this->setLayout('layout/'. $item['layout']);
        } else if(!empty($category['layout'])) {
            $this->setLayout('layout/'. $category['layout']);
        } else {
            $this->setLayout('layout/default');
        }
        
        // Lấy danh sách breadcrumb
        $breadcrumb = $this->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem($category, array('task' => 'list-breadcrumb'));
        $urlHelper  = $this->getServiceLocator()->get('viewhelpermanager')->get('url');
        foreach ($breadcrumb AS $key => $val) {
            $link   = $urlHelper('routePostCategory', array('alias' => $val['alias'], 'index' => $val['index']), true);
            $name   = $val['name'];
            $this->_params['breadcrumb'][] = array(
                'url' => $link,
                'name' => $name
            );
        }
        $this->_params['breadcrumb'][] = array(
            'url' => null,
            'name' => $item['name']
        );
        
        // Thiết lập Meta
        $this->_params['meta']['image']         = $item['image_medium'];
        $this->_params['meta']['title']         = $item['meta_title'] ? $item['meta_title'] : $item['name'];
        $this->_params['meta']['description']   = $item['meta_description'];
        $this->_params['meta']['keywords']      = $item['meta_keywords'];
        $this->_params['meta']['category_id']   = $category['id'];
            
        $this->_viewModel['params']             = $this->_params;
        $this->_viewModel['category']           = $category;
        $this->_viewModel['item']               = $item;
        $this->_viewModel['itemInvolve']        = $itemInvolve;
        $this->_viewModel['user']               = $this->getServiceLocator()->get('Admin\Model\UserTable')->listItem(null, array('task' => 'cache'));
        $this->_viewModel['teacher']            = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'teacher')), array('task' => 'cache'));
        $this->_viewModel['city']               = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array('where' => array('code' => 'city')), array('task' => 'cache'));
        return new ViewModel($this->_viewModel);
    }
    
    public function searchAction() {
        // Lấy danh sách breadcrumb
        $this->_params['breadcrumb'][] = array(
            'url' => '',
            'name' => 'Tìm kiếm'
        );
        
        // Thiết lập lại thông số phân trang khi đọc được cấu hình module
        $this->_paginator['itemCountPerPage'] = 20;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $_GET['page'] ? $_GET['page'] : 1;
        $this->_params['paginator'] = $this->_paginator;
        $this->_params['data']['keyword'] = $_GET['keyword'] ? $_GET['keyword'] : null;
        
        // Lấy danh sách dữ liệu
        $items = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params, array('task' => 'list-search'));
        $count = $this->getServiceLocator()->get('Post\Model\PostItemTable')->countItem($this->_params, array('task' => 'list-search'));
        
        // Thiết lập Meta
        $this->_params['meta']['title'] = 'Tìm kiếm';
        
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $count;
        $this->_viewModel['params']     = $this->_params;
        return $this->_viewModel;
    }

    public function pageAction() {
    	$item = $this->getServiceLocator()->get('Admin\Model\PageTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
    	if(empty($item)) {
    		$this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
    	}
        
        if($this->_params['route']['routeName'] == 'routePageThankyou') {
            $this->setLayout('template/'. 'thankyou/index');
        } else {
        	if(!empty($item['form'])) {
        		$this->setLayout('template/'. $item['form']);
        	} else{
        		$this->setLayout('template/'. 'form-default/index');
        	}
        }
    	
    	// Thiết lập Meta
    	$this->_params['meta']['image']         = $item['image_medium'];
    	$this->_params['meta']['title']         = $item['meta_title'] ? $item['meta_title'] : $item['name'];
    	$this->_params['meta']['description']   = $item['meta_description'];
    	$this->_params['meta']['keywords']      = $item['meta_keywords'];
    	
    	$this->_viewModel['item'] = $item;
    	$this->_viewModel['params'] = $this->_params;
    	return new ViewModel($this->_viewModel);
    }
    
    public function surveyAction() {
        $this->setLayout('template/khao-sat/index');
        
        $this->_viewModel['params'] = $this->_params;
        return new ViewModel($this->_viewModel);
    }

    //TienNV (HBR-Online)
    public function courseAction() {
        $this->setLayout('layout/online');
        
        $userInfo = new ContactInfo();
        
        // Kiểm tra xem khóa học này đã được mua chưa
        $course = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem($this->_params['route'], array('task' => 'alias'));
        $contact_course = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->getItem(array('contact_id' => $userInfo->getContactInfo('id'), 'course_id' => $course['id']), array('task' => 'check-course'));
        if(empty($course)) {
            return $this->redirect()->toRoute('routeHome');
        }
        
        $detail  = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem(array('course_id' => $course['id']), array('task' => 'public-course'));
              
        // Lấy danh sách breadcrumb
        $this->_params['breadcrumb'][] = array(
            'url' => '',
            'name' => 'Khóa học'
        );
        
        $this->_viewModel['userInfo']       = $userInfo->getContactInfo();
        $this->_viewModel['contact_course'] = $contact_course;
        $this->_viewModel['course']         = $course;
        $this->_viewModel['params']         = $this->_params;
        return new ViewModel($this->_viewModel);
    }

    //DaiNV - Get list course for Landing Page
    public function getListCourseAction() {
        $this->_viewModel['items'] = $this->getServiceLocator()->get('Post\Model\PostItemTable')->listItem($this->_params['data'], array('task' => 'list-course'));
        
        $viewModel = new ViewModel($this->_viewModel);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}
