<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;
use Zend\Session\Container;

class OnlineController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/online-home');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\CourseItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute(), $_GET);
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author' => $this->_settings['General.Meta.Author']['value'],
            'image' => $this->_settings['General.Meta.Image']['image'],
            'title' => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['language'] = $this->_language;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->setLayout('layout/online-home');
        
        return new ViewModel($this->_viewModel);
    }
    
    // Category
    public function categoryAction() {
        $this->setLayout('layout/online-course');
        
        
        // Lấy thông tin danh mục dựa vào alias từ route
        $category = $this->getServiceLocator()->get('Post\Model\CourseCategoryTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        
        // Kiểm tra nếu không tồn tại thì đến trang thông báo
        if(empty($category) || $category['status'] == 0) {
            $this->goRoute(array('route' => 'routePost/default', 'controller' => 'notice', 'action' => 'no-data'));
        }
         
        // Thiết lập layout hiển thị
        if(!empty($category['layout'])) {
            $this->setLayout('layout/'. $category['layout']);
        }
        
        // Lấy toàn bộ khóa học từ danh mục trên
        $items = $this->getServiceLocator()->get('Post\Model\CourseItemTable')->listItem($this->_params, array('task' => 'list-all'));
        $count = $this->getServiceLocator()->get('Post\Model\CourseItemTable')->countItem($this->_params, array('task' => 'list-item'));
        
        $this->_viewModel['category']   = $category;
        $this->_viewModel['items']      = $items;
        $this->_viewModel['count']      = $count;
        return new ViewModel($this->_viewModel);
    }
    
    public function detailAction() {
        $this->setLayout('layout/online-course');
        
        $userInfo = new ContactInfo();
        
        // Kiểm tra xem khóa học này đã được mua chưa
        $course = $this->getServiceLocator()->get('Post\Model\CourseItemTable')->getItem($this->_params['route'], array('task' => 'get-by-alias'));
        $contact_course = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->getItem(array('contact_id' => $userInfo->getContactInfo('id'), 'course_id' => $course['id']), array('task' => 'check-course'));
        
        //$detail  = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem(array('item_id' => $course['id']), array('task' => 'list-all'));
        //$group   = $this->getServiceLocator()->get('Admin\Model\CourseGroupTable')->listItem(array('group_id' => $detail['id']), array('task' => 'list-all'));
              
        // Lấy danh sách breadcrumb
        $this->_params['breadcrumb'][] = array(
            'url' => '',
            'name' => 'Khóa học online'
        );
        
        $this->_viewModel['userInfo']       = $userInfo->getContactInfo();
        $this->_viewModel['contact_course'] = $contact_course;
        $this->_viewModel['course_detail']  = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem($this->_params, array('task' => 'cache'));
        $this->_viewModel['course_group']   = $this->getServiceLocator()->get('Admin\Model\CourseGroupTable')->listItem($this->_params, array('task' => 'cache'));
        $this->_viewModel['teacher']        = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache'));
        $this->_viewModel['course']         = $course;
        $this->_viewModel['params']         = $this->_params;
        return new ViewModel($this->_viewModel);
    }
    
    public function buyAction() {
        $this->setLayout('layout/online-buy');
        $userInfo = new ContactInfo();
        
        if(empty($userInfo->getContactInfo())) {
            $linkLogin = $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'), array('force_canonical' => true));
            $linkLogin = $linkLogin .'?redirect=' . $_SERVER['REDIRECT_URL'];
            $this->redirect()->toUrl($linkLogin);
        }
        
        $course = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem($this->_params['route'], array('task' => 'alias'));
        $contact_course = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->getItem(array('contact_id' => $userInfo->getContactInfo('id'), 'course_id' => $course['id']), array('task' => 'check-course'));
        
        
        // Lấy danh sách breadcrumb
        $this->_params['breadcrumb'] = array(
            array(
                'url' => null,
                'name' => 'Mua khóa học'
            ),
            array(
                'url' => null,
                'name' => 'Chọn hình thức thanh toán'
            ),
        );
        
        if($this->getRequest()->isPost()){
            $errors = array();
            if(!isset($this->_params['data']['payment'])) {
                $errors['payment'] = 'Vui lòng chọn hình thức thanh toán của bạn';
            }
            if(empty($this->_params['data']['name'])) {
                $errors['name'] = 'Giá trị này là bắt buộc';
            }
            if(empty($this->_params['data']['phone'])) {
                $errors['phone'] = 'Giá trị này là bắt buộc';
            }
            if(empty($this->_params['data']['address'])) {
                $errors['address'] = 'Giá trị này là bắt buộc';
            }
            
            if(count($errors) == 0){
                // Lưu thông tin đơn hàng
                $this->_params['data']['contact_id']    = $userInfo->getContactInfo('id');
                $this->_params['data']['course_id']     = $course['id'];
                $this->_params['data']['payment_id']    = $this->_params['data']['payment'];
                $result = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($this->_params, array('task' => 'buy-course'));
        
                return $this->redirect()->toRoute('routeSuccess');
            }
        }
        
        $this->_viewModel['errors']         = $errors;
        $this->_viewModel['params']         = $this->_params;
        $this->_viewModel['course']         = $course;
        $this->_viewModel['contact_course'] = $contact_course;
        $this->_viewModel['payments']       = $this->getServiceLocator()->get('Admin\Model\PaymentTable')->listItem(null, array('task' => 'list-all'));
        $this->_viewModel['userInfo']       = $userInfo->getContactInfo();
        return new ViewModel($this->_viewModel);
    }
    
    public function successAction() {
        $this->setLayout('layout/none');
    
        return new ViewModel($this->_viewModel);
    }
    
    public function videoAction() {
        $this->setLayout('layout/video');
    
        $userInfo = new ContactInfo();
        if(empty($userInfo->getContactInfo())) {
            $arrRoute = array('controller' => 'index', 'action' => 'login');
            $this->redirect()->toRoute('routeUser/default', $arrRoute);
        }
    
        $this->_viewModel['userInfo']   = $userInfo->getContactInfo();
        return new ViewModel($this->_viewModel);
    }
    
    public function courseCartAction(){
        $this->setLayout('layout/online-cart');
        $ssFilter = new Container('myCourse');
        $userInfo = new ContactInfo();
        $productCart = $ssFilter->data;
        
        $error = '';
        if($this->getRequest()->isPost()){
            if($this->_params['data']['address'] == null ){
                $error = 'Vui lòng nhập đầy đầy đủ các thông tin: Họ tên, email, điện thoại, phương thức thanh toán và địa chỉ giao hàng';
                
                
            } else {
                if ($userInfo->getContactInfo()['id']){
                    $this->_params['data']['contact_id'] = $userInfo->getContactInfo()['id'];
                    $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($this->_params, array('task' => 'add-item'));
                    
                    // Gửi mail
                    /* $options['fromName']    = 'Hệ thống';
                     $options['to']          = 'sale2.tooltechvietnam@gmail.com'; //sale2.tooltechvietnam@gmail.com
                     $options['toName']      = 'Đặt hàng';
                     $options['subject']     = 'Đặt hàng - ' . @date('d/m/Y H:i');
                     $options['content']     = '
                     <p>Có 1 đơn đặt hàng</p>
                     <p>Thông tin khách hàng</p>
                     <p>Họ tên: '. $this->_params['data']['name'] .'</p>
                     <p>Điện thoại: '. $this->_params['data']['phone'] .'</p>
                     <p>Email: '. $this->_params['data']['email'] .'</p>
                     <p>Nội dung: '. $this->_params['data']['note'] .'</p>
                     <p>Vui lòng truy cập vào admin để xem chi tiết các sản phẩm</p>
                     ';
                     $mailObj = new \ZendX\Mail\Mail();
                     $mailObj->sendMail($options); */
                    
                    $ssFilter->submitData = $this->_params['data'];
                    
                    //$this->goRoute(array('route' => 'routeCourse/default', 'controller' => $this->_params['controller'], 'action' => 'success-cart'));
                    $error = 'success';
                } else {
                    
                    $this->_params['data']['contact_id'] = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'register'));
                    $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($this->_params, array('task' => 'add-item'));
                    
                    // Gửi mail
                    /* $options['fromName']    = 'Hệ thống';
                     $options['to']          = 'sale2.tooltechvietnam@gmail.com'; //sale2.tooltechvietnam@gmail.com
                     $options['toName']      = 'Đặt hàng';
                     $options['subject']     = 'Đặt hàng - ' . @date('d/m/Y H:i');
                     $options['content']     = '
                     <p>Có 1 đơn đặt hàng</p>
                     <p>Thông tin khách hàng</p>
                     <p>Họ tên: '. $this->_params['data']['name'] .'</p>
                     <p>Điện thoại: '. $this->_params['data']['phone'] .'</p>
                     <p>Email: '. $this->_params['data']['email'] .'</p>
                     <p>Nội dung: '. $this->_params['data']['note'] .'</p>
                     <p>Vui lòng truy cập vào admin để xem chi tiết các sản phẩm</p>
                     ';
                     $mailObj = new \ZendX\Mail\Mail();
                     $mailObj->sendMail($options); */
                    
                    $ssFilter->submitData = $this->_params['data'];
                    
                    //$this->goRoute(array('route' => 'routeCourse/default', 'controller' => $this->_params['controller'], 'action' => 'success-cart'));
                    $error = 'success';
                }
            }
            echo $error;
            return $this->response;
        }
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Giỏ hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_params['error']	         = $error;
        $this->_viewModel['params']	     = $this->_params;
        $this->_viewModel['userInfo']    = $userInfo->getContactInfo();
        $viewModel = new ViewModel($this->_viewModel);
        return $viewModel;
    }
    
    public function addCartAction(){
        $ssFilter = new Container('myCourse');
        
        //Lấy thông tin sản phẩm theo id
        $item   = $this->getServiceLocator()->get('Post\Model\CourseItemTable')->getItem(array('id' => $this->_params['data']['id']));
        $dataCart = $ssFilter->data;
        $json = array();
        if(empty($dataCart[$item['id']])){
            $number = !empty((int)$this->_params['data']['number']) ? (int)$this->_params['data']['number'] : 1;
            
            $price = intval($price);
            $dataCart[$this->_params['route']['id'] . $this->_params['data']['model']] = array(
                'product' => array(
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'image' => $item['image'],
                    'image_medium' => $item['image_medium'],
                    'image_thumb' => $item['image_thumb'],
                    'price' => $item['price'],
                    'price_sale' => $item['price_sale'],
                    'price_sale_percent' => $item['price_sale_percent'],
                ),
            );
        } else {
            $dataCart[$this->_params['route']['id'] . $this->_params['data']['model']] = array(
                'product' => array(
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'image' => $item['image'],
                    'image_medium' => $item['image_medium'],
                    'image_thumb' => $item['image_thumb'],
                    'price' => $item['price'],
                    'price_sale' => $item['price_sale'],
                    'price_sale_percent' => $item['price_sale_percent'],
                ),
            );
        }
        $ssFilter->data = $dataCart;
        
        foreach ($dataCart AS $key => $value) {
            $json['total_product']++;
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'course-cart'));
        }
    }
    
    public function updateCartAction(){
        $ssFilter = new Container('myCourse');
        $productCart = $ssFilter->data;
        
        if(!empty($productCart[$this->_params['data']['key']])) {
            $productCart[$this->_params['data']['key']]['amount'] = $this->_params['data']['amount'];
        }
        $ssFilter->data = $productCart;
        
        return $this->response;
    }
    
    public function deleteCartAction(){
        
        //$this->setLayout('layout/online-cart');
        // Xóa 1 sản phẩm trong giỏ hàng đi
        $ssFilter = new Container('myCourse');
        $dataCarts = $ssFilter->data;
        unset($ssFilter->data[$this->_params['route']['id']]);
        return  $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'course-cart'));
    }
    
    public function countCartAction(){
        $ssCart     = new Container('myCourse');
        $totalCarts = $ssCart->data;
        $total = 0;
        if(count($totalCarts) > 0){
            foreach ($totalCarts AS $totalCart){
                $amount = $totalCart['amount'];
                $total += $amount;
            }
        }
        echo $total;
        return $this->response;
    }
    
    public function successCartAction(){
        $this->setLayout('layout/success-course');
        $ssFilter = new Container('myCourse');
        $productCart = $ssFilter->data;
        $submitData = $ssFilter->submitData;
        
        $ssFilter->getManager()->getStorage()->clear('myCourse');
        $success = 'Bạn đã hoàn tất đặt hàng';
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Hoàn tất đặt hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_viewModel['submitData']  = $submitData;
        $this->_viewModel['params']	     = $this->_params;
        $this->_viewModel['success']     = $success;
        return new ViewModel($this->_viewModel);
    }
    
    public function reportAction(){
        $ssFilter = new Container('myCourse');
        $dataCart = $ssFilter->data;
    
        $json = array();
        if(!empty($dataCart)) {
            foreach ($dataCart AS $key => $value) {
                $json['total_product']++;
                $json['total_amount'] = $json['total_amount'] + $value['amount'];
            }
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routeCourse/default', 'controller' => $this->_params['controller'], 'action' => 'course-cart'));
        }
    }
}
