<?php

namespace Post\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class ProductController extends ActionController {
    protected $_settings;
    
    public function init() {
        $this->setLayout('layout/product');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Thiết lập options
        $this->_options['tableName'] = 'Post\Model\PostItemTable';
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray(), $this->params()->fromRoute());
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
            'author' => $this->_settings['General.Meta.Author']['value'],
            'image' => $this->_settings['General.Meta.Image']['image'],
            'title' => $this->_settings['General.Meta.Title']['value'],
            'description' => $this->_settings['General.Meta.Description']['description'],
            'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
        $this->setLayout('home');
        
        return new ViewModel($this->_viewModel);
    }
    
    public function cartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        $error = '';
        if($this->getRequest()->isPost()){
            if($this->_params['data']['name'] == null && $this->_params['data']['email'] == null && $this->_params['data']['phone'] == null && $this->_params['data']['address'] == null ){
                $error = 'Vui lòng nhập đầy đầy đủ các thông tin: Họ tên, email, điện thoại và địa chỉ giao hàng';
            } else {
                $this->getServiceLocator()->get('Admin\Model\ProductCartTable')->saveItem($this->_params, array('task' => 'add-item'));
                
                // Gửi mail
                /* $options['fromName']    = 'Hệ thống';
                $options['to']          = 'sale2.tooltechvietnam@gmail.com'; //sale2.tooltechvietnam@gmail.com
                $options['toName']      = 'Đặt hàng';
                $options['subject']     = 'Đặt hàng - ' . @date('d/m/Y H:i');
                $options['content']     = '
                    <p>Có 1 đơn đặt hàng</p>
                    <p>Thông tin khách hàng</p>
                    <p>Họ tên: '. $this->_params['data']['name'] .'</p>
                    <p>Điện thoại: '. $this->_params['data']['phone'] .'</p>
                    <p>Email: '. $this->_params['data']['email'] .'</p>
                    <p>Nội dung: '. $this->_params['data']['note'] .'</p>
                    <p>Vui lòng truy cập vào admin để xem chi tiết các sản phẩm</p>
                ';
                $mailObj = new \ZendX\Mail\Mail();
                $mailObj->sendMail($options); */
                
                $ssFilter->submitData = $this->_params['data'];
                
                //$this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'success-cart'));
                $error = 'success';
            }
            echo $error;
            return $this->response;
        }
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Giỏ hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_params['error']	         = $error;
        $this->_viewModel['params']	     = $this->_params;
        $viewModel = new ViewModel($this->_viewModel);
        return $viewModel;
    }
    
    public function addCartAction(){
        $ssFilter = new Container('myCart');
        //$ssFilter->setExpirationSeconds(500);
        //$ssFilter->getManager()->getStorage()->clear('myCart');
        
        //Lấy thông tin sản phẩm theo id
        $item   = $this->getServiceLocator()->get('Post\Model\PostItemTable')->getItem(array('id' => $this->_params['data']['id']));
        $dataCart = $ssFilter->data;
        $json = array();
        if(empty($dataCart[$item['id']])){
            $number = !empty((int)$this->_params['data']['number']) ? (int)$this->_params['data']['number'] : 1;
            $amount = !empty($number) ? $number : 1;
            
            $price = $amount * intval($price);
            $dataCart[$this->_params['route']['id'] . $this->_params['data']['model']] = array(
                'product' => $item,
                'amount' => $amount, // Số lượng
            );
        } else {
            $number = !empty((int)$this->_params['data']['number']) ? (int)$this->_params['data']['number'] : 1;
            
            $amount = $ssFilter->data[$item['id'] . $this->_params['data']['model']]['amount'] + $number;
            $amount = !empty($amount) ? $amount: 1;
            
            $dataCart[$this->_params['route']['id'] . $this->_params['data']['model']] = array(
                'product' => $item,
                'amount' => $amount, // Số lượng
            );
        }
        $ssFilter->data = $dataCart;
        
        foreach ($dataCart AS $key => $value) {
            $json['total_product']++;
            $json['total_amount'] = $json['total_amount'] + $value['amount'];
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'cart'));
        }
    }
    
    public function updateCartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        
        if(!empty($productCart[$this->_params['data']['key']])) {
            $productCart[$this->_params['data']['key']]['amount'] = $this->_params['data']['amount'];
        }
        $ssFilter->data = $productCart;
        
        return $this->response;
    }
    
    public function deleteCartAction(){
        // Xóa 1 sản phẩm trong giỏ hàng đi
        $ssFilter = new Container('myCart');
        $dataCarts = $ssFilter->data;
        unset($ssFilter->data[$this->_params['route']['id']]);
        return  $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'cart'));
    }
    
    public function countCartAction(){
        $ssCart     = new Container('myCart');
        $totalCarts = $ssCart->data;
        $total = 0;
        if(count($totalCarts) > 0){
            foreach ($totalCarts AS $totalCart){
                $amount = $totalCart['amount'];
                $total += $amount;
            }
        }
        echo $total;
        return $this->response;
    }
    
    public function successCartAction(){
        $ssFilter = new Container('myCart');
        $productCart = $ssFilter->data;
        $submitData = $ssFilter->submitData;
        
        $ssFilter->getManager()->getStorage()->clear('myCart');
        $success = 'Bạn đã hoàn tất đặt hàng';
        
        // Thiết lập Meta
        $this->_params['meta']['title']  = 'Hoàn tất đặt hàng';
        
        $this->_viewModel['productCart'] = $productCart;
        $this->_viewModel['submitData']  = $submitData;
        $this->_viewModel['params']	     = $this->_params;
        $this->_viewModel['success']     = $success;
        return new ViewModel($this->_viewModel);
    }
    
    public function reportAction(){
        $ssFilter = new Container('myCart');
        $dataCart = $ssFilter->data;
    
        $json = array();
        if(!empty($dataCart)) {
            foreach ($dataCart AS $key => $value) {
                $json['total_product']++;
                $json['total_amount'] = $json['total_amount'] + $value['amount'];
            }
        }
        
        if($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($json);
            return $this->response;
        } else {
            return $this->goRoute(array('route' => 'routePost/default', 'controller' => $this->_params['controller'], 'action' => 'cart'));
        }
    }
}
