<?php
namespace Post\Filter;

class FormRegister {
	protected $_error;
	
	public function __construct($data = null){
	    $dataForm = $data['form'];
	    $dataParam = $data['data'];
	    
	    if(empty($dataForm)) {
	        $this->_error['form_id'] = 'Không tìm thấy form đăng ký';
	    } else {
	        $configFields = new \ZendX\Functions\Form($dataForm['fields']);
	        $configFields = $configFields->getSetting();
	        
    	    $valid = new \Zend\Validator\NotEmpty();
    	    foreach ($configFields AS $config) {
    	        if($config['require'] == 'true') {
        	        if (!$valid->isValid($dataParam[$config['name']])) {
        	            $this->_error[$config['name']] = "Giá trị này bắt buộc phải nhập";
        	        }
    	        }
    	    }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}