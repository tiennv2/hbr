<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class CourseCategoryTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-category-box') {
		    $items = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
		        $ssSystem = New Container('system');
		        
		        $select -> order('left ASC')
        		        -> where -> equalTo($arrParam['box'], 1)
        		                 -> equalTo('type', $arrParam['type']);
		        $select -> where -> equalTo('language', $ssSystem->language);
		    });
		    
		    if($items->count() > 0) {
    	        foreach ($items AS $item) {
    	            $item['childs'] = $this->tableGateway->select(function (Select $select) use ($item){
    	                $select -> order('left ASC')
            	                -> where->greaterThan('level', 0)
            	                -> where->between('left', $item->left, $item->right);
    	            })->toArray();
    	            
    	            $result[] = $item;
    	        }
		    }
		}
		
		if($options['task'] == 'list-branch') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select -> order('left ASC')
        		        -> where->greaterThan('level', 0)
        		        -> where->between('left', $arrParam['left'], $arrParam['right']);
		    })->toArray();
		}
		
		if($options['task'] == 'list-breadcrumb') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select -> order('left ASC')
        		        -> where->greaterThan('level', 0)
        		        -> where->lessThanOrEqualTo('left', $arrParam['left'])
        		        -> where->greaterThanOrEqualTo('right', $arrParam['right']);
		    })->toArray();
		}
		
		if($options['task'] == 'list-all') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $ssSystem = New Container('system');
		        
		        if(!empty($arrParam['type'])) {
		            $select -> where -> equalTo('type', $arrParam['type']);
		        }
		    })->toArray();
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null || $options['task'] == 'get-by-id') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		if($options['task'] == 'get-by-index') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select->where->equalTo('index', $arrParam['index']);
    		})->current();
		}
		
		if($options['task'] == 'get-by-alias') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('alias', $arrParam['alias']);
		    })->current();
		}
	
		return $result;
	}
}