<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Admin\Model\DefaultTable;

class CourseItemTable extends DefaultTable {
	
	public function countItem($arrParam = null, $options = null){
	    if($options == null) {
	        $result	= $this->tableGateway->select()->count();
	    }
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select -> where -> equalTo('status', $ssFilter['filter_status']);
	            }
	            
	        	if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
	        })->current();
	    }
	    
	    if($options['task'] == 'user-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            $select -> columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(1)')));
	    		$select -> where -> equalTo('status', 1);
	            
	        	if(!empty($arrParam['data']['category_id'])) {
	    			$select -> where -> equalTo('category_id', $arrParam['data']['category_id']);
	    		}
	    		
	        })->current();
	    }
	    
	    return $result->count;
	}
	
	public function listItem($arrParam = null, $options = null){
	    
		if($options['task'] == 'list-item') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$paginator = $arrParam['paginator'];
				$ssFilter  = $arrParam['ssFilter'];
				 
				$select -> limit($paginator['itemCountPerPage'])
						-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
		
				if(!empty($ssFilter['order_by']) && !empty($ssFilter['order'])) {
					$select -> order(array($ssFilter['order_by'] .' '. strtoupper($ssFilter['order'])));
				}
		
				if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
					$select -> where -> equalTo('status', $ssFilter['filter_status']);
				}
		
				if(!empty($ssFilter['filter_keyword'])) {
					$select -> where -> like('name', '%'. $ssFilter['filter_keyword'] . '%');
				}
		
				if(!empty($ssFilter['filter_category'])) {
					$select -> where -> equalTo('category_id', $ssFilter['filter_category']);
				}
				
			});
		}
		
		if($options['task'] == 'list-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select -> join( TABLE_COURSE_CATEGORY, TABLE_COURSE_ITEM . '.category_id = '. TABLE_COURSE_CATEGORY .'.id', array('category_alias' => 'alias'), $select::JOIN_INNER );
				$select -> where -> equalTo(TABLE_COURSE_ITEM .'.status', 1);
				$select	-> order(array(TABLE_COURSE_ITEM .'.ordering' => 'ASC'));
				$select -> join(TABLE_DOCUMENT, TABLE_DOCUMENT .'.id = '. TABLE_COURSE_ITEM .'.teacher', array('teacher_name' => 'name', 'teacher_alias' => 'alias'), $select::JOIN_INNER);
				
				if(!empty($arrParam['category_id'])) {
					$select -> where -> equalTo(TABLE_COURSE_ITEM .'.category_id', $arrParam['category_id']);
				}
				
				if(!empty($arrParam['block'])) {
					$select -> where -> equalTo(TABLE_COURSE_ITEM .'.'. $arrParam['block'], 1);
				}
			});
		}
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'CourseItem';
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select -> order(array('ordering' => 'ASC', 'name' => 'ASC'));
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
	                 
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
	    if($options['task'] == 'user-item') {
	    	$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	    		$paginator = $arrParam['paginator'];
	    		$ssFilter  = $arrParam['ssFilter'];
	    		
	    		$select -> join(TABLE_COURSE_CATEGORY, TABLE_COURSE_CATEGORY .'.id='. TABLE_COURSE_ITEM .'.category_id', array('category_type' => 'type'), 'inner');
	    		
	    		if(!isset($options['paginator']) || $options['paginator'] == true) {
	    			$select -> limit($paginator['itemCountPerPage'])
	    					-> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
	    		}
	    		$select -> where -> equalTo(TABLE_COURSE_ITEM .'.status', 1);
	    		
	    		$select	-> order(array(TABLE_COURSE_ITEM .'.ordering' => 'ASC'));
	    
	    		if(!empty($arrParam['data']['category_id'])) {
	    			$select -> where -> equalTo(TABLE_COURSE_ITEM .'.category_id', $arrParam['data']['category_id']);
	    		} else {
	    			$select -> where -> equalTo(TABLE_COURSE_CATEGORY .'.type', 'free');
	    		}
	    		
	    	});
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
					$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
	
	    if($options['task'] == 'get-by-alias') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                $select ->where->equalTo('alias', $arrParam['alias']);
    		})->current();
		}
	
		return $result;
	}
}