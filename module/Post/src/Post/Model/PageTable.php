<?php
namespace Post\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZendX\Functions\Date;

class PageTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
	
    public function countItem($arrParam = null, $options = null){
        if($options['task'] == 'list-all') {
            $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $arrData    = $arrParam['data'];
                $date       = new Date();
                $select -> order('created DESC')->where->equalTo('status', 1)
                        -> where -> equalTo('seminor_type', $arrData['data_search']);;
               
            })->count();
        } 
        return $result;
    }
    
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-all') {
	        $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $date = new Date();
	            $paginator     = $arrParam['paginator'];
	            $arrData       = $arrParam['data'];
	            
	            $select -> limit($paginator['itemCountPerPage'])
	                    -> offset(($paginator['currentPageNumber'] - 1) * $paginator['itemCountPerPage']);
	            $select -> order('created DESC')->where->equalTo('status', 1)
	                    -> where -> equalTo('seminor_type', $arrData['data_search']);
	            
	        })->toArray();
	    }
	    
	    if($options['task'] == 'list-course') {
	        $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $date = new Date();
                $date_course = $date->sub(date('Y-m-d'), 10); 
                $select -> order('date_public DESC')
                        -> where -> greaterThanOrEqualTo('date_public', $date_course);
	        })->toArray();
	    }
	    
	    if($options['task'] == 'list-page') {
	        $result = $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
	            $select->order('created DESC')->where->equalTo('status', 1);//
	             
	            if(!empty($arrParam['limit'])) {
	                $select->limit($arrParam['limit']);
	            }
	        });
	    }
	    
	    return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
	    if($options == null || $options['task'] == 'get-by-id') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select->where->equalTo('id', $arrParam['id']);
	        })->current();
	    }
	
	    if($options['task'] == 'get-by-index') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select->where->equalTo('index', $arrParam['index']);
	        })->current();
	    }
	
	    if($options['task'] == 'get-by-alias') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $select->where->equalTo('alias', $arrParam['alias']);
	        })->current();
	    }
	
	    return $result;
	}
	
	
	
	
	
	
	
	
	
}