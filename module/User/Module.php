<?php

namespace User;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\GlobalAdapterFeature;

class Module {

	public function onBootstrap(MvcEvent $e) {
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	
		$adapter = $e->getApplication()->getServiceManager()->get('dbConfig');
		GlobalAdapterFeature::setStaticAdapter($adapter);
	}
	
	public function getConfig() {
        return array_merge(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/router.config.php'
        );
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        return array(
            'factories'	=> array(
                'User\Model\ContactTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_CONTACT, $adapter, null);
                    return new \User\Model\ContactTable($tableGateway);
                },
                'User\Model\ContractTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_CONTRACT, $adapter, null);
                    return new \User\Model\ContractTable($tableGateway);
                },
                'User\Model\LocationCityTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_LOCATION_CITY, $adapter, null);
                    return new \User\Model\LocationCityTable($tableGateway);
                },
                'User\Model\DocumentTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_DOCUMENT, $adapter, null);
                    return new \User\Model\DocumentTable($tableGateway);
                },
                'User\Model\ReportTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_REPORT, $adapter, null);
                    return new \User\Model\ReportTable($tableGateway);
                },
                'User\Model\FormTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_FORM, $adapter, null);
                    return new \User\Model\FormTable($tableGateway);
                },
                'User\Model\FormDataTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_FORM_DATA, $adapter, null);
                    return new \User\Model\FormDataTable($tableGateway);
                },
                'User\Model\CourseTable'	=> function ($sm) {
                    $adapter = $sm->get('dbHbr');
                    $tableGateway = new TableGateway(TABLE_CRM_COURSE, $adapter, null);
                    return new \User\Model\CourseTable($tableGateway);
                },
            ),
        );
    }

    public function getFormElementConfig() {
        return array(
            'factories' => array(
            )
        );
    }
}
