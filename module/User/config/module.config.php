<?php

namespace User;

return array (
	'controllers' => array(
		'invokables' => array(
			'User\Controller\Index'		    => Controller\IndexController::class,
			'User\Controller\Course'		=> Controller\CourseController::class,
			'User\Controller\Document'		=> Controller\DocumentController::class,
			'User\Controller\Public'		=> Controller\PublicController::class,
			'User\Controller\Notice'		=> Controller\NoticeController::class,
			'User\Controller\Form'			=> Controller\FormController::class,
		)
	),
    'view_manager' => array(
        'doctype'					=> 'HTML5',
        'template_path_stack'		=> array(__DIR__ . '/../view'),
    )
);


