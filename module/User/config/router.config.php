<?php

$routeUser = array(
    'type' => 'Segment',
    'options' => array (
        'route' => '/user',
        'defaults' => array (
            '__NAMESPACE__' => 'User\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'index'
        )
    ),
    'may_terminate' => true,
    'child_routes' => array (
        'default' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/[:controller[/:action[/id/:id[/code/:code]]]][/]',
                'constraints' => array (
                    'controller' 	=> '[a-zA-Z0-9_-]*',
                    'action' 		=> '[a-zA-Z0-9_-]*',
                    'id' 		    => '[a-zA-Z0-9_-]*',
                    'code' 		    => '[a-zA-Z0-9_-]*',
                ),
                'defaults' => array ()
            )
        ),
        'paginator' => array(
            'type' => 'Segment',
            'options' => array (
                'route' => '/:controller/index/page/:page[/]',
                'constraints' => array (
                    'controller'    => '[a-zA-Z0-9_-]*',
                    'page'          => '[0-9]*'
                ),
                'defaults' => array ()
            )
        )
    )
);

$routeLogin = array(
    'type' 		=> 'Segment',
    'options' => array (
        'route' => '/dang-nhap',
        'defaults' => array (
            '__NAMESPACE__' => 'User\Controller',
            'controller' 	=> 'Index',
            'action' 		=> 'login'
        )
    ),
);

return array (
    'router' => array(
        'routes' => array(
            'routeUser' => $routeUser,
            'routeLogin' => $routeLogin,
        ),
    )
);