<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;
use Zend\Session\Container;

class CourseController extends ActionController {
    protected $_settings;
    protected $_userInfo;
    
    public function init() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
        $this->setLayout('layout/user-online');
        
        // Lấy thông tin user đăng nhập
        $this->_userInfo = new ContactInfo();
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 100;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data']  = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
        	'author' => $this->_settings['General.Meta.Author']['value'],
        	'image' => $this->_settings['General.Meta.Image']['image'],
        	'title' => $this->_settings['General.Meta.Title']['value'],
        	'description' => $this->_settings['General.Meta.Description']['description'],
        	'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        if($this->_params['action'] != 'login' && $this->_params['action'] != 'logout' && $this->_params['action'] != 'register' && $this->_params['action'] != 'forgot-password' && $this->_params['action'] != 'confirm-forgot-password') {
        	if(empty($this->_userInfo->getContactInfo())) {
        		if(!empty($_GET['utm_source'])) {
        			$ssFilter->utm_source = $_GET['utm_source'];
        		}
        		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
        	}
        }
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function indexAction() {
    	$caption = 'Bài học';
    	if(!empty($this->_params['route']['id'])) {
    		$category = $this->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->getItem(array('id' => $this->_params['route']['id']));
    		$caption = $category['name'];
    	}
    	$this->_params['meta']['title'] = $caption;
    	$this->_params['data']['category_id'] = $category['id'];
    	
        $this->_viewModel['category'] 	      = $category;
        $this->_viewModel['items'] 		      = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['count'] 	          = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->countItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['caption'] 	      = $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function itemAction() {
    	$userInfo = new ContactInfo();
        $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_userInfo->getContactInfo('id')));
        
    	if(!empty($this->_params['route']['id'])) {
    		$item = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem(array('id' => $this->_params['route']['id']));
    		$caption = $item['name'];
    	} else {
    		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
    	}
    	$this->_params['meta']['title'] = $caption;
    	$this->_params['data']['item_id'] = $item['id'];
    	
        $this->_viewModel['item'] 		     = $item;
        $this->_viewModel['contact_course']  = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->listItem($this->_params, array('task' => 'list-item'));
        $this->_viewModel['category'] 	     = $category = $this->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->getItem(array('id' => $item['category_id']));
        $this->_viewModel['group']           = $this->getServiceLocator()->get('Admin\Model\CourseGroupTable')->listItem(array('course_item_id' => $item['id']), array('task' => 'list-all'));
        $this->_viewModel['detail'] 	     = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['item_other']      = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(array('category_id' => $item['category_id']), array('task' => 'list-all'));
        $this->_viewModel['teacher']         = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache'));
        $this->_viewModel['caption'] 	     = $caption;
        
        // Update view
        $detail = $this->_viewModel['detail'];
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['contact'] = $contact;
        if(!empty($detail)) {
            $arrVideo = array();
            foreach ($detail AS $key_detail => $item_detail) {
                $arrVideo[$key_detail] = $item_detail;
            }
            $current_item = !empty($_GET['video']) ? $arrVideo[$_GET['video']] : $arrVideo[0];
            $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->saveItem(array('data' => $current_item), array('task' => 'update-view'));
        }
        
        return new ViewModel($this->_viewModel);
    }
    
    public function addContactUserAction() {
        if($this->getRequest()->isXmlHttpRequest()) {
            if($this->getRequest()->isPost()){
                $this->_params['item'] 	= $category = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->getItem(array('id' => $this->_params['data']['id']));
                $result 	= $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->saveItem($this->_params, array('task' => 'add-contact-user'));
            }
        } else {
            return $this->redirect()->toRoute('routeUser/default', array('controller' => 'notice', 'action' => 'not-found'));
        }
        
        return $this->response;
    }
    
    public function detailAction() {
    	if(!empty($this->_params['route']['id'])) {
    		$item = $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->getItem(array('id' => $this->_params['route']['id']));
    		$detail = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem(array('id' => $item['item_id']));
    		$caption = $detail['name'];
    	} else {
    		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
    	}
    	$this->_params['meta']['title'] = $caption;
    	$this->_params['data']['item_id'] = $detail['id'];
    	
        $this->_viewModel['item'] 			= $item;
        $this->_viewModel['detail'] 		= $detail;
        $this->_viewModel['detail_list'] 	= $this->getServiceLocator()->get('Admin\Model\CourseDetailTable')->listItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['item_other'] 	= $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(array('category_id' => $item['category_id']), array('task' => 'list-all'));
        $this->_viewModel['caption'] 		= $caption;
        return new ViewModel($this->_viewModel);
    }
    
    public function videoAction() {
    	$ssFilter = new Container('Video');
    	
        if(strpos($_SERVER['HTTP_REFERER'], "/user/course/item/id/") > 0) {
    	    if($ssFilter->token == $_GET['token'] && !empty($_GET['token']) && !empty($ssFilter->token)) {
    	        $ssFilter->token = '';
    	        
    	        $datetime = date('YmdHis');
    	        if($datetime - $ssFilter->time <= 5) {
                	$videoStream = new \ZendX\Functions\VideoStream(PATH_FILES .'/course/'. $_GET['course'] .'/'. $_GET['id'] .'.mp4');
                	$videoStream->start();
    	        }
    	    }
    	}
    	return $this->response;
    }
}
