<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;

class DocumentController extends ActionController {
    protected $_settings;
    protected $_userInfo;
    
    public function init() {
        $this->setLayout('layout/user');
        
        // Lấy thông tin user đăng nhập
        $this->_userInfo = new ContactInfo();
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data']  = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
        	'author' => $this->_settings['General.Meta.Author']['value'],
        	'image' => $this->_settings['General.Meta.Image']['image'],
        	'title' => $this->_settings['General.Meta.Title']['value'],
        	'description' => $this->_settings['General.Meta.Description']['description'],
        	'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        if($this->_params['action'] != 'login' && $this->_params['action'] != 'logout' && $this->_params['action'] != 'register' && $this->_params['action'] != 'forgot-password' && $this->_params['action'] != 'confirm-forgot-password') {
        	if(empty($this->_userInfo->getContactInfo())) {
        		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
        	}
        }
    }
    
    public function indexAction() {
    	$caption = 'Tài liệu';
    	if(!empty($this->_params['route']['id'])) {
    		$category = $this->getServiceLocator()->get('Admin\Model\DocumentCategoryTable')->getItem(array('id' => $this->_params['route']['id']));
    		$caption = $category['name'];
    	}
    	$this->_params['meta']['title'] = $caption;
    	$this->_params['data']['category_id'] = $category['id'];
    	
        $this->_viewModel['category'] = $category;
        $this->_viewModel['items'] = $this->getServiceLocator()->get('Admin\Model\DocumentItemTable')->listItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['count'] = $this->getServiceLocator()->get('Admin\Model\DocumentItemTable')->countItem($this->_params, array('task' => 'user-item'));
        $this->_viewModel['caption'] = $caption;
        return new ViewModel($this->_viewModel);
    }
}
