<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;
use Zend\Form\FormInterface;
use Zend\Session\Container;

class IndexController extends ActionController {
    protected $_settings;
    protected $_userInfo;
    
    public function init() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
        $this->setLayout('layout/user');
        
        // Lấy thông tin user đăng nhập
        $this->_userInfo = new ContactInfo();
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data']  = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
        	'author' => $this->_settings['General.Meta.Author']['value'],
        	'image' => $this->_settings['General.Meta.Image']['image'],
        	'title' => $this->_settings['General.Meta.Title']['value'],
        	'description' => $this->_settings['General.Meta.Description']['description'],
        	'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
        
        if($this->_params['action'] != 'login' && $this->_params['action'] != 'logout' && $this->_params['action'] != 'register' && $this->_params['action'] != 'forgot-password' && $this->_params['action'] != 'confirm-forgot-password') {
        	if(empty($this->_userInfo->getContactInfo())) {
        		if(!empty($_GET['utm_source'])) {
        			$ssFilter->utm_source = $_GET['utm_source'];
        		}
        		$this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
        	}
        }
    }
    
    public function indexAction() {
    	$this->_params['meta']['title'] = 'Trang thành viên';
    	$userInfo = new ContactInfo();
    	
    	$contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_userInfo->getContactInfo('id')));
        if(empty($contact['company_name']) || empty($contact['company_size']) || empty($contact['company_problem'])) {
        	return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'update-company'));
        }
    	if(empty($contact['birthday_year']) || empty($contact['sex']) || empty($contact['location_city_id'])) {
    		return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'update-profile'));
    	}
    	
    	$contact_course = array();
    	$course = array();
    	if($userInfo->getContactInfo('status') == 1) {
            $contact_course     = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->listItem(array('contact_id' => $userInfo->getContactInfo('id')), array('task' => 'my-course'));
            $courseItem         = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(array( "where" => array( "type" => "free" )), array('task' => 'list-free'));
            $courseCategory     = $this->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->listItem(null, array('task' => 'cache'));
            $courseItem_default = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->listItem(null, array('task' => 'cache'));
    	    $payment            = $this->getServiceLocator()->get('Admin\Model\PaymentTable')->listItem(null, array('task' => 'cache'));
    	} else {
    	    $course = $this->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->listItem(array(), array('task' => 'public-list'));
    	}
    	
        $this->_viewModel['contact']            = $contact;
        $this->_viewModel['contact_course']     = $contact_course;
        $this->_viewModel['course_item']        = $courseItem;
        $this->_viewModel['course_category']    = $courseCategory;
        $this->_viewModel['course_item_default'] = $courseItem_default;
        $this->_viewModel['teacher']    = $this->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array( "where" => array( "code" => "teacher" )), array('task' => 'cache'));
        $this->_viewModel['payment'] = $payment;
        return new ViewModel($this->_viewModel);
    }
    
    public function updateProfileAction() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
    	$this->_params['meta']['title'] = 'Cập nhật thông tin cá nhân';
    	 
    	$contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_userInfo->getContactInfo('id')));
        
        $myForm = new \User\Form\Contact\Profile($this->getServiceLocator());
        $myForm->setInputFilter(new \User\Filter\Contact\Profile());
        $myForm->bind($contact);
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
        
            if($myForm->isValid()){
            	// Cập nhật thống kế
            	if((!empty($ssFilter->report_register_link) || !empty($ssFilter->report_register_home)) && empty($ssFilter->report_update_profile)) {
            		$report = $this->getServiceLocator()->get('User\Model\ReportTable')->saveItem(null, array('task' => 'update', 'field' => 'update_profile'));
            		$ssFilter->report_update_profile = 1;
            	}
            	
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['id'] = $this->_userInfo->getContactInfo('id');
                
                $result = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'update-profile'));
        
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['contact']    = $contact;
        return new ViewModel($this->_viewModel);
    }
    
    public function updateCompanyAction() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
    	$this->_params['meta']['title'] = 'Cập nhật thông tin tài khoản';
    	
        $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_userInfo->getContactInfo('id')));
        $contact['company_problem'] = $contact['company_problem'] ? explode(',', $contact['company_problem']) : array();
        
        $myForm = new \User\Form\Contact\Company($this->getServiceLocator());
        $myForm->setInputFilter(new \User\Filter\Contact\Company());
        $myForm->bind($contact);
        
        if($this->getRequest()->isPost()){
            $myForm->setData($this->_params['data']);
        
            if($myForm->isValid()){
            	// Cập nhật thống kế
            	if((!empty($ssFilter->report_register_link) || !empty($ssFilter->report_register_home)) && empty($ssFilter->report_update_company)) {
            		$report = $this->getServiceLocator()->get('User\Model\ReportTable')->saveItem(null, array('task' => 'update', 'field' => 'update_company'));
            		$ssFilter->report_update_company = 1;
            	}
            	
                $this->_params['data'] = $myForm->getData(FormInterface::VALUES_AS_ARRAY);
                $this->_params['data']['id'] = $this->_userInfo->getContactInfo('id');
                
                $result = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'update-profile'));
        
                $this->flashMessenger()->addMessage('Dữ liệu đã được cập nhật thành công');
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
        
        $this->_viewModel['myForm']	    = $myForm;
        $this->_viewModel['contact']    = $contact;
        return new ViewModel($this->_viewModel);
    }
    
    public function changePasswordAction() {
    	$this->_params['meta']['title'] = 'Thay đổi mật khẩu';
    	
        $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_userInfo->getContactInfo('id')));
        if($this->getRequest()->isPost()){
            $post   = $this->_params['data'];
            $errors = array();
            $flag   = true;
            
            if(md5($post['password_current']) != $contact['password']) {
                $errors[] = '<b>Mật khẩu cũ:</b> không chính xác';
                $flag = false;
            }
            
            $validator = new \Zend\Validator\StringLength(array('min' => 6));
            if (!$validator->isValid($post['password_new'])) {
                $errors[] = '<b>Mật khẩu mới:</b> phải lớn hơn 6 ký tự và bao gồm cả chữ và số';
                $flag = false;
            }
            
            if(md5($post['password_new']) != md5($post['password_confirm'])) {
                $errors[] = '<b>Xác nhận mật khẩu mới:</b> không đúng';
                $flag = false;
            }
            
            if($flag == true){
                $this->_params['data']['id'] = $contact['id'];
                $result = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'change-password'));
    
                $this->flashMessenger()->addMessage('Mật khẩu của bạn đã được đổi thành công.');
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
    
        $this->_viewModel['errors']     = $errors;
        $this->_viewModel['contact']    = $contact;
        return new ViewModel($this->_viewModel);
    }
    
    public function activeCodeAction() {
    	$this->_params['meta']['title'] = 'Kích hoạt tài khoản';
    	
        $userInfo = new ContactInfo();
        if(empty($userInfo->getContactInfo())) {
            $linkLogin = $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'), array('force_canonical' => true));
            $linkLogin = $linkLogin .'?redirect=' . $_SERVER['REDIRECT_URL'];
            
            $this->redirect()->toUrl($linkLogin);
        } else {
            $error  = '';
            $result = '';
            $item   = '';
            if($this->getRequest()->isPost()) {
                if(empty($this->_params['data']['code'])) {
                    $error = 'Vui lòng nhập mã kích hoạt';
                } else {
                    $item = $this->getServiceLocator()->get('Admin\Model\CodeTable')->getItem(array('code' => $this->_params['data']['code']), array('task' => 'by-code-status'));
                    if(!empty($item) && !empty($item['course_id']) && $item['status'] == 0) {
                        $contact_course = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->getItem(array('contact_id' => $userInfo->getContactInfo('id'), 'course_id' => $item['course_id']), array('task' => 'check-course'));
                        //$course         = $this->getServiceLocator()->get('Admin\Model\CourseItemTable')->getItem(array('id' => $item['course_id']));
                        $link_course = $this->url()->fromRoute('routeUser/default', array('action' => 'index'), array('force_canonical' => true));
                        if(!empty($contact_course)) {
                            // Thêm contact_course
                            $arrParams['data']['contact_id']    = $userInfo->getContactInfo('id');
                            $arrParams['data']['course_id']     = $item['course_id'];
                            $arrParams['data']['code']          = $item['code'];
                            //$arrParams['data']['product']       = $course;
                            $arrParams['code']                  = $item;
                            $result = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($arrParams, array('task' => 'buy-course-code-add'));
                            
                            // Update trạng thái mã kích hoạt lên 1
                            $code = $this->getServiceLocator()->get('Admin\Model\CodeTable')->saveItem(array('data' => $item), array('task' => 'update-status'));
                            
                            $result = 'Bạn đã kích hoạt khóa học thành công. <a href="'. $link_course .'">Nhấn vào đây</a> để bắt đầu học';
                        } elseif (empty($contact_course)) {
                            // Thêm contact_course
                            $arrParams['data']['contact_id']    = $userInfo->getContactInfo('id');
                            $arrParams['data']['course_id']     = $item['course_id'];
                            $arrParams['data']['code']          = $item['code'];
                            //$arrParams['data']['product']       = $course;
                            $arrParams['code']                  = $item;
                            $result = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($arrParams, array('task' => 'buy-course-code-add'));
                            
                            // Update trạng thái mã kích hoạt lên 1
                            $code = $this->getServiceLocator()->get('Admin\Model\CodeTable')->saveItem(array('data' => $item), array('task' => 'update-status'));
                            
                            $result = 'Bạn đã kích hoạt khóa học thành công. <a href="'. $link_course .'">Nhấn vào đây</a> để bắt đầu học';
                        } else {
                            if(empty($contact_course['checked']) || empty($contact_course['checked_by'])) {
                                // Update contact_course: checked: date_now, checked_by: system
                                $arrParams['data']['contact_id']    = $userInfo->getContactInfo('id');
                                $arrParams['data']['course_id']     = $item['course_id'];
                                $arrParams['data']['code']          = $item['code'];
                                $arrParams['data']['id']            = $contact_course['id'];
                                $arrParams['code']                  = $item;
                                $result = $this->getServiceLocator()->get('Admin\Model\ContactCourseTable')->saveItem($arrParams, array('task' => 'buy-course-code-update'));
                                
                                // Update trạng thái mã kích hoạt lên 1
                                $code = $this->getServiceLocator()->get('Admin\Model\CodeTable')->saveItem(array('data' => $item), array('task' => 'update-status'));
                                
                                $result = 'Bạn đã kích hoạt khóa học thành công. <a href="'. $link_course .'">Nhấn vào đây</a> để bắt đầu học';
                            } else {
                                $error = 'Khóa học của bạn đã được kích hoạt trước đó. <a href="'. $link_course .'">Nhấn vào đây</a> để vào khóa học.';
                            }
                        }
                    } else {
                        $error = 'Mã kích hoạt không đúng. Vui lòng kiểm tra lại';
                    }
                }
            }
        }
    
        $this->_viewModel['error']  = $error;
        $this->_viewModel['result'] = $result;
        $this->_viewModel['item']   = $item;
        return new ViewModel($this->_viewModel);
    }
    
    public function loginAction() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	
    	$this->_params['meta']['title'] = 'Đăng nhập';
    	
    	$userInfo = new ContactInfo();
    	if(!empty($userInfo->getContactInfo())) {
    		if(!empty($_GET['redirect'])) {
				return $this->redirect()->toUrl($_GET['redirect']);
			} else {
               	return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
    	}
    	
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\Login($this->_params, $this->getServiceLocator());
        
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem($this->_params['data'], array('task' => 'login'));
                
                if(empty($contact)) {
                    $result['error'] = 'Tài khoản hoặc mật khẩu không đúng';
                } else {
                	// Lấy danh sách hợp đồng của khách hàng
                	$contract = $this->getServiceLocator()->get('User\Model\ContractTable')->listItem(array('contact_id' => $contact['id']), array('task' => 'list-all'));
                	$course_ids = array(); // Mảng các khóa học đã đăng ký trong CRM
                	if(!empty($contract)) {
                		foreach ($contract AS $key => $val) {
                			$course_ids[] = $val['course_id'];
                		}
                	
                		$contact['course_ids'] = $course_ids;
                		$contact['contract'] = $contract;
                	}
                	
                    $userInfo = new ContactInfo();
                    $userInfo->storeInfo(array('contact' => $contact));
                    
                    if(!empty($_GET['redirect'])) {
                    	return $this->redirect()->toUrl($_GET['redirect']);
                    } else {
                    	return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                    }
                }
            }
        }
        
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    public function logoutAction() {
        $userInfo = new ContactInfo();
        $userInfo->destroyInfo();
    
        return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'));
    }
    
    public function registerAction() {
    	// Thiết lập session filter
    	$ssFilter = new Container('User');
    	if(!empty($ssFilter->utm_source)) {
    		if(empty($ssFilter->report_document)) {
    			$report = $this->getServiceLocator()->get('User\Model\ReportTable')->saveItem(null, array('task' => 'update', 'field' => 'document'));
	    		$ssFilter->report_document = 1;
    		}
    	}
    	
    	$this->_params['meta']['title'] = 'Đăng ký tài khoản';
    	
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\Register($this->_params, $this->getServiceLocator());
            
            if($this->getRequest()->isXmlHttpRequest()) {
                if(!empty($filter->getError())) {
                    $result['error'] = $filter->getError();
                } else {
                    $id = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'register'));
                    $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $id), null);
                
                    // Đăng nhập luôn
                    if(empty($contact)) {
                        $result['error'] = 'Tài khoản hoặc mật khẩu không đúng';
                    } else {
                    	// Cập nhật thống kế
                    	if(empty($ssFilter->report_register_home)) {
                    		$report = $this->getServiceLocator()->get('User\Model\ReportTable')->saveItem(null, array('task' => 'update', 'field' => 'register_home'));
	                    	$ssFilter->report_register_home = 1;
                    	}
                    	
                    	// Lấy danh sách hợp đồng của khách hàng
                    	$contract = $this->getServiceLocator()->get('User\Model\ContractTable')->listItem(array('contact_id' => $contact['id']), array('task' => 'list-all'));
                    	$course_ids = array(); // Mảng các khóa học đã đăng ký trong CRM
                    	if(!empty($contract)) {
                    		foreach ($contract AS $key => $val) {
                    			$course_ids[] = $val['course_id'];
                    		}
                    		 
                    		$contact['course_ids'] = $course_ids;
                    		$contact['contract'] = $contract;
                    	}
                    	
                        $userInfo = new ContactInfo();
                        $userInfo->storeInfo(array('contact' => $contact));
                        
                        $result['success'] = 1;
                    }
                    
                }
                
                echo json_encode($result);
                
                return $this->response;
            } else {
                if(!empty($filter->getError())) {
                    $result['error'] = $filter->getError();
                } else {
                    $id = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'register'));
                    $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $id), null);
                    
                    /* $options['fromName']    = 'Edumart';
                    $options['to']          = $contact['email'];
                    $options['toName']      = $contact['name'];
                    $options['subject']     = 'Kích hoạt tài khoản';
                    $options['content']     = '
                        <p>Xin chào: <strong>'. $contact['name'] .'</strong></p>
                        <p>Bạn đã đăng ký tài khoản thành công. Vui lòng nhấn vào link phía dưới để kích hoạt tài khoản</p>
                        <p><a href="'. $linkActive .'">'. $linkActive .'</a></p>
                    ';
                    $mailObj = new \ZendX\Mail\Mail();
                    $mailObj->sendMail($options); */
                    
                    // Đăng nhập luôn
                    if(empty($contact)) {
                        $result['error'] = 'Tài khoản hoặc mật khẩu không đúng';
                    } else {
                    	// Cập nhật thống kế
                    	if(empty($ssFilter->report_register_link)) {
                    		$report = $this->getServiceLocator()->get('User\Model\ReportTable')->saveItem(null, array('task' => 'update', 'field' => 'register_link'));
                    		$ssFilter->report_register_link = 1;
                    	}
                    	
                    	// Lấy danh sách hợp đồng của khách hàng
                    	$contract = $this->getServiceLocator()->get('User\Model\ContractTable')->listItem(array('contact_id' => $contact['id']), array('task' => 'list-all'));
                    	$course_ids = array(); // Mảng các khóa học đã đăng ký trong CRM
                    	if(!empty($contract)) {
                    		foreach ($contract AS $key => $val) {
                    			$course_ids[] = $val['course_id'];
                    		}
                    		 
                    		$contact['course_ids'] = $course_ids;
                    		$contact['contract'] = $contract;
                    	}
                    	
                        $userInfo = new ContactInfo();
                        $userInfo->storeInfo(array('contact' => $contact));
                            
                        $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
                    }
                    
                    //$this->flashMessenger()->addMessage('Thông tin của bạn đã được đăng ký thành công.');
                    //return $this->redirect()->toRoute('routeUser/default', array('controller' => 'notice', 'action' => 'register-success'));
                }
            }
        }
        
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    public function activeAction() {
    	$this->_params['meta']['title'] = 'Kích hoạt tài khoản';
    	
        $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'active'));
        
        $this->flashMessenger()->addMessage('Tài khoản của bạn đã được kích hoạt thành công');
        return $this->redirect()->toRoute('routeUser/default', array('controller' => 'notice', 'action' => 'active-success'));
        
        return $this->response;
    }
    
    public function forgotPasswordAction() {
    	$this->_params['meta']['title'] = 'Lấy lại mật khẩu';
    	
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\ForgotPassword($this->_params, $this->getServiceLocator());
    
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem($this->_params['data'], array('task' => 'by-email'));
    
                if(empty($contact)) {
                    $result['error'] = 'Địa chỉ email không tồn tại';
                } else {
                    // Update password_code cho user
                    $password_code = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem(array('data' => array('id' => $contact['id'])), array('task' => 'update-password_code'));
                    
                    // Gửi email
                    $linkPassword = $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'confirm-forgot-password', 'id' => $contact['id'], 'code' => $password_code), array('force_canonical' => true));
                    
                    $options['fromName']    = 'HBR';
                    $options['to']          = $contact['email'];
                    $options['toName']      = $contact['name'];
                    $options['subject']     = 'Xac nhan lay lai mat khau';
                    $options['content']     = '
                        <p>Xin chào: <strong>'. $contact['name'] .'</strong></p>
                        <p>
                        	Theo yêu cầu xin lại mật khẩu mới của bạn. Chúng tôi gửi mail này để xác nhận có phải bạn muốn xin lại mật khẩu.<br> 
                           	Nếu đồng ý Bạn vui lòng nhấn vào liên kết bên dưới để xác nhận mật khẩu mới.
                        </p>
                        <p><a href="'. $linkPassword .'">'. $linkPassword .'</a></p>
                        <p>Nếu Bạn không thực hiện việc yêu cầu lấy lại mật khẩu vui lòng xóa email này.</p>
                    ';
                    $mailObj = new \ZendX\Mail\Mail();
                    $mailObj->sendMail($options);
                    
                    $this->flashMessenger()->addMessage('Mật khẩu đã được gửi vào địa chỉ email của bạn. Vui lòng kiểm tra trong Inbox hoặc Spam');
                    return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'forgot-password'));
                }
            }
        }
    
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    public function confirmForgotPasswordAction() {
    	$this->_params['meta']['title'] = 'Xác nhận lấy lại mật khẩu';
    	
        $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $this->_params['route']['id'], 'password_code' => $this->_params['route']['code']), array('task' => 'forgot-password'));
        
        if(empty($this->_params['route']['id']) || empty($this->_params['route']['code']) || empty($contact)) {
            return $this->redirect()->toRoute('routePost/default', array('controller' => 'notice', 'action' => 'no-data'));
        }
        
        $result = array();
        if($this->getRequest()->isPost()) {
            $filter = new \User\Filter\ConfirmForgotPassword($this->_params, $this->getServiceLocator());
    
            if(!empty($filter->getError())) {
                $result['error'] = $filter->getError();
            } else {
                // Update password mới
                $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem(array('data' => array('id' => $contact['id'], 'password' => $this->_params['data']['password'])), array('task' => 'confirm-forgot-password'));
                
                // Lấy danh sách hợp đồng của khách hàng
                $contract = $this->getServiceLocator()->get('User\Model\ContractTable')->listItem(array('contact_id' => $contact['id']), array('task' => 'list-all'));
                $course_ids = array(); // Mảng các khóa học đã đăng ký trong CRM
                if(!empty($contract)) {
                	foreach ($contract AS $key => $val) {
                		$course_ids[] = $val['course_id'];
                	}
                	 
                	$contact['course_ids'] = $course_ids;
                	$contact['contract'] = $contract;
                }
                
                // Đăng nhập
                $userInfo = new ContactInfo();
                $userInfo->storeInfo(array('contact' => $contact));
                
                return $this->redirect()->toRoute('routeUser/default', array('controller' => 'index', 'action' => 'index'));
            }
        }
    
        $this->_viewModel['result'] = $result;
        return new ViewModel($this->_viewModel);
    }
    
    public function facebookAction() {
    	$linkReturn 	= $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'facebook'), array('force_canonical' => true));
    	$linkLogin 		= $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'login'), array('force_canonical' => true));
    	$linkRegister 	= $this->url()->fromRoute('routeUser/default', array('controller' => 'index', 'action' => 'register'), array('force_canonical' => true));
    	 
    	require_once PATH_VENDOR .'/Facebook/autoload.php';
    
    	$fb = new \Facebook\Facebook ([
    			'app_id' => '1038941506233067',
    			'app_secret' => '5f33422d60f76c7019b9e26690df478e',
    			'default_graph_version' => 'v2.2',
    	]);
    
    	$helper = $fb->getRedirectLoginHelper();
    
    	try {
    		$accessToken = $helper->getAccessToken();
    	} catch(\Facebook\Exceptions\FacebookResponseException $e) { // When Graph returns an error
    		exit;
    	} catch(\Facebook\Exceptions\FacebookSDKException $e) { // When validation fails or other local issues
    		exit;
    	}
    
    	if (!isset($accessToken)) {
    		if ($helper->getError()) {
    			header('HTTP/1.0 401 Unauthorized');
    			echo "Error: " . $helper->getError() . "\n";
    			echo "Error Code: " . $helper->getErrorCode() . "\n";
    			echo "Error Reason: " . $helper->getErrorReason() . "\n";
    			echo "Error Description: " . $helper->getErrorDescription() . "\n";
    		} else {
    			$permissions = array('public_profile','email'); // Optional permissions
    			$loginUrl = $helper->getLoginUrl($linkReturn, $permissions);
    			header("Location: ".$loginUrl);
    		}
    		exit;
    	}
    
    	try {
    		// Returns a `Facebook\FacebookResponse` object
    		$fields = array('id', 'name', 'email');
    		$response = $fb->get('/me?fields='.implode(',', $fields).'', $accessToken);
    	} catch(\Facebook\Exceptions\FacebookResponseException $e) { // When Graph returns an error
    		exit;
    	} catch(\Facebook\Exceptions\FacebookSDKException $e) { // When validation fails or other local issues
    		exit;
    	}
    	 
    	// Thông tin người dùng lấy được
    	$user = $response->getGraphUser();
    
    	if(!empty($user['email'])) {
    		$contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('email' => $user['email']), array('task' => 'by-email'));
    		if(!empty($contact)) { // Nếu đã tồn tại tài khoản trong hệ thống
    			// Cập nhật lại thông tin
    			$arrParamContact = $this->_params;
    			$arrParamContact['data']['id'] 			= $contact['id'];
    			$arrParamContact['data']['type'] 		= 'facebook';
    			$arrParamContact['data']['name'] 		= $user['name'];
    			$arrParamContact['data']['facebook_id'] = $user['id'];
    			if($contact['active_code'] != '1') {
    				$arrParamContact['data']['active_code'] = 1;
    				$arrParamContact['data']['active_time'] = date('Y-m-d H:i:s');
    			}
    			$contact_id = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($arrParamContact, array('task' => 'facebook-update'));
    			 
    			$contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $contact_id));
    			$userInfo = new ContactInfo();
    			$userInfo->storeInfo(array('contact' => $contact));
    			 
    			return $this->redirect()->toRoute('routeCourse/default', array('action' => 'topic'));
    		} else {
    			// Thêm liên hệ
    			$arrParamContact = $this->_params;
    			$arrParamContact['data']['type'] 		= 'facebook';
    			$arrParamContact['data']['name'] 		= $user['name'];
    			$arrParamContact['data']['email'] 		= $user['email'];
    			$arrParamContact['data']['facebook_id'] = $user['id'];
    			$contact_id = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($arrParamContact, array('task' => 'facebook-add'));
    
    			$contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('id' => $contact_id));
    			$userInfo = new ContactInfo();
    			$userInfo->storeInfo(array('contact' => $contact));
    			 
    			return $this->redirect()->toRoute('routeCourse/default', array('action' => 'topic'));
    		}
    	} else {
    		echo 'Không tìm thấy địa chỉ email. Vui lòng <a href="'. $linkRegister .'">nhấn vào đây</a> để đăng ký tài khoản';
    	}
    
    	return $this->response;
    }
    
}
