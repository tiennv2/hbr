<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;

class NoticeController extends ActionController {
    
    public function init() {
        $this->setLayout('template/default/notice');
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Lấy dữ liệu post của form
        $this->_params['data'] = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
    }
    
    public function noDataAction() {
                
        return new ViewModel($this->_viewModel);
    }
    
    public function registerSuccessAction() {
        if(empty($this->flashMessenger()->getMessages())) {
            return $this->redirect()->toRoute('routeHome');
        }
        
        return new ViewModel($this->_viewModel);
    }
    
    public function activeSuccessAction() {
        if(empty($this->flashMessenger()->getMessages())) {
            return $this->redirect()->toRoute('routeHome');
        }
        
        return new ViewModel($this->_viewModel);
    }
    
    public function notFoundAction() {
        return new ViewModel($this->_viewModel);
    }
}
