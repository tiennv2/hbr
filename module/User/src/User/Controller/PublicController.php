<?php

namespace User\Controller;

use ZendX\Controller\ActionController;
use Zend\View\Model\ViewModel;
use ZendX\System\ContactInfo;
use User\Form\Brochure;
use Zend\Form\FormInterface;
use Zend\Session\Container;

class PublicController extends ActionController {
    protected $_settings;
    protected $_userInfo;
    
    public function init() {
        $this->setLayout('layout/user');
        
        // Lấy thông tin user đăng nhập
        $this->_userInfo = new ContactInfo();
        
        // Lấy thông tin setting
        $this->_settings = $this->getServiceLocator()->get('Admin\Model\SettingTable')->listItem(array('code' => 'General'), array('task' => 'cache-by-code'));
        
        // Thiết lập lại thông số phân trang
        $this->_paginator['itemCountPerPage'] = 5;
        $this->_paginator['pageRange'] = 5;
        $this->_paginator['currentPageNumber'] = $this->params()->fromRoute('page', 1);
        $this->_params['paginator'] = $this->_paginator;
        
        // Lấy dữ liệu post của form
        $this->_params['data']  = array_merge($this->getRequest()->getPost()->toArray(), $this->getRequest()->getFiles()->toArray());
        $this->_params['route'] = $this->params()->fromRoute();
        
        // Truyển dữ dữ liệu ra ngoài view
        $this->_viewModel['settings'] = $this->_settings;
        $this->_viewModel['params'] = $this->_params;
        
        // Thiết lập Meta
        $this->_params['meta'] = array(
        	'author' => $this->_settings['General.Meta.Author']['value'],
        	'image' => $this->_settings['General.Meta.Image']['image'],
        	'title' => $this->_settings['General.Meta.Title']['value'],
        	'description' => $this->_settings['General.Meta.Description']['description'],
        	'keywords' => $this->_settings['General.Meta.Keywords']['description'],
        );
    }
    
    public function brochureAction() {
        //HBR-Brochure.pdf
        $file = PATH_FILES .'/download/HBR-Brochure.pdf';
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header("Content-Type: application/force-download");
        header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
        // header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
        
        return $this->response;
    }
    
    public function inhouseAction(){
        $ssFilter = new Container('User');
        $data = $ssFilter->data;
        
        if($this->getRequest()->isPost()){
            $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('phone' => trim($this->_params['data']['phone'])), array('task' => 'by-phone'));
//             $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('email' => trim($this->_params['data']['email'])), array('task' => 'by-email'));
            if(!empty($contact)) {
            	$this->_params['data']['id'] = $contact['id'];
            	$this->_params['data']['type'] = 'inhouse';
            	$this->_params['contact'] = $contact;
            	$this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'update-in-house'));
            } else {
            	$this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($this->_params, array('task' => 'in-house'));
            }
            
        }     
        return $this->response;
    }    

    
    function campaignAction() {
        $ssFilter = new Container('Admin');
        $data = $ssFilter->data;
        $this->getServiceLocator()->get('Admin\Model\CampaignDataTable')->saveItem($this->_params, array('task' => 'add-item'));
        return $this->response;
    }  
}
