<?php
namespace User\Filter;

use Zend\InputFilter\InputFilter;

class Brochure extends InputFilter {
	
	public function __construct($options = null){
		
		// Họ tên
		$this->add(array(
			'name'		=> 'name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				)
			)
		));
		 
		// Điện thoại
		$this->add(array(
			'name'		=> 'phone',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
		 
		// Email
		$this->add(array(
			'name'		=> 'email',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
		 
		// Giới tính
		$this->add(array(
			'name'		=> 'sex',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
		 
		// Năm sinh
		$this->add(array(
			'name'		=> 'birthday_year',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
		 
		// Tỉnh thành
		$this->add(array(
			'name'		=> 'location_city_id',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
					'options'	=> array(
						'messages'	=> array(
							\Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
						)
					),
					'break_chain_on_failure'	=> true
				),
			)
		));
		
		// Tên công ty
		$this->add(array(
			'name'		=> 'company_name',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				)
			)
		));
	    
		// Chức vụ
		$this->add(array(
			'name'		=> 'company_position',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
			)
		));
	    
		// Quy mô công ty
		$this->add(array(
			'name'		=> 'company_size',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
			)
		));
	    
		// Ngành nghề công ty
		$this->add(array(
			'name'		=> 'company_career',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
			)
		));
	    
		// Vấn đề bạn đang gặp phải trong kinh doanh
		$this->add(array(
			'name'		=> 'company_problem',
			'required'	=> true,
			'validators'	=> array(
				array(
					'name'		=> 'NotEmpty',
				    'options'	=> array(
				        'messages'	=> array(
				            \Zend\Validator\NotEmpty::IS_EMPTY => 'Giá trị này không được để trống'
				        )
				    ),
					'break_chain_on_failure'	=> true
				),
			)
		));
	}
}