<?php
namespace User\Filter;

class ConfirmForgotPassword {
	protected $_error;
	
	public function __construct($data = null, $sm){
	    $dataParam = $data['data'];
	    
	    if(!empty($dataParam)) {
	        if(empty($dataParam['password'])) {
	            return $this->_error = 'Mật khẩu bắt buộc phải nhập';
	        } else {
	            $validator = new \Zend\Validator\StringLength(array('min' => 6));
	            if (!$validator->isValid($dataParam['password'])) {
	                return $this->_error = 'Mật khẩu phải lớn hơn 6 ký tự';
	            }
	        }
	        
	        if(empty($dataParam['confirm_password'])) {
	            return $this->_error = 'Nhập lại mật khẩu bắt buộc phải nhập';
	        } else {
	            if ($dataParam['password'] != $dataParam['confirm_password']) {
	                return $this->_error = 'Nhập lại mật khẩu không chính xác';
	            }
	        }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}