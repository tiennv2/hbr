<?php
namespace User\Filter;

class ForgotPassword {
	protected $_error;
	
	public function __construct($data = null, $sm){
	    $dataParam = $data['data'];
	    
	    if(!empty($dataParam)) {
	        if(empty($dataParam['email'])) {
	            return $this->_error = 'Email bắt buộc phải nhập';
	        } else {
	            $validator = new \Zend\Validator\EmailAddress();
	            if (!$validator->isValid($dataParam['email'])) {
	                return $this->_error = 'Email không đúng định dạng';
	            }
	        }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}