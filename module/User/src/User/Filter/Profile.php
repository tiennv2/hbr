<?php
namespace User\Filter;

class Profile {
	protected $_error;
	
	public function __construct($data = null, $sm){
	    $dataParam = $data['data'];
	    
	    if(!empty($dataParam)) {
	        if(empty($dataParam['name'])) {
	            return $this->_error = 'Họ tên bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['phone'])) {
	            return $this->_error = 'Số điện thoại bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['city'])) {
	            return $this->_error = 'Tỉnh thành bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['career'])) {
	            return $this->_error = 'Ngành nghề lĩnh vực bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['birthday'])) {
	            return $this->_error = 'Ngày sinh bắt buộc phải nhập';
	        }
	        
	        if(empty($dataParam['sex'])) {
	            return $this->_error = 'Giới tính bắt buộc phải nhập';
	        }
	    }
	}
	
	public function getError() {
	    return $this->_error;
	} 
}