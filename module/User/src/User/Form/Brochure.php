<?php
namespace User\Form;
use \Zend\Form\Form as Form;

class Brochure extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Họ tên
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Họ và tên',
			),
		));
		
		// Điện thoại
		$this->add(array(
			'name'			=> 'phone',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Số điện thoại',
			),
		));
		
		// Email
		$this->add(array(
			'name'			=> 'email',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Email',
			),
		));
		
		// Sex
		$this->add(array(
			'name'			=> 'sex',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
			'options'		=> array(
				'empty_option'	=> 'Giới tính',
				'disable_inarray_validator' => true,
				'value_options'	=> array('male' => 'Nam', 'female' => 'Nữ'),
			)
		));
		
		// Birthday Year
		$birthday_year = array();
		for ($i = date('Y'); $i >= date('Y') - 50; $i--) {
			$birthday_year[$i] = $i;
		}
		$this->add(array(
			'name'			=> 'birthday_year',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
			'options'		=> array(
				'empty_option'  => 'Năm sinh',
				'disable_inarray_validator' => true,
				'value_options'	=> $birthday_year,
			)
		));
		
		// Chức vụ công ty
		$this->add(array(
			'name'			=> 'location_city_id',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
			'options'		=> array(
				'empty_option'	=> 'Tỉnh thành',
				'disable_inarray_validator' => true,
				'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\LocationCityTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
			),
		));
		
		// Tên công ty
		$this->add(array(
			'name'			=> 'company_name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder'=> 'Tên Công ty',
			),
		));
		
		// Chức vụ công ty
		$this->add(array(
			'name'			=> 'company_position',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
			'options'		=> array(
				'empty_option'	=> 'Chức vụ/ Vị trí',
				'disable_inarray_validator' => true,
				'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-position" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
			),
		));
		
		// Quy mô công ty
		$this->add(array(
		    'name'			=> 'company_size',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Quy mô công ty',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-size" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Ngành nghề công ty
		$this->add(array(
		    'name'			=> 'company_career',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Chuyên ngành hoạt động',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-career"), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Vấn đề bạn đang gặp phải trong kinh doanh
		$this->add(array(
		    'name'			=> 'company_problem',
		    'type'			=> 'MultiCheckbox',
		    'options'		=> array(
	    		'label_attributes' => array(
	    			'class'		=> 'checkbox-inline',
	    		),
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-problem"), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
	}
}