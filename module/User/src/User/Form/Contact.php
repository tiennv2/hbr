<?php
namespace User\Form;
use \Zend\Form\Form as Form;

class Contact extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Tên công ty
		$this->add(array(
			'name'			=> 'company_name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder'=> 'Tên Công ty',
			),
		));
		
		// Chức vụ công ty
		$this->add(array(
			'name'			=> 'company_position',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
			'options'		=> array(
				'empty_option'	=> 'Chức vụ/ Vị trí',
				'disable_inarray_validator' => true,
				'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-position" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
			),
		));
		
		// Quy mô công ty
		$this->add(array(
		    'name'			=> 'company_size',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Quy mô công ty',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-size" )), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Ngành nghề công ty
		$this->add(array(
		    'name'			=> 'company_career',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Chuyên ngành hoạt động',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-career"), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
		
		// Vấn đề bạn đang gặp phải trong kinh doanh
		$this->add(array(
		    'name'			=> 'company_problem',
		    'type'			=> 'Select',
		    'attributes'	=> array(
		        'class'		=> 'form-control',
		    ),
		    'options'		=> array(
		        'empty_option'	=> 'Anh/Chị mong muốn được HBR hỗ trợ trong lĩnh vực nào',
		        'disable_inarray_validator' => true,
		        'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-problem"), "order" => array("ordering" => "ASC", "name" => "ASC")), array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
		    ),
		));
	}
}