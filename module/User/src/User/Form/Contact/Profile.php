<?php
namespace User\Form\Contact;
use \Zend\Form\Form as Form;

class Profile extends Form {
	
	public function __construct($sm){
		parent::__construct();
		
		// FORM Attribute
		$this->setAttributes(array(
			'action'	=> '',
			'method'	=> 'POST',
			'class'		=> 'horizontal-form',
			'role'		=> 'form',
			'name'		=> 'adminForm',
			'id'		=> 'adminForm',
		));
		
		// Họ tên
		$this->add(array(
			'name'			=> 'name',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Họ và tên',
			),
		));
		
		// Điện thoại
		$this->add(array(
			'name'			=> 'phone',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Số điện thoại',
			),
		));
		
		// Email
		$this->add(array(
			'name'			=> 'email',
			'type'			=> 'Text',
			'attributes'	=> array(
				'class'		=> 'form-control',
				'placeholder' => 'Email',
			),
		));
		
		// Sex
		$this->add(array(
			'name'			=> 'sex',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
			'options'		=> array(
				'empty_option'	=> '- Giới tính -',
				'disable_inarray_validator' => true,
				'value_options'	=> array('male' => 'Nam', 'female' => 'Nữ'),
			)
		));
		
		// Birthday Year
		$birthday_year = array();
		for ($i = date('Y'); $i >= date('Y') - 50; $i--) {
			$birthday_year[$i] = $i;
		}
		$this->add(array(
			'name'			=> 'birthday_year',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control select2 select2_basic',
			),
			'options'		=> array(
				'empty_option'  => '- Năm sinh - ',
				'disable_inarray_validator' => true,
				'value_options'	=> $birthday_year,
			)
		));
		
		// Chức vụ công ty
		$this->add(array(
			'name'			=> 'location_city_id',
			'type'			=> 'Select',
			'attributes'	=> array(
				'class'		=> 'form-control',
			),
			'options'		=> array(
				'empty_option'	=> '- Tỉnh thành - ',
				'disable_inarray_validator' => true,
				'value_options'	=> \ZendX\Functions\CreateArray::create($sm->get('User\Model\LocationCityTable')->listItem(null, array('task' => 'cache')), array('key' => 'id', 'value' => 'name')),
			),
		));
	}
}

