<?php
namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Sql\Where;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ContractTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {
	
    protected $tableGateway;
	protected $userInfo;
	protected $serviceLocator;
	
	public function __construct(TableGateway $tableGateway) {
	    $this->tableGateway	= $tableGateway;
	    $this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
	    $this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator() {
	    return $this->serviceLocator;
	}
	
	public function countItem($arrParam = null, $options = null){
	    
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $ssFilter  = $arrParam['ssFilter'];
	            
	            if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
	                $select->where->equalTo('status', $ssFilter['filter_status']);
	            }
	            
	            if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
			        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('module', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('controller', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->like('action', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->UNNEST;
				}
	        })->count();
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
		if($options['task'] == 'list-all') {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				
				$select -> where -> equalTo('contact_id', $arrParam['contact_id']);
			})->toArray();
		}
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
		
		if($options['task'] == 'login') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select -> where -> NEST 
		        				 -> equalTo('email', $arrParam['email'])
		        				 -> OR
		        				 -> equalTo('phone', $arrParam['email'])
		        				 -> UNNEST;
		        $select -> where -> equalTo('password', md5($arrParam['password']));
		    })->current();
		}
		
		if($options['task'] == 'by-email') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('email', $arrParam['email']);
		    })->current();
		}
		
		if($options['task'] == 'active') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('id', $arrParam['id'])
		               ->where->notEqualTo('active_code', 1)
		               ->where->equalTo('active_code', $arrParam['code']);
		    })->current();
		}
		
		if($options['task'] == 'check-by-data') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('module', $arrParam['module'])
        		       ->where->equalTo('controller', $arrParam['controller'])
        		       ->where->equalTo('action', $arrParam['action']);
		    })->current();
		}
		
		if($options['task'] == 'forgot-password') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
		        $select->where->equalTo('id', $arrParam['id'])
        		              ->equalTo('password_code', $arrParam['password_code']);
		    })->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	     
	    $filter   = new \ZendX\Filter\Purifier(array(array('HTML.AllowedElements', '')));
	    $gid      = new \ZendX\Functions\Gid();
	    $date     = new \ZendX\Functions\Date();
	    $number	  = new \ZendX\Functions\Number();
	    
		if($options['task'] == 'register') {
			$id = $gid->getId();
			$data	= array(
				'id'                => $id,
				'name'              => $filter->filter($arrData['name']),
				'email'             => $filter->filter($arrData['email']),
				'phone'             => $filter->filter($arrData['phone']),
				'password'          => md5($arrData['password']),
			    'password_status'   => 1,
			    'status'            => 1,
			    'type'            	=> 'register',
			    'created'           => date('Y-m-d H:i:s'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		if($options['task'] == 'update-profile') {
			$id = $arrData['id'];
			$data	= array();
			
			if(!empty($arrData['name'])) {
				$data['name'] = $filter->filter($arrData['name']);
			}
			if(!empty($arrData['phone'])) {
				$data['phone'] = $number->formatToData($arrData['phone']);
			}
			if(!empty($arrData['email'])) {
				$data['email'] = strtolower(trim($filter->filter($arrData['email'])));
			}
			if(!empty($arrData['sex'])) {
				$data['sex'] = $filter->filter($arrData['sex']);
			}
			if(!empty($arrData['birthday_year'])) {
				$data['birthday_year'] = $filter->filter($arrData['birthday_year']);
			}
			if(!empty($arrData['company_name'])) {
				$data['company_name'] = $filter->filter($arrData['company_name']);
			}
			if(!empty($arrData['company_position'])) {
				$data['company_position'] = $filter->filter($arrData['company_position']);
			}
			if(!empty($arrData['company_size'])) {
				$data['company_size'] = $filter->filter($arrData['company_size']);
			}
			if(!empty($arrData['company_career'])) {
				$data['company_career'] = $filter->filter($arrData['company_career']);
			}
			if(!empty($arrData['company_problem'])) {
				$data['company_problem'] = implode(',', $arrData['company_problem']);
			}
			
			if(!empty($data)) {
				$this->tableGateway->update($data, array('id' => $id));
			}
			return $id;
		}
		
		// Kích hoạt tài khoản
		if($options['task'] == 'active') {
		    $id = $arrRoute['id'];
			$data	= array(
				'status'        => 1,
				'active_time'   => date('Y-m-d H:i:s'),
				'active_code'   => 1,
			);
			
			$this->tableGateway->update($data, array('id' => $id, 'active_code' => $arrRoute['code']));
			return $id;
		}
		
		// Update thông tin khi đăng nhập bằng facebook
		if($options['task'] == 'facebook-update') {
		    $id = $arrData['id'];
			$data	= array(
				'status'        => 1,
				'type'        	=> $arrData['type'],
				'name'        	=> $arrData['name'],
				'facebook_id'   => $arrData['facebook_id'],
			);
			
			if(!empty($arrData['active_code'])) {
				$data['active_code'] = $arrData['active_code'];
			}
			if(!empty($arrData['active_time'])) {
				$data['active_time'] = $arrData['active_time'];
			}
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
		
		// Insert liên hệ khi đăng nhập bằng facebook
		if($options['task'] == 'facebook-add') {
		    $id = $gid->getId();
			$data	= array(
				'id'            => $id,
				'name'          => $filter->filter($arrData['name']),
				'email'         => $filter->filter($arrData['email']),
				'password'      => md5($arrData['facebook_id'] . 'namnv'),
			    'status'        => 1,
			    'register_time' => date('Y-m-d H:i:s'),
			    'register_ip'   => $_SERVER['REMOTE_ADDR'],
			    'active_code'   => 1,
			    'active_time'   => date('Y-m-d H:i:s'),
			    'type'   		=> $arrData['type'],
			    'facebook_id'   => $arrData['facebook_id'],
			    'created'       => date('Y-m-d H:i:s'),
			    'modified'      => date('Y-m-d H:i:s'),
			);
			
			$this->tableGateway->insert($data);
			return $id;
		}
		
		// Update mã password_code
		if($options['task'] == 'update-password_code') {
		    $id = $arrData['id'];
		    $password_code = $gid->random(20);
		    $data	= array(
		        'password_code'   => $password_code
		    );
		    	
		    $this->tableGateway->update($data, array('id' => $id));
		    return $password_code;
		}
		
		// Xác nhận lấy lại mật khẩu
		if($options['task'] == 'confirm-forgot-password') {
		    $id = $arrData['id'];
		    
		    $data	= array(
		        'password'        => md5($arrData['password']),
		        'password_code'   => null
		    );
		    
		    $this->tableGateway->update($data, array('id' => $id));
		    return $id;
		}
		
		if($options['task'] == 'change-password') {
			$id = $arrData['id'];
			$data	= array(
				'password'	=> md5($arrData['password_new']),
			);
				
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
	
    public function deleteItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'delete-item') {
	        $where = new Where();
	        $where->in('id', $arrData['cid']);
	        $this->tableGateway->delete($where);
	        
	        return count($arrData['cid']);
	    }
	
	    return false;
	}
	
	public function changeStatus($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-status') {
	        if(!empty($arrData['cid'])) {
    	        $data	= array( 'status'	=> ($arrData['status'] == 1) ? 0 : 1 );
    			$this->tableGateway->update($data, array("id IN('". implode("','", $arrData['cid']) ."')"));
	        }
	        return true;
	    }
	    
	    return false;
	}
	
	public function changeOrdering($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    if($options['task'] == 'change-ordering') {
            foreach ($arrData['cid'] AS $id) {
                $data	= array('ordering'	=> $arrData['ordering'][$id]);
                $where  = array('id' => $id);
                $this->tableGateway->update($data, $where);
            }
            
            return count($arrData['cid']);
	    }
	    return false;
	}
}