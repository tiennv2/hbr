<?php
namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Expression;

class DocumentTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
    public function countItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-item') {
	        $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	            $configs   = $arrParam['configs'];
                $ssFilter  = $arrParam['ssFilter'];
                
                $select -> where->equalTo('code', $configs['code']);
                 
                if(isset($ssFilter['filter_status']) && $ssFilter['filter_status'] != '') {
                    $select->where->equalTo('status', $ssFilter['filter_status']);
                }
                
                if(isset($ssFilter['filter_keyword']) && $ssFilter['filter_keyword'] != '') {
    		        $select->where->NEST
                			      ->like('name', '%'. $ssFilter['filter_keyword'] . '%')
                			      ->or
                			      ->equalTo('id', $ssFilter['filter_keyword'])
                			      ->UNNEST;
    			}
            })->count();
	    }
	    
	    return $result;
	}
	
	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'list-all') {
            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
                if(!empty($arrParam['order'])) {
                    $select->order($arrParam['order']);
                } else {
                	$select->order(array('ordering' => 'ASC', 'name' => 'ASC'));
                }
                if(!empty($arrParam['where'])) {
                    foreach ($arrParam['where'] AS $key => $value) {
                        if(!empty($value)) {
                            $select->where->equalTo($key, $value);
                        }
                    }
                }
            });
	    }
		
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'Hbr'. $arrParam['where']['code'];
	        $result = $cache->getItem($cache_key);
	         
	        if (empty($result)) {
	            $items	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                if(!empty($arrParam['order'])) {
                        $select->order($arrParam['order']);
	                }
	                if(!empty($arrParam['where'])) {
	                    foreach ($arrParam['where'] AS $key => $value) {
	                        if(!empty($value)) {
                                $select->where->equalTo($key, $value);
	                        }
	                    }
	                }
	            });
                $result = \ZendX\Functions\CreateArray::create($items, array('key' => 'id', 'value' => 'object'));
                 
                $cache->setItem($cache_key, $result);
	        }
	    }
		
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
			    $select -> columns( array( '*',
            			        'date_begin' => new Expression("DATE_FORMAT(date_begin, '%d/%m/%Y')"),
            			        'date_end' => new Expression("DATE_FORMAT(date_end, '%d/%m/%Y')"),
            			    ))
                        ->where->equalTo('id', $arrParam['id']);
    		})->current();
		}
	
		return $result;
	}
}