<?php
namespace User\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormDataTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

	protected $tableGateway;
	protected $userInfo;
	protected $serviceLocator;
	
	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway	= $tableGateway;
		$this->userInfo	= new \ZendX\System\UserInfo();
	}
	
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}
	
	public function getServiceLocator() {
		return $this->serviceLocator;
	}

	public function listItem($arrParam = null, $options = null){
	    if($options['task'] == 'cache') {
	        $cache = $this->getServiceLocator()->get('cache');
	        $cache_key = 'UserPermission';
	        $result = $cache->getItem($cache_key);

	        if (empty($result)) {
	            $result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
	                $select->order(array('id' => 'ASC'));
	            })->toArray();
    
                $cache->setItem($cache_key, $result);
	        }
	    }
	    
		return $result;
	}
	
	public function getItem($arrParam = null, $options = null){
	
		if($options == null) {
			$result	= $this->tableGateway->select(function (Select $select) use ($arrParam){
				$select->where->equalTo('id', $arrParam['id']);
			})->current();
		}
		
		if($options['task'] == 'get-by-phone') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('form_id', $arrParam['form_id'])
                                 -> equalTo('phone', $arrParam['phone'])
                                 -> greaterThanOrEqualTo('created', date('Y-m-d'));
    		})->current();
		}
		
		if($options['task'] == 'by-contact') {
		    $result	= $this->tableGateway->select(function (Select $select) use ($arrParam, $options){
                $select -> where -> equalTo('form_id', $arrParam['form_id'])
                                 -> equalTo('contact_id', $arrParam['contact_id']);
    		})->current();
		}
	
		return $result;
	}
	
	public function saveItem($arrParam = null, $options = null){
	    $arrData  = $arrParam['data'];
	    $arrRoute = $arrParam['route'];
	    
	    $image    = new \ZendX\Functions\Thumbnail($arrData['image']);
	    $filter   = new \ZendX\Filter\Purifier(array( array('HTML.AllowedElements', '') ));
	    $gid      = new \ZendX\Functions\Gid();
	    
		if($options['task'] == 'public-add') {
			// Check tồn tại liên hệ
		    $contact = $this->getServiceLocator()->get('User\Model\ContactTable')->getItem(array('phone' => $arrData['phone']), array('task' => 'by-phone'));
		    if(!empty($contact)) {
		    	$contact_id = $contact['id'];
		    } else {
		    	$contact_id = $this->getServiceLocator()->get('User\Model\ContactTable')->saveItem($arrParam, array('task' => 'register'));
		    }
		    
		    // Kiểm tra tồn tại trong form_data
		    $form_data = $this->getItem(array('form_id' => $arrData['form_id'], 'contact_id' => $contact_id), array('task' => 'by-contact'));
		    
		    $data = array();
		    foreach ($arrData AS $key => $val) {
		        if(is_array($val)) {
		            $arrTmp = array();
		            foreach ($val AS $k => $v) {
		                $arrTmp[$k] = $filter->filter($v);
		            }
		            $value = serialize($arrTmp);
		        } else {
		            $value = $filter->filter($val);
		        }
		        $data[$key] = $value;
		    }
		    
		    // Loại bỏ những trường dữ liệu không có trong form data
		    unset($data['name']);
		    unset($data['phone']);
		    unset($data['email']);
		    unset($data['address']);
		    unset($data['company_name']);
		    unset($data['company_position']);
		    unset($data['company_size']);
		    unset($data['company_career']);
		    unset($data['company_problem']);
		    unset($data['type']);
		    
		    if(!empty($form_data)) {
		        $id = $form_data['id'];
		        
		        $this->tableGateway->update($data, array('id' => $id));
		    } else {
    			$data['status']  	= 0;
    			$data['created'] 	= date('Y-m-d H:i:s');
    			$data['contact_id'] = $contact_id;
    			
    			$this->tableGateway->insert($data);
    			$id = $this->tableGateway->getLastInsertValue();
		    }
			return $id;
		}
	    
		if($options['task'] == 'public-update') {
		    $id = $arrData['form_data_id'];
			
			$data = array();
			
			if(!empty($arrData['company'])) {
			    $data['company'] = $arrData['company'];
			    $data['job']     = $arrData['job'];
			    $data['option']  = serialize($arrData['option']);
			    $data['content'] = $arrData['content'];
			}
			
			$this->tableGateway->update($data, array('id' => $id));
			return $id;
		}
	}
}