<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class ReportTable extends AbstractTableGateway implements ServiceLocatorAwareInterface {

    protected $tableGateway;
    protected $userInfo;
    protected $serviceLocator;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway	= $tableGateway;
        $this->userInfo	= new \ZendX\System\UserInfo();
    }
    
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function getServiceLocator() {
        return $this->serviceLocator;
    }
    
	public function saveItem($arrParam = null, $options = null){
	
		if($options['task'] == 'update') {
			if(!empty($options['field'])) {
				$data    = array(
	    	        $options['field'] => new Expression('(`'. $options['field'] .'` + ?)', array(1))
	    	    );
	    	    
				$where   = new Where();
	    	    $where->equalTo('id', 1);
	    	    $this->tableGateway->update($data, $where);
			}
		}
	
		return $options['field'];
	}
}