CKEDITOR.editorConfig = function( config ) {
	config.language 		= 'vi';					
	config.removePlugins 	= 'iframe';
	config.extraPlugins 	= 'youtube,widget,lineutils,codesnippet,tableresize,autogrow';
	
	config.autoGrow_minHeight = 200;
	config.autoGrow_maxHeight = 600;
	 
	config.toolbar_Basic	= [
	      { name: 'document'	, items : [ 'Preview'] },             	   
	      { name: 'basicstyles'	, items : [ 'Bold','Italic','Underline','Strike','-','RemoveFormat' ] },
	      { name: 'styles'		, items : [ 'FontSize' ] },
	      { name: 'colors'		, items : [ 'TextColor','BGColor' ] },
	      { name: 'tools'		, items : [ 'Maximize' ] }
	];
	 
	config.toolbar	= 'Basic';
};