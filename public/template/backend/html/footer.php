<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/respond.min.js';?>"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/excanvas.min.js';?>"></script> 
<![endif]-->
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery-1.12.4.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap/js/bootstrap.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery-migrate-1.2.1.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery-ui/jquery-ui-1.10.3.custom.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery.blockui.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery.cokie.min.js';?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/select2/select2.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/select2/select2_locale_vi.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-toastr/toastr.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootbox/bootbox.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/fancybox/source/jquery.fancybox.pack.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery-inputmask/jquery.inputmask.bundle.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-datepicker/js/bootstrap-datepicker.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-colorpicker/js/bootstrap-colorpicker.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/numeric.min.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-modal/js/bootstrap-modalmanager.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/bootstrap-modal/js/bootstrap-modal.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/highcharts/highcharts.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jquery.form.min.js';?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN FORM PLUGINS -->
<script src="<?php echo URL_SCRIPTS . '/ckeditor/ckeditor.js';?>" type="text/javascript"></script>
<!-- END FORM PLUGINS -->

<script src="<?php echo $this->arrParams['template']['urlJs'] . '/define.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlJs'] . '/app.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlJs'] . '/form.js';?>" type="text/javascript"></script>
<script src="<?php echo $this->arrParams['template']['urlJs'] . '/me.js';?>" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
	App.init();
	Form.init();
});
</script>
<!-- END JAVASCRIPTS -->

<?php echo $this->headScript();?>