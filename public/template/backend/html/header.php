<head>
<?php 
	// Title
	echo $this->headTitle('Admin Panel');
	
	// Meta
	echo $this->headMeta()
	          ->appendName('viewport', 'width=device-width, initial-scale=1.0')
              ->appendName('description', '')
              ->appendName('author', '')
              ->appendName('MobileOptimized', '320')
              ->appendHttpEquiv('X-UA-Compatible', 'IE=edge')
			  ->setCharset('utf-8');
	// CSS
	echo $this->headLink(array('rel' => 'shortcut icon', 'href' => $this->arrParams['template']['urlImg'] . '/favicon.ico', 'type' => 'image/x-icon'), 'PREPEND')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/font-awesome/css/font-awesome.min.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap/css/bootstrap.min.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/select2/select2_metro.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/data-tables/DT_bootstrap.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap-toastr/toastr.min.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/fancybox/source/jquery.fancybox.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap-datepicker/css/datepicker.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap-colorpicker/css/colorpicker.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap-modal/css/bootstrap-modal-bs3patch.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] . '/bootstrap-modal/css/bootstrap-modal.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/style-metronic.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/style.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/style-responsive.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/plugins.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/pages/tasks.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/themes/default.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] . '/custom.css?v=1');
?>
</head>