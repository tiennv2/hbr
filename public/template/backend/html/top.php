<?php
    $ssSystem = new \Zend\Session\Container('system');  

    // Thông tin user
    $userInfo = new \ZendX\System\UserInfo();
    $userInfo = $userInfo->getUserInfo();
    
    $fullname = $userInfo->fullname;
    $avatar = URL_FILES . '/users/thumb/user-male-50.png';
    if(!empty($userInfo->avatar)) {
        $avatar = URL_FILES . '/users/thumb/' . $userInfo->avatar;
    }
?>

<div class="header navbar navbar-inverse">
	<div class="header-inner">
	
		<div class="hor-menu hidden-sm hidden-xs">
			<?php echo $this->blkMenu($this->arrParams, array('code' => 'Menu.Admin', 'cache' => true), 'admin');?>
		</div>
		
		<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><img src="<?php echo $this->arrParams['template']['urlImg'] . '/menu-toggler.png';?>" alt=""/></a>
		
		<ul class="nav navbar-nav pull-right">
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
    				<img src="<?php echo $avatar;?>">
    				<span class="username"><?php echo $fullname;?></span>
    				<i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->url('routeAdmin/default', array('controller' => 'user', 'action' => 'change-password'));?>"><i class="fa fa-lock"></i> Đổi mật khẩu</a> </li>
					<li class="divider"></li>
					<li> <a href="<?php echo $this->url('routeAdmin/default', array('controller' => 'user', 'action' => 'logout'));?>"><i class="fa fa-key"></i> Đăng xuất</a> </li>
				</ul>
			</li>
			<?php
			$language = explode(';', LANGUAGE);
			if(count($language) > 0) {
			?>
			<li class="language">
                <select class="form-control input-sm select2_basic" id="language">
                    <?php
                    foreach ($language AS $key => $value) {
                        $item = explode('-', $value);
                        $select = ($ssSystem->language == trim($item[0])) ? ' selected="selected"' : '';
                        echo '<option value="'. trim($item[0]) .'"'. $select .'>'. trim($item[1]) .'</option>';
                    }
                    ?>
                </select>
            </li>
            <?php
			} 
            ?>
		</ul>
	</div>
</div>
<div class="clearfix"></div>