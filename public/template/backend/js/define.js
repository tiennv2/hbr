/**
 * Author: NamNV
 * Desciption: Tham số mặc định của hệ thống
 */
var formAdmin 		= '#adminForm';
var urlTemplate		= '/public/template/backend';
var moduleName 		= jQuery('body').attr('data-module');
var controllerName 	= jQuery('body').attr('data-controller');
var actionName 		= jQuery('body').attr('data-action');

var xMessage = {
	'no-access': 'Bạn không có quyền thực hiện chức năng này',
	'no-checked': 'Bạn cần chọn những phần tử muốn thao tác',
	'record-exists': 'Dữ liệu đã tồn tại và không thể thêm',
	'success': 'Cập nhật dữ liệu thành công'
};
