<?php 
    $ssCart     = new \Zend\Session\Container('myCart');
    $carts      = $ssCart->data;
    
    $amount     = 0;
    $price      = 0;
    if(count($carts) > 0) {
        foreach ($carts AS $cart) {
            $amount += !empty($cart['amount']) ? $cart['amount'] : 0;
            $price  += !empty($cart['price']) ? $cart['price'] : 0;
        }
    }
?>
<div class="box_cart">
    <div class="box_title"><div class="title">Giỏ hàng của bạn</div></div>
    <div class="box_content">
    	<div class="row">
    		<div class="col-xs-6">Số sản phẩm: </div>
    		<div class="col-xs-6"><b><?php echo $amount; ?></b></div>
    	</div>
    	<div class="row">
    		<div class="col-xs-6">Tổng tiền: </div>
    		<div class="col-xs-6"><b><span class="mask_currency"><?php echo $price; ?></span> đ</b></div>
    	</div>
        <a href="<?php echo $this->url('routePost/default', array( 'controller' => 'product', 'action' => 'cart'));?>" class="control"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Xem giỏ hàng</a>
    </div>
</div>