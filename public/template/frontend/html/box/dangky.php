<div class="box_dangKy">
  	<div class="container">
    		<div class="box_wrapper">
      			<div class="box_content">
        				<div class="box_title"><div class="title font-title">ĐĂNG KÝ TRUY CẬP TÀI LIỆU ONLINE<br>MIỄN PHÍ TẠI HBR</div></div>
        				<div class="box_text">Với hơn 100 bài giảng trải rộng trên các chủ đề nổi bật như bán hàng, Marketing, nhân sự,... từ các chuyên gia hàng đầu trong nước và trên thế giới, HBR hi vọng có thể cung cấp cho anh/chị hệ thống kiến thức khoa học, kèm các công cụ quan trọng & các kỹ năng cần thiết về Lãnh đạo & Quản trị.</div>
        				<div class="box_form">
            				<form method="post" action="" id="frmRegister">
                     		<div class="form-group">
            						   <input type="text" class="form-control" name="name" id="name" placeholder="Họ Tên" required="required"/>
                    		</div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Số Điện Thoại" required="required"/>
            					  </div>
                    		<div class="form-group">
                      			<input type="text" class="form-control" name="email" id="email" placeholder="Email" required="required"/>
                  			</div>
                   			<div class="form-group">
                      			<input type="password" class="form-control" name="password" id="password" placeholder="Mật Khẩu" required="required"/>
                  			</div>
                   			<div class="form-group">
                        		<div class="alert alert-danger hidden"></div>
                  			</div>
                     		<div class="form-group">
                       			<button type="button" class="form-control btn-sucsess" name="submit">Đăng ký ngay</button>
                    		</div>
                    </form>
        				</div>
      			</div>
    		</div>
    </div>
</div>
<?php $this->headScript()->captureStart() ?>
    $('#frmRegister .btn-sucsess').click(function() {
        $.ajax({
            url     : '<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'register'));?>',
            type    : 'POST',
            data    : $('#frmRegister').serialize(),
            beforeSend: function() {
                $('body').append('<div class="page-loading"><div class="loader"></div></div>');
            },
            success : function(data){
                var result = JSON.parse(data);
                if(result.error) {
                    $('#frmRegister .alert').html(result.error).removeClass('hidden');
                } else if(result.success == 1) {
                    window.location.href = "<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index')) ?>";
                }
                $('body .page-loading').remove();
            }
        });
    });
<?php $this->headScript()->captureEnd() ?>