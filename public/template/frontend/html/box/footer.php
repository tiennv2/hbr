<?php include_once $this->arrParams['template']['pathHtml'] . '/box/tuvan.php';?>
<div class="box_footer">
	<div class="container">
        <div class="wrap clearfix">
    		<div class="col-sm-6 col-md-3">
                <div class="item-footer">
                    <div class="logo"><a href="/" title="<?php echo $this->viewModel['settings']['General.System.Company']['value'];?>"><img class="imageroyal" itemprop="image" src="<?php echo $this->viewModel['settings']['General.System.LogoWhite']['image'];?>" alt="Trường doanh nhân HBR"></a></div>
                    <div class="name"><?php echo $this->viewModel['settings']['General.System.Company']['value'];?></div>
                    <div class="slogan"><?php echo $this->viewModel['settings']['General.System.Slogan']['value'];?></div>
                    <div class="address"><i class="fa fa-heart" aria-hidden="true"></i> <?php echo $this->viewModel['settings']['General.System.Address']['value'];?></div>
                    <div class="hotline"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $this->viewModel['settings']['General.System.Hotline']['value'];?></div>
                    
                    <div class="social">
                        <a href="<?php echo $this->viewModel['settings']['General.System.Facebook']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_facebook.png';?>"></a>
                        <a href="<?php echo $this->viewModel['settings']['General.System.Youtube']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_youtube.png';?>"></a>
                        <a href="<?php echo $this->viewModel['settings']['General.System.Instagram']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_instagram.png';?>"></a>
                        <a href="<?php echo $this->viewModel['settings']['General.System.Google']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_google.png';?>"></a>
                    </div>
                </div>
        	</div>
    		<div class="col-sm-6 col-md-3">
                <div class="item-footer">
        			<div class="box_title">Menu</div>
        			<div class="box_content">
        				<?php echo $this->blkMenu($this->arrParams, array('code' => 'Menu.Footer', 'cache' => true), 'footer'); ?>
        			</div>
                </div>
        	</div>
    		<!-- <div class="col hidden">
    			<div class="box_title">Liên kết</div>
    			<div class="box_content">
    				<?php echo $this->blkDocument($this->arrParams, 'ads-footer', array('cache' => true));?>
    			</div>
        	</div> -->
    		<div class="col-sm-6 col-md-3">
                <div class="item-footer">
        			<div class="box_title">Đăng ký nhận tin</div>
        			<div class="box_content">
        				<form action="#" id="frmEmail" name="frmEmail" method="POST" class="">
        					<div class="form-group">
                            	<div class="input-group" id="input-email">
                                  	<input name="email" type="text" class="form-control" placeholder="Email">
                                  	<div class="input-group-addon" onclick="javascript:formRegister('frmEmail');"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
                            	</div>
                            	<div class="btnSuccess"></div>
                          	</div>
                          	<input type="hidden" name="form_id" value="1516006325-075c-1vk9-u6ra-3a452y47811q">
        				</form>
                        <div class="langtech">
                            <div class="title">Vận hành bởi</div>
                            <div class="img"><a href="<?php echo $this->viewModel['settings']['General.System.Langtech']['value'];?>" target="_blank"><img src="<?php echo $this->viewModel['settings']['General.System.Langtech']['image'];?>" style="width: 100%;"></a></div>
                        </div>
        			</div>
                </div>
        	</div>
            <div class="col-sm-6 col-md-3">
                <div class="item-footer">
                    <div class="box_title">Tải app</div>
                    <div class="box_content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7">
                                <img src="<?php echo $this->viewModel['settings']['General.System.QRcode']['image'];?>" alt="Trường doanh nhân HBR" />
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="inline-app">
                                    <a href="<?php echo $this->viewModel['settings']['General.System.AppAndroid']['description'];?>"><img src="<?php echo $this->viewModel['settings']['General.System.AppAndroid']['image'];?>" alt="Trường doanh nhân HBR"/></a>
                                    <a href="<?php echo $this->viewModel['settings']['General.System.AppIOS']['description'];?>"><img src="<?php echo $this->viewModel['settings']['General.System.AppIOS']['image'];?>" alt="Trường doanh nhân HBR" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </div>
        </div>
    </div>
</div>
<div class="livechat" id="livechat">
    <a href="<?php echo $this->viewModel['settings']['General.System.Livechat']['value'];?>" target="_blank"><i class="fa fa-commenting"></i>Hỗ trợ trực tuyến</a>
</div>
<style>
    .item-footer img{max-width: 100%;}
    .inline-app a{display: inline-block; margin: 5px 0; }
    @media screen and (max-width: 767px){
        .item-footer{padding: 15px 0; border-bottom: #fff thin dashed}
        .inline-app a{margin-top: 15px; display: inline-block; width: 50%; float: left; padding: 0 5px; }
        .inline-app a:first-child{padding-left: 0;}
        .inline-app a:last-child{padding-right: 0;}
    }
</style>