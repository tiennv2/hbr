<?php
	$userInfo = new \ZendX\System\ContactInfo();
	$userInfo = $userInfo->getContactInfo();
?>
<div class="box_top" id="box_top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contact">
                    <span class="hotline">
                    	<i class="fa fa-phone" aria-hidden="true"></i> 
                    	<?php
                    	$hotline = explode(' - ', $setting['General.System.Hotline']['value']);
                    	foreach ($hotline AS $key => $val) {
                    		if($key == 0) {
                    			echo '<a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		} else {
                    			echo ' - <a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		}
                    	}
                    	?>
                    </span>
                    <a href="mail:<?php echo $setting['General.System.Email']['value'];?>" class="email"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo $setting['General.System.Email']['value'];?></a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="user">
	                <ul class="nav navbar-nav">
	                	<?php
	                	if(!empty($userInfo)) { 
	                	?>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index'));?>"><?php echo $userInfo['name'];?></a></li>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'logout'));?>">Thoát</a></li>
	                	<?php
	                	} else { 
	                	?>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'login'));?>">Đăng nhập</a></li>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'register'));?>">Đăng ký</a></li>
		                <?php
	                	} 
		                ?>
		                <li class="hidden-md hidden-sm hidden-lg"><a style="background-color: #d3a23b; color: #FFF;" href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'active-code'));?>">Điền mã kích hoạt</a></li>
	            	</ul>
	            </div>
	            <div class="social">
        			<a href="<?php echo $this->viewModel['settings']['General.System.Google']['value'];?>" target="_blank" class="item"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Instagram']['value'];?>" target="_blank" class="item"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Youtube']['value'];?>" target="_blank" class="item"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Facebook']['value'];?>" target="_blank" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        		</div>
            </div>
        </div>
    </div>
</div>
<div class="box_header_online">
		<div class="container">          
            <div class="bottom">
                <div class="logo">
                    <a href="<?php echo $this->url('routeCourse'); ?>" title=""><img src="<?php echo $setting['General.System.Logo']['image'];?>"></a>
                </div>
                <div class="topic" id="show_box_menu">
                    <ul>
                        <li>
                            <i class="fa fa-th"></i>&nbsp; Chủ đề 
                            <ul>
                                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index'));?>"><i class="fa fa-user-secret"></i> Khóa học của tôi</a></li>
                                <?php 
                                $courses = $this->getHelperPluginManager()->getServiceLocator()->get('Post\Model\CourseCategoryTable')->listItem(array('block' => 'box_highlight'), array('task' => 'list-all'));
                    	    	foreach ($courses AS $item){
                    	    	    if ($item['type'] == 'default'){
                    	    	        $link = $this->url('routeCourse/category', array('alias' => $item['alias']));
                    	    	        $alias = $item['alias'];
                    	    	        $name = $item['name'];
                    	    	    
                	    	    ?>
                	    	    <li><a href="<?php echo $link; ?>"><i class="fa fa-folder" aria-hidden="true"></i> <?php echo $name; ?></a></li>
                	    	    <?php 
                    	    	    }
                    	    	}
                    	    	?>
                    	    </ul>
                        </li>
                    </ul>
                </div>
                <div class="search hidden-xs">
                    <form action="<?php echo $this->url('routeCourse'); ?>" method="POST">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Tìm kiếm">
                                <div class="input-group-addon">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="menu hidden-xs">
                    <ul class="nav navbar-nav">
                        <li><a class="active_code" href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'active-code'));?>" rel="nofollow">Điền mã kích hoạt</a></li>
                        <li><a class="icon cart_total" href="<?php echo $this->url('routePost/default', array('controller' => 'online', 'action' => 'course-cart'));?>"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                </div>
            </div>			
        </div>
    </div>