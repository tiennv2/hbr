<?php
	$userInfo = new \ZendX\System\ContactInfo();
	$userInfo = $userInfo->getContactInfo();
?>
<?php

$useragent = $_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
    $link_app = $setting['General.System.AppIOS']['description'];
    if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android')){
        $link_app = $setting['General.System.AppAndroid']['description'];
    }
    ?>
    <div class="app-box-header" id="app-box-header">
        <a href="javacript:;" class="close-app-box text-center"><i class="fa fa-times"></i></a>
        <div class="app-logo">
            <a href="<?php echo $link_app; ?>"><img src="<?php echo $setting['General.System.Logoleft']['image'] ?>" alt="Trường doanh nhân HBR" /></a>
        </div>
        <div class="app-content">
            <h4 class="app-title text-main"><a href="<?php echo $link_app; ?>">Trường doanh nhân HBR</a></h4>
            <div class="app-description">
                <small>Learning Today - Leading Tomorrow</small><br />
                <small class="text-green"><b>TẢI MIỄN PHÍ</b></small>
                <a href="<?php echo $link_app; ?>" class="btn btn-primary bg-main btn-download">Tải app</a>
            </div>
        </div>
    </div>
    <?php
}
?>
<div class="box_top" id="box_top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contact">
                    <span class="hotline">
                    	<i class="fa fa-phone" aria-hidden="true"></i> 
                    	<?php
                    	$hotline = explode(' - ', $setting['General.System.Hotline']['value']);
                    	foreach ($hotline AS $key => $val) {
                    		if($key == 0) {
                    			echo '<a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		} else {
                    			echo ' - <a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		}
                    	}
                    	?>
                    </span>
                    <a href="mail:<?php echo $setting['General.System.Email']['value'];?>" class="email"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo $setting['General.System.Email']['value'];?></a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="user">
	                <ul class="nav navbar-nav">
	                	<?php
	                	if(!empty($userInfo)) { 
	                	?>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index'));?>"><?php echo $userInfo['name'];?></a></li>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'logout'));?>">Thoát</a></li>
	                	<?php
	                	} else { 
	                	?>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'login'));?>">Đăng nhập</a></li>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'register'));?>">Đăng ký</a></li>
		                <?php
	                	} 
		                ?>
                        <li><a href="http://careers.hbr.edu.vn/" style="background: #FF0000; color: #FFF;">Tuyển dụng</a></li>
	            	</ul>
	            </div>
	            <div class="social">
        			<a href="<?php echo $this->viewModel['settings']['General.System.Google']['value'];?>" target="_blank" class="item"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Instagram']['value'];?>" target="_blank" class="item"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Youtube']['value'];?>" target="_blank" class="item"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Facebook']['value'];?>" target="_blank" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        		</div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->blkMenu($this->arrParams, array('code' => 'Menu.Main', 'cache' => true), 'main'); ?>
