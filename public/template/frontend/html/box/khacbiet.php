<div class="box_khacBiet" id="box_khacBiet">
	<div class="container">
		<div class="box_title">HBR - 6 Điểm khác biệt</div>
		<div class="video">
			<iframe width="100%" src="https://www.youtube.com/embed/06eSsOWcKYA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	</div>
	<div class="container">
		<ul class="nav nav-tabs">
		    <li class="col-sm-2 col-xs-4 active">
		    	<a data-toggle="tab" href="#home">
			    	<div class="img">
			    		<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-5.png">
			    		<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-11.png">
			    	</div>
			    </a>
		    	<div class="title">1. Chuyên gia bài bản & thực chiến</div>
		    </li>
		    <li class="col-sm-2 col-xs-4">
		    	<a data-toggle="tab" href="#home1">
		    		<div class="img">
		    			<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-6.png">
		    			<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-12.png">
		    		</div>
		    	</a>
		    	<div class="title">2.	Kiến thức cập nhật & mang tính ứng dụng cao</div>
		    </li>
		    <li class="col-sm-2 col-xs-4">
		    	<a data-toggle="tab" href="#home2">
			    	<div class="img">
			    		<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-7.png">
			    		<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-13.png">
			    	</div>
			    </a>
		    	<div class="title">3. Mạng lưới kết nối cộng đồng doanh nghiệp rộng rãi</div>
		    </li>
		    <li class="col-sm-2 col-xs-4">
		    	<a data-toggle="tab" href="#home3">
			    	<div class="img">
			    		<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-8.png">
			    		<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-14.png">
			    	</div>
			    </a>
		    	<div class="title">4. Hệ thống khóa học trực tuyến 24/7</div>
		    </li>
		    <li class="col-sm-2 col-xs-4">
		    	<a data-toggle="tab" href="#home4">
			    	<div class="img">
			    		<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-9.png">
			    		<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-15.png">
			    	</div>
			    </a>
		    	<div class="title">5. Chương trình học tập qua trải nghiệm thực tế</div>
		    </li>
		    <li class="col-sm-2 col-xs-4">
		    	<a data-toggle="tab" href="#home5">
			    	<div class="img">
			    		<img class="img-1" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-10.png">
			    		<img class="img-2" src="<?php echo $this->arrParams['template']['urlImg']; ?>/icon-16.png">
			    	</div>
			    </a>
		    	<div class="title">6. Chương trình Tư vấn và Huấn luyện chuyên biệt</div>
		    </li>
		</ul>
	</div>
	<div class="box_content">
		<div class="container">
			<div class="tab-content">
			    <div id="home" class="tab-pane fade in active">
				    <p>Các giảng viên của HBR đều là những chuyên gia hàng đầu đang làm việc và nghiên cứu tại các công ty/ tập đoàn nổi tiếng trong nước và quốc tế. Các chuyên gia đều sở hữu kiến thức chuyên môn bài bản & hệ thống, khả năng ứng dụng vào thực tiễn doanh nghiệp Việt Nam linh hoạt cùng phương pháp giảng dạy hiện đại, mang lại hiệu quả cao. </p>
			    </div>
			    <div id="home1" class="tab-pane fade">
			    	<p>Các giảng viên của HBR đều là những chuyên gia hàng đầu đang làm việc và nghiên cứu tại các công ty/ tập đoàn nổi tiếng trong nước và quốc tế. Các chuyên gia đều sở hữu kiến thức chuyên môn bài bản & hệ thống, khả năng ứng dụng vào thực tiễn doanh nghiệp Việt Nam linh hoạt cùng phương pháp giảng dạy hiện đại, mang lại hiệu quả cao. </p>
			    </div>
			    <div id="home2" class="tab-pane fade">
			    	<p>Không đơn thuần là môi trường học tập giữa thầy và trò, HBR mang đến cơ hội hợp tác, học hỏi giữa các doanh nghiệp với nhau, kết nối cộng đồng doanh nghiệp lớn nhỏ trong và ngoài nước.</p>
			    </div>
			    <div id="home3" class="tab-pane fade">
			    	<p>Trường doanh nhân HBR còn cung cấp các khóa học Online với hệ thống bài giảng phong phú, dễ dàng ôn lại kiến thức mọi lúc mọi nơi cũng như tiết kiệm thời gian & chi phí cho doanh nghiệp. </p>
			    </div>
			    <div id="home4" class="tab-pane fade">
			    	<p>Không chỉ là những giờ học trên lớp, học viên còn có cơ hội tham quan môi trường làm việc thực tế của các doanh nghiệp khác nhau. Tại đây, học viên có cơ hội lắng nghe những chia sẻ đến từ các chuyên gia, học hỏi từ các doanh nghiệp khác để vận dụng vào thực tiễn doanh nghiệp mình một cách linh hoạt.</p>
			    </div>
			    <div id="home5" class="tab-pane fade">
			    	<p>Bên cạnh các khóa đào tạo, Trường doanh nhân HBR còn chuyên cung cấp các chương trình tư vấn và huấn luyện chuyên biệt. Chương trình được thiết kế phù hợp với từng mục tiêu và đặc thù riêng của mỗi doanh nghiệp, giúp các tổ chức tháo gỡ những vấn đề hiện tại, tối ưu lại bộ máy quản trị để đạt lợi thế cạnh tranh bền vững.</p>
			    </div>
			</div>
		</div>
	</div>
</div>

<?php $this->headScript()->captureStart() ?>


<?php $this->headScript()->captureEnd() ?>