<div class="box_filter" id="box_filter">
	<?php
	$category = $this->getHelperPluginManager()->getServiceLocator()->get('Post\Model\PostCategoryTable')->listItem(array('type' => 'product'), array('task' => 'list-all'));
	$city = $this->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\DocumentTable')->listItem(array("where" => array( "code" => "city" )), array('task' => 'cache'));
	?>
	<div class="box_title">Tìm kiếm</div>
	<div class="box_content">
		<form action="/search" method="get" name="frmFilter" id="frmFilter">
          	<div class="form-group">
                <div class="input-group">
                  	<input name="keyword" type="text" class="form-control" placeholder="Từ khóa" value="<?php echo $_GET['keyword'];?>">
                  	<div class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></div>
                </div>
          	</div>
          	<div class="form-group">
                <select name="category" class="form-control">
                	<option value="">- Tất cả danh mục -</option>
                	<?php
                	$i = 0;
                	foreach ($category AS $key => $value) {
                	    $i++;
                	    $level = str_repeat('<span class="gi">--- </span>', $value['level'] - 2);
                	    $select = ($value['id'] == $_GET['category']) ? ' selected="selected"' : '';
                	    if($i > 1) {
                	        echo '<option value="'. $value['id'] .'"'. $select .'>'. $level . $value['name'] .'</option>';
                	    }
                	}
                	?>
                </select>
          	</div>
          	<div class="form-group">
                <select name="city" class="form-control">
                	<option value="">- Tất cả tỉnh thành - </option>
                	<?php
                	foreach ($city AS $key => $value) {
                	    $select = ($value['id'] == $_GET['city']) ? ' selected="selected"' : '';
            	        echo '<option value="'. $value['id'] .'"'. $select .'>'. $value['name'] .'</option>';
                	}
                	?>
                </select>
          	</div>
          	<div class="form-group">
                <select name="district" class="form-control">
                	<option value="">- Tất cả quận huyện - </option>
                </select>
          	</div>
          	<div class="form-group">
          		<input type="hidden" name="type" value="product">
          		<button type="submit" class="form-control my_btn">Tìm kiếm</button>
          	</div>
          	<?php $this->headScript()->captureStart() ?>
          	function load_district(city_id) {
          		if(city_id == '') {
          			$('#box_filter select[name="district"]').html('<option value="">- Tất cả quận huyện - </option>');
          		} else {
                  	$.ajax({
                		type: "POST",
                		url: '/xadmin/api/select',
                		data: {
                			'data-parent': city_id,
                            'data-parent-field': 'document_id',
                            'data-table': 'x_document',
                            'data-id': 'id',
                            'data-text': 'name'
                		},
                		dataType: "json",
                		cache: false,
                		success: function(result){
                			var xhtml = '<option value="">- Tất cả quận huyện - </option>';
                			if(result) {
                				$.each(result, function(key, value) {
                					if(key > 0) {
                						var selected = '';
                						if(value.id == '<?php echo $_GET['district'];?>') {
                							selected = ' selected="selected"';
                						}
                						xhtml += '<option value="'+ value.id +'"'+ selected +'>'+ value.text +'</option>';
                					}
                				});
                			}
                			$('#box_filter select[name="district"]').html(xhtml);
                		}
                	});
            	}
          	}
          	load_district('<?php echo $_GET['city'];?>');
          	
          	$('#box_filter select[name="city"]').change(function() {
          		load_district($(this).val());
        	});
        	<?php $this->headScript()->captureEnd() ?>
        </form>
	</div>
</div>
<?php echo $this->blkDocument($this->arrParams, 'support', array('cache' => true));?>
<?php include_once $this->arrParams['template']['pathHtml'] . '/box/page.php';?>
<?php echo $this->blkPostItemBox($this->arrParams, array('type' => 'video', 'box' => 'box_highlight', 'cache' => true), 'highlight');?>
<?php echo $this->blkDocument($this->arrParams, 'ads-left', array('cache' => true));?>
