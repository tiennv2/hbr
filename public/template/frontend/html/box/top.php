<?php
	$userInfo = new \ZendX\System\ContactInfo();
	$userInfo = $userInfo->getContactInfo();
?>
<div class="box_top" id="box_top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-8 hidden-xs hidden-sm">
                <div class="contact">
                    <span class="hotline">
                    	<i class="fa fa-phone" aria-hidden="true"></i> 
                    	<?php
                    	$hotline = explode(' - ', $setting['General.System.Hotline']['value']);
                    	foreach ($hotline AS $key => $val) {
                    		if($key == 0) {
                    			echo '<a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		} else {
                    			echo ' - <a href="tel:'. str_replace('.', '', $val) .'">'. $val .'</a>';
                    		}
                    	}
                    	?>
                    </span>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
	            <div class="social" style="float: right;">
        			<a href="<?php echo $this->viewModel['settings']['General.System.Google']['value'];?>" target="_blank" class="item"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Instagram']['value'];?>" target="_blank" class="item"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Youtube']['value'];?>" target="_blank" class="item"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Facebook']['value'];?>" target="_blank" class="item"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        		</div>
            </div>
        </div>
    </div>
</div>
