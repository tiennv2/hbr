<form action="#" id="frmRegisterTuVan" name="frmRegisterTuVan" method="POST" class="">
    <div class="box_dangKyTuVan" id="box_dangKyTuVan" id="form-1">
        <div class="container">
            <div class="row">
                <div class="dangKy">
                    <div class="box_content">
                        <div class="step_1">
                            <div class="box_title title_1">Đăng ký nhận tư vấn khóa học của HBR</div>
                            <div class="col-md-6">
                                <div class="form-group" id="input-name">
                                    <input name="name" type="text" class="form-control" placeholder="Họ và tên">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="input-phone">
                                    <input name="phone" type="text" class="form-control" placeholder="Điện thoại">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="input-email">
                                    <input name="email" type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" id="input-location_city_id">
                                    <select name="location_city_id" class="form-control">
                                        <option value=""  selected="selected">Tỉnh thành</option>
                                        <option value="1471419030-5m0i-1336-1k80-275892616r50">Hà Nội</option>
                                        <option value="1471419543-77s5-83it-g000-9j600uz74951">TP Hồ Chí Minh</option>
                                        <option value="1547720621b8ro00k43n97">Tỉnh khác</option>
                                    </select>
                                 </div>
                            </div>
                        </div>
                        <div class="step_2 hidden">
                            <div class="box_title title_2 hidden">Anh/chị vui lòng cập nhật thêm thông tin</div>
                            <div class="col-md-6">
                                <div class="form-group" id="input-company_name">
                                    <input name="company_name" type="text" class="form-control" placeholder="Công ty">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php
                                    $company_position = $this->getHelperPluginManager()->getServiceLocator()->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-position" )), array('task' => 'cache'));
                                ?>
                                <div class="form-group" id="input-company_position_id">
                                    <select name="company_position_id" class="form-control">
                                        <option class="company_position_id" value="">Chức vụ</option>
                                        <?php
                                            foreach ($company_position AS $val) {
                                                $id     = $val['id'];
                                                $name   = $val['name'];
                                        ?>  
                                        <option class="company_position_id" value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php
                                    $company_size = $this->getHelperPluginManager()->getServiceLocator()->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-size" )), array('task' => 'cache'));
                                ?>
                                <div class="form-group" id="input-company_size">
                                    <select name="company_size" class="form-control">
                                        <option class="company_size" value="">Quy mô công ty</option>
                                        <?php
                                            foreach ($company_size AS $val) {
                                                $id     = $val['id'];
                                                $name   = $val['name'];
                                        ?>  
                                        <option class="company_size" value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <?php
                                    $company_career = $this->getHelperPluginManager()->getServiceLocator()->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-career" )), array('task' => 'cache'));
                                ?>
                                <div class="form-group" id="input-company_career">
                                    <select name="company_career" class="form-control">
                                        <option class="company_career" value="">Chuyên ngành hoạt động</option>
                                        <?php
                                            foreach ($company_career AS $val) {
                                                $id     = $val['id'];
                                                $name   = $val['name'];
                                        ?>  
                                        <option class="company_career" value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="col-md-12">
                                <?php
                                    $company_problem = $this->getHelperPluginManager()->getServiceLocator()->get('User\Model\DocumentTable')->listItem(array( "where" => array( "code" => "company-problem" )), array('task' => 'cache'));
                                ?>
                                <div class="form-group" id="input-company_problem">
                                    <select name="company_problem[]" class="selectpicker form-control" multiple data-selected-text-format="count > 4" title="Lĩnh vực bạn đang gặp phải trong kinh doanh.">
                                        <?php
                                            foreach ($company_problem AS $value) {
                                                $id     = $value['id'];
                                                $name   = $value['name'];
                                        ?>  
                                        <option class="company_problem" value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="btnSuccess">
                                <input type="hidden" name="form_id" value="154771049754653129f3x0">
                                <input type="hidden" name="form_data_id" value="">
                                <input type="hidden" name="type" value="form">
                                <?php 
                                if ($link == '') {
                                    $source = '<input type="hidden" name="source" value="'.$url.'">';
                                }else{
                                    $source = '<input type="hidden" name="source" value="'.$link.'">';
                                }
                                ?>
                                <?php
                                echo $source;
                                ?>
                                <input type="hidden" name="source_detail" value="<?php echo $_SERVER['HTTP_REFERER'];?>">
                                <input type="button" onclick="javascript:formRegisterDownloadCRM('frmRegisterTuVan');" class="input-lg btn-blue step_1" value="Đăng ký ngay">
                                <input type="button" onclick="javascript:formRegisterUpdateCRM('frmRegisterTuVan');" class="input-lg btn-blue step_2 hidden" value="Hoàn tất đăng ký">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- end box_dangKy -->

<div class="box_dangKyFree go_box hidden" id="box_dangKyFree" data-box="box_dangKyTuVan">
    <div class="box_content">
        <div class="images">
            <div class="img"><img src="<?php echo $this->viewModel['settings']['General.System.Logoleft']['image'];?>"></div>
        </div>
        <div class="content">
            <div class="title">Đăng ký nhận tư vấn miễn phí các khóa học của HBR</div>
            <div class="input-lg btn-blue btn">Đăng ký ngay</div>
        </div>
    </div>
</div>
