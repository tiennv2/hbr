<style>
	.mobile-hotline-right{float: right; text-align: center;margin-left: 5px; margin-right: 10px; margin-top: 7px; height: 45px; border-radius: 4px;}
	.mobile-hotline{display: block; bottom: 10px; width: 100%;height: 85px; position: fixed; z-index:9999999}
	@media ( max-width : 1000px){
		.mobile-hotline-right{width:49%;}
	}
	@media(max-width:768px){
		.mobile-hotline{bottom:-10px;}
		.mobile-hotline-right{margin-right:0;}
		.mobile-hotline-right img{width:90%;}
		
	}
</style>
<div class="mobile-hotline">
	<div class="mobile-hotline-right">
        <a href="tel:0829996633"><img src="public/template/frontend/img/hotline.png"></a><br>
		<a href="https://m.me/hbr.edu.vn" target="blank" class="chat-facebook"><img alt="facebook" src="https://langmaster.edu.vn/public/images/chat.png" ></a>
	</div>
</div>
<div class="box_footer">
	<div class="container">
        <div class="wrap clearfix">
    		<div class="col col-1">
        		<div class="logo"><a href="/" title="<?php echo $this->viewModel['settings']['General.System.Company']['value'];?>"><img src="<?php echo $this->viewModel['settings']['General.System.LogoWhite']['image'];?>" alt="Logo HBR"></a></div>
        		<div class="name"><?php echo $this->viewModel['settings']['General.System.Company']['value'];?></div>
        		<div class="slogan"><?php echo $this->viewModel['settings']['General.System.Slogan']['value'];?></div>
        		<div class="address"><i class="fa fa-heart" aria-hidden="true"></i> <?php echo $this->viewModel['settings']['General.System.Address']['value'];?></div>
        		<div class="hotline"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $this->viewModel['settings']['General.System.Hotline']['value'];?></div>
        		<div class="social">
        			<a href="<?php echo $this->viewModel['settings']['General.System.Facebook']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_facebook.png';?>"></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Youtube']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_youtube.png';?>"></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Instagram']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_instagram.png';?>"></a>
        			<a href="<?php echo $this->viewModel['settings']['General.System.Google']['value'];?>" target="_blank" class="item"><img src="<?php echo $this->arrParams['template']['urlImg'] .'/icon_google.png';?>"></a>
        		</div>
        	</div>
    		<div class="col col-2">
    			<div class="box_title">Menu</div>
    			<div class="box_content">
    				<?php echo $this->blkMenu($this->arrParams, array('code' => 'Menu.Footer', 'cache' => true), 'footer'); ?>
    			</div>
        	</div>
    		<div class="col col-3">
    			<div class="box_title">Liên kết</div>
    			<div class="box_content">
    				<?php echo $this->blkDocument($this->arrParams, 'ads-footer', array('cache' => true));?>
    			</div>
        	</div>
    		<div class="col col-4">
    			<div class="box_title">Đăng ký nhận tin</div>
    			<div class="box_content">
    				<form action="#" id="frmEmail" name="frmEmail" method="POST" class="">
    					<div class="form-group">
                        	<div class="input-group" id="input-email">
                              	<input name="email" type="text" class="form-control" placeholder="Email">
                              	<div class="input-group-addon" onclick="javascript:formRegister('frmEmail');"><i class="fa fa-paper-plane" aria-hidden="true"></i></div>
                        	</div>
                        	<div class="btnSuccess"></div>
                      	</div>
                      	<input type="hidden" name="form_id" value="1516006325-075c-1vk9-u6ra-3a452y47811q">
    				</form>
    			</div>
        	</div>
        </div>
    </div>
</div>

