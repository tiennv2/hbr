<script type="text/javascript">
$('body').delegate('#frmRegister','submit',function(e){
    e.preventDefault();
    var _this = this;
    $(_this).find('.has-error').removeClass('has-error');
    $(_this).find('.alert').remove();
    $.ajax({
        url: $(this).attr('action'),
        method: 'POST',
        contentType: false,
        processData: false,
        data: new FormData(this),
        dataType: 'json',
        success: function(resp) {
            if (!resp.save) {
                Object.keys(resp.error).forEach(key => {
                    $(_this).find('[name="'+key+'"]').parent().addClass('has-error');
                });
                $(_this).find('.btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đẩy đủ thông tin</div>');
            } else {
                $(_this).find('.btnSuccess').prepend('<div class="alert alert-success">Dữ liệu đã được gửi thành công</div>');
            }
        }
    })
});
/**
 * Desciption: Form đăng ký
 */
function formRegisterCRM(form) {
    var idForm  = "#" + form;
    var urlAjax = "https://langmaster.vn/erp/api/hbr-form-data/add";
    var textBtn = $(idForm + ' .btnSuccess .btn').val();
    
    $.ajax({
        type: "POST",
        url: urlAjax,
        data: $(idForm).serialize(),
        dataType: "json",
        cache: false,
        beforeSend: function() {
            $(idForm + ' .form-group').removeClass('has-error');
            $(idForm + ' .alert').remove();
            $(idForm + ' .btnSuccess .btn').val('Đang gửi thông tin...').attr('disabled', 'disabled');
            $('body').append('<div class="page-loading"><div class="loader"></div></div>');
        },
        success: function(result){
            if(result.error) {
                if(result.error.form_id) {
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                } else {
                    $.each(result.error, function(key, value) {
                        $(idForm + ' #input-'+ key).addClass('has-error');
                    })
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                }
            } else {
                $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
                $(idForm + ' .form-control').val('');
                window.location.href = "<?php echo '/'.$item['alias'].'/thankyou'; ?>";
            }
            
            $(idForm + ' .btnSuccess .btn').val(textBtn).removeAttr('disabled', 'disabled');
            $('body .page-loading').remove();
        }
    });
}

//formRegister
function formRegisterDefault(form) {
    var idForm  = "#" + form;
    var urlAjax = "http://crm.demo/api/sale-form-data/add-data";
    var textBtn = $(idForm + ' .btnSuccess .btn.step_1').val();
        
    $.ajax({
        type: "POST",
        url: urlAjax,
        data: $(idForm).serialize(),
        dataType: "json",
        cache: false,
        beforeSend: function() {
            $('.btnSuccess .panel.panel-danger').remove();
            $(idForm + ' .form-group').removeClass('has-error');
            $(idForm + ' .alert').remove();
            $(idForm + ' .btnSuccess .btn.step_1').val('Đang gửi thông tin...').attr('disabled', 'disabled');
            $('body').append('<div class="page-loading"><div class="loader"></div></div>');
        },
        success: function(result){
            $('.btnSuccess .panel.panel-danger').remove();
            var subjectLength = $('#phone-input').length ? $('#phone-input').val().length :'';
            if(result.error) {
                if(result.error.form_id) {
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                } else if(subjectLength !== 10) {
                    $.each(result.error, function(key, value) {
                        $(idForm + ' #input-'+ key).addClass('has-error');
                    })
                    $('.btnSuccess').before('<div class="panel panel-danger"><div class="panel-heading">Số điện thoại không hợp lệ</div></div>');
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                } else {
                    $.each(result.error, function(key, value) {
                        $(idForm + ' #input-'+ key).addClass('has-error');
                    })
                    $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                }
            } else {
                $(idForm + ' .btnSuccess').prepend('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>');
            }

            $(idForm + ' .btnSuccess .btn.step_1').val(textBtn).removeAttr('disabled', 'disabled');
            $('body .page-loading').remove();
        }
    });
}

//formUpdate
function formRegisterDefaultUpdate(form) {
    var idForm  = "#" + form;
    var urlAjax = "https://langmaster.vn/erp/api/hbr-form-data/update";
    var textBtn = $(idForm + ' .btnSuccess .btn.step_2').val();

    if($(idForm + ' input[name="company_name"]').val() == '') {
        $(idForm + ' #input-company_name').addClass('has-error');
        $(idForm + ' #input-company_position_id').addClass('has-error');
        $(idForm + ' #input-company_size').addClass('has-error');
        $(idForm + ' #input-company_career').addClass('has-error');
        $(idForm + ' #input-company_problem').addClass('has-error');
        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
    } else {
        $.ajax({
            type: "POST",
            url: urlAjax,
            data: $(idForm).serialize(),
            dataType: "json",
            cache: false,
            beforeSend: function() {
                $(idForm + ' .form-group').removeClass('has-error');
                $(idForm + ' .alert').remove();
                $(idForm + ' .btnSuccess .btn.step_2').val('Đang gửi thông tin...').attr('disabled', 'disabled');
                $('body').append('<div class="page-loading"><div class="loader"></div></div>');
            },
            success: function(result){
                if(result.error) {
                    if(result.error.form_id) {
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">'+ result.error.form_id +'</div>');
                    } else {
                        $.each(result.error, function(key, value) {
                            $(idForm + ' #input-'+ key).addClass('has-error');
                        })
                        $(idForm + ' .btnSuccess').prepend('<div class="alert alert-danger">Vui lòng điền đầy đủ các thông tin bắt buộc</div>');
                    }
                } else {
                    $(idForm + ' .btnSuccess').prepend(
                        '<div class="alert alert-success text-center" style="background: #1d2c53; color: #fff;"><h3>Thank you!</h3><br>Cảm ơn anh/chị đã đăng ký tham gia khoá học của Trường Doanh nhân HBR.<br>Thân mời anh/chị tham khảo các khóa học sắp diễn ra:<br><a href="https://hbr.edu.vn/hbr-dao-tao-public"><img class="text-center" src="https://hbr.edu.vn/public/template/frontend/img/btnThamKhao.png"></a></div>'
                    );

                    $('.step_2').addClass('hidden');
                    $(idForm + ' .btnSuccess .btn.step_2').addClass('hidden');
                    
                    // window.location.href = "<?php //echo '/'.$item['alias'].'/thankyou'; ?>";
                }

                $(idForm + ' .btnSuccess .btn.step_2').val(textBtn).removeAttr('disabled', 'disabled');
                $('body .page-loading').remove();
            }
        });
    }
}
</script>