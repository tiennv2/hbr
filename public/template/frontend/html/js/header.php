<link href="<?php echo $this->arrParams['template']['urlCss'] .'/tuvan.css?v='. strtotime("now"); ?>" media="screen" rel="stylesheet" type="text/css">
<link href="<?php echo $this->arrParams['template']['urlCss'] .'/livechat.css?v='. strtotime("now"); ?>" media="screen" rel="stylesheet" type="text/css">
<?php
$url  = $_SERVER['REQUEST_URI'];
$link = $_SERVER['QUERY_STRING'];
?>

<?php 
if($layout == 'layout/home'){
  ?>   
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:site_name" content="Trường doanh nhân HBR - HBR Business School - Khóa học CEO"/>
    <script type='application/ld+json'>
      {
        "@context": "http://www.schema.org",
        "@type": "ProfessionalService",
        "name": "Trường doanh nhân HBR",
        "url": "https://hbr.edu.vn/",
        "logo": "https://hbr.edu.vn/public/files/upload/default/images/he-thong/logo-medium.png",
        "image": "https://hbr.edu.vn/public/files/upload/06_2019/images/alok/alok-11-12-05/khoa-hoc-ceo-tai-ha-noi-danh-cho-lanh-dao-quan-ly-chien-luoc-thu-hut-tuyen-dung-dao-tao-va-giu-chan-nhan-tai-anh--5-.jpg",
        "description": "Trường doanh nhân HBR giới thiệu các khóa học CEO tại Hà Nội, HCM dành cho giám đốc, lãnh đạo & các nhà quản lý.",
        "address": {
           "@type": "PostalAddress",
           "streetAddress": "139 Cầu Giấy",
           "addressLocality": "Hà Nội",
           "addressRegion": "Hà Nội",
           "postalCode": "122008",
           "addressCountry": "Việt Nam"
        },
        "geo": {
           "@type": "GeoCoordinates",
           "latitude": "21.0319525",
           "longitude": "105.7987537"
        },
        "hasMap": "https://www.google.com/maps/place/Tr%C6%B0%E1%BB%9Dng+doanh+nh%C3%A2n+HBR/@21.0320243,105.7968491,17z/data=!3m1!4b1!4m5!3m4!1s0x3135ab454846b8a1:0x1de05eba0f60ec0a!8m2!3d21.0320193!4d105.7990378",
        "telephone": "0829996886"
      }
    </script>
  <?php
}
?>

<!-- Google Tag Manager Thiết -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KCKJNR5');</script>
<!-- End Google Tag Manager -->

<!-- Google Code dành cho Thẻ tiếp thị lại -->
<!--------------------------------------------------
Không thể liên kết thẻ tiếp thị lại với thông tin nhận dạng cá nhân hay đặt thẻ tiếp thị lại trên các trang có liên quan đến danh mục nhạy cảm. Xem thêm thông tin và hướng dẫn về cách thiết lập thẻ trên: http://google.com/ads/remarketingsetup
--------------------------------------------------->



<!-- <script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 857130601;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/857130601/?guid=ON&amp;script=0"/>
</div>
</noscript> -->

<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1795088050778253'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1795088050778253&ev=PageView
&noscript=1"/>
</noscript>


<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '276860759374626'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=276860759374626&ev=PageView
&noscript=1"/>
</noscript>



<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '255235298381221');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=255235298381221&ev=PageView&noscript=1"
/></noscript>



<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '186081862307592');
  fbq('track', 'PageView');
</script>

<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=186081862307592&ev=PageView&noscript=1"
/></noscript>


<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '373867603175905',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.2'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     //js.src = "https://connect.facebook.net/en_US/sdk.js";
     js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<meta property="fb:app_id" content="373867603175905">
<meta property="fb:admins" content="100001815369009">



<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '429645474447907'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=429645474447907&ev=PageView
&noscript=1"/>
</noscript>

<script>
  fbq('track', 'CompleteRegistration');
</script>

<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '255235298381221');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=255235298381221&ev=PageView&noscript=1"
/></noscript>

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-787541852"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-787541852');
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '409018929827004');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=409018929827004&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-115080902-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-115080902-1');
</script>
