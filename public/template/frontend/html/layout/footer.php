<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/jquery.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/bootstrap.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/bootstrap-select.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/fancybox/source/jquery.fancybox.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/bxslider/jquery.bxslider.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/hover-dropdown.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlPlugin'] .'/numeric.min.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/app.js';?>"></script>
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/me.js?v='. date('dmYHis');?>"></script>

<!--[if gt IE 8]><!-->
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/hls.light.min.js';?>"></script>
<!--<![endif]-->
<script type="text/javascript" src="<?php echo $this->arrParams['template']['urlJs'] .'/flowplayer.min.js';?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		App.init();
	});
</script>
<?php echo $this->headScript();?>