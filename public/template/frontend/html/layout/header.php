<?php
    echo $this->headTitle($this->arrParams['meta']['title']);
    
    echo $this->headMeta()
         ->appendHttpEquiv('X-UA-Compatible', 'IE=edge')
         ->setCharset('utf-8')
         ->appendName('viewport', 'width=device-width, initial-scale=1.0')
         ->appendName('author', $this->arrParams['meta']['author'])
         ->appendName('description', $this->arrParams['meta']['description'])
         ->appendName('keywords', $this->arrParams['meta']['keywords'])
         ->appendProperty('og:url', 'https://'. $_SERVER['SERVER_NAME'] . $_SERVER['REDIRECT_URL'])
         ->appendProperty('og:type', 'website')
         ->appendProperty('og:title', $this->arrParams['meta']['title'])
         ->appendProperty('og:description', $this->arrParams['meta']['description'])
         ->appendProperty('og:image', 'https://hbr.edu.vn' . $this->arrParams['meta']['image']);
	
	// CSS
	echo $this->headLink(array('rel' => 'icon', 'href' => $this->arrParams['template']['urlImg'] . '/favicon.png', 'type' => 'image/x-icon'), 'PREPEND')
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/bootstrap.min.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/bootstrap-select.min.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/font-awesome.min.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] .'/fancybox/source/jquery.fancybox.css')
              ->appendStylesheet($this->arrParams['template']['urlPlugin'] .'/bxslider/jquery.bxslider.css')
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/style.css?v='. strtotime("now"))
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/tuvan.css?v='. strtotime("now"))
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/user.css?v='. strtotime("now"))
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/online.css?v='. strtotime("now"))
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/livechat.css?v='. strtotime("now"))
              ->appendStylesheet($this->arrParams['template']['urlCss'] .'/flowplayer.css');
?>
<meta name="google-site-verification" content="oYdEer8A_tGpOr2jkuRrLHI2PWvAerLN--ZrPXW8k0A" />
<meta name="google-site-verification" content="GsnR9ADwQvwN6doF79IibqSZfaPhJi32l6yHX5-d-fY" />
<script src="<?php echo $this->arrParams['template']['urlPlugin'] . '/jwplayer/jwplayer.js';?>"></script>
<script>jwplayer.key="d0yOTiBMff3JjXImHA4lOwWuoW/csJkEaNyBhg==";</script>