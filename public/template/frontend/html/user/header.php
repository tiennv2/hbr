<?php
	$userInfo = new \ZendX\System\ContactInfo();
	$userInfo = $userInfo->getContactInfo();
?>
<div class="box_top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="contact">
                    <a href="tel:<?php echo str_replace('.', '', $setting['General.System.Hotline']['value']);?>" class="hotline"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $setting['General.System.Hotline']['value'];?></a>
                    <a href="mail:<?php echo $setting['General.System.Email']['value'];?>" class="email"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo $setting['General.System.Email']['value'];?></a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="user">
	                <ul class="nav navbar-nav">
	                	<?php
	                	if(!empty($userInfo)) { 
	                	?>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index'));?>"><?php echo $userInfo['name'];?></a></li>
	                	<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'logout'));?>">Thoát</a></li>
	                	<?php
	                	} else { 
	                	?>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'login'));?>">Đăng nhập</a></li>
		                <li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'register'));?>">Đăng ký</a></li>
		                <?php
	                	} 
		                ?>
	            	</ul>
	            </div>
            </div>
        </div>
    </div>
</div>