<?php
	$course_category = $this->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\CourseCategoryTable')->listItem(null, array('task' => 'cache'));
	$document_category = $this->getHelperPluginManager()->getServiceLocator()->get('Admin\Model\DocumentCategoryTable')->listItem(null, array('task' => 'cache'));
?>
<div class="user_box_menu">
	<div class="box_title">Tài liệu miễn phí</div>
	<div class="box_content">
		<ul>
			<?php
			$class_all = '';
			if($this->params['controller'] == 'index' && $this->params['action'] == 'index') {
				$class_all = ' class="active"';
			}
			echo '<li><a href="'. $this->url('routeUser/default', array('controller' => 'index', 'action' => 'index')) .'"'. $class_all .'><i class="fa fa-arrow-right"></i> Tất cả</a></li>';
			foreach ($course_category AS $key => $val) {
				if($val['type'] == 'free') {
					$link = $this->url('routeUser/default', array('controller' => 'course', 'action' => 'index', 'id' => $val['id']));
					
					$class = '';
					if(($this->params['route']['id'] == $val['id']) || ($this->item['category_id'] == $val['id'])) {
						$class = ' class="active"';		
					}
					echo '<li><a href="'. $link .'"'. $class .'><i class="fa fa-arrow-right"></i> '. $val['name'] .'</a></li>';
				}
			} 
			?>
		</ul>
	</div>
</div>
<div class="user_box_menu">
	<div class="box_title">Dành cho học viên HBR</div>
	<div class="box_content">
		<ul>
			<?php
			foreach ($course_category AS $key => $val) {
				if($val['type'] == 'hbr') {
					$link = $this->url('routeUser/default', array('controller' => 'course', 'action' => 'index', 'id' => $val['id']));
					
					$class = '';
					if(($this->params['route']['id'] == $val['id']) || ($this->item['category_id'] == $val['id'])) {
						$class = ' class="active"';		
					}
					echo '<li><a href="'. $link .'"'. $class .'><i class="fa fa-arrow-right"></i> '. $val['name'] .'</a></li>';
				}
			} 
			?>
		</ul>
	</div>
</div>
<div class="user_box_menu">
	<div class="box_title">Tài khoản</div>
	<div class="box_content">
		<ul>
			<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'update-profile'));?>"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
			<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'change-password'));?>"><i class="fa fa-lock"></i> Đổi mật khẩu</a></li>
			<li><a href="<?php echo $this->url('routeUser/default', array('controller' => 'index', 'action' => 'logout'));?>"><i class="fa fa-history"></i> Thoát</a></li>
		</ul>
	</div>
</div>
