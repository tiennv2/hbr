<?php echo $this->doctype() ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php echo $this->headTitle() ?>
    <?php echo $this->headMeta() ?>
	<?php echo $this->headLink() ?>
	<?php echo $this->headScript() ?>
	<link href="<?php echo $this->jsUrl;?>/fancybox/source/jquery.fancybox.css" media="screen" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo $this->jsUrl;?>/fancybox/source/jquery.fancybox.js"></script>
	<meta name="author" content="langmaster.edu.vn">
	<meta property="og:title" content="" />
	<meta property="og:type" content="article" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="http://langmaster.edu.vn/khoanh-khac-langmaster" />
	<meta property="og:image" content="<?php echo $this->imgUrl . '/527-01.jpg'; ?>" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	
	<?php require_once PUBLIC_PATH . '/code/header.php';?>
</head>
<body class="body">
<?php require_once PUBLIC_PATH . '/code/body.php';?>