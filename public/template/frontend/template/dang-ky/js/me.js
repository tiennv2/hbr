/**
 * Desciption: Form đặt câu hỏi
 */


// Menu sticky
function stickyMenu() {
    var boxMenu = '#box_menu';
    var topPage = $(window).scrollTop();
    var topFix = 72;
    if(topPage >= topFix) {
        $(boxMenu).addClass('sticky');
        $(boxMenu).css({'box-shadow' : '0 2px 5px 0 rgba(0,0,0,0.1)'});
    } else {
        $(boxMenu).removeClass('sticky');
        $(boxMenu).removeAttr('style');
    }
}

// Menu Mobile
function close_memu_mobile() {
    if($('.box_header .menu.mobile').size() > 0) {
        $('.box_header .menu.mobile').addClass('hidden-xs hidden-sm');
        
    }
    if($('.menu_mobile_bg').size() > 0) {
        $('.menu_mobile_bg').remove();
    }
}
$('.menu_mobile a').click(function() {
    $('.box_header .menu').removeClass('hidden-xs hidden-sm').addClass('mobile');
    $('.box_header').append('<div class="menu_mobile_bg" onclick="javascript:close_memu_mobile();"></div>');
});

// Popup video
function play_video(url) {
    var html = '';
    html += '<div class="popup_video" onclick="close_popup()">';
        html += '<div class="popup_content">';
            html += '<div class="embed-responsive embed-responsive-16by9">';
                html += '<iframe class="embed-responsive-item" src="'+ url +'" frameborder="0" allowfullscreen></iframe>';
            html += '</div>';
            html += '<a href="javascript:void(0)" onclick="close_popup()" class="popup_close"><i class="fa fa-times"></i></a>';
        html += '</div>';
    html += '</div>';
    $('body').append(html);
}

// Close Popup video
function close_popup() {
    $('.popup_video').remove();
}

$(document).ready(function () {
	$(window).scroll(function () {
	    stickyMenu();
	});
	
    //Chi nhap so
    $(".auto_init").keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    });
    
    // Menu go box
    $('.go_box').click(function() {
        var box = $(this).attr('data-box');
        if($('#' + box).length) {
            var top_scroll = 0;
            top_scroll = $("#" + box).offset().top;
            $('html,body').animate({
                scrollTop: top_scroll - 60
            }, 'slow');
        }
        $('.box_header .menu.mobile').addClass('hidden-xs hidden-sm');
        $('.go_box').removeClass('active');
        $(this).addClass('active');
        $('.menu_mobile_bg').remove();
    });

    // Popup video, image
    if ($(".fancybox-button").size() > 0) {
        $(".fancybox-button").fancybox({
            groupAttr: 'data-rel',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            helpers: {
                title: {
                    type: 'inside'
                }
            }
        });

        $('.fancybox-video').fancybox({
            type: 'iframe'
        });
    }
});