$(document).ready(function(){
	$("#submit").click(function(){
		var idForm = '#myForm';
		// AJAX Code To Submit Form.
		$.ajax({
			type: "POST",
			url: "validate.php",
			data: {
				'name'		: $(idForm + ' input[name="name"]').val(),
				'email'		: $(idForm + ' input[name="email"]').val(),
				'phone'		: $(idForm + ' input[name="phone"]').val(),
				'address'	: $(idForm + ' input[name="address"]').val(),
			},
			dataType: "json",
			cache: false,
			beforeSend: function() {
				$(idForm + ' .error').remove();
				$(idForm + ' .alert').remove();
				$(idForm + ' #submit').val('Đang gửi...');
				$(idForm + ' #submit').attr('disabled', 'disabled');
			},
			success: function(result){
				if(result.error) {
					result.error.forEach(function(error) {
						$('<div class="error">Giá trị này bắt buộc phải nhập</div>').insertAfter(idForm + ' input[name="'+ error +'"]');
					});
				} else {
					console.log(result);
					$('<div class="alert alert-success">Thông tin của bạn đã được gửi thành công</div>').insertBefore(idForm + ' #submit');
					$(idForm + ' input[name="name"]').val('');
					$(idForm + ' input[name="email"]').val('');
					$(idForm + ' input[name="phone"]').val('');
					$(idForm + ' input[name="address"]').val('');
				}
				
				$(idForm + ' #submit').val('Gửi thông tin');
				$(idForm + ' #submit').removeAttr('disabled');
			}
		});
	});
});


